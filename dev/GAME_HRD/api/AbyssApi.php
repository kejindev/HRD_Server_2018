<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/AbyssManager.php";
require_once './lib/ParamChecker.php';

class AbyssApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function ReqAbyssReward($param) {
		$resultFail = array('Protocol' => 'ResAbyssReward', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new AbyssManager();
		$resultC = $Manager ->AbyssReward($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqAbyssRewardList($param) {
		$resultFail = array('Protocol' => 'ResAbyssRewardList', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new AbyssManager();
		$resultC = $Manager ->AbyssRewardList($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqAbyssRanking($param) {
		$resultFail = array('Protocol' => 'ResAbyssRanking', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new AbyssManager();
		$resultC = $Manager ->AbyssRanking($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

}
?>

