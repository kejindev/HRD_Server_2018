<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/CharManager.php";
require_once './lib/ParamChecker.php';

class CharApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function ReqBuyCharacs($param) {
		$resultFail = array('Protocol' => 'ResBuyCharacs', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new CharManager();
		$resultC = $Manager ->BuyCharacs($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}
}
?>

