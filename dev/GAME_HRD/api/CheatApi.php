<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/CheatManager.php";

class CheatApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function ReqCheat($param) {
		$resultFail = array('Protocol' => 'ResCheat', 'ResultCode' => 200);

		$Manager = new CheatManager();
		$resultC = $Manager ->Cheat($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

}
	?>

