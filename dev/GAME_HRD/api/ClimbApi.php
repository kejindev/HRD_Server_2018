<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/ClimbManager.php";
require_once './lib/ParamChecker.php';

class ClimbApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function ReqClimbReward($param) {
		$resultFail = array('Protocol' => 'ResClimbReward', 'ResultCode' => 200);
		$base_param = 0;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ClimbManager();
		$resultC = $Manager ->ClimbReward($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}
	public function ReqGetRankNew($param) {
		$resultFail = array('Protocol' => 'ReqGetRankNew', 'ResultCode' => 200);
		
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ClimbManager();
		$resultC = $Manager ->GetRankNew($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}
	public function ReqSearchRank($param) {
		$resultFail = array('Protocol' => 'ResSearchRank', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ClimbManager();
		$resultC = $Manager ->SearchRank($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqNewClimbData($param) {
		$resultFail = array('Protocol' => 'ResNewClimbData', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ClimbManager();
		$resultC = $Manager ->NewClimbData($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}
	public function ReqGetClimbRewardList($param) {
		$resultFail = array('Protocol' => 'ResGetClimbRewardList', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ClimbManager();
		$resultC = $Manager ->GetClimbRewardList($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

}
?>

