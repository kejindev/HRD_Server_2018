<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/CouponManager.php";
require_once './lib/ParamChecker.php';

class CouponApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function ReqCoupon($param) {
		$resultFail = array('Protocol' => 'ResCoupon', 'ResultCode' => 200);

		$Manager = new CouponManager();
		$resultC = $Manager ->Coupon($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}
}
?>

