<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/DailyQuestManager.php";
require_once './lib/ParamChecker.php';

class DailyQuestApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function ReqDailyQuestComplete($param) {
		$resultFail = array('Protocol' => 'ResDailyQuestComplete', 'ResultCode' => 200);
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param = null);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new DailyQuestManager();
		$resultC = $Manager ->DailyQuestComplete($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

}
?>

