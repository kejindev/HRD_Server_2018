<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/GShopManager.php";
require_once './lib/ParamChecker.php';

class GShopApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function ReqOpenCheckGShop($param) {
		$ParamChecker = new ParamChecker();
		$base_param = null;
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['Protocol'] = "ResGShopCheck";
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}
		$resultFail = array('Protocol' => 'ResOpenCheckGShop', 'ResultCode' => 200);

		$Manager = new GShopManager();
		$resultC = $Manager ->OpenCheckGShop($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqResetGShop($param) {
		$ParamChecker = new ParamChecker();
		$base_param = null;
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['Protocol'] = "ResResetGShop";
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}
		$resultFail = array('Protocol' => 'ResResetGShop', 'ResultCode' => 200);

		$Manager = new GShopManager();
		$resultC = $Manager ->ResetGShop($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqBuyGShop($param) {
		$ParamChecker = new ParamChecker();
		$base_param = null;
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['Protocol'] = "ResBuyGShop";
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}
		$resultFail = array('Protocol' => 'ResBuyGShop', 'ResultCode' => 200);

		$Manager = new GShopManager();
		$resultC = $Manager ->BuyGShop($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}


}
	?>

