<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/GoddessManager.php";
require_once './lib/ParamChecker.php';

class GoddessApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function ReqGodPresent($param) {
		$resultFail = array('Protocol' => 'ResGodPresent', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new GoddessManager();
		$resultC = $Manager ->GodPresent($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqGodInform($param) {
		$resultFail = array('Protocol' => 'ResGodInform', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new GoddessManager();
		$resultC = $Manager ->GodInform($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}
	public function ReqGodQuest($param) {
		$resultFail = array('Protocol' => 'ResGodQuest', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new GoddessManager();
		$resultC = $Manager ->GodQuest($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else { //FAIL RETURN 
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

}
?>

