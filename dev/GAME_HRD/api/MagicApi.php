<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/MagicManager.php";
require_once './lib/ParamChecker.php';

class MagicApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}
	public function ReqGetMagicsInfo($param) {
		$resultFail = array('Protocol' => 'ResGetMagicsInfo', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new MagicManager();
		$resultC = $Manager ->GetMagicsInfo($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}


	public function ReqBuyMana($param) {
		$resultFail = array('Protocol' => 'ResBuyMana', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new MagicManager();
		$resultC = $Manager ->BuyMana($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}
	public function ReqComposeMagicGold($param){
		$resultFail = array('Protocol' => 'ResComposeMagicGold', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new MagicManager();
		$resultC = $Manager ->ComposeMagicGold($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqSellMagic ($param){
		$resultFail = array('Protocol' => 'ResSellMagic', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new MagicManager();
		$resultC = $Manager ->SellMagic($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqUpgradeMagicGold ($param){
		$resultFail = array('Protocol' => 'ResUpgradeMagicGold', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new MagicManager();
		$resultC = $Manager ->UpgradeMagicGold($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

	function ReqSetUnEquipMagic ($param){
		$resultFail = array('Protocol' => 'ResSetUnEquipMagic', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new MagicManager();
		$resultC = $Manager ->SetUnEquipMagic($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

	function ReqEquipMagic ($param){
		$resultFail = array('Protocol' => 'ResEquipMagic', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new MagicManager();
		$resultC = $Manager ->EquipMagic($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

	public function ReqMagicExtraction ($param){
		$resultFail = array('Protocol' => 'ResMagicExtraction', 'ResultCode' => 200);

		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new MagicManager();
		$resultC = $Manager ->MagicExtraction($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

}
?>

