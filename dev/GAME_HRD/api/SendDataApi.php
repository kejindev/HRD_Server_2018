<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./send_data/SendDataManager.php";
require_once './lib/ParamChecker.php';

class SendDataApi {

	public function __construct() {
		$this -> logger = Logger::get();

	}

	public function ReqGetLobbyData($param) {
		$resultFail = array('Protocol' => 'ResGetLobbyData', 'ResultCode' => 200);

		$APIManager = new SendDataManager();
		$resultAPI = $APIManager ->GetLobbyData($param);
		if ($resultAPI['ResultCode'] == 100) {
			return $resultAPI;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultAPI['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

	public function ReqGetDailyReward() {
		$resultFail = array('Protocol' => 'ResGetDailyReward', 'ResultCode' => 200);

		$APIManager = new SendDataManager();
		$resultAPI = $APIManager ->GetDailyReward();
		if ($resultAPI['ResultCode'] == 100) {
			return $resultAPI;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultAPI['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

	public function ReqGetCharacInfo() {
		$resultFail = array('Protocol' => 'ResGetCharacInfo', 'ResultCode' => 200);

		$APIManager = new SendDataManager();
		$resultAPI = $APIManager ->GetCharacInfo();
		if ($resultAPI['ResultCode'] == 100) {
			return $resultAPI;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultAPI['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

	public function ReqGetWeaponInfo() {
		$resultFail = array('Protocol' => 'ResGetWeaponInfo', 'ResultCode' => 200);

		$APIManager = new SendDataManager();
		$resultAPI = $APIManager ->GetWeaponInfo();
		if ($resultAPI['ResultCode'] == 100) {
			return $resultAPI;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultAPI['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}
	public function ReqGetMagicInfo() {
		$resultFail = array('Protocol' => 'ResGetMagicInfo', 'ResultCode' => 200);

		$APIManager = new SendDataManager();
		$resultAPI = $APIManager ->GetMagicInfo();
		if ($resultAPI['ResultCode'] == 100) {
			return $resultAPI;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultAPI['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

	public function ReqGetCharacData() {
		$resultFail = array('Protocol' => 'ResGetCharacData', 'ResultCode' => 200);

		$APIManager = new SendDataManager();
		$resultAPI = $APIManager ->GetCharacData();
		if ($resultAPI['ResultCode'] == 100) {
			return $resultAPI;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultAPI['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

	public function ReqGetCharacEvolveStatus() {
		$resultFail = array('Protocol' => 'ResGetCharacEvolveStatus', 'ResultCode' => 200);

		$APIManager = new SendDataManager();
		$resultAPI = $APIManager ->GetCharacEvolveStatus();
		if ($resultAPI['ResultCode'] == 100) {
			return $resultAPI;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultAPI['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

	public function ReqGetMonData() {
		$resultFail = array('Protocol' => 'ResGetMonData', 'ResultCode' => 200);

		$APIManager = new SendDataManager();
		$resultAPI = $APIManager ->GetMonData();
		if ($resultAPI['ResultCode'] == 100) {
			return $resultAPI;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultAPI['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}




}
?>

