<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once './lib/ParamChecker.php';
require_once "./classes/ShopManager_New.php";
require_once "./classes/InAppManager.php";
require_once "./classes/InAppManager_YK.php";

class ShopApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function ReqCheckBuy($param) {
		$resultFail = array('Protocol' => 'ResCheckBuy', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new InAppManager();
		$resultC = $Manager ->CheckBuy($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}


	function ReqBuyCashGoogle($param) {
		$resultFail = array('Protocol' => 'ResBuyCashGoogle', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new InAppManager();
		$resultC = $Manager ->BuyCashGoogle($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}


	 function ReqBuyChest($param) {
		$resultFail = array('Protocol' => 'ResBuyChest', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		//Test $this->checkEventFirst($db, $userId, $price);
		$userId = $param['userId'];
		if ( $userId == '200075628') {
			$Manager = new InAppManager();
			$db = new DB();
			$price = 7777;
			$resultC = $Manager ->checkEventFirst($db, $userId, $price, null);
		}
		//~Test
		//

		$Manager = new ShopManager();
		$resultC = $Manager ->BuyChest($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} 

		$resultFail['ResultCode'] = $resultC['ResultCode'];
		return $resultFail;
	}

	 function ReqBuyResource($param) {
		$resultFail = array('Protocol' => 'ResBuyResource', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ShopManager();
		$resultC = $Manager ->BuyResource($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	function ReqBuyCheck($param) {
		$resultFail = array('Protocol' => 'ResBuyCheck', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ShopManager();
		$resultC = $Manager ->BuyCheck($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	function ReqGetShopData($param) {
		$resultFail = array('Protocol' => 'ResGetShopData', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ShopManager();
		$resultC = $Manager ->GetShopData($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

	public function ReqBuyCash($param) {
		$resultFail = array('Protocol' => 'ResBuyCash', 'ResultCode' => 200);
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ShopManager();
		$resultC = $Manager ->BuyCash($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

	 function ReqBuyError($param) {
		$resultFail = array('Protocol' => 'ResBuyError', 'ResultCode' => 200);
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new ShopManager();
		$resultC = $Manager ->BuyError($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

	 function ReqSetShopData($param) {
		$resultFail = array('Protocol' => 'ResSetShopData', 'ResultCode' => 200);
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			return $resultFail;
		}

		$Manager = new ShopManager();
		$resultC = $Manager ->SetShopData($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}
		return $resultFail;
	}

	function ReqBuyCashYK($param) {
		$resultFail = array('Protocol' => 'ResBuyCashYK', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new InAppManager_YK();
		$resultC = $Manager ->BuyCashYK($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	function ReqYKReceive($param) {
		$resultFail = array('Protocol' => 'ResYKReceive', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new InAppManager_YK();
		$resultC = $Manager ->YKReceive($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

}
?>

