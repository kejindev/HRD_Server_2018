<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/SkillManager.php";
require_once './lib/ParamChecker.php';

class SkillApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function ReqResetSkillPoint($param) {
		$resultFail = array('Protocol' => 'ResResetSkillPoint', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new SkillManager();
		$resultC = $Manager ->ResetSkillPoint($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqUpSkillLV($param) {
		$resultFail = array('Protocol' => 'ResUpSkillLV', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}

		$Manager = new SkillManager();
		$resultC = $Manager ->UpSkillLV($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

}
?>

