<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/StageManager.php";
require_once './lib/ParamChecker.php';

class StageApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function ReqEnterStage($param) {
		$resultFail = array('Protocol' => 'ResEnterStage', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			return $resultFail;
		}

		$Manager = new StageManager();
		$resultC = $Manager->EnterStage($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqGetChapterReward($param) {
		$resultFail = array('Protocol' => 'ResGetChapterReward', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			return $resultFail;
		}

		$Manager = new StageManager();
		$resultC = $Manager ->GetChapterReward($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqGetStage($param) {
		$resultFail = array('Protocol' => 'ResGetStage', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			return $resultFail;
		}

		$Manager = new StageManager();
		$resultC = $Manager ->GetStage($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqGetStageReward($param) {
		$resultFail = array('Protocol' => 'ResGetStageReward', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			return $resultFail;
		}

		$Manager = new StageManager();
		$resultC = $Manager ->GetStageReward($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	public function ReqGetMyClearData($param) {
		$resultFail = array('Protocol' => 'ResGetMyClearData', 'ResultCode' => 200);
		$base_param = null;
		$ParamChecker = new ParamChecker();
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			return $resultFail;
		}

		$Manager = new StageManager();
		$resultC = $Manager ->GetMyClearData($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else {
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

}
?>

