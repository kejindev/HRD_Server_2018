<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/WebviewManager.php";

class WebviewApi {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function ReqWebviewEvent($param) {
		$base_param = null;
		$resultFail = array('Protocol' => 'ResWebviewEvent', 'ResultCode' => 200);

		$Manager = new WebviewManager();
		$resultC = $Manager ->WebviewEvent($param);
		if ($resultC['ResultCode'] == 100) {
			return $resultC;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultC['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

}
?>

