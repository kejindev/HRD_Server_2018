<?php
class apc_handler {

    function get_result_apc_data($FILE_KEY, $read_token) {

        // default cnt
        $token_cnt = 0;

        $CAN_COL = array();
        $result_data = array();

        /*아래 switch-case 문에 정의된 배열 순으로 apc에 저장된다.
         *
         *
         *
         * 이와 같이 정의하는 이유는
         * 1. 기획데이터에서 쓸데없는 칼럼을 제거해 apc에 올라가는 양을 줄인다 ( 가장큰이유)
         * 2. 기획데이터에 순서가 꼬이거나 칼럼이 중간에 추가되더라도 서버측은 아래와같이 정의된 순서대로 저장되게 함으로써 기획데이터가 꼬이더라도 기존 코드를 다시 수정할 필요가 없어진다.
         *
         *
         *
         * set_apc 로직
         * 1 .기획문서에 아래 칼럼명과 순서가 다르더라도 아래 정의된 배열 순서로 정의된다.
         * 2 .만약 아래 정의된 칼럼명과 기획문서 칼럼명이 다르다면 에러(fatal 은 아니고 notice)가 발생된다.(반드시 같아야된다 , 같지 않을경우 echo로 찍어둠)
         * 3 .새로운 칼럼이 추가되야한다면 뒤에 칼럼명으로 추가해주면 아래 배열에 정의된 index 순으로 저장될것이며 소스코드에 적용하면된다.
         * 4 .만약 아래와 같이 키값을 정의하지 않는다면 기획 엑셀파일 순서대로 그대로 들어간다.
         *
         *
         */

        switch($FILE_KEY) {
			case 'AbyssReward_' :
				$CAN_COL = array('Floor','Reward_type','Amount');
                break;
			case 'BlockCharacInShop_' :
				$CAN_COL = array('CharacNumber');
                break;
			case 'CharacIndex_' :
				$CAN_COL = array('CharacMinidx','CharacMaxidx','CharacHiddenStartIdx','ChargingCharacs');
                break;
			case 'ClimbRankReward_' :
				$CAN_COL = array('RankPercent','Reward_type1','Amount1','Reward_type2','Amount2','Reward_type3','Amount3','Reward_type4','Amount4');
                break;
			case 'DailyReward_' :
				$CAN_COL = array('index','Reward1','Reward2');
				break;
			case 'Descend_' :
				$CAN_COL = array('StartTime','Time','Value');
                break;
			case 'EffectEtcAB_' :
				$CAN_COL = array('AbtilityNumber','Effect_Type','Percent');
                break;
			case 'EffectRewardAB_' :
				$CAN_COL = array('AbtilityNumber','Effect_Type','Percent');
                break;
			case 'EffectRewardCheckSP_' :
				$CAN_COL = array('UserSP','Reward_type','Percent');
                break;
			case 'EffectRewardPreCheckSP_' :
				$CAN_COL = array('UserSP','Reward_type','Percent');
                break;
			case 'Event_Korea' : case 'Event_Korea_QA': case 'Event_China': case 'Event_China_QA':
				$CAN_COL = array('id','StartTime','EventNo','Time','EndTime');
				break;
			case 'FirstClearBonus_New_' :
				$CAN_COL = array('StageNumber','Item_type','Item_amount','Item_type1','Item_amount2');
				break;
			case 'MagicPowderValue_' :
				$CAN_COL = array('Grade','ConvertValue','MinValue','MaxValue');
				break;
			case 'TempleExp_' :
				$CAN_COL = array('Grade','TempleExp');
				break;
			case 'TempleProduct_' :
				$CAN_COL = array('TempleIndx','ResourceType','RewardAmount','MaxStrage','NeedLevelUpMinute');
				break;
			case 'TempleProductLevelUp_' :
				$CAN_COL = array('TempleIndx','TypeLv1','AmountLv1','TypeLv2','AmountLv2','TypeLv3','AmountLv3','TypeLv4','AmountLv4','TypeLv5','AmountLv5','TypeLv6','AmountLv6','TypeLv7','AmountLv7','TypeLv8','AmountLv8','TypeLv9','AmountLv9','Defualt','Defualt');
				break;
			case 'UserExp_MaxHeart_' :
				$CAN_COL = array('Level','UserExp','MaxThunder','UserExpAccu');
				break;
			case 'WeaponInform_' :
				$CAN_COL = array('name','value');
				break;
			case 'WeaponPrice_Exp_' :
				$CAN_COL = array('Level', 'WeaponPrice','WeaponAccuPrice','WeaponNeedExp','WeaponAccuExp');
				break;
			case 'WeaponStat_' :
				$CAN_COL = array('WeaponNo','BaskAtk','LVPerAtk');
                break;
			case 'characData25_' :
			case 'monData11_23_' :
			case 'characEvolveStatus19_' :
				$CAN_COL = null;
                break;
			case 'ShopHero_' :
				$CAN_COL = array('idx','type','price','type1','price1');
                break;
			case 'RandomWeapon_' :
				$CAN_COL = array('idx','option1','option2','option3','option4','option5','option6');
                break;
			case 'RandomWeaponOption_' :
				$CAN_COL = array('index','type');
                break;
			case 'stageDataTotal_' :
				$CAN_COL = array('StageId','TotalWave/MaxMon/monCount/intervalTimer','StartMamber','MakeChaPosition','NeedHeart','MonWay','ClearType','ClearTemp','RewardId','RewardAmount','RewardPercent','NeedClearIdx','GimicType','Gimic');
                break;
			case 'NewShopInAppData_' :
				$CAN_COL = array("ProductId","TapName","Desc","Name","ImageAtlas","ImageName","PriceType","Price","GoodsId","itemId","itemCount");
                break;
			case 'NewShopData_Chiness_' :
				$CAN_COL = array("Id","TapName","Desc","Name","ImageAtlas","ImageName","PriceType","Price","iabId/chestCount","itemId","itemCount");
                break;
			case 'ShopChest_' :
				$CAN_COL = array('ChestType','item_id','item_Point');
                break;

			case 'AchieveData_' :
				$CAN_COL = array('idx','Category','Purpose','Goal','RewardType','Count');
                break;
			case 'BlackMarket_' :
				$CAN_COL = array('Idx','ItemType','ItemCount','Price','ResourceType','Rate','Random');
                break;
			case 'RandomOption_' :
				$CAN_COL = array('index','101','102','103');
                break;
			case 'WeaponOption_' :
				$CAN_COL = array('idx','option1','min/max1','class1','option2','min/max2','class2','option3','min/max3','class3','option4','min/max4','class4','option5','min/max5','class5','option6','min/max6','class6',);
                break;
			case 'WeaponSpecialOption_' :
				$CAN_COL = array('index','type');
                break;
			case 'Stage_boss_' :
				$CAN_COL = array('stageId','1','2','3','4','5','6','7','8','9','10');
                break;
			case 'DailyMission_' :
				$CAN_COL = array('index','max','Type','Count');
                break;
            default :
				$CAN_COL = array("Wave","Timer","MonIdx","Atribute","Size","Hp","Def","Speed","Price","isBoss","reward" );
                break;
        }

        $col_cnt = count($CAN_COL);

        if ($col_cnt > 0) {
            $token_cnt = count($read_token);
        }

        for ($i = 0; $i < $token_cnt; $i++) {

            for ($j = 0; $j < $col_cnt; $j++) {
				
                if ($read_token[$i] == $CAN_COL[$j]) {
                    $result_data[$j] = $i;
				}

            }

        }

        return $result_data;
    }

    function get_FileInfo() {
        // FILENAME
        $FILENAME_arr[]['FILE_NAME'] =  "AbyssReward.csv";

        $FILENAME_arr[]['FILE_NAME'] =  "BlockCharacInShop.csv";
        $FILENAME_arr[]['FILE_NAME'] =  "CharacIndex.csv";
        $FILENAME_arr[]['FILE_NAME'] =  "ClimbRankReward.csv";
        $FILENAME_arr[]['FILE_NAME'] =  "DailyReward.csv";
        $FILENAME_arr[]['FILE_NAME'] =  "Descend.csv";
        $FILENAME_arr[]['FILE_NAME'] =  "EffectEtcAB.csv";
        $FILENAME_arr[]['FILE_NAME'] =  "EffectRewardAB.csv";
        $FILENAME_arr[]['FILE_NAME'] =  "EffectRewardCheckSP.csv";
        $FILENAME_arr[]['FILE_NAME'] =  "EffectRewardPreCheckSP.csv";
        // $FILENAME_arr[]['FILE_NAME'] = "Event_Korea.csv";
        $FILENAME_arr[]['FILE_NAME'] = "FirstClearBonus_New.csv";
        $FILENAME_arr[]['FILE_NAME'] = "MagicPowderValue.csv";
        $FILENAME_arr[]['FILE_NAME'] = "TempleExp.csv";
        $FILENAME_arr[]['FILE_NAME'] = "UserExp_MaxHeart.csv";
        $FILENAME_arr[]['FILE_NAME'] = "WeaponInform.csv";
        $FILENAME_arr[]['FILE_NAME'] = "WeaponPrice_Exp.csv";
        $FILENAME_arr[]['FILE_NAME'] = "WeaponStat.csv";
        $FILENAME_arr[]['FILE_NAME'] = "TempleProduct.csv";
        $FILENAME_arr[]['FILE_NAME'] = "TempleProductLevelUp.csv";

		$FILENAME_arr[]['FILE_NAME'] = "0.csv";
		$FILENAME_arr[]['FILE_NAME'] = "1.csv";
		$FILENAME_arr[]['FILE_NAME'] = "2.csv";
		$FILENAME_arr[]['FILE_NAME'] = "3.csv";
		$FILENAME_arr[]['FILE_NAME'] = "4.csv";
		$FILENAME_arr[]['FILE_NAME'] = "7.csv";
		$FILENAME_arr[]['FILE_NAME'] = "10.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11.csv";
		$FILENAME_arr[]['FILE_NAME'] = "16.csv";
		$FILENAME_arr[]['FILE_NAME'] = "20.csv";
		$FILENAME_arr[]['FILE_NAME'] = "21.csv";
		$FILENAME_arr[]['FILE_NAME'] = "24.csv";
		$FILENAME_arr[]['FILE_NAME'] = "30.csv";
		$FILENAME_arr[]['FILE_NAME'] = "31.csv";
		$FILENAME_arr[]['FILE_NAME'] = "36.csv";
		$FILENAME_arr[]['FILE_NAME'] = "40.csv";
		$FILENAME_arr[]['FILE_NAME'] = "41.csv";
		$FILENAME_arr[]['FILE_NAME'] = "46.csv";
		$FILENAME_arr[]['FILE_NAME'] = "50.csv";
		$FILENAME_arr[]['FILE_NAME'] = "51.csv";
		$FILENAME_arr[]['FILE_NAME'] = "52.csv";
		$FILENAME_arr[]['FILE_NAME'] = "59.csv";
		$FILENAME_arr[]['FILE_NAME'] = "60.csv";
		$FILENAME_arr[]['FILE_NAME'] = "66.csv";
		$FILENAME_arr[]['FILE_NAME'] = "67.csv";
		$FILENAME_arr[]['FILE_NAME'] = "68.csv";
		$FILENAME_arr[]['FILE_NAME'] = "69.csv";
		$FILENAME_arr[]['FILE_NAME'] = "70.csv";
		$FILENAME_arr[]['FILE_NAME'] = "71.csv";
		$FILENAME_arr[]['FILE_NAME'] = "75.csv";
		$FILENAME_arr[]['FILE_NAME'] = "79.csv";
		$FILENAME_arr[]['FILE_NAME'] = "80.csv";
		$FILENAME_arr[]['FILE_NAME'] = "81.csv";
		$FILENAME_arr[]['FILE_NAME'] = "85.csv";
		$FILENAME_arr[]['FILE_NAME'] = "88.csv";
		$FILENAME_arr[]['FILE_NAME'] = "90.csv";
		$FILENAME_arr[]['FILE_NAME'] = "91.csv";
		$FILENAME_arr[]['FILE_NAME'] = "93.csv";
		$FILENAME_arr[]['FILE_NAME'] = "95.csv";
		$FILENAME_arr[]['FILE_NAME'] = "97.csv";
		$FILENAME_arr[]['FILE_NAME'] = "100.csv";
		$FILENAME_arr[]['FILE_NAME'] = "111.csv";
	
		for ( $i=600; $i<605; $i++ )
			$FILENAME_arr[]['FILE_NAME'] = $i.".csv";
		for ( $i=499; $i<521; $i++ )
			$FILENAME_arr[]['FILE_NAME'] = $i.".csv";
		for ( $i=1001; $i<1061; $i++ )
			$FILENAME_arr[]['FILE_NAME'] = $i.".csv";
		for ( $i=2100; $i<3100; $i+=100 ) {
			for ( $j=0; $j<6; $j++ )
				$FILENAME_arr[]['FILE_NAME'] = ($i+$j).".csv";
		}
		
		$FILENAME_arr[]['FILE_NAME'] = "9100.csv";
		$FILENAME_arr[]['FILE_NAME'] = "10980.csv";
		$FILENAME_arr[]['FILE_NAME'] = "10981.csv";
		$FILENAME_arr[]['FILE_NAME'] = "10982.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11910.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11911.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11912.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11920.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11921.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11922.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11940.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11941.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11942.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11950.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11951.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11952.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11960.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11961.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11962.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11970.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11971.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11972.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11980.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11981.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11982.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11990.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11991.csv";
		$FILENAME_arr[]['FILE_NAME'] = "11992.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12840.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12841.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12842.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12850.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12851.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12852.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12870.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12871.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12872.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12880.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12881.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12882.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12890.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12891.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12892.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12900.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12901.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12902.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12910.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12911.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12912.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12920.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12921.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12922.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12930.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12931.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12932.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12940.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12941.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12942.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12950.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12951.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12952.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12960.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12961.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12962.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12970.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12971.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12972.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12980.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12981.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12982.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12990.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12991.csv";
		$FILENAME_arr[]['FILE_NAME'] = "12992.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13750.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13751.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13752.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13760.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13761.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13762.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13770.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13771.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13772.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13880.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13881.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13882.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13890.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13891.csv";
		$FILENAME_arr[]['FILE_NAME'] = "13892.csv";

		for ( $i=20001; $i<20011; $i++ )
			$FILENAME_arr[]['FILE_NAME'] = $i.".csv";

		$FILENAME_arr[]['FILE_NAME'] = "characData25.txt";
		$FILENAME_arr[]['FILE_NAME'] = "characEvolveStatus19.txt";
		$FILENAME_arr[]['FILE_NAME'] = "monData11_23.txt";
		$FILENAME_arr[]['FILE_NAME'] = "ShopHero.csv";
		$FILENAME_arr[]['FILE_NAME'] = "RandomWeapon.csv";
		$FILENAME_arr[]['FILE_NAME'] = "RandomWeaponOption.csv";
		$FILENAME_arr[]['FILE_NAME'] = "stageDataTotal.csv";

		$FILENAME_arr[]['FILE_NAME'] = "ShopChest.csv";
		$FILENAME_arr[]['FILE_NAME'] = "AchieveData.csv";
		$FILENAME_arr[]['FILE_NAME'] = "BlackMarket.csv";
		$FILENAME_arr[]['FILE_NAME'] = "RandomOption.csv";
		$FILENAME_arr[]['FILE_NAME'] = "WeaponOption.csv";
		$FILENAME_arr[]['FILE_NAME'] = "WeaponSpecialOption.csv";
		$FILENAME_arr[]['FILE_NAME'] = "Stage_boss.csv";
		$FILENAME_arr[]['FILE_NAME'] = "DailyMission.csv";

		
		$FILENAME_arr[]['FILE_NAME'] = "NewShopData_Chiness.csv";

		

		$FILENAME_arr[]['FILE_NAME'] = "NewShopInAppData.csv";
		// $FILENAME_arr[]['FILE_NAME'] = "Event_Korea_QA.csv";
		// $FILENAME_arr[]['FILE_NAME'] = "Event_China.csv";
		// $FILENAME_arr[]['FILE_NAME'] = "Event_China_QA.csv";

		$len = count($FILENAME_arr);
		for ( $i=0; $i<$len; $i++ ) {
			$replaceChar = '_';
			// if ( false == strpos( $FILENAME_arr[$i]['FILE_NAME'], 'Event_' ) )
			// 	$replaceChar = '_';
			// else
			// 	$replaceChar = '';
			$FILENAME_arr[$i]['FILE_KEY'] = str_replace(array('.csv', '.txt'), $replaceChar, $FILENAME_arr[$i]['FILE_NAME']);
		}
        
        return $FILENAME_arr;

    }

}
?>
