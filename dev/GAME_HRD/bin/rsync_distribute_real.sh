#! /bin/sh

# run for web 01 10.107.22.227

#WEB02
sudo rsync -avz --delete /home/seedwar_admin/trunk/ 10.107.22.228::SOW

#WEB03
sudo rsync -avz --delete /home/seedwar_admin/trunk/ 10.107.22.229::SOW

#WEB04
sudo rsync -avz --delete /home/seedwar_admin/trunk/ 10.107.22.230::SOW

#WEB05
sudo rsync -avz --delete /home/seedwar_admin/trunk/ 10.107.22.231::SOW

#WEB02_MANAGER/BIN
sudo rsync -avz --delete /home/seedwar_admin/manager/ 10.107.22.228::SOW_MANAGER

#WEB03_MANAGER/BIN
sudo rsync -avz --delete /home/seedwar_admin/manager/ 10.107.22.229::SOW_MANAGER

#WEB04_MANAGER/BIN
sudo rsync -avz --delete /home/seedwar_admin/manager/ 10.107.22.230::SOW_MANAGER

#WEB05_MANAGER/BIN
sudo rsync -avz --delete /home/seedwar_admin/manager/ 10.107.22.231::SOW_MANAGER
