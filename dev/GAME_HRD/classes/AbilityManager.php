<?php
include_once './common/DB.php';
include_once './common/define.php';
require_once './lib/Logger.php';
include_once './classes/AccountManager.php';
include_once './classes/ConsumeManager.php';
include_once './classes/AssetManager.php';
include_once './classes/AddManager.php';
require_once './classes/LogDBSendManager.php';

class AbilityManager {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function GetAbillityNeedSlot($id) {
		switch($id) {
			case 0: return 1;
			case 1: return 3;
			case 2: return 2;
			case 3: return 1;
			case 4: return 3;

			case 5: return 2;
			case 6: return 2;
			case 7: return 2;
			case 8: return 4;
			case 9: return 2;

			case 10: return 1;
			case 11: return 2;
			case 12: return 3;
			case 13: return 2;
			case 14: return 3;

			case 15: return 3;
			case 16: return 1;
			case 17: return 2;
			case 18: return 3;
			case 19: return 5;

			case 20: return 2;
			case 21: return 3;
			case 22: return 2;
			case 23: return 4;
			case 24: return 2;

			case 25: return 2;
			case 26: return 3;
			case 27: return 2;
			case 28: return 3;
			case 29: return 2;
			default:        //powder
					 return 1;
		}
	}

	public function GetCanUnlock($param) {

		$resultFail['Protocol'] = 'ResGetCanUnlock';
		$resultFail['ResultCode'] = 300;

		$userId 	= $param['userId'];
		$isCantUnlock = null;
		$db = new DB();
		$sql = "SELECT cantUnlockSlot FROM frdAbillity WHERE userId= :userId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$openedSlot = 0;
			$cantUnlockSlot = $this->GetCantUnlockSlotValue();
			$bundle0 = 0;
			$bundle1 = 0;
			$usingBundle0 = 0;
			$usingBundle1 = 0;

			$sql = "INSERT INTO frdAbillity 
				( userId, openedSlot, cantUnlockSlot, bundle0, bundle1, usingBundle0, usingBundle1, update_date, reg_date ) 
				VALUES 
				( :userId,:openedSlot, :cantUnlockSlot, :bundle0, :bundle1, :usingBundle0, :usingBundle1, now(), now())";
			$db->prepare($sql);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db->bindValue(':openedSlot', $openedSlot, PDO::PARAM_INT);
			$db->bindValue(':cantUnlockSlot', $cantUnlockSlot, PDO::PARAM_STR);
			$db->bindValue(':bundle0', $bundle0, PDO::PARAM_INT);
			$db->bindValue(':bundle1', $bundle1, PDO::PARAM_INT);
			$db->bindValue(':usingBundle0', $usingBundle0, PDO::PARAM_INT);
			$db->bindValue(':usingBundle1', $usingBundle1, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('setUnlockAbility:: INSERT FAIL UID.' . $userId. ' sql : ' . $sql);
				return $resultFail;
			}
		} else {
			$cantUnlockSlot = $row["cantUnlockSlot"];
		}
		$resultVal = $cantUnlockSlot;

		$isUpdate= null;
		$MaxCnt = 25; // HARDCODE
		for ( $i=0; $i<$MaxCnt; $i++ ) {
			if ( ($cantUnlockSlot & (1<<$i)) > 0 ) {
				$isUpdate= false;
				switch((int)$i) {
					case 1:
					case 2:
					case 3:
						$sql = "SELECT exp,userLevel from frdUserData where userId = :userId";
						$db->prepare($sql);
						$db->bindValue(':userId', $userId, PDO::PARAM_INT);
						$db->execute();
						$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
						if (!$row) {
							$this->logger->logError(__FUNCTION__.': FAIL  userId.'.$userId.' sql : '.$sql);
							return $resultFail;
						}

						if ( $i == 1 ) { 
							$needLevel = 13-1;  	
						} else if ( $i == 2 ) {
							 $needLevel = 33-1; 
						} else  {
							$needLevel = 53-1;
						}

						if ($row['userLevel'] > $needLevel) {
							$isUpdate = true;
						}

						break;
					case 8:
						$charId = 134;

						$tempExp = 0;	
						$sql = "SELECT exp FROM frdCharInfo WHERE userId=:userId and charId=:charId";
						$db->prepare($sql);
						$db->bindValue(':userId', $userId, PDO::PARAM_INT);
						$db->bindValue(':charId', $charId, PDO::PARAM_INT);
						$db->execute();
						$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
						if (!$row) {
						//	$this->logger->logError(__FUNCTION__.' : userId.'.$userId.'charId : '.$charId.' sql : '.$sql);
							break;
						}

						if ( $row['exp'] >= 10 ) {
							$isUpdate = true;
						}

						break;

					case 9:
						$goddessId = 10;
						$sql = "select level from frdGoddess where userId=:userId and goddessId=:goddessId";
						$db->prepare($sql);
						$db->bindValue(':userId', $userId, PDO::PARAM_INT);
						$db->bindValue(':goddessId', $goddessId, PDO::PARAM_INT);
						$db->execute();
						$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
						if (!$row) {
//							$this->logger->logError('setUnlockAbility:: no Goddess userId.'.$userId);
							break;
						}
						$level = $row["level"];
						if ( $level > 0 )
							$isUpdate = true;
						break;

					case 14:
						$sql = "select bestRank, bestTotalUserCount from frdClimbTopData where userId=:userId";
						$db->prepare($sql);
						$db->bindValue(':userId', $userId, PDO::PARAM_INT);
						$db->execute();
						$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
						if ($row) {
							if ( ($row["bestRank"] / $row["bestTotalUserCount"]) < 0.05 ) {
								$isUpdate = true;
							}
						}

						break;

					case 15:
						$sql = "select stageId from frdAbyss where userId=:userId";
						$db->prepare($sql);
						$db->bindValue(':userId', $userId, PDO::PARAM_INT);
						$db->execute();
						$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
						if (!$row) {
//							$this->logger->logError('setUnlockAbility:: no frdAbyss userId.'.$userId.' sql : '.$sql);
							break;
						}

						if ( $row["stageId"] >= 1030 ) {
							$isUpdate = true;
						}

						break;

					case 16:
						$sql = "select userId from frdLogBuy where userId = :userId and productId='package_abyss'";
						$db->prepare($sql);
						$db->bindValue(':userId', $userId, PDO::PARAM_INT);
						$db->execute();
						$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
						if (!$row) {
//							$this->logger->logError('setUnlockAbility : no frdAbyss userId.'.$userId.' sql : '.$sql);
							break;
						}
						$isUpdate = true;
						break;

					case 18:
						$sql = "SELECT clearCount FROM frdClearCount WHERE userId=:userId and stageId>=2100 and stageId<5000";
						$db->prepare($sql);
						$db->bindValue(':userId', $userId, PDO::PARAM_INT);
						$db->execute();
						$ClearResult = null;
						while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
							$ClearResult[] = $row;	
						}

						$playCount = 0;
						$CrCnt = count($ClearResult);
						if ($CrCnt > 0 ) {
							foreach($ClearResult as $CR) {
								$playCount += $CR['clearCount'];
							} 
							if ( $playCount >= 100 ) {
								$isUpdate = true;
							}
						} 
						break;

					case 21:
						$sql = "select userId from frdLogBuy where userId=:userId and productId='package_event_abillity'";
						$db->prepare($sql);
						$db->bindValue(':userId', $userId, PDO::PARAM_INT);
						$db->execute();
						$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
						if (!$row) {
//							$this->logger->logError('setUnlockAbility::  case 21 no  frdLogBuy userId.'.$userId.' sql : '.$sql);
							break;
						}
						$isUpdate = true;
						break;
					case 23:
						$sql = "SELECT accuSkillPoint FROM frdUserData WHERE userId=:userId";
						$db->prepare($sql);
						$db->bindValue(':userId', $userId, PDO::PARAM_INT);
						$db->execute();
						$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
						if (!$row) {
//							$this->logger->logError('setUnlockAbility:: SELECT FAIL frdUserData userId.'.$userId.' sql : '.$sql);
							break;
						}

						if ( $row["accuSkillPoint"] >= 50000 ) {
							$isUpdate = true;
						}

						break;

					case 24:
						$sql = "select clearCount from frdClearCount where userId=:userId and stageId>=1 and stageId<500";
						$db->prepare($sql);
						$db->bindValue(':userId', $userId, PDO::PARAM_INT);
						$db->execute();
						$ClearResult = null;
						while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
							$ClearResult[] = $row;	
						}
						$playCount = 0;
						if ($ClearResult) {	
							foreach($ClearResult as $CR) {
								$playCount += $CR['clearCount'];
							}
							if ( $playCount >= 100 ) {
								$isUpdate = true;
							}
						}
						break;
					default:
						$isUpdate = true;
						break;
				}
				if ( $isUpdate ) {
					$resultVal -= (1<<$i);
				}
				else {
					$isCantUnlock[] = $i;
				}
			}
		}

		if ( $resultVal !== $cantUnlockSlot ) {
			if ( $isUpdate ) {
				$sql = "update frdAbillity set cantUnlockSlot= :resultVal where userId= :userId";
				$db -> prepare($sql);
				$db -> bindValue(':resultVal', $resultVal, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!$row) {
					$this -> logger -> logError("setUnlockAbility : UPDATE FAIL  sql :". $sql. " userId : ".$userId);
					return $resultFail;
				}
			}
		}

		$Data["isCantUnlock"] = $isCantUnlock;
		$result['Protocol'] = 'ResGetCanUnlock'; 
		$result['ResultCode'] = 100; 
		$result['Data'] = $Data; 

		return $result;
	}

	function GetUnLockMats($slotIdx) {
		switch($slotIdx) {
			case 0:
				return array(5000,500);
			case 4:
				return array(5004,100);
			case 5:
				return array(5004,300);
			case 6:
				return array(115001,200);
			case 7:
				return array(115003,100);
			case 8:
				return array(5000,100000);
			case 9:
				return array(5000,1500000);
			case 10:
				return array(5005,5000);
			case 11:
				return array(5006,10000);
			case 12:
				return array(110000,10);
			case 13:
				return array(110000,30);
			case 14:
				return array(5000,50000);
			case 15:
				return array(5000,100000);
			case 17:
				return array(106000,1,106001,1,106002,1,106003,1,106004,1,106005,1,106006,1,106007,1,106008,1,106009,1);
			case 18:
				return array(5002,50);
			case 19:
				return array(110000,100);
			case 20:
				return array(5004,500);
			case 22:
				return array(5004,1000);
			case 23:
				return array(5005,10000);
			case 24:
				return array(5004,150);
			default:
				return null;
		}
	}

	function GetCantUnlockSlotValue() {
		$val= 9223372036854775807;  // 2^63 -1
		return $val;
	}

	function OpenSlot($param) {
		$resultFail['Protocol'] = 'ResOpenSlot';
		$resultFail['ResultCode'] = 200;

		$userId = $param['userId'];
		$slotIdx = $param['slotIdx'];

		$db = new DB();
		$ConsumeManager = new ConsumeManager();
		$mats = $this->GetUnLockMats($slotIdx);
		$matCount = (int)(count($mats)/2);
		$temps = null;
		for ( $i=0; $i<$matCount; $i++ ) {
			$rewardType = $mats[$i*2];
			$rewardCount = $mats[$i*2+1];
			$resultCon = $ConsumeManager->setConsumeResource($db, $userId, $i, $rewardType, $rewardCount);
			$temps[] = $resultCon;

			if ($resultCon['ResultCode'] != 100) {
				$this->logger->logError(__FUNCTION__.': FAIL  userId.'.$userId);
				return $resultFail;
			}

//			$data["consumeType"][] = $rewardType;
//			$data["consumeCount"][] = $rewardCount;
			if ( $resultCon['ResultCode'] != 100) {
				$resultFail['ResultCode'] = $resultCon['ResultCode'];
				return $resultFail;
			}
		}


		$sql = "select openedSlot from frdAbillity where userId=:userId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$defaultVal = $this->setUnlockAbility($param);

			$sql = "INSERT INTO frdAbillity (userId, openedSlot, cantUnlockSlot, bundle0, bundle1, usingBundle0, usingBundle1, update_date, reg_date ) VALUES 
				(:userId, 0, :defaultVal, 0, 0, 0, 0, now(), now())";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':defaultVal', $defaultVal, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
				return $resultFail;
			}
			$openedSlot = 0;
		} else {
			$openedSlot = $row["openedSlot"];
		}

		$openedSlot |= (1<<$slotIdx);
		$sql = "UPDATE frdAbillity SET openedSlot=:openedSlot WHERE userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':openedSlot', $openedSlot, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

		$data["openedSlot"] = $openedSlot;

		if ($temps) {
			foreach($temps as $tp){
				if (isset($tp['gold'])) {
					$data['use']['gold'] = $tp['gold'];
				}
				if (isset($tp['jewel'])) {
					$data['use']['jewel'] = $tp['jewel'];
				}
				if (isset($tp['medal'])) {
					$data['use']['medal'] = $tp['medal'];
				}
				if (isset($tp['heart'])) {
					$data['use']['heart'] = $tp['heart'];
				}
				if (isset($tp['powder'])) {
					$data['use']['powder'] = $tp['powder'];
				}

				if (isset($tp['weapons'])) {
					$data['use']['weapons'][] = $tp['weapons'];
				}
				if (isset($tp['magics'])) {
					$data['use']['magics'][] = $tp['magics'];
				}
				if (isset($tp['items'])) {
					$data['use']['items'][] = $tp['items'];
				}
			}
		}

		$result['Protocol'] = 'ResOpenSlot';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}

	function EquipAbility( $param) {
		$resultFail['ResultCode'] = 300;
		$maxSlotCount = 25; // HARDCODE

		$userId = $param["userId"];
		$abillityId = (int)$param["abillityId"];

		$db = new DB();
		if ( $abillityId < 63 ) {
			$bundleIdx = 0;
			$mask = 1 << $abillityId;
			$bundleName = "bundle0";
			$usingBundleName = "usingBundle0";
		}
		else {
			$bundleIdx = 1;
			$mask = 1 << ($abillityId-64);
			$bundleName = "bundle1";
			$usingBundleName = "usingBundle1";
		}

		$sql = "select openedSlot,".$bundleName." , usingBundle0, usingBundle1 from frdAbillity where userId=:userId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.': FAIL  userId.'.$userId.' sql : '.$sql);
			return $resultFail;
		}

		/*
		   if ( $res->num_rows <= 0 ) {
		   echo 1;
		   addBlacklist($id, "hack_EquipAbillity");
		   $db->close();
		   return;
		   }

		   $row = $res->fetch_assoc();									//보유중인 어빌리티 아닐시 핵
		   $resultMask = $row[$bundleName] & $mask;
		   if ( $resultMask <= 0 ) {
		   echo 1;
		   addBlacklist($id, "hack_EquipAbillity2");
		   $db->close();
		   return;
		   }
		 */

		$openSlotVal = $row["openedSlot"];
		$openedSlotCount=0;											// 오픈된 슬롯 카운트 측정
		for ( $i=0; $i<$maxSlotCount; $i++ ) {
			if ( ($openSlotVal & 1) == 1 )
				$openedSlotCount++;
			$openSlotVal >>= 1;
		}

		$val0 = $row["usingBundle0"];
		$val1 = $row["usingBundle1"];
		$usingSlotCount=0;
		for ( $i=0; $i<64; $i++ ) {									// 사용중인 슬롯 카운트 측정
			if ( ($val0 & 1) == 1 )
				$usingSlotCount += $this->GetAbillityNeedSlot($i);
			$val0 >>= 1;

			if ( ($val1 & 1) == 1 )
				$usingSlotCount += $this->GetAbillityNeedSlot($i+64);
			$val1 >>= 1;
		}


		$needSlotCount = $this->GetAbillityNeedSlot($abillityId);
		if ( $needSlotCount + $usingSlotCount > $openedSlotCount ) {
			$this->logger->logError(__FUNCTION__.' : ERROR FAIL userId:'.$userId.' need : '. $needSlotCount. ' using : '. $usingSlotCount. 'opened : '.$openedSlotCount);
			$resultFail['ResultCode'] = 303;
			return $resultFail;
		}

		$resultUsingMask = $row[$usingBundleName] | $mask;			//장착


		$sql = "UPDATE frdAbillity SET ".$usingBundleName." = :resultUsingMask WHERE userId=:userId ";
		$db -> prepare($sql);
		$db -> bindValue(':resultUsingMask', $resultUsingMask, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

		$apc_key = "EffectRewardAB_" . $abillityId;
		$sp = apc_fetch($apc_key);
		if ($sp) {  
			$type = (int)$sp[1];
			$val = (int)$sp[2];
			$sql = "SELECT * from frdEffectForRewards where userId=:userId and type=:type"; 
			$db->prepare($sql);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db->bindValue(':type', $type, PDO::PARAM_INT);
			$db->execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				$sql = "INSERT into frdEffectForRewards (userId, type, val ) values (:userId, :type, :val)";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':type', $type, PDO::PARAM_INT);
				$db -> bindValue(':val', $val, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
					return $resultFail;
				}
			} else {
				$sql = "UPDATE frdEffectForRewards SET val= val + :val WHERE userId=:userId AND type=:type";
				$db -> prepare($sql);
				$db -> bindValue(':val', $val, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':type', $type, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
					return $resultFail;
				}
			}
		}

		$apc_key = "EffectEtcAB_".$abillityId;
		$sp = apc_fetch($apc_key);
		if ( $sp !== false ) {
			$type = (int)$sp[1];
			$val = (int)$sp[2];

			$ApplyResult = $this->ApplyAbillity($db, $userId, null, $type, $val, true);
			if ( $ApplyResult['ResultCode'] != 100 ) {
				$this->logger->logError('SetEquipAbillity:: ApplyAbil.. FAIL userID:'.$userId);
				return $result;
			}

			if (isset($ApplyResult['rewards']) ){
				$data['rewards'] = $ApplyResult['rewards'];
			}

			if (isset($ApplyResult['use']) ){
				$data['use'] =  $ApplyResult['use'];
			}


			$sql = "SELECT * FROM frdEffectForEtc WHERE userId=:userId and type=:type";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':type', $type, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				$sql = "INSERT INTO frdEffectForEtc (userId, type, val ) values (:userId, :type, :val)";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':type', $type, PDO::PARAM_INT);
				$db -> bindValue(':val', $val, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
					return $resultFail;
				}
			}
			else {
				$sql = "UPDATE frdEffectForEtc SET val= val + :val WHERE userId=:userId AND type=:type"; 
				$db -> prepare($sql);
				$db -> bindValue(':val', $val, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':type', $type, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
					return $resultFail;
				}
			}
		}

		if ( $bundleIdx == 0) {
			$data["usingBundle0"] = $resultUsingMask;
			$data["usingBundle1"] = $val1;
		}
		else {
			$data["usingBundle0"] = $val0;
			$data["usingBundle1"] = $resultUsingMask;
		}

		$result['Protocol'] = 'ResEquipAbility';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}

	function RemEffectAB ($db, $userId, $data, $abillityId) {
		$rewards = null;
		$use = null;


		$resultFail['ResultCode'] = 300;
		//  echo $abillityId."!!\n";
		$apc_key = "EffectRewardAB_" . $abillityId;
		$sp = apc_fetch($apc_key);
		if ( $sp !== false ) {
			$type = (int)$sp[1];
			$val = (int)$sp[2];

			$sql = "select val from frdEffectForRewards where userId=:userId and type=:type";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':type', $type, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				$this -> logger -> logError(__FUNCTION__.' : fail userId :'.$userId.' sql : '.$sql );
				return $resultFail;
			}

			$resultVal = $row["val"] - $val;
			if ( $resultVal > 0 ) {
				$sql = "update frdEffectForRewards set val=val-:val where userId=:userId and type=:type";
				$db -> prepare($sql);
				$db -> bindValue(':val', $val, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':type', $type, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this -> logger -> logError(__FUNCTION__.' : fail userId :'.$userId.' sql : '.$sql );
					return $resultFail;
				}
			} else {
				$sql = "delete from frdEffectForRewards where userId=:userId and type=:type";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':type', $type, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this -> logger -> logError(__FUNCTION__.' : fail userId :'.$userId.' sql : '.$sql );
					return $resultFail;
				}

			}
		}
		
		$apc_key = "EffectEtcAB_" . $abillityId;
		$sp = apc_fetch($apc_key);
		if ( $sp !== false ) {
			$type = (int)$sp[1];
			$val = (int)$sp[2];
			if ($val < 0 ) {
				$this -> logger -> logError('RemEffectAB :value apc error userId :' . $userId );
				return $resultFail;
			}

			$aaResult = $this->ApplyAbillity($db, $userId, $data, $type, $val, false);
			if ( $aaResult['ResultCode'] != 100 ) {
				$this -> logger -> logError('ApplyAbillity : fail userId :' . $userId );
				return $resultFail;
			}
			
			if (isset($aaResult['rewards'])) {
				$rewards = $aaResult['rewards'];
				$result['rewards'] = $rewards;
			}

			if (isset($aaResult['use'])) {
				$use = $aaResult['use'];
				$result['use'] = $use;
			}

			$sql = "SELECT val FROM frdEffectForEtc WHERE userId=:userId and type=:type";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':type', $type, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if ($row) {
				$resultVal = $row["val"] - $val;
				if ( $resultVal > 0 ) {
					$sql = "update frdEffectForEtc set val=val-:val where userId=:userId and type=:type";
					$db -> prepare($sql);
					$db -> bindValue(':val', $val, PDO::PARAM_INT);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> bindValue(':type', $type, PDO::PARAM_INT);
				} else {
					$sql = "delete from frdEffectForEtc where userId=:userId and type=:type";
					$db -> prepare($sql);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> bindValue(':type', $type, PDO::PARAM_INT);

				}
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this -> logger -> logError(__FUNCTION__.' : fail userId :'.$userId.' sql : '.$sql );
					return $resultFail;
				}
			}
		}
		$result['ResultCode'] = 100;
		return $result;
	}

	function ApplyAbillity($db, $userId, $data, $abillityType, $val, $isEquip) {
		$resultFail['ResultCode'] = 300;
		$result['ResultCode'] = 100;

		switch($abillityType) {
			case 1000:      //Increase_TempleOutput
				$sql = "SELECT Temple_Table_ID, templeId, templeLevel, clearLevel, upState,
					TIMESTAMPDIFF(SECOND, lastRecvTime, now()) as recvTime, 
					UNIX_TIMESTAMP(lastRecvTime) as lastRecvTime,
					UNIX_TIMESTAMP(startLvUpTime) as lvUpTime
						from frdTemple where userId = :userId";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				$resultList = null;
				while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
					$resultList[] = $row;		
				}

				foreach($resultList  as $rs) {
					$templeId 		= $rs['templeId'];
					$templeLevel 	= $rs['templeLevel'];
					$recvTime 		= $rs['recvTime'];

					$char = array();
					$char['templeId'] 		= $templeId;
					$char['templeLevel'] 	= $templeLevel;
					$char['clearLevel'] 	= $rs['clearLevel'];
					$char['lastRecvTime'] 	= $rs['lastRecvTime'];

					// 레벨업중인지 검사 
					if ( $rs['upState'] ) {
						$char['upState'] 		= $rs['upState'];		

						$apc_key = 'TempleProduct_'.$templeId;
						$apc_data = apc_fetch($apc_key);
						$lvData = $apc_data[4];
						$myLVTime = explode('/', $lvData);
						$myTime = $myLVTime[$templeLevel-1];
						$char['requireTime'] = $myTime*60;

						$char['lvUpTime'] 	= $rs['lvUpTime']+ ($myTime*60); // 레벨업 잔여 시간 
					} else {
						$char['upState'] 		= $rs['upState'];
						$char['lvUpSpareTime'] 	= -1;
					}
					$data["temples"][] = $char;
				}
			
				return $result;
			case 1004:      //legendWeapon
				if ( $isEquip ) {
					$weapons = null;

					$sql = "SELECT * FROM frdHavingWeapons 
						WHERE userId=:userId and weaponId = :val";
					$db->prepare($sql);
					$db->bindValue(':userId', $userId, PDO::PARAM_INT);
					$db->bindValue(':val', $val, PDO::PARAM_INT);
					$db -> execute();
					$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
					if ($row) {
						$this->logger->logError(__FUNCTION__.' : DON`T REACH THIS!! delete userId :' . $userId .' val : '.$val );
						$this->logger->logError(__FUNCTION__.' : DON`T REACH THIS!! sql :' . $sql);

						break;
					}

					$AddManager = new AddManager();	
					$AddResult = $AddManager->addWeapon($db, $userId, $val, 0);
					$weapons = null;
					$weapons['weaponTableId'] = $AddResult['weaponTableId'];
					$weapons['weaponId'] = $AddResult['weaponId'];
					$weapons['level'] = $AddResult['level'];
					$weapons['statIds'] = $AddResult['statIds'];
					$weapons['statVals'] = $AddResult['statVals'];
					$weapons['exp'] = $AddResult['exp'];
			
					$rewards['weapons'][] = $weapons;
					$result['rewards'] = $rewards;
				}
				else {
					$this->logger->logError(__FUNCTION__.' : 1004 userId : ' . $userId . ", val : " . $val);
					$use = null;
					$sql = "SELECT * FROM frdHavingWeapons 
						WHERE userId=:userId and weaponId = :val";
					$db->prepare($sql);
					$db->bindValue(':userId', $userId, PDO::PARAM_INT);
					$db->bindValue(':val', $val, PDO::PARAM_INT);
					$db -> execute();
					$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
					if ($row) {
						$WTID = $row['weaponTableId'];

						$logDBSendManager = new LogDBSendManager();
						$logDBSendManager->sendWeapon($userId, $row['weaponId'], 2,$row['level'], $row['exp'], $row['statId0'], $row['statId1'], $row['statId2'], $row['statId3'], $row['statVal0'], $row['statVal1'], $row['statVal2'], $row['statVal3']);

						$sql = "DELETE FROM frdHavingWeapons where userId= :userId and weaponId = :val ";
						$db -> prepare($sql);
						$db -> bindValue(':val', $val, PDO::PARAM_INT);
						$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
						$row = $db -> execute();
						if (!isset($row) || is_null($row) || $row == 0) {
							$this -> logger -> logError(__FUNCTION__.' : fail delete userId :' . $userId );
							$this -> logger -> logError('sql' . $sql );
							return $resultFail;
						}
						
					} else {
						$this->logger->logError(__FUNCTION__.' : DON`T REACH THIS userId : ' . $userId . ", val : " . $val);
						$this->logger->logError(__FUNCTION__.' : DON`T REACH THIS sql : ' . $sql);
					}

					$use['weapons']['delTableIds'][] = $WTID;
					$result['use'] = $use;
				}
				return $result;
			case 1019:      //legendMagic
				if ( $isEquip ) {
					$AddManager = new AddManager();	
					$MagicResult = $AddManager->addMagic($db, $userId, $val);
					$magicTemp = null;

					$magicTemp = null;
					$magicTemp['magicTableId'] = $MagicResult['magicTableId'];
					$magicTemp['magicId'] = $MagicResult['magicId'];
					$magicTemp['exp'] = 0;
			
					$rewards['magics'][] = $magicTemp;
					$result['rewards'] = $rewards;
				} else {
					$sql = "
						SELECT magicTableId, magicId, level FROM frdHavingMagics
						WHERE userId=:userId AND magicId=:val ORDER BY level ASC ";
					$db->prepare($sql);
					$db->bindValue(':userId', $userId, PDO::PARAM_INT);
					$db->bindValue(':val', $val, PDO::PARAM_INT);
					$db -> execute();
					$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
					if ($row) {
						$magicTableId = $row['magicTableId'];
	
						$logDBSendManager = new LogDBSendManager();
						$logDBSendManager->sendMagic($userId, $row['magicId'], 2, $row['level']);

						$sql = "DELETE FROM frdHavingMagics where magicTableId = :magicTableId and userId= :userId";
						$db -> prepare($sql);
						$db -> bindValue(':magicTableId', $magicTableId, PDO::PARAM_INT);
						$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
						$row = $db -> execute();
						if (!isset($row) || is_null($row) || $row == 0) {
							$this -> logger -> logError(__FUNCTION__.' : fail delete userId :' . $userId );
							$this -> logger -> logError('sql' . $sql );
							return $resultFail;
						}

						$result['use']['magics']['delTalbeIds'][] = (int)$magicTableId;
					}
				}
				return $result;

			case 1032:
				if ( !$isEquip ) {
					$sql = "
						SELECT userLevel, heart, maxHeart FROM frdUserData WHERE userId=:userId ";
					$db->prepare($sql);
					$db->bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> execute();
					$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
					if ($row) {
						$level = $row['userLevel'];
						$maxHeart = $row['maxHeart'];
						$heart = $row['heart'];

						$myKey = $level;
						$lv_apc_key = 'UserExp_MaxHeart_'.$myKey;
						$lv_data = apc_fetch($lv_apc_key);
						$myMaxHeart = $lv_data[2];

						$sql = "
							UPDATE frdUserData SET maxHeart = :maxHeart WHERE userId = :userId ";
						$db -> prepare($sql);
						$db -> bindValue(':maxHeart', $myMaxHeart, PDO::PARAM_INT);
						$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
						$row = $db -> execute();
						if (!isset($row) || is_null($row) || $row == 0) {
							$this->logger->logError(__FUNCTION__.': update3 userId.'.$userId .', sql : '.$sql);
							$this->logger->logError(__FUNCTION__.': update3 userId.'.$userId .', maxHeart : '.$maxHeart);
							$result['ResultCode'] = 300;
							return $result;
						}
					}
					$heart = null;
					$AssetManager = new AssetManager();
					$AssetResult = $AssetManager->checkHeart($db, $userId);
					if ($AssetResult['ResultCode'] == 100) {
						$heart['passTime'] = $AssetResult['passTime'];
						$heart['curHeart'] = $AssetResult['heart'];
						$heart['maxHeart'] = $AssetResult['maxHeart'];
					}
					$result['rewards']['heart'] = $heart;
					return $result;

				} else {
					$sql = "
						SELECT userLevel, heart, maxHeart FROM frdUserData WHERE userId=:userId ";
					$db->prepare($sql);
					$db->bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> execute();
					$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
					if ($row) {
						$level = $row['userLevel'];
						$maxHeart = $row['maxHeart'];
						$heart = $row['heart'];

						$myKey = $level;
						$lv_apc_key = 'UserExp_MaxHeart_'.$myKey;
						$lv_data = apc_fetch($lv_apc_key);
						$myMaxHeart = $lv_data[2];

						$curMaxHeart = $maxHeart;
						$bonusMaxHeart = round($myMaxHeart * $val*0.00001);
						$curMaxHeart += $bonusMaxHeart;

						if ($maxHeart < $curMaxHeart) {
							$sql = "
								UPDATE frdUserData SET maxHeart = :maxHeart
								WHERE userId = :userId ";
							$db -> prepare($sql);
							$db -> bindValue(':maxHeart', $curMaxHeart, PDO::PARAM_INT);
							$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
							$row = $db -> execute();
							if (!isset($row) || is_null($row) || $row == 0) {
								$this->logger->logError(__FUNCTION__.': update2 userId.'.$usreId .', sql : '.$sql);
								$result['ResultCode'] = 300;
								return $result;
							}
						}
					}
					$heart = null;
					$AssetManager = new AssetManager();
					$AssetResult = $AssetManager->checkHeart($db, $userId);
					if ($AssetResult['ResultCode'] == 100) {
						$heart['passTime'] = $AssetResult['passTime'];
						$heart['curHeart'] = $AssetResult['heart'];
						$heart['maxHeart'] = $AssetResult['maxHeart'];
					}
					$result['rewards']['heart'] = $heart;
					return $result;
				}
			default:        
				return $result;
		}
	}

	public function ResetAbility($param) {
		$resultFail['ResultCode'] = 300;
		$resultFail['Protocol'] = 'ResResetAbility';

		$userId = $param["userId"];
		$price = 10;
		$data = null;
		$db = new DB();

		$sql = "SELECT usingBundle0, usingBundle1 from frdAbillity WHERE userId=:userId"; 
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this -> logger -> logError(__FUNCTION__.' : fail userId :'.$userId.' sql : '.$sql );
			return $resultFail;
		}

		$val0 = $row["usingBundle0"];
		$val1 = $row["usingBundle1"];
		$usingSlotCount=0;

		for ( $i=0; $i<64; $i++ ) {                                 // 사용중인 슬롯 카운트 측정
			$temp = null;
			if ( ($val0 & 1) > 0 ) {
				$data = $this->RemEffectAB($db, $userId, $data, $i);
			}
			$val0 >>= 1;

			if ( ($val1 & 1) > 0 ) {
				$data = $this->RemEffectAB($db, $userId, $data, $i+64);
			}
			$val1 >>= 1;
//			$data = array_merge($data,$temp);
			
		}

		$sql = "update frdAbillity set usingBundle0=0, usingBundle1=0 where userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.' : fail userId :'.$userId.' sql : '.$sql );
			return $resultFail;
		}

		$sql = "select itemTableId, itemCount 
			from frdHavingItems where userId = :userId and itemId=100002";
		// 100002 = pass_ResetABSlot
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$AssetManager = new AssetManager();
			$AssetResult = $AssetManager->useJewel($db, $userId, $price);
			if ($AssetResult['ResultCode'] != 100) {
				return $resultFail;
			}
			$jewel = null;
			$jewel["curJewel"] = $AssetResult['jewel'];
			$jewel["addJewel"] = 0 - $price;
			
			$data['use']['jewel'] = $jewel;

		} else {
			$restItem = (int)$row["itemCount"] -1;
			if ( $restItem <= 0 ) {
				$sql = "delete from frdHavingItems where userId=:userId and itemId=100002";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			} else {
				$sql = "update frdHavingItems set itemCount=:restItem where userId=:userId and itemId=100002";
				$db -> prepare($sql);
				$db -> bindValue(':restItem', $restItem, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			}
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError(__FUNCTION__.' : fail userId :'.$userId.' sql : '.$sql );
				return $resultFail;
			}

			$logDBSendManager = new LogDBSendManager();
			$logDBSendManager->sendItem($userId, 100002, $restItem+1, $restItem, -1);


			$item = null;
			$item['itemId'] = 100002;
            $item['add'] = -1;
            $item['cur'] = $restItem;
            $data['use']['items'][] = $item;
		} 

		$result['Data'] = $data;
		$result['Protocol'] = 'ResResetAbility';
		$result['ResultCode'] = 100;
		return $result;	
	}
}
?>
