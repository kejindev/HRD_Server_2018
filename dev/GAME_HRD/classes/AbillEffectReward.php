<?php
include_once './common/define.php';
require_once './lib/Logger.php';

class AbillEffectReward {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function getAbillEffectReward($db, $userId) {
		$sql = "select type, val from frdEffectForRewards where userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$sres = array();
		while ($temp = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$sres[] = $temp;
		}
		/*
		   effectRewardAB, 2, "1002,15000"  //Get More Medal      + 15%
		   effectRewardAB, 6, "1006,20000"  //Get More Gold       + 20%
		   effectRewardAB, 15, "1015,20000"  //Get More Rune       + 20%
		   effectRewardAB, 26, "1006,30000"  //Get More Gold       + 30%
		   effectRewardAB, 36, "1036,15000"  //Get Medal Percent   + 15%
		   effectRewardAB, 38, "1038,30000"  //Get Rune Percent   + 30%
		   effectRewardAB, 43, "1,25000"  //Get exp Percent   + 25%
		   effectRewardAB, 46, "1046,20000"  //Get QuestItem Percent   + 20%
		   effectRewardAB, 47, "1,30000"  //Get exp Percent   + 25%
		*/
		$medalAddPer = 0; // 2
		$medalGetPer = 0; // 36
		$addGold = 0; // 6, 26
		$runeAddPer = 0; // 15
		$runeGetPer = 0; // 38
		$expAddPer = 0; // 43, 47
		$questItemGetPer = 0; // 46

		foreach($sres as $rs) {
			switch($rs['type']){ 
				case 1002:
					$medalAddPer += $rs['val'];
					break;
				case 1006: 
					$addGold += $rs['val'];
					break;
				case 1015: 
					$runeAddPer += $rs['val'];
					break;
				case 1036: 
					$medalGetPer += $rs['val'];
					break;
				case 1038: 
					$runeGetPer += $rs['val'];
					break;
				case 1: 
					$expAddPer += $rs['val'];
					break;
				case 1046: 
					$questItemGetPer += $rs['val'];
					break;
				default:
					break;
			}
		}
		$abEfResult = null;
		$abEfResult['medalAddPer'] = 		$medalAddPer;
		$abEfResult['medalGetPer'] = 		$medalGetPer;
		$abEfResult['addGold'] = 			$addGold;
		$abEfResult['runeAddPer'] = 		$runeAddPer;
		$abEfResult['runeGetPer'] = 		$runeGetPer;
		$abEfResult['expAddPer'] = 			$expAddPer;
		$abEfResult['questItemGetPer'] = 	$questItemGetPer;

		return $abEfResult;
	}
}
?>
