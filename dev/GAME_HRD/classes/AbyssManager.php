<?php
include_once './common/DB.php';
include_once './common/RedisDB.php';
require_once './lib/Logger.php';
require_once './classes/RewardManager.php';
require_once './classes/WeaponManager.php';
require_once './classes/EventManager.php';

class AbyssManager {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function getRedisAbyssName() {
		switch($GLOBALS['Type']) {
			case 'OS':
			case 'AS':
				$climbName = "abyssRank_AIOS";
				break;
			case 'PS':
				$climbName = "abyssRank_google";
				break;
			default:
				$climbName = "climbRank_Error";
				break;
		}
	
		return $climbName;	
	}

	function AbyssRewardList($param) {
		$resultFail['ResultCode'] = 300;
		$userId = $param["userId"];

		$apc_result = null;

		$len = 60;
		for ($i = 1001; $i < 1001+$len; $i++) {
			$apc_key = "AbyssReward_".$i;
			$apc_data = apc_fetch($apc_key);
			$temp_str = $apc_data[1].','.$apc_data[2];
			$apc_result[] = $temp_str;
		}

		$data["abyssRewards"] = $apc_result;
		$data["count"] = $len;

		$db = new DB();
		$sql = "select stageId, sec from frdAbyss where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$data["stageId"] = 1000;
			$data["recvVal"] = 0;
		} else {
			$data["stageId"] = $row["stageId"];
			$data["sec"] = $row["sec"];
		}

		$result['Protocol'] = 'ResAbyssRewardList';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}

	function AbyssReward($param){
		$resultFail['ResultCode'] = 300;
		$userId = $param["userId"];

		// 추가된 Id // 이전reqidx 
		$stageId = $param["stageId"];
		$sec 	= $param['sec'];

		$db = new DB();
		// 보상 하는 구간이 아닌 결과를 저장 하는 프로토콜로 변경한다
		$sql = "SELECT clearCount, bestSecs from frdClearCount where userId=:userId and stageId=:stageId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row ) {
			$sql = "insert into frdClearCount 
				(userId, stageId, clearCount, bestSecs) values (:userId, :stageId, 1, :bestSecs)";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
			$db -> bindValue(':bestSecs', $sec, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
				return $resultFail;
			}
			$data['bestSecs'] = $seconds;
		} else {
			$finalBestSecs = $seconds < (int)$row['bestSecs'] ? $seconds : (int)$row['bestSecs'];
			$sql = "UPDATE frdClearCount SET 
				clearCount=clearCount+1,
				bestSecs=:bestSecs
				WHERE userId=:userId and stageId=:stageId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
			$db -> bindValue(':bestSecs', $finalBestSecs, PDO::PARAM_INT);
			$returnBestSecs = $row['bestSecs'];
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
				return $resultFail;
			}
			$data['bestSecs'] = $returnBestSecs;
		}

		// reqIdx 는 실제 사용 하는 값으로 바뀌어야 한다. 
		$sql = "select stageId, sec from frdAbyss where userId = :userId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			if ($stageId == 1001) {
				$myStageId = 1000;	
				// 첫클리어 정보 저장 
				$sql = "INSERT INTO frdAbyss (userId, stageId, sec, update_date) values
						( :userId, :stageId, :sec, now() ) ";
				$db -> prepare($sql);
				$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
				$db -> bindValue(':sec', $sec, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this -> logger -> logError(__FUNCTION__.' :  FAIL userId :' . $userId.' sql :'.$sql );
					return $resultFail;
				}
				$row = null;
				$row['sec'] = 0;
			} else {
				$this -> logger -> logError(__FUNCTION__.' :  FAIL userId :' . $userId.' sql :'.$sql );
				$resultFail['ResultCode'] = 100;
				$resultFail['Data'] = null;
				return $resultFail;
			}
			$mySec = 0;
		} else {
			$myStageId = $row['stageId'];	
			$mySec = $row['sec'];	

			// 이전 클리어 결과 보다 높은지 계산.
			// 해당 스테이지를 처음 클리어 한것인지 검증 
			// 첫 클리어가 아니라면 여기서 리턴 하여 중지 한다. 
			if  ($myStageId >= $stageId) {
				$this -> logger -> logError(__FUNCTION__.' :  already reward userId :' . $userId);
				$result['ResultCode'] = 100;
				$result['Protocol'] = 'ResAbyssReward';
				$result['Data'] = null;

				// 업적 처리 로직 
				$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
					where userId=:userId and achieveId > 2300 and achieveId < 2400";
				$db->prepare($sql);
				$db->bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				if ($row) {
					$achieveId = $row['achieveId'];
					if ($row['cnt'] != -1 ) {
						require_once './classes/AchieveManager.php';
						$AchieveManager = new AchieveManager();
						$AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
					}
				}

				return $result;
			}

			// 첫클리어 정보 저장 
			$sql = "UPDATE frdAbyss set stageId=:stageId, sec = sec + :sec where userId = :userId";
			$db -> prepare($sql);
			$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
			$db -> bindValue(':sec', $sec, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError(__FUNCTION__.' :  FAIL userId :' . $userId.' sql :'.$sql );
				return $resultFail;
			}
		}


		// 지급 처리 진행 
		$apc_key = "AbyssReward_".$stageId;
		$apc_result = apc_fetch($apc_key);
		if (!$apc_result) {
			$this -> logger -> logError(__FUNCTION__.' :  apc_none userId :' . $userId.' stageId :'.$stageId );
			$resultFail['ResultCode'] = 100;
			$resultFail['Data'] = null;
			return $resultFail;
		}

		// 0 : floor
		// 1 : rewardtype '/' array
		// 2 : amount'/' array
		$types = explode('/', $apc_result[1]);
		$counts = explode('/', $apc_result[2]);
		$EventManager = new EventManager();
		$EventManager->getDoubleBonus_Abyss($stageId, $types, $counts);
		
		// 랭킹 업데이트 진행 
		#### REDIS SET  RANK
		$abyssName = $this->getRedisAbyssName();

		$RedisServer = new RedisServer();
		// totalScore = floor * 10000 - sec;
		$myFloor = substr($stageId, 2,2);

		settype($myFloor,"integer");

		$totalScore = $myFloor * 1000000 - ($mySec+$sec);
		$rr = $RedisServer->zAdd($abyssName, $totalScore, $userId);

		$rewardManager = new RewardManager();	
		$WeaponManager = new WeaponManager();

		$rew = $rewardManager->getReward($db, $types, $counts, $userId, $WeaponManager, null,null);
		if ( $rew['ResultCode'] != 100 ) {
			$this -> logger -> logError('get_AbyssReward : fail UPDATE userId :' . $userId );
			return $resultFail;
		}

		// 업적 처리 로직 
		 $sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
                    where userId=:userId and achieveId > 2300 and achieveId < 2400";
		 $db->prepare($sql);
		 $db->bindValue(':userId', $userId, PDO::PARAM_INT);
		 $db -> execute();
		 $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		 if ($row) {
			 $achieveId = $row['achieveId'];
			 if ($row['cnt'] != -1 ) {
				 require_once './classes/AchieveManager.php';
				 $AchieveManager = new AchieveManager();
				 $AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
			 }
		 }


		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResAbyssReward';

		$data['rewards'] = $rew['Data'];
		$data['stageId'] = $stageId;
		$data['playTime'] = $mySec + $sec;

		$result['Data'] = $data;

		return $result;
	}

	function AbyssBossSkill ($param) {
		$userId = $param["userId"];

		$apc_result = null;

		$db = new DB();
		$sql = "select level, recvVal from frdAbyss where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$data["level"] = $row["level"];
			$data["recvVal"] = $row["recvVal"];
		}

		$apc_key = "AbyssReward_10";
		$apc_10 = apc_fetch($apc_key);
		$temp_str = $apc_10[1].','.$apc_10[2];
		$apc_result[] = $temp_str;

		$data['rewards'] = $reward;
		$result['Data'] = $data;

		return $result;
	}

	function AbyssRanking ($param) {
		$userId = $param["userId"];
		$nextRanking = $param["nextRanking"]; // 1 ~ 10

		$apc_result = null;

		$db = new DB();

		// stageId 를 실제 apc 에서 사용하는 stageId 로 전달
		// 당연 저장하는 구간도 abyssReward 에서 진행 

		$rankRedis = new RedisServer();	
		
		$abyssName = $this->getRedisAbyssName();

		$userIds = $rankRedis->zRevRange($abyssName, $nextRanking, $nextRanking+9, null);

		$length = count($userIds);
		$temp = null;
		$rank = null;

		switch ($GLOBALS['COUNTRY']) {
			case 'Korea':
				

				switch ($GLOBALS['Type']) {
					case 'AS':case 'OS':
						$GLOBALS['DB_HOST'] = "172.27.100.3";
						$GLOBALS['DB_NAME'] = "hrd_db_game_21";
						$db_21 = new DB();
						$GLOBALS['DB_NAME'] = "hrd_db_game_22";
						$db_22 = new DB();

						$GLOBALS['DB_NAME'] = "hrd_db_game_71";
						$db_71 = new DB();
						$GLOBALS['DB_NAME'] = "hrd_db_game_72";
						$db_72 = new DB();

						$GLOBALS['DB_HOST'] = "172.27.100.4";
						$GLOBALS['DB_NAME'] = "hrd_db_game_51";
						$db_51 = new DB();
						$GLOBALS['DB_NAME'] = "hrd_db_game_52";
						$db_52 = new DB();

						$GLOBALS['DB_NAME'] = "hrd_db_game_81";
						$db_81 = new DB();
						$GLOBALS['DB_NAME'] = "hrd_db_game_82";
						$db_82 = new DB();
						break;

					case 'PS':
						$GLOBALS['DB_HOST'] = "172.27.100.3";
						$GLOBALS['DB_NAME'] = "hrd_db_game_20";
						$db_20 = new DB();	

						$GLOBALS['DB_NAME'] = "hrd_db_game_70";
						$db_70 = new DB();


						$GLOBALS['DB_HOST'] = "172.27.100.4";
						$GLOBALS['DB_NAME'] = "hrd_db_game_50";
						$db_50 = new DB();

						$GLOBALS['DB_NAME'] = "hrd_db_game_80";
						$db_80 = new DB();
						break;
					
					default:
						break;
				}
				break;

			case 'Korea_QA':
				$GLOBALS['DB_HOST'] = "172.27.100.10";
				$GLOBALS['DB_NAME'] = "hrd_db_test";
				$db_20 = new DB();
				$db_21 = $db_20;
				$db_22 = $db_20;
				$db_50 = $db_20;
				$db_51 = $db_20;
				$db_52 = $db_20;
				$db_70 = $db_20;
				$db_71 = $db_20;
				$db_72 = $db_20;
				$db_80 = $db_20;
				$db_81 = $db_20;
				$db_82 = $db_20;
				break;
			
			case 'China_QA':
				$GLOBALS['DB_HOST'] = "106.15.0.164";
				$GLOBALS['DB_NAME'] = "hrd_db_test";
				$db_20 = new DB();
				$db_21 = $db_20;
				$db_22 = $db_20;
				$db_50 = $db_20;
				$db_51 = $db_20;
				$db_52 = $db_20;
				$db_70 = $db_20;
				$db_71 = $db_20;
				$db_72 = $db_20;
				$db_80 = $db_20;
				$db_81 = $db_20;
				$db_82 = $db_20;
				break;

			default:
				break;
		}


		for($i=0; $i < $length; $i++) {
			$targetUserId  = $userIds[$i];
			$accNo = substr($targetUserId, 0,3);

			switch($accNo) {
				case 200:	$db = $db_20;	break;
				case 201:	$db = $db_21;	break;
				case 202:	$db = $db_22;	break;

				case 600:
				case 599:
				case 500:	$db = $db_50;	break;
				case 501:	$db = $db_51;	break;
				case 502:	$db = $db_52;	break;

				case 700:	$db = $db_70;	break;
				case 701:	$db = $db_71;	break;
				case 702:	$db = $db_72;	break;

				case 800:	$db = $db_80;	break;
				case 801:	$db = $db_81;	break;
				case 802:	$db = $db_82;	break;
				default:break;
			}
			

			$sql = "select ud.userId, userLevel, name, stageId,sec  from frdUserData as ud
				INNER JOIN frdAbyss as ab ON ud.userId = ab.userId
			where ud.userId = :userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $targetUserId, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				$temp["rank"] = null;
			} else {
				$temp['rank'] = $i+$nextRanking;
				$temp['name'] = $row['name'];
				$temp['floor'] = (int)substr($row['stageId'], 2,2);
				$temp['playTime'] = $row['sec'];
				$temp['userLevel'] = $row['userLevel'];
				$rank[] = $temp;
			}
		}
/*
params
  - nextRanking : int

return value
  - nextRanking포함 이후 10개 랭킹 데이터
  - 랭킹데이터에 포함되어야할 정보
    * 등수
    * 이름
    * 티어
    * 단계수
    * 누적 시간
*/

		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResAbyssRanking';
		$data['rank'] = $rank;
		$result['Data'] = $data;

		return $result;
	}
}
?>
