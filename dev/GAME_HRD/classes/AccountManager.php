<?php
include_once './common/DB.php';
require_once './lib/Logger.php';

class AccountManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }
	
    public function create_account($db, $userId, $name) {
        $result['ResultCode'] = 300;

		// initialling user data
		// $userId
		// $name
		$name = 't_'.$name;
		$blockState = 0;
		$tutoLevel = 0;
		$userLevel = 0;
		$manaLevel = 1;
		$exp = 0;
		$gold = 4000;
		$jewel = 0;
		$powder = 0;
		$stageLevel = 0;
		$skillPoint = 0;
		$chapterReward = 0;
		$accuSkillPoint = 0;
		$ticket = 0;
		$heart = 30;
		$lastLoginDate = 0;
		$maxHeart = 30;
		$session = 0;

        $sql = <<<SQL
	INSERT INTO frdUserData (
			`userId` ,
			`name` ,
			`blockState` ,
			`tutoLevel` ,
			`userLevel` ,
			`manaLevel` ,
			`exp` ,
			`gold` ,
			`jewel` ,
			`powder` ,
			`stageLevel` ,
			`skillPoint` ,
			`chapterReward` ,
			`accuSkillPoint` ,
			`ticket` ,
			`heart` ,
			`maxHeart` ,
			`session`,
			`heartStartTime` ,
			`lastLoginDate` ,
			`update_date`,
			`reg_date`
	) VALUES (
	:userId, :name, :blockState, :tutoLevel, :userLevel, :manaLevel, :exp, :gold, :jewel, :powder, :stageLevel, 
		:skillPoint, :chapterReward, :accuSkillPoint, :ticket, :heart, :maxHeart, :session, now(), now(), now(), now()
	)
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId',$userId, PDO::PARAM_INT);
        $db -> bindValue(':name',$name, PDO::PARAM_STR);
        $db -> bindValue(':blockState',$blockState, PDO::PARAM_STR);
		$db -> bindValue(':tutoLevel',$tutoLevel, PDO::PARAM_INT);
		$db -> bindValue(':userLevel',$userLevel, PDO::PARAM_INT);
		$db -> bindValue(':manaLevel',$manaLevel, PDO::PARAM_INT);
		$db -> bindValue(':exp',$exp, PDO::PARAM_INT);
		$db -> bindValue(':gold',$gold, PDO::PARAM_INT);
		$db -> bindValue(':jewel',$jewel, PDO::PARAM_INT);
		$db -> bindValue(':powder',$powder, PDO::PARAM_INT);
		$db -> bindValue(':stageLevel',$stageLevel, PDO::PARAM_INT);
		$db -> bindValue(':skillPoint',$skillPoint, PDO::PARAM_INT);
		$db -> bindValue(':chapterReward',$chapterReward, PDO::PARAM_INT);
		$db -> bindValue(':accuSkillPoint',$accuSkillPoint, PDO::PARAM_INT);
		$db -> bindValue(':ticket',$ticket, PDO::PARAM_INT);
		$db -> bindValue(':heart',$heart, PDO::PARAM_INT);
		$db -> bindValue(':maxHeart',$maxHeart, PDO::PARAM_INT);
		$db -> bindValue(':session',$session, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__. ':  FAIL  userId : '.$userId.' sql :'.$sql);
            return $result;
        }

		// SET DATA frdSkillPoint Table 	
		$accuSkillPoint = 0;
		/*
        $sql = <<<SQL
	INSERT INTO frdSkillPoints (
			`userId`,
			`accuSkillPoint`,
			update_date, 
			reg_date
	) VALUES (
		:userId, :accuSkillPoint, now(), now()
	)
SQL;
        $db -> prepare($sql);
		$db -> bindValue(':userId',$userId, PDO::PARAM_INT);
		$db -> bindValue(':accuSkillPoint',$accuSkillPoint, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__. ':  FAIL  userId : ' . $userId. ' sql :'.$sql);
            return $result;
        }
		*/

		// SET DATA userData
        $sql = <<<SQL
		INSERT INTO frdCharInfo 
			(`userId`, `charId`, `exp`, reg_date) 
			VALUES
( :userId, 0, 10, now() ),
( :userId, 1, 10, now() ),
( :userId, 2, 10, now() ),
( :userId, 3, 10, now() ),
( :userId, 4, 10, now() ),
( :userId, 5, 10, now() ),
( :userId, 6, 10, now() ),
( :userId, 7, 10, now() ),
( :userId, 8, 10, now() ),
( :userId, 9, 10, now() ),
( :userId, 10, 10, now() ),
( :userId, 11, 10, now() ),
( :userId, 12, 10, now() ),
( :userId, 13, 10, now() ),
( :userId, 14, 10, now() ),
( :userId, 15, 10, now() ),
( :userId, 16, 10, now() ),
( :userId, 17, 10, now() ),
( :userId, 18, 10, now() ),
( :userId, 19, 10, now() ),
( :userId, 20, 10, now() ),
( :userId, 21, 10, now() ),
( :userId, 22, 10, now() ),
( :userId, 23, 10, now() ),
( :userId, 24, 10, now() ),
( :userId, 25, 10, now() ),
( :userId, 26, 10, now() ),
( :userId, 27, 10, now() ),
( :userId, 28, 10, now() ),
( :userId, 29, 10, now() ),
( :userId, 30, 10, now() ),
( :userId, 31, 10, now() ),
( :userId, 32, 10, now() ),
( :userId, 33, 10, now() ),
( :userId, 34, 10, now() ),
( :userId, 35, 10, now() ),
( :userId, 36, 10, now() ),
( :userId, 37, 10, now() ),
( :userId, 38, 10, now() ),
( :userId, 39, 10, now() ),
( :userId, 40, 10, now() ),
( :userId, 41, 10, now() ),
( :userId, 42, 10, now() ),
( :userId, 43, 10, now() ),
( :userId, 44, 10, now() ),
( :userId, 45, 10, now() ),
( :userId, 46, 10, now() ),
( :userId, 47, 10, now() ),
( :userId, 48, 10, now() ),
( :userId, 49, 10, now() ),
( :userId, 50, 10, now() ),
( :userId, 51, 10, now() ),
( :userId, 52, 10, now() ),
( :userId, 53, 10, now() ),
( :userId, 54, 10, now() ),
( :userId, 55, 10, now() ),
( :userId, 56, 10, now() ),
( :userId, 57, 10, now() ),
( :userId, 58, 10, now() ),
( :userId, 59, 10, now() ),
( :userId, 60, 10, now() ),
( :userId, 61, 10, now() ),
( :userId, 62, 10, now() ),
( :userId, 63, 10, now() ),
( :userId, 64, 10, now() ),
( :userId, 65, 10, now() ),
( :userId, 66, 10, now() ),
( :userId, 67, 10, now() ),
( :userId, 68, 10, now() ),
( :userId, 69, 10, now() ),
( :userId, 70, 10, now() ),
( :userId, 71, 10, now() ),
( :userId, 72, 10, now() ),
( :userId, 73, 10, now() ),
( :userId, 74, 10, now() )
SQL;

        $db -> prepare($sql);
		$db -> bindValue(':userId',$userId, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__. ':  FAIL  userId : ' . $userId. ' sql :'.$sql);
            return $result;
		}


        $sql = <<<SQL
	INSERT INTO frdEquipWeapons (
			`userId`,slotNo,
			update_date 
	) VALUES (
		:userId,0, now()
	)
SQL;
        $db -> prepare($sql);
		$db -> bindValue(':userId',$userId, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__. ':  FAIL  userId : ' . $userId. ' sql :'.$sql);
            return $result;
        }

        $sql = <<<SQL
	INSERT INTO frdEquipMagics (
			`userId`,slotNo,
			update_date 
	) VALUES (
		:userId, 0, now()
	)
SQL;
        $db -> prepare($sql);
		$db -> bindValue(':userId',$userId, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__. ':  FAIL  userId : ' . $userId. ' sql :'.$sql);
            return $result;
        }
		$sql = "INSERT INTO frdAchieveInfo (userId, achieveId,cnt,  update_date) VALUES 
			(:userId, 1101,0, now()),
			(:userId, 1201,100, now()),
			(:userId, 1301,0, now()),
			(:userId, 1501,1, now()),
			(:userId, 1601,0, now()),
			(:userId, 1701,0, now()),
			(:userId, 1801,0, now()),
			(:userId, 1901,0, now()),

			(:userId, 2101,0, now()),
			(:userId, 2201,0, now()),
			(:userId, 2301,0, now()),
			(:userId, 2401,0, now()),
			(:userId, 2501,0, now()),
			(:userId, 2601,0, now()),

			(:userId, 3101,0, now()),
			(:userId, 3201,0, now()),
			(:userId, 3301,0, now()),
			(:userId, 3401,0, now()),
			(:userId, 3501,0, now()),
			(:userId, 3601,0, now()),

			(:userId, 4101,0, now()),
			(:userId, 4201,0, now()),
			(:userId, 4301,0, now()),
			(:userId, 4401,0, now()),
			(:userId, 4501,0, now()),
			(:userId, 4601,0, now())

			";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

/*
///////////////////////// FOR TEST 
        $sql = <<<SQL
	INSERT INTO frdHavingItems (
			`userId`,
			`itemId`,
			itemCount, 
			reg_date
	) VALUES (
		:userId, 102007, 6, now()
	)
SQL;
        $db -> prepare($sql);
		$db -> bindValue(':userId',$userId, PDO::PARAM_INT);
        $row = $db -> execute();

        $sql = <<<SQL
	INSERT INTO frdHavingMagics (
			`userId`,
			`magicId`,
			level, 
			exp,
			equipIdx,
			reg_date
	) VALUES (
		:userId, 207, 1, 0, 0, now()
	)
SQL;
        $db -> prepare($sql);
		$db -> bindValue(':userId',$userId, PDO::PARAM_INT);
        $row = $db -> execute();

        $sql = <<<SQL
	INSERT INTO frdHavingWeapons (
			`userId`,`weaponId`,level, 	exp,
			`statId0`,`statId1`,`statId2`,`statId3`, 
			`statVal0`,	`statVal1`,`statVal2`,`statVal3`,
			equipIdx0, equipIdx1, equipIdx2, reg_date
	) VALUES (
		:userId, 0,208,0,
		1,10,10,10,
		10,26,55,76, 
		0,0,0,now()
	)
SQL;
        $db -> prepare($sql);
		$db -> bindValue(':userId',$userId, PDO::PARAM_INT);
        $row = $db -> execute();
*/


        $result['ResultCode'] = 100;
        $result['userId'] = $userId;

        return $result;
    }

    //계정 정지
    public function set_user_block($param) {
        $db = new db();
        $PlayerID = $param['PlayerID'];
        $date = $param['date'];

        $resultFail['Protocol'] = 'ResBlockAccount';
        $resultFail['ResultCode'] = 300;

        if ($date < 0) {
            $ResUnlimited = $this -> set_user_unlimited_block($db, $PlayerID);
            if ($ResUnlimited['ResultCode'] != 100) {
                $resutlFail['ResultCode'] = $ResUnlimited['ResultCode'];
                return $resutlFail;
            } else {
                $result['Protocol'] = 'ResBlockAccount';
                $result['ResultCode'] = $ResUnlimited['ResultCode'];
                return $result;
            }
        }

        $sql = <<<SQL
			SELECT status,DATE_ADD(now(), INTERVAL :date DAY) as unblock_date FROM User_Regist WHERE PlayerID =:PlayerID
		
		
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':PlayerID', $PlayerID, PDO::PARAM_STR);
        $db -> bindValue(':date', $date, PDO::PARAM_STR);
        $r = $db -> execute();
        if (!$r) {
            $this -> logger -> logError("DB : {$GLOBALS['AccessNo']}, PlayerID : {$PlayerID}, AccountManager :: set_user_block FAIL QUERY sql : {$sql}");
            return $resultFail;
        }
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        $unblock_date = $row['unblock_date'];
        $status = $row['status'];
        /*if ($status > 2) {
         $this -> logger -> logError("DB : {$GLOBALS['AccessNo']}, PlayerID : {$PlayerID}, AccountManager :: set_user_block status > 2 : {$status}");
         return $resultFail;
         }*/

        $sql = <<<SQL
		UPDATE User_Regist SET status=2 ,unblock_date = :unblock_date, update_date=now(),unblock_reg_date =now() WHERE PlayerID =:PlayerID 		
		
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':unblock_date', $unblock_date, PDO::PARAM_STR);
        $db -> bindValue(':PlayerID', $PlayerID, PDO::PARAM_STR);
        $r = $db -> execute();
        if (!$r) {
            $this -> logger -> logError("DB : {$GLOBALS['AccessNo']}, PlayerID : {$PlayerID}, AccountManager :: set_user_block FAIL QUERY sql : {$sql}");
            return $resultFail;
        }
        $result['Protocol'] = 'ResBlockAccount';
        $result['ResultCode'] = 100;
        return $result;
    }

    public function set_user_unlimited_block($db, $PlayerID) {
        $resultFail['ResultCode'] = 300;

        $sql = <<<SQL
		UPDATE User_Regist SET status=5 , update_date=now(),unblock_reg_date =now() WHERE PlayerID =:PlayerID 		
		
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':PlayerID', $PlayerID, PDO::PARAM_STR);
        $r = $db -> execute();
        if (!$r) {
            $this -> logger -> logError("DB : {$GLOBALS['AccessNo']}, PlayerID : {$PlayerID}, AccountManager :: set_user_block FAIL QUERY sql : {$sql}");
            return $resultFail;
        }
        $result['ResultCode'] = 100;
        return $result;

    }

}
?>

