<?php
include_once './common/DB.php';
include_once './common/define.php';
require_once './lib/Logger.php';
include_once './classes/AssetManager.php';
include_once './classes/SendMailManager.php';

class AchieveManager {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function AchieveList($param) {
		$resultFail['Protocol'] = 'ResAchieveList';
		$resultFail['ResultCode'] = 200;

		$userId = $param['userId'];
		// achieve 테이블을 검사.
		$db = new DB();

		$sql = "select bestRank, bestTotalUserCount 
			FROM frdClimbTopData WHERE userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		$myRank = null;
		if ($row) {	
			$myRank = $row['bestRank'] / $row['bestTotalUserCount'] * 100;
		}

		$sql = "select achieveId, cnt  from frdAchieveInfo where userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$temp 	= null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$temp[] = $row;
		}	
		if ($temp == null) {
			$this->setAchiveInit($db, $userId);
			$sql = "select achieveId, cnt  from frdAchieveInfo where userId=:userId ";
			$db->prepare($sql);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db->execute();
			$temp 	= null;
			while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
				$temp[] = $row;
			}	
		}

		$achieve = null;
		foreach($temp as $tp) {
			$aData = null;
			$apc_key = 'AchieveData_'.$tp['achieveId'];
			$apc_data = apc_fetch($apc_key);
			if (!$apc_data) {
				$this->logger->logError(__FUNCTION__.' : SHIT APC NOT FOUND userId :'.$userId.' idx : '.$row['achieveId']);
				continue;
			}

			$aData['achieveId'] = $tp['achieveId'];

			if ($aData['achieveId'] > 1200 && $aData['achieveId'] < 1300 ){
				if ($tp['cnt'] == -1 ) {
					$aData['cnt'] = "-1";
				} else {
					if ($myRank  != null) {
						$aData['cnt'] = $myRank;
					}  else {
						if( $tp['cnt'] == 0 ) {
							$aData['cnt'] = "-1";
						}  else {
							$aData['cnt'] = $tp['cnt'];
						}
					}
				}
			} else {
				$aData['cnt'] = $tp['cnt'];
			}

			$aData['max'] = $apc_data[3];
			$aData['type'] = $apc_data[4];
			$aData['amount'] = $apc_data[5];

			$achieve[] = $aData;	
		}	
		$data['achieve'] = $achieve;
		$result['Protocol'] = 'ResAchieveList';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}

	function AchieveReceive($param){ 
		$resultFail['ResultCode'] = 300;

		$userId = $param['userId'];
		$achieveId = $param['achieveId'];

		$apc_key = 'AchieveData_'.$achieveId;
		$apc_data = apc_fetch($apc_key);
		if (!$apc_data) {
			$this->logger->logError(__FUNCTION__.' : SHIT APC NOT FOUND userId :'.$userId.' idx : '.$achieveId);
			return $resultFail;
		}

		$db = new DB();

		$sql = "select achieveTableId, achieveId, cnt  from frdAchieveInfo where userId=:userId AND achieveId = :achieveId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':achieveId', $achieveId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}	
		$cnt = $row['cnt'];

		$achieveTableId = $row['achieveTableId'];
		$max = $apc_data[3];

		$tempIdx = substr($row['achieveId'], 0, 2);


		if ($achieveId > 1200 &&  $achieveId < 1300 ) {
			$sql = "select bestRank, bestTotalUserCount 
				FROM frdClimbTopData WHERE userId=:userId ";
			$db->prepare($sql);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			$myRank = null;
			if ($row) {	
				$myRank = $row['bestRank'] / $row['bestTotalUserCount'] * 100;
				$cnt = $myRank;
			} else {
				$this->logger->logError(__FUNCTION__.' : ach1 userId :'.$userId.' achieveId : '.$achieveId. ' cnt :'.$cnt);
				$resultFail['ResultCode'] = 307;
				return $resultFail;

			}
		}


		if ( $cnt == -1 ) {
			$this->logger->logError(__FUNCTION__.' : ach2 userId :'.$userId.' achieveId : '.$achieveId. ' cnt :'.$cnt);
			$resultFail['ResultCode'] = 307;
			return $resultFail;
		}

		if ($cnt >= $max) {
			$rewardType[] = $apc_data[4];
			$rewardCount[] = $apc_data[5];
		} else {
			if ( $tempIdx == 12 || $tempIdx == 13 || $tempIdx == 15 ) {
				if ($cnt > $max) {
					$this->logger->logError(__FUNCTION__.' : FAIL 3 userId :'.$userId.' cnt : '.$cnt.' max'.$max.' achieveId : '. $achieveId);
					$resultFail['ResultCode'] = 307;
					return $resultFail;
				}
			} else {
				$this->logger->logError(__FUNCTION__.' : FAIL 4 userId :'.$userId.' cnt : '.$cnt.' max'.$max.' achieveId : '. $achieveId );
				$resultFail['ResultCode'] = 307;
				return $resultFail;
			}
		}

		switch($tempIdx) {
			case 11:	
				$defineMax = achieve_1100;
				break;
			case 12:	
				$defineMax = achieve_1200;
				break;
			case 13:	
				$defineMax = achieve_1300;
				break;
			case 14:	
				$defineMax = achieve_1400;
				break;
			case 15:	
				$defineMax = achieve_1500;
				break;
			case 16:	
				$defineMax = achieve_1600;
				break;
			case 17:	
				$defineMax = achieve_1700;
				break;
			case 18:	
				$defineMax = achieve_1800;
				break;
			case 19:	
				$defineMax = achieve_1900;
				break;



			case 21:	
				$defineMax = achieve_2100;
				break;
			case 22:	
				$defineMax = achieve_2200;
				break;
			case 23:	
				$defineMax = achieve_2300;
				break;
			case 24:	
				$defineMax = achieve_2400;
				break;
			case 25:	
				$defineMax = achieve_2500;
				break;
			case 26:	
				$defineMax = achieve_2600;
				break;

			case 31:	
				$defineMax = achieve_3100;
				break;
			case 32:	
				$defineMax = achieve_3200;
				break;
			case 33:	
				$defineMax = achieve_3300;
				break;
			case 34:	
				$defineMax = achieve_3400;
				break;
			case 35:	
				$defineMax = achieve_3500;
				break;
			case 36:	
				$defineMax = achieve_3600;
				break;

			case 41:	
				$defineMax = achieve_4100;
				break;
			case 42:	
				$defineMax = achieve_4200;
				break;
			case 43:	
				$defineMax = achieve_4300;
				break;
			case 44:	
				$defineMax = achieve_4400;
				break;
			case 45:	
				$defineMax = achieve_4500;
				break;
			case 46:	
				$defineMax = achieve_4600;
				break;

			default:
				break;
		}

		$apc_key = 'AchieveData_'.$achieveId;
		$apc_data = apc_fetch($apc_key);
		if (!$apc_data) {
			$this->logger->logError(__FUNCTION__.' : SHIT APC NOT FOUND userId :'.$userId.' idx : '.$row['achieveId']);
		}

		if ($achieveId >= $defineMax) {
			$resultCnt = -1; // 더이상 진행 불가 처리 
		} else {
			if ( $tempIdx == 11 
				|| $tempIdx == 12 
				|| $tempIdx == 13 
				|| $tempIdx == 15 
				|| $tempIdx == 16
				|| $tempIdx == 17
				|| $tempIdx == 18
				|| $tempIdx == 19 ) {
				$resultCnt = $cnt;
				$achieveId = $achieveId+1;
			} else  {
				$resultCnt = $cnt - $max;
				if ($cnt < $resultCnt) {
					$resultCnt = 0;
				}
				$achieveId = $achieveId+1;
			}
		}

		$aData = null;
		$aData['achieveId'] = $achieveId;
		$aData['cnt'] = $resultCnt;
		$aData['max'] = $apc_data[3];

		$aData['type'] = $apc_data[4];
		$aData['amount'] = $apc_data[5];

		$sql = "UPDATE frdAchieveInfo SET achieveId = :achieveId, cnt = :resultCnt
			where userId=:userId AND achieveTableId = :achieveTableId";
		$db->prepare($sql);
		$db->bindValue(':achieveId', $achieveId, PDO::PARAM_INT);
		$db->bindValue(':resultCnt', $resultCnt, PDO::PARAM_INT);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':achieveTableId', $achieveTableId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

		$SendMailManager = new SendMailManager();

		$SendResult = $SendMailManager->sendMail($db, $userId, 6,  $aData['type'], $aData['amount'] );
		if ($SendResult['ResultCode'] != 100 ) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' type : '.$aData['type']);
			$this->logger->logError(__FUNCTION__.' : FAIL rewardCounts :'.$aData['amount']);
		}

		$apc_key = 'AchieveData_'.$achieveId;
		$apc_data = apc_fetch($apc_key);
		if (!$apc_data) {
			$this->logger->logError(__FUNCTION__.' : SHIT APC NOT FOUND userId :'.$userId.' idx : '.$row['achieveId']);
		}

		if ($achieveId > 1200 &&  $achieveId < 1300 ) {
			$sql = "select bestRank, bestTotalUserCount 
				FROM frdClimbTopData WHERE userId=:userId ";
			$db->prepare($sql);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			$myRank = null;
			if ($row) {	
				$myRank = $row['bestRank'] / $row['bestTotalUserCount'] * 100;
			}
		}

		$aData = null;
		$aData['achieveId'] = $achieveId;
		if ($achieveId > 1200 &&  $achieveId < 1300 ) {
			if ($resultCnt == -1 ) {
				$aData['cnt'] = -1;
			} else {
				$aData['cnt'] = $myRank;
			}
		} else {
			$aData['cnt'] = $resultCnt;
		}
		$aData['max'] = $apc_data[3];

		$aData['type'] = $apc_data[4];
		$aData['amount'] = $apc_data[5];




		$achieve[] = $aData;	

		$result['Protocol'] = 'ResAchieveReceive';
		$result['ResultCode'] = 100;

		$data['achieve'] = $achieve;

		$result['Data'] = $data;
		return $result;
	}


	function addAchieveData($db, $userId, $achieveId, $amount) {
		$resultFail['ResultCode'] = 300;

		$apc_key = 'AchieveData_'.$achieveId;
		$apc_data = apc_fetch($apc_key);
		if (!$apc_data) {
			$this->logger->logError(__FUNCTION__.' : SHIT APC NOT FOUND userId :'.$userId.' idx : '.$achieveId);
			return $resultFail;
		}

		$sql = "select achieveId, cnt from frdAchieveInfo where userId=:userId AND achieveId = :achieveId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':achieveId', $achieveId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}	

		$cnt = $row['cnt'];
		$max = $apc_data[3];

		$tempIdx = substr($row['achieveId'], 0, 2);

		$defineMax = $this->getDefineMax($tempIdx);
		if ($defineMax == null){
			$this->logger->logError(__FUNCTION__.' : worng userId :'.$userId.' max : '.$defineMax);
			return $resultFail;
		}

		if ($row['achieveId'] >= $defineMax && $row['cnt'] == -1) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

		$resultCnt = $cnt + $amount;	
		$sql = "UPDATE frdAchieveInfo SET achieveId = :achieveId, cnt = :resultCnt, update_date=now()
			where userId=:userId AND achieveId = :achieveId";
		$db->prepare($sql);
		$db->bindValue(':resultCnt', $resultCnt, PDO::PARAM_INT);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':achieveId', $achieveId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

		$result['ResultCode'] = 100;
		return $result;
	}

	function updateAchieveData($db, $userId, $achieveId, $curValue) {
		$resultFail['ResultCode'] = 300;

		$apc_key = 'AchieveData_'.$achieveId;
		$apc_data = apc_fetch($apc_key);
		if (!$apc_data) {
			$this->logger->logError(__FUNCTION__.' : SHIT APC NOT FOUND userId :'.$userId.' idx : '.$achieveId);
			return $resultFail;
		}

		$sql = "select achieveId, cnt  from frdAchieveInfo where userId=:userId AND achieveId = :achieveId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':achieveId', $achieveId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}	

		$cnt = $row['cnt'];
		$max = $apc_data[3];

		$tempIdx = substr($row['achieveId'], 0, 2);

		$defineMax = $this->getDefineMax($tempIdx);
		if ($defineMax == null){
			$this->logger->logError(__FUNCTION__.' : worng userId :'.$userId.' max : '.$defineMax);
			return $resultFail;
		}

		if ($row['achieveId'] >= $defineMax && $row['cnt'] == -1) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' cnt : '.$row['cnt']);
			return $resultFail;
		}
		if ($curValue > $cnt ) {
			$sql = "UPDATE frdAchieveInfo SET achieveId = :achieveId, cnt = :resultCnt
				where userId=:userId AND achieveId = :achieveId";
			$db->prepare($sql);
			$db->bindValue(':resultCnt', $curValue, PDO::PARAM_INT);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db->bindValue(':achieveId', $achieveId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
				return $resultFail;
			}
		}

		$result['ResultCode'] = 100;
		return $result;
	}

	function getDefineMax($tempIdx) {
		switch($tempIdx) {
			case 11:	
				$defineMax = achieve_1100;
				break;
			case 12:	
				$defineMax = achieve_1200;
				break;
			case 13:	
				$defineMax = achieve_1300;
				break;
			case 14:	
				$defineMax = achieve_1400;
				break;
			case 15:	
				$defineMax = achieve_1500;
				break;
			case 16:	
				$defineMax = achieve_1600;
				break;
			case 17:	
				$defineMax = achieve_1700;
				break;
			case 18:	
				$defineMax = achieve_1800;
				break;
			case 19:	
				$defineMax = achieve_1900;
				break;


			case 21:	
				$defineMax = achieve_2100;
				break;
			case 22:	
				$defineMax = achieve_2200;
				break;
			case 23:	
				$defineMax = achieve_2300;
				break;
			case 24:	
				$defineMax = achieve_2400;
				break;
			case 25:	
				$defineMax = achieve_2500;
				break;
			case 26:	
				$defineMax = achieve_2600;
				break;

			case 31:	
				$defineMax = achieve_3100;
				break;
			case 32:	
				$defineMax = achieve_3200;
				break;
			case 33:	
				$defineMax = achieve_3300;
				break;
			case 34:	
				$defineMax = achieve_3400;
				break;
			case 35:	
				$defineMax = achieve_3500;
				break;
			case 36:	
				$defineMax = achieve_3600;
				break;

			case 41:	
				$defineMax = achieve_4100;
				break;
			case 42:	
				$defineMax = achieve_4200;
				break;
			case 43:	
				$defineMax = achieve_4300;
				break;
			case 44:	
				$defineMax = achieve_4400;
				break;
			case 45:	
				$defineMax = achieve_4500;
				break;
			case 46:	
				$defineMax = achieve_4600;
				break;
			default:
				$defineMax = null;
				break;
		}
		return $defineMax;
	}




	function setAchiveInit($db, $userId) {

		$sql = "SELECT userLevel, manaLevel, session FROM frdUserData WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logerror(__FUNCTION__.' : fail userId :'.$userId.' sql:'.$sql);
			return $resultFail;
		}  
		$userLevel = $row['userLevel'];
		$manaLevel = $row["manaLevel"];

		$myRank = 100;
		$sql = "select bestRank, bestTotalUserCount 
			FROM frdClimbTopData WHERE userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {	
			$myRank = $row['bestRank'] / $row['bestTotalUserCount'] * 100;
		}

		$godCnt = 0;
		$sql = "SELECT SUM(LEVEL-1) AS tCnt ,userId  FROM frdGoddess WHERE userId = :userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {	
			if ($row['tCnt'] > 0 ) {
				$godCnt = $row['tCnt'];
			}
		}

		$sql = "SELECT exp FROM frdCharInfo WHERE userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$aCharTemp   = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$aCharTemp[] = $row;
		}

		$brCnt = 0;
		$arCnt = 0;
		$srCnt = 0;

		foreach ($aCharTemp as $acr) {
			if ($acr['exp'] >= 30 ) {
				$brCnt++;
			}
			if ($acr['exp'] >= 100 ) {
				$arCnt++;
			}
			if ($acr['exp'] >= 254 ) {
				$srCnt++;
			}
		}

		$sql = "INSERT INTO frdAchieveInfo (userId, achieveId,cnt,  update_date) VALUES 
			(:userId, 1101,:userLevel, now()),
			(:userId, 1201,:myRank, now()),
			(:userId, 1301,0, now()),
			(:userId, 1501,:manaLevel, now()),
			(:userId, 1601,0, now()),
			(:userId, 1701,:brCnt, now()),
			(:userId, 1801,:arCnt, now()),
			(:userId, 1901,:srCnt, now()),

			(:userId, 2101,0, now()),
			(:userId, 2201,0, now()),
			(:userId, 2301,0, now()),
			(:userId, 2401,0, now()),
			(:userId, 2501,0, now()),
			(:userId, 2601,0, now()),

			(:userId, 3101,0, now()),
			(:userId, 3201,0, now()),
			(:userId, 3301,0, now()),
			(:userId, 3401,0, now()),
			(:userId, 3501,0, now()),
			(:userId, 3601,0, now()),

			(:userId, 4101,0, now()),
			(:userId, 4201,0, now()),
			(:userId, 4301,:godCnt, now()),
			(:userId, 4401,0, now()),
			(:userId, 4501,0, now()),
			(:userId, 4601,0, now())

			";
		$db -> prepare($sql);
		$db -> bindValue(':userLevel', $userLevel, PDO::PARAM_INT);
		$db -> bindValue(':myRank', $myRank, PDO::PARAM_INT);
		$db -> bindValue(':manaLevel', $manaLevel, PDO::PARAM_INT);
		$db -> bindValue(':brCnt', $brCnt, PDO::PARAM_INT);
		$db -> bindValue(':arCnt', $arCnt, PDO::PARAM_INT);
		$db -> bindValue(':srCnt', $srCnt, PDO::PARAM_INT);
		$db -> bindValue(':godCnt', $godCnt, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

		$result['ResultCode'] = 100;

		return $result;
	}
}

?>
