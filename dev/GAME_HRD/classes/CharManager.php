<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
require_once './classes/AssetManager.php';
require_once './classes/AddManager.php';
	

class CharManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

	function BuyCharacs($param) {
		$resultFail['ResultCode'] = 300;

		$userId = $param["userId"];
		$charId = $param["charId"];
		$payType = $param["payType"];
		$price = $param["price"];

		// temp code 
		$apc_key = 'ShopHero_'.$charId;
		$CharPrice = apc_fetch($apc_key);
		$tempType1 = $CharPrice[1];
		$tempType2 = $CharPrice[3];

		if ( $payType == $tempType1 ) {
			$tempPrice = $CharPrice[2];	
		} else if ( $payType == $tempType2 ) {
			$tempPrice = $CharPrice[4];	
		} else {
			$resultFail['ResultCode'] = 304;
			$this->logger->logError(__FUNCTION__.': TYPE NONE userId : '.$userId.", type : ".$payType);
			return $resultFail;
		}

		if ($price != $tempPrice) {
			$resultFail['ResultCode'] = 305;
			$this->logger->logError(__FUNCTION__.':  userId : '.$userId.", tempprice : ".$tempPrice);
			return $resultFail;
		}

		$AssetManager = new AssetManager();	

		$db = new DB();

		switch ($payType) {
			case 5000: // GOLD
				$AResult = $AssetManager->useGold($db, $userId, $price);
				if($AResult['ResultCode'] != 100) {
					$resultFail['ResultCode'] = 307;
					return $resultFail;
				}
				$gold = null;
				$gold['curGold'] = $AResult['gold'];
				$gold['addGold'] = 0 - $price;
				break;
			case 5004: // jewel
				$AResult = $AssetManager->useJewel($db, $userId, $price);
				if($AResult['ResultCode'] != 100) {
					$resultFail['ResultCode'] = 306;
					return $resultFail;
				}
				$jewel = null;
				$jewel['curJewel'] = $AResult['jewel'];
				$jewel['addJewel'] = 0 - $price;

				break;
			case 5006: // powder
				$AResult = $AssetManager->usePowder($db, $userId, $price);
				if($AResult['ResultCode'] != 100) {
					$resultFail['ResultCode'] = 308;
					return $resultFail;
				}
				$powder = null;
				$powder['curPowder'] = $AResult['powder'];
				$powder['addPowder'] = 0 - $price;
				break;
			default : 
				$resultFail['ResultCode'] = 304;
				return $resultFail;
		}

		if ($AResult['ResultCode'] != 100 ) {
			$resultFail['ResultCode'] = $AResult['ResultCode'];	
			return $resultFail;
		}

		$AddManager = new AddManager();
		$addResult = $AddManager->setAddHero($db, $userId, $charId, 10);
		if ($addResult['ResultCode'] != 100 ) {
			$resultFail['ResultCode'] = 300;
			return $resultFail;
		}
		
		$cTemp['charTableId'] = $addResult['charTableId'];
		$cTemp['charId'] = $addResult['charId'];
		$cTemp['exp'] = $addResult['curExp'];
		$data['rewards']['characs'][]  = $cTemp;

		if (isset($gold)) {
			$data['use']['gold']  = $gold;
		}
		if (isset($jewel)) {
			$data['use']['jewel']  = $jewel;
		}
		if (isset($powder)) {
			$data['use']['powder']  = $powder;
		}

		$result['Protocol'] = 'ResBuyCharacs';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;
		return $result;
	}
}
?>
