<?php
include_once './common/DB.php';
include_once './common/define.php';
require_once './lib/Logger.php';

class CheatManager {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function cheat($param){
		$resultFail['ResultCode'] = 300;

		$userId = $param['userId'];
		$type = $param['type'];

		if (isset($param['val'])) {
			$val = $param['val'];
		} else {
			$val = 0;
		}
		if (isset($param['amount'])) {
			$amount = $param['amount'];
		} else {
			$amount = 0;
		}

		$db = new DB();
		/*
		type : 1 아이템  
		type : 2 유물 
		type : 3 마법 
		type : 4 골드, 보석, 번개 99999
		type : 5 스테이지 치트 100		
		*/

		switch ($type) {
			case 1:
				$this->addItem($db, $userId, $val, $amount);
				break;
			case 2:
				$this->addWeapon($db, $userId, $val, $amount);
				break;
			case 3:
				$this->addMagic($db, $userId, $val);
				break;
			case 4:
				$this->updateAsset($db, $userId);
				break;
			case 5:
				$this->updateStage($db, $userId);
				break;
			case 6:
				$this->updateTuto($db, $userId,$val);
				break;
			default:
				break;
		}

		$result['ResultCode'] = 100;	
		return $result;
	}

	function updateAsset($db, $userId) {
		$sql = "
			UPDATE frdUserData SET gold = 999999, jewel = 99999, heart = 9999 WHERE userId = :userId
		";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.' : fail UPDATE userId :' . $userId );
			$this -> logger -> logError('sql : ' . $sql );
			return $resultFail;
		}

		return true;
	}
	
	function updateStage($db, $userId) {
		$sql = "
			UPDATE frdUserData SET tutoLevel = 4248591333, stageLevel = 100 WHERE userId = :userId
		";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.' : fail UPDATE userId :' . $userId );
			$this -> logger -> logError('sql : ' . $sql );
			return $resultFail;
		}

		return true;
	}

	function updateTuto($db, $userId,$val) {
		$sql = "
			UPDATE frdUserData SET tutoLevel = :tuto WHERE userId = :userId
		";
		$db -> prepare($sql);
		$db -> bindValue(':tuto', $val, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.' : fail UPDATE userId :' . $userId );
			$this -> logger -> logError('sql : ' . $sql );
			return $resultFail;
		}

		return true;
	}


	function addItem($db, $userId, $itemId, $cnt) {
		$sql = "SELECT itemTableId, itemCount 
				FROM frdHavingItems WHERE userId = :userId and itemId=:itemId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':itemId', $itemId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {

			$sql = "  INSERT INTO frdHavingItems (
				`userId`,
				`itemId`,
				itemCount,
				reg_date
					) VALUES (
						:userId, :itemId, :cnt, now()
						)";
			$db -> prepare($sql);
			$db -> bindValue(':userId',$userId, PDO::PARAM_INT);
			$db -> bindValue(':itemId',$itemId, PDO::PARAM_INT);
			$db -> bindValue(':cnt',$cnt, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('sql : ' . $sql. ' userId: ' .$userId );
				$this->logger->logError('sql : itemId: ' .$itemId );
				return $resultFail;
			}
			$itemTableId = $db->lastinsertID();
			$curCnt = 0;
		} else {
			$itemTableId = $row['itemTableId'];
			$curCnt = $row['itemCount'];
			$sql = "UPDATE frdHavingItems SET itemCount= itemCount + :cnt 
				WHERE itemTableId = :itemTableId AND userId = :userId";
			$db -> prepare($sql);
			$db -> bindValue(':cnt', $cnt, PDO::PARAM_INT);
			$db -> bindValue(':itemTableId', $itemTableId, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('selItem : fail UPDATE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}
		}

		$result['ResultCode'] = 100;
		$result['itemId'] = $itemId;
		$result['itemTableId'] = $itemTableId;
		$result['cnt'] = $curCnt + $cnt;
		return $result;
	}

	function addWeapon($db, $userId, $weaponId, $exp) {
		$resultFail['ResultCode'] = 300;
		if ($exp == null) {
			$exp = 0;
		}

		$apc_key = 'WeaponOption_'.$weaponId;
		$apc_data = apc_fetch($apc_key);
		if (!$apc_data) {
			$this->logger->logError(__FUNCTION__.' : APC Wrong userId : '.$userId.", weaponId : ".$weaponId);
			return $resultFail;
		}

		$statId0 = 0;
		$statVal0 = 0;
		$statId1 = 0;
		$statVal1 = 0;
		$statId2 = 0;
		$statVal2 = 0;
		$statId3 = 0;
		$statVal3 = 0;

		$idNo0 = (int)$apc_data[1];
		$idNo1 = (int)$apc_data[4];
		$idNo2 = (int)$apc_data[7];
		$idNo3 = (int)$apc_data[10];

		$No101Max = 14; // 0 ~ 16
		$No102Max = 14; // 0 ~ 16

		if ($idNo0 ) {
			if ($idNo0 == 101) {
				$statId0 = rand(0,$No101Max);
			} else if ($idNo0 == 102) {
				$statId0 = rand(0,$No102Max);
			} else {
				$statId0 = $idNo0;
			}
			$statVal0 = rand(0, 250);
		}

		if ($idNo1 ) {
			if ($idNo1 == 101) {
				$statId1 = rand(0,$No101Max);
			} else if ($idNo1 == 102) {
				$statId1 = rand(0,$No102Max);
			} else {
				$statId1 = $idNo1;
			}
			$statVal1 = rand(0, 250);
		}

		if ($idNo2 ) {
			if ($idNo2 == 101) {
				$statId2 = rand(0,$No101Max);
			} else if ($idNo2 == 102) {
				$statId2 = rand(0,$No102Max);
			} else {
				$statId2 = $idNo2;
			}
			$statVal2 = rand(0, 250);
		}

		if ($idNo3 ) {
			if ($idNo3 == 101) {
				$statId3 = rand(0,$No101Max);
			} else if ($idNo3 == 102) {
				$statId3 = rand(0,$No102Max);
			} else {
				$statId3 = $idNo3;
			}
			$statVal3 = rand(0, 250);
		}

		$sql = "
			INSERT  INTO frdHavingWeapons
			( userId, weaponId, exp, 
			  statId0, statId1, statId2, statId3, 
			  statVal0, statVal1, statVal2,statVal3, reg_date )
			VALUES (:userId, :weaponId, :exp, 
					:statId0,:statId1,:statId2,:statId3,
					:statVal0,:statVal1,:statVal2,:statVal3, now()
				   )
			";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':weaponId', $weaponId, PDO::PARAM_INT);
		$db -> bindValue(':exp', $exp, PDO::PARAM_INT);
		$db -> bindValue(':statId0', $statId0, PDO::PARAM_INT);
		$db -> bindValue(':statId1', $statId1, PDO::PARAM_INT);
		$db -> bindValue(':statId2', $statId2, PDO::PARAM_INT);
		$db -> bindValue(':statId3', $statId3, PDO::PARAM_INT);
		$db -> bindValue(':statVal0', $statVal0, PDO::PARAM_INT);
		$db -> bindValue(':statVal1', $statVal1, PDO::PARAM_INT);
		$db -> bindValue(':statVal2', $statVal2, PDO::PARAM_INT);
		$db -> bindValue(':statVal3', $statVal3, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.' : FAIL userId : '.$userId.", sql : ".$sql);
			$this -> logger -> logError(__FUNCTION__.' :  weaponId : '.$weaponId);
			return $resultFail;
		}

		$weaponTableId = $db->lastInsertId();	

		$result['ResultCode'] = 100;
		$result['weaponId'] = $weaponId;
		$result['weaponTableId'] = $weaponTableId;
		$statIds = null;
		$statVals = null;
		$statIds[]  = $statId0;
		$statVals[] = $statVal0;
		$statIds[]  = $statId1;
		$statVals[] = $statVal1;
		$statIds[]  = $statId2;
		$statVals[] = $statVal2;
		$statIds[]  = $statId3;
		$statVals[] = $statVal3;

		$result['statIds']    = $statIds;
		$result['statVals']   = $statVals;

		$result['exp'] = 0;

		return $result;
	}

	function addMagic($db, $userId, $magicId) {
		$resultFail['ResultCode'] = 300;
		$sql =" 
			INSERT  INTO frdHavingMagics
			( userId, magicId, level,exp, reg_date )
			VALUES (:userId, :magicId, 0, 0, now()) ";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':magicId', $magicId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.' : FAIL userId : '.$userId.", sql : ".$sql);
			return $resultFail;
		}

		$magicTableId = $db->lastinsertID();


		$result['ResultCode'] = 100;
		$result['magicId'] = $magicId;
		$result['magicTableId'] = $magicTableId;

		return $result;	
	}

	function addWeaponExp($db, $userId, $weaponIdxs, $resultExp, $type) {
		$resultFail['ResultCode'] = 300;

		// 기획의도 10% 고정으로 적용
		$weaponExp = (int)($resultExp/10);
	
		$wLen = count($weaponIdxs);
		$sql_str = null;

		$temp = null;
		if (@$isSub) {
			$weaponExp = round($weaponExp*0.3);
		}

		for($i =0; $i < $wLen; $i++) {
			if($sql_str == null) {
				$sql_str = $weaponIdxs[$i];
			} else {
				$sql_str = $sql_str.','.$weaponIdxs[$i];
			}				
		}
		$sql = "SELECT weaponTableId, weaponId, level, exp 
			FROM frdHavingWeapons WHERE weaponTableId IN ($sql_str) AND userId = :userId ";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$resultList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$resultList[] = $row;
		}

		if ($wLen != count($resultList)) {
			$this->logger->logError(__FUNCTION__.': userId :'.$userId." not Match ");
			$resultFail['ResultCode'] = 300;
		}

		if ($type) {
			$sql = "select val from frdEffectForEtc where userId = :userId and type=1032";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				$val = 1;
			} else {
				$val = $row['val'];
			}
		} else {
			$val = 1;
		}

		for($i =0; $i < count($resultList); $i++) {

			$exp =  null;
			//Level,WeaponPrice,WeaponNeedExp

			$wi = $resultList[$i];
			$apc_key = "WeaponPrice_Exp_".$wi['level'];
			$apc_data = apc_fetch($apc_key);
			$curLevel = $wi['level'];
			$curMyExp = $wi['exp']+ $weaponExp;

			$curMaxExp = $apc_data[2];
			$myMaxExp = $curMaxExp;
			$preLevel = $curLevel;

			if ($curLevel > 49 ) {
				$exp[] = $wi['weaponTableId'];
				$exp[] = $weaponExp;
				$exp[] = $myMaxExp;
				$exp[] = $curLevel;
				$exp[] = $preLevel;
				$temp[] = $exp;

				continue;
			}

			$resultExp = $curMyExp;

			if ( $curMyExp >= $curMaxExp ) {
				$next_key = $wi['level'] + 1;
				$apc_key = "WeaponPrice_Exp_".$next_key;
				$apc_data = apc_fetch($apc_key);
				$nextMaxExp = $apc_data[2];

				$myMaxExp = $nextMaxExp;

				$resultExp = $curMyExp - $curMaxExp;

				
				if ($resultExp > $nextMaxExp ) {
					for ($j = 0; $j < 40; $j++) {
						$next_key = $next_key+1;
						$resultExp = $resultExp - $nextMaxExp;
						$next_apc_key = "WeaponPrice_Exp_".$next_key;
						$next_lv_data = apc_fetch($next_apc_key);
						$nextMaxExp = $next_lv_data[2];

						$myMaxExp = $nextMaxExp;
						if ($resultExp < $nextMaxExp ) {
							break;
						}
					}
				}

				$curLevel = $next_key;
				$myMaxExp = $nextMaxExp;
			}

			if ( $curLevel != $preLevel) {
				$grade = $this->GetWeaponGrade((int)$wi["weaponId"]);
				$apc_key = "WeaponInform_WeaponMexLevel";
				$max_lv_data = apc_fetch($apc_key);
				$max_lv_data = explode("/",$max_lv_data[1]);	
				if (count($max_lv_data)) {
					$limitLV = $max_lv_data[$grade];
					if ($curLevel > $limitLV) {
						$curLevel = $limitLV;
					}
				}

				$sql = "UPDATE frdHavingWeapons SET 
					exp = :weaponExp , level = :level, update_date = now()
					WHERE weaponTableId = :pId and userId=:userId";
				$db -> prepare($sql);
				$db -> bindValue(':weaponExp', $resultExp, PDO::PARAM_INT);
				$db -> bindValue(':level', $curLevel, PDO::PARAM_INT);
				$db -> bindValue(':pId', $wi['weaponTableId'], PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);

			} else {
				$sql = "UPDATE frdHavingWeapons SET 
					exp = exp + :weaponExp, update_date = now()
					WHERE weaponTableId = :pId and userId=:userId";
				$db -> prepare($sql);
				$db -> bindValue(':weaponExp', $weaponExp, PDO::PARAM_INT);
				$db -> bindValue(':pId', $wi['weaponTableId'], PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			}
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('addWeaponExp : FAIL userId : ' . $userId . ", sql : " . $sql);
				return $resultFail;
			}
			$exp = null;
			$exp[] = $wi['weaponTableId'];
			$exp[] = $weaponExp;
			$exp[] = $resultExp;
			$exp[] = $curLevel;
			$exp[] = $preLevel;
			$temp[] = $exp;
		}
		$result['exp'] = $temp;
		$result['ResultCode'] = 100;
		return $result;
	}

	function setAddHero($db, $userId, $charId, $amount){
		$resultFail['ResultCode'] = 300;
		$sql = "
		SELECT charTableId, exp FROM frdCharInfo 
		WHERE userId=:userId and charId=:charId";
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':charId', $charId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$sql = "
				INSERT INTO frdCharInfo 
				(userId, charId, exp, reg_date)
 				 VALUES( :userId, :charId, 10, now()) ";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':charId', $charId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}
			$charTableId =  $db -> lastInsertId();
			$exp = 10;
			$amount = 10;
			$myExp = 10;
		} else {
			$charTableId = $row['charTableId'];
			$exp = $row['exp'];
			if ($amount == null ) {
				// shop chest only
				$amount = 5;
			}
			$sql = "
                UPDATE frdCharInfo SET exp = exp + :amount, update_date = now()
                WHERE charTableId = :charTableId and userId = :userId  ";
			$db -> prepare($sql);
			$db -> bindValue(':amount', $amount, PDO::PARAM_INT);
			$db -> bindValue(':charTableId', $charTableId, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}
			$myExp = $exp + $amount;
			$exp = $exp + $amount;
		}
		// 랭크 업이 되면 검사를 해야 한다.
		// C 랭크는 검사 하지 않는다.

		// 이전 랭크가 랭크 업 전이고
		// 현재 랭크가 랭크업 상태다 이때 업적 검사를 한다.
		// 랭크 업 단계는 
		// 10 : C
		// 30 : B
		// 100 : A
		// 254 : S
		// 해당 내용은 누적이다. 

		$achieveTemp  = array(10,30,100,254);

		// 일단 b랭크 이상을 조건으로 시작 
		// 신규 케릭터는 타지 않게 된다. 
		// 업적 체크 시작 ---> 
		/*
		if ( $exp >= 30) {
			//이전 값을 재 계산. 
			$aVal = $exp - $amount;
			// exp 는 현재 값

			// 랭크 변화가 있는지 검사 한다.
			for($ai=0; $ai < count($achieveTemp)-1; $ai++) {
				if ($aVal >= $achieveTemp[$ai] && $achieveTemp[$ai+1] > $aVal) {
					$preRank = $ai;
				}
				if ($exp >= $achieveTemp[$ai] && $achieveTemp[$ai+1] > $exp) {
					$curRank = $ai;
				}
			}
		
			// 랭크 변화가 감지 되었다!
			if ($preRank != $curRank) {
				// 내 업적 상태값이 현재 어떤지 가져 옴	
				$sql = "select achieveId, cnt from frdAchieveInfo where userId=:userId and achieveId > 1400 and achieveId < 1500";
				$db->prepare($sql);
				$db->bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				if (!$row) {
					$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
					return $reaulFail;
				}

				$achieveId = $row['achieveId'];
				$achieveCnt = $row['cnt'];

				$achieveValues  = array(10,30,100,254);
				
				$apc_key = 'AchieveData_'.$achieveId;
				$apc_data = apc_fetch($apc_key);
				if (!$apc_data) {
					$this->logger->logError(__FUNCTION__.' : SHIT APC NOT FOUND userId :'.$userId.' idx : '.$row['achieveId']);
					continue;
				}

				$sql = " SELECT exp FROM frdCharInfo WHERE userId=:userId ";
				$db->prepare($sql);
				$db->bindValue(':userId', $userId, PDO::PARAM_INT);
				$db->execute();
				$aCharTemp   = null;
				while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
					$aCharTemp[] = $row;
				}
				$brCnt = 0;
				$arCnt = 0;
				$srCnt = 0;
				//	$achieveTemp  = array(10,30,100,254);
				foreach ($aCharTemp as $acr) {
					if ($acr['exp'] > 30 ) {
						$brCnt++;
						$arCnt++;
						$srCnt++;
						continue;
					}
					if ($acr['exp'] > 100 ) {
						$arCnt++;
						$srCnt++;
						continue;
					}
					if ($acr['exp'] > 254 ) {
						$srCnt++;
						continue;
					}
				}

				// 랭크 변화 감지 한 구간 
				//if ($preRank != $curRank) {
				// 아직 여기를 타는 중임
				switch($achieveId) {
					case 1401:
						$tempRankCnt = $brCnt;	
						$maxRankCnt = 10;
						break;
					case 1402:
						$tempRankCnt = $brCnt;	
						$maxRankCnt = 50;
						break;
					case 1403:
						$tempRankCnt = $brCnt;	
						$maxRankCnt = 100;
						break;
					case 1404:
						$tempRankCnt = $arCnt;	
						$maxRankCnt = 10;
						break;
					case 1405:
						$tempRankCnt = $arCnt;	
						$maxRankCnt = 50;
						break;
					case 1406:
						$tempRankCnt = $arCnt;	
						$maxRankCnt = 100;
						break;
					case 1407:
						$tempRankCnt = $srCnt;	
						$maxRankCnt = 10;
						break;
					case 1408:
						$tempRankCnt = $srCnt;	
						$maxRankCnt = 50;
						break;
					case 1409:
						$tempRankCnt = $srCnt;	
						$maxRankCnt = 100;
						break;
					default:
						break;
				}

				if ($tempRankCnt >= $maxRankCnt) {
					// update
					// achieveManager 소환 
					$AchieveManager = new AchieveManager();
					$cnt = $tempRankCnt - $achieveCnt;
					$AchieveManager->addAchieveData($db, $userId, $achieveId, $cnt);
				}
			}
		}
		// < --- 업적 체크 완료 
		*/

		$data = null;
		$result['charTableId'] = $charTableId;
		$result['charId'] = $charId;
		$result['curExp'] = $myExp;
		$result['addExp'] = $amount;

		$result['ResultCode'] = 100;
		$result['Data'] = $data;

        return $result;
    }
	function GetWeaponGrade($privateId) {
		switch($privateId) {
			case 498:
				return 1;
			case 497:
				return 2;
			case 496:
				return 3;
			case 495:
				return 4;
			default:
				if ( $privateId < 10 )
					return 1;
				else if ( $privateId < 50 )
					return 2;
				else if ( $privateId < 100 )
					return 3;
				else if ( $privateId < 200 )
					return 4;
				else if ( $privateId < 300 )
					return 5;
				else
					return 6;
		}
	}



}
?>
