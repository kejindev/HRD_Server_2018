<?php
include_once './common/DB.php';
include_once './common/RedisDB.php';
require_once './lib/Logger.php';
require_once './common/define.php';
require_once './classes/AssetManager.php';


require_once './classes/WeaponManager.php';
require_once "./classes/EquipWeaponEffect.php";

require_once './classes/WebviewManager.php';
require_once './send_data/SendDataManager.php';
require_once "./classes/GetRewardManager.php";

class ClimbManager {
	public function __construct() {
		$this -> logger = Logger::get();
		//$this->
	}
		
	function getRedisName() {
		switch($GLOBALS['Type']) {
			case 'OS':
			case 'AS':
				$climbName = "climbRank_2";
				break;
			case 'PS':
				$climbName = "climbRank_New";
				break;
			case 'YK':
				$climbName = "climbRank_YK";
				break;
			default:
				$climbName = "climbRank_Error";
				break;
		}
	
		return $climbName;	
	}

	function GetClimeTopInfo($db, $userId, $name) {
		$resultFail['ResultCode'] = 300;

		// GET REDIS DATA;
		$rankRedis = new RedisServer();
		$result = null;

		$climbName = $this->getRedisName();
		$result["myRank"] = $rankRedis->zRevRank($climbName, $name);
		$result["userCount"] = $rankRedis->zCard($climbName);

		if ( !$result["myRank"] ) {
			$result["myRank"] = 0;
		} 

		$sql = "
			SELECT * FROM frdClimbTopData WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$result["climbAccuData"]["bestRank"] 			= 0;
			$result["climbAccuData"]["bestTotalUserCount"] 	= 0;
			$result["climbAccuData"]["playCount"] 			= 0;
			$result["climbAccuData"]["averageRank"] 		= 0;
			$result["climbAccuData"]["avrgTotalUserCount"] 	= 0;
			$result["climbAccuData"]["lastRank"] 			= 0;
			$result["climbAccuData"]["lastTotalUserCount"] 	= 0;
		} else {
			$result["climbAccuData"]["bestRank"] 			= $row["bestRank"];
			$result["climbAccuData"]["bestTotalUserCount"] 	= $row["bestTotalUserCount"];
			$result["climbAccuData"]["playCount"] 			= $row["playCount"];
			$result["climbAccuData"]["averageRank"] 		= $row["averageRank"];
			$result["climbAccuData"]["avrgTotalUserCount"] 	= $row["avrgTotalUserCount"];
			$result["climbAccuData"]["lastRank"] 			= $row["lastRank"];
			$result["climbAccuData"]["lastTotalUserCount"] 	= $row["lastTotalUserCount"];
		}

		$result['ResultCode'] = 100;
		return $result;
	}

	public function getClimbRewardList($param) {
		$key_arr = array( "10","100","500","1000","2000","3000","4000","5000","6000","7000","8000","9000","9999");
		foreach($key_arr as $ka) {	
			$apc_key = 'ClimbRankReward_'.$ka;
			$apc_data = apc_fetch($apc_key);
			$temp = null;
			$temp = (int)$apc_data[0];
			if ($apc_data[1] != null) {
				$temp .= ','.(int)$apc_data[1];
			}

			if ($apc_data[2] != null) {
				$temp .= ','.(int)$apc_data[2];
			}

			if ($apc_data[3] != null) {
				$temp .= ','.(int)$apc_data[3];
			}

			if ($apc_data[4] != null) {
				$temp .= ','.(int)$apc_data[4];
			}

			if ($apc_data[5] != null) {
				$temp .= ','.(int)$apc_data[5];
			}

			if ($apc_data[6] != null) {
				$temp .= ','.(int)$apc_data[6];
			}

			$data[] = $temp;
		}

		$Data['climbList'] = $data;

		$result['Protocol'] = 'ResGetClimbRewardList';
		$result['ResultCode'] = 100;

		$result['Data'] = $Data;

		return $result;
	}

	function setLastPlayLog($db, $userId, $lastWave, $seconds, $stageId) {
		$resultFail['ResultCode'] = 300;
		// Log ~
		$sql = "select userId from frdLogPlay where userId = :userId and stageId=0";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!isset($row) || is_null($row) || $row == 0) { 
			$sql = "INSERT into frdLogPlay values (:userId, 0, :lastWave , :seconds, 1, 0,now(),now())";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':lastWave', $lastWave, PDO::PARAM_INT);
			$db -> bindValue(':seconds', $seconds, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) { 
				$this->logger->logError(__FUNCTION__.':  FAIL userId : '.$userId. ' sql : '.$sql);
				return $resultFail;
			}
		} else {
			$sql = "UPDATE frdLogPlay 
				SET totalWave=totalWave+:lastWave, totalSecs=totalSecs+:seconds, playCount=playCount+1 
				WHERE userId=:userId AND stageId=0";
			$db -> prepare($sql);
			$db -> bindValue(':lastWave', $lastWave, PDO::PARAM_INT);
			$db -> bindValue(':seconds', $seconds, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) { 
				$this->logger->logError(__FUNCTION__.':  FAIL userId : '.$userId. ' sql : '.$sql);
				return $resultFail;
			}
		}

		$result['ResultCode'] = 100;
		return  $result;
	}

	function IsSuccess($conditionType, $lastWave, $std, $seconds, $isClear) {

		switch($conditionType) {
			case 0:
				$value = (int)$seconds;
				if ( $std < $value || !$isClear )
					return false;
				else
					return true;

			case 4:
				if ( $lastWave >= $std )
					return true;
				else
					return false;

			case 5:
				if ( $isClear > 0 )
					return true;
				else
					return false;

			case 6:
				return true;
			default:
				return false;
		}
	}

	public function ClimbReward($param) {
		$resultFail['Protocol'] = 'ResClimbReward';
		$resultFail['ResultCode'] = 300;

		$userId = $param['userId'];
		$isClear = $param['isClear'];
		$stageId = $param['stageId'];
		$lastWave = $param['lastWave'];
		$seconds = $param['seconds'];
		$lastFlowTime = $param['lastFlowTime'];

		// 추가된 파라미터
//		$isHack = $param["isHack"];
		$mainSkillType = $param["mainSkillType"];
		$getSoul = $param["getSoul"];
		$getDarkSoul = $param["getDarkSoul"];
		$useSoul = $param["useSoul"];
		$useDarkSoul = $param["useDarkSoul"];

		$resultRewardCount = null;
		$resultRewardType = null;
		$db = new DB();

		$sql = "SELECT timestampdiff(second, enter_time, now()) as lastTime, status
			FROM frdLastClimbTime WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) { 
			// climb_staus description	
			// 0 : 입장전 1: 입장후 2: 보상완료
			// 보상은 1 인 상태에만 받을 수 있다. 
			// 이 프로토콜 마지막에는 2 : 보상완료로 변경하여야 한다.
			if ( $lastWave > 100 ) {  //check hack
				$sql = "UPDATE frdLastClimbTime 
					SET status = 2, enter_time=now(), update_date=now() 
					WHERE userId=:userId";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) { 
					$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId. ' sql : '.$sql);
					return $resultFail;
				}
			}
			// 체크 입장 상태 
			/* 임시 체크 해제 
			if ($row['status'] != 1) {
				$this->logger->logError(__FUNCTION__.' : FAIL status userId : '.$userId. ' status : '.$row['status']); 
				return $resultFail;
			}

			if ($row['lastTime'] < 300 )  {
				$this->logger->logError(__FUNCTION__.' : lastTime Error userID : ' . $userId. ' lastTime : ' . $lastTime);
				return $resultFail;
			}
			*/
		} else {
			$sql = "
				INSERT INTO frdLastClimbTime (userId, status, update_date) 
				VALUES ( :userId, 2, now())";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) { 
				$this->logger->logError(__FUNCTION__.':  FAIL userId : '.$userId. ' sql : '.$sql);
				return $resultFail;
			}
		}

		// 마지막 플레이를 업데이트 한다.
		// error check 를 하지 않아 버린다.
		$this->setLastPlayLog($db, $userId, $lastWave, $seconds, $stageId);

		$sql = "SELECT name, exp, heart, ticket, userLevel
				FROM frdUserData WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!isset($row) || is_null($row) || $row == 0) { 
			$this->logger->logError(__FUNCTION__.':  FAIL userId : '.$userId. ' sql : '.$sql);
			return $resultFail;
		}

		$userLevel = $row['userLevel'];
		$exp = $row['exp'];
		$userLevel = $row['userLevel'];

		//Heart Check;
		$AssetManager = new AssetManager();	
		$AssetResult = $AssetManager->checkHeart($db, $userId);
		if ($AssetResult['ResultCode'] != 100 ) {
			$resultFail['ResultCode'] = $AssetResult['ResultCode'];
			return $resultFail;
		}


		#### REDIS SET  RANK 
		$RedisServer = new RedisServer();
		$userName = $row["name"];

		$climbName = $this->getRedisName();

		$preTotalScore = (int)$RedisServer->zScore($climbName, $userName);
		$totalSkipTime = (int)$param["climbTopData0"];
		if ( (int)$lastWave > 300 ) {
			if ( $isClear > 0 )
				$lastWave++;
			$totalSkipTime -= $lastFlowTime;
		} else {
			$totalSkipTime += $lastFlowTime;
		}

		$bonusPercent = 0; // totalSkipTime, compose, kill, destroyStone, quest
		for ( $i=0; $i<5; $i++ ) {
			$val = (int)$param["climbTopData".$i];
			$climbTopData[] = (int)$val;
			if ( $i !== 0 )
				$bonusPercent += $val;
		}
		
		$type = ABILL_BonusScore_ClimbTop;
		$abilBonusPercent = 0;
		$sql = "SELECT val FROM frdEffectForEtc WHERE userId = :userId and type=:type";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':type', $type, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) { 
			$abilBonusPercent = round($row["val"]/10);
			$bonusPercent += (int)$abilBonusPercent;
		}

//		$apc_key = "UserExp_MaxHeart_".$userLevel;
		$levelPercent = (int)($userLevel*100);
		if ( $bonusPercent > 5000 ) {
			$bonusPercent = 5000;
		}

		$waveScore = 1000000*$lastWave;
		if ( $lastWave < 1 ) {
			$lastWave = 1;
		}

		$timeScore = round($totalSkipTime * $totalSkipTime / $lastWave);
		$bonusScore = round($timeScore * ($levelPercent + $bonusPercent)*0.0001);
		$totalScore = $waveScore + $timeScore + $bonusScore;
		//  $rewardScore = $lastFlowTime*150;
		$difficulty = null;

        $EquipWeaponEffect = new EquipWeaponEffect();
		$Weapon_Main_Sub = $EquipWeaponEffect->getEquipWeaponForEffect($db,$userId);
		$weaponIdxs = null;
		$WeaponResult = $Weapon_Main_Sub['main'];
		for ($weaponI=0; $weaponI < 8; $weaponI++) {
			$WR = $WeaponResult[$weaponI];	
			if (!isset($WR)) {
				$wTemp[] = -1;
				$wTemp[] = -1;
				$wTemp[] = -1;
				$wTemp[] = -1;
				$wTemp[] = -1;
				$wTemp[] = -1;
				$wTemp[] = -1;
				$wTemp[] = -1;
				$wTemp[] = -1;
			} else {
				$wTemp[] = (int)$WR["weaponId"];
				$weaponIdxs[] = (int)$WR["weaponTableId"];
				$wTemp[] = (int)$WR["statId0"];
				$wTemp[] = (int)$WR["statId1"];
				$wTemp[] = (int)$WR["statId2"];
				$wTemp[] = (int)$WR["statId3"];
				if ( $WR["weaponId"] >= 300 && $WR["weaponId"] < 390 ){		//6성유물 옵션 5개
					$wTemp[] = (int)$WR["statId4"] + (int)($WR["transcend"]*1000);
				//	$data['test'][] = (int)$WR["statId4"] + (int)($WR["transcend"]*1000);
				//	$data['test'][] = (int)$WR["statId4"];
				//	$data['test'][] = (int)($WR["transcend"]*1000);
				}
				$wTemp[] = (int)$WR["statVal0"];
				$wTemp[] = (int)$WR["statVal1"];
				$wTemp[] = (int)$WR["statVal2"];
				$wTemp[] = (int)$WR["statVal3"];
				if ( $WR["weaponId"] >= 300 && $WR["weaponId"] < 390 )		//6성유물 옵션 5개
					$wTemp[] = (int)$WR["statVal4"];
			}
		}
		$subWeaponIdxs = null;
		$subResult = $Weapon_Main_Sub['sub'];
		if ($subResult) {
			foreach($subResult as $SR) {
				$subWeaponIdxs[] = (int)$SR["weaponTableId"];
			}
		}

		// 최근 스코어가 높을때만 업데이트 한다.
		if ( $totalScore > $preTotalScore ) {
//			$climbSpecDatas[] = $exp;
			$climbSpecDatas[] = (int)$userLevel;
			for ( $i=0; $i<4; $i++ ) {
				$climbSpecDatas[] = @(int)$param["characId".$i];
				$climbSpecDatas[] = @(int)$param["characRank".$i];
			}
		
			foreach($wTemp as $wt) {
				$climbSpecDatas[] = $wt;
			}	

			$climbScoreDatas = null;
			$climbScoreDatas[] = (int)$lastFlowTime; //25 ~ 33, 34
			$climbScoreDatas[] = (int)$lastWave;
			$climbScoreDatas[] = (int)$seconds;

			for ( $i=0; $i<5; $i++ ) {
				$climbScoreDatas[] = $climbTopData[$i];
			}

			$climbScoreDatas[] = (int)$levelPercent;
			if ( !is_null($abilBonusPercent) ) {
				$climbScoreDatas[] = (int)$abilBonusPercent;
			}

			$time = time() + 9*3600 - 24*3600*4-5*3600;
			$totalWeeks = (int)($time / 604800);
			$weekVal = (int)600 + ((int)($totalWeeks%5));

			$climbName = $this->getRedisName();

			if ( (int)$weekVal == (int)$stageId ) {

				$rr = $RedisServer->zAdd($climbName, $totalScore, $userName);
				$RedisServer->zAdd($climbName.'_Resource', $totalScore, ($mainSkillType.", ".$getSoul.", ".$useSoul.", ".$getDarkSoul.", ".$useDarkSoul));
				$RedisServer->hSet('climbSpecDatas', $userName, json_encode($climbSpecDatas));
				$RedisServer->hSet('climbScoreDatas', $userName, json_encode($climbScoreDatas));
				$RedisServer->hSet('climbUserId', $userName, $userId);
			}
		
		//	$test = $RedisServer->zRevRangeByScore($climbName,0, -1, null);

			$data['myRank']['rank'] = $RedisServer->zRevRank($climbName, $userName);
			$data['myRank']['totalUser'] = $RedisServer->zCard($climbName)-1;
			$data['myRank']["climbScoreDatas"] = $climbScoreDatas;
		} else {
			// 최근 스코어가 높지 않기 때문에 climbScoreDatas 에 저장만 한다.
			$climbScoreDatas[] = $lastFlowTime; //25 ~ 33, 34
			$climbScoreDatas[] = $lastWave;
			$climbScoreDatas[] = $seconds;
			for ( $i=0; $i<5; $i++ ) {
				$climbScoreDatas[] = (int)$climbTopData[$i];
			}
			$climbScoreDatas[] = $levelPercent;
			if ( !is_null($abilBonusPercent) ) {
				$climbScoreDatas[] = $abilBonusPercent;
			}
			$data["climbScoreDatas"] = $climbScoreDatas;

		}

        // ----------------- reward set start ----------------------
        if ( empty($result) )
			$result = array();

        $GetRewardManager = new GetRewardManager();
		$data['rewards'] = $GetRewardManager->GetReward(
			$result, $db, $userId, $userLevel, $stageId, $lastWave, $isClear, $seconds, null, 0, true, null, $weaponIdxs, $subWeaponIdxs
		);

		$this->achieveClimb($db, $userId);
		$this->eventClimb($db, $userId, $lastWave);

		$event_Key = EVENT_WEBVIEW;
		$apc_key =  "Event_".$GLOBALS['COUNTRY'].'_'.$event_Key;
		$eventType = apc_fetch($apc_key);
		$event_now = date('ymdH');
		if ($eventType) {
			$SendDataManager = new SendDataManager();
			$eventCheck = $SendDataManager->getEventData($event_Key+1, $eventType[1],$event_now);
			if ($eventCheck) {
				$WebviewManager = new WebviewManager();
				$WebviewManager->WebviewEventAddItem($db, $userId);
			}
		}

		$data['use']['heart']["curHeart"] = $AssetResult['heart'];// 현재 하트 개수 ;
		$data['use']['heart']["maxHeart"] = $AssetResult['maxHeart']; //하트 보유 최대치  ;
		$data['use']['heart']["passTime"] = $AssetResult['passTime'];//현재 시간 ;

		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResClimbReward';
		$result['Data'] = $data;

		return $result;
	}

	function eventClimb($db, $userId,$floor ) {
		$event_now = date('ymdH');
		$event_Key = EVENT_CLIMB_FLOOR;
		$apc_key =  "Event_".$GLOBALS['COUNTRY'].'_'.$event_Key;
		$eventType = apc_fetch($apc_key);
		if ($eventType) {
			$SendDataManager = new SendDataManager();
			$eventCheck = $SendDataManager->getEventData($event_Key+1, $eventType[1],$event_now);
			if (!$eventCheck) {
				return false;
			}
		} else {
			return false;
		}

		$eventGift = 0;
		$gift = "INSERT INTO frdUserPost
			(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
		$gift1 = "($userId, 0, 5004, 30, DATE_ADD(now(), INTERVAL 3 DAY ), now()) ";  
		$gift1 .= ",($userId, 0, 5002, 3, DATE_ADD(now(), INTERVAL 3 DAY ), now()) ";  
		$gift1 .= ",($userId, 0, 5005, 300, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";  
		$gift2 = "($userId, 0, 5004, 50, DATE_ADD(now(), INTERVAL 3 DAY ), now()) ";  
		$gift2 .= ",($userId, 0, 5002, 5, DATE_ADD(now(), INTERVAL 3 DAY ), now()) ";  
		$gift2 .= ",($userId, 0, 5005, 500, DATE_ADD(now(), INTERVAL 3 DAY ), now()) ";  
		$gift3 = "($userId, 0, 5004, 100, DATE_ADD(now(), INTERVAL 3 DAY ), now()) ";  
		$gift3 .= ",($userId, 0, 5002, 10, DATE_ADD(now(), INTERVAL 3 DAY ), now()) ";  
		$gift3 .= ",($userId, 0, 100000, 1, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";  

		if ( $floor > 99  && $floor < 200) {
			$eventGift = 1;
		} else if ( $floor > 199  && $floor < 300) {
			$eventGift = 2;
		} else if ( $floor > 299) {
			$eventGift = 3;
		} else {
			// no count
		} 

		$sql = "SELECT progress FROM frdClimeEvent where userId=:userId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$sql = "INSERT INTO frdClimeEvent (userId, progress, update_date) values (:userId, 0, now())";
			$db->prepare($sql);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) { 
				$this->logger->logError(__FUNCTION__.':  FAIL userId : '.$userId. ' sql : '.$sql);
				return false;
			}
			$row['progress'] = 0;
		}

		if ($row['progress'] == 3) {
			return false;
		}

		$this->logger->logError(__FUNCTION__.':  TEST userId : '.$userId. ' sql : '.$eventGift.' t : '.$row['progress']);

		if ($eventGift > $row['progress']) {
			switch ($row['progress']) {
				case 0 :
					switch($eventGift) {
						case 1 :
							$gift = $gift.$gift1;
							break;
						case 2 :
							$gift = $gift.$gift1.','.$gift2;
							break;
						case 3 :
							$gift = $gift.$gift1.','.$gift2.','.$gift3;
							break;
						default :
							break;
					}
					break;
				case 1 :
					switch($eventGift) {
						case 2 :
							$gift = $gift.$gift2;
							break;
						case 3 :
							$gift = $gift.$gift2.','.$gift3;
							break;
						default :
							break;
					}
					break;
				case 2 :
					switch($eventGift) {
						case 3 :
							$gift = $gift.$gift3;
							break;
						default :
							break;
					}
					break;
				case 3 :
					return false;
				default :
					return false;
			}
			$db -> prepare($gift);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : INSERT FAIL userId :'.$userId.' sql : '.$gift);
				return $resultFail;
			}
		}

		$sql = "UPDATE frdClimeEvent SET progress = :progress where userId=:userId";
		$db->prepare($sql);
		$db->bindValue(':progress', $eventGift, PDO::PARAM_INT);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) { 
			$this->logger->logError(__FUNCTION__.':  FAIL userId : '.$userId. ' sql : '.$sql);
			return false;
		}

		return true;
	}
	function achieveClimb($db, $userId ) {
		// 고난의 탑 관련 업적 1 2400 플레이 횟수 
		$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
			where userId=:userId and achieveId > 2400 and achieveId < 2500";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$achieveId = $row['achieveId'];

			if ($row['cnt'] != -1 ) {
				require_once './classes/AchieveManager.php';
				$AchieveManager = new AchieveManager();
				$AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
			}
		}

		return true;
	}

	/*
	function setClimeRewardArray($stageId, $lastWave, $isClear,$seconds) {

		// APC DATA GET 
		$apc_key = 'stageDataTotal_'.$stageId;
		$stageData = apc_fetch($apc_key);
		#0 StageId
		#1 TotalWave/MaxMon/monCount/intervalTimer
		#2 StartMamber
		#3 MakeChaPosition
		#4 NeedHeart
		#5 MonWay
		#6 ClearType
		#7 ClearTemp
		#8 RewardId
		#9 RewardAmount
		#10 RewardPercent
		#11 NeedClearIdx
		#12 GimicType
		#13 Gimic

		$clearType = $stageData[6];
		$clearConditionType = explode('/',$clearType);
		$length = count($clearConditionType);
		$clearTemp = $stageData[7];
		$clearConditionVal = explode('/',$clearTemp);

		$rewardIds = $stageData[8];
		$rewardId = explode('/',$rewardIds);

		$rewardCounts = $stageData[9];
		$rewardCount = explode('/',$rewardCounts);

		$rewardPSs = $stageData[10];
		$rewardPercent = explode('/',$rewardPSs);

		$NeedClearIdx = $stageData[11];
		$clearConditionIdx = explode('/',$NeedClearIdx);

		$isSuccess = null;
		for ( $i=0; $i<$length; $i++ ) {
			$isSuccess[$i] = $this->IsSuccess($clearConditionType[$i], $lastWave, $clearConditionVal[$i], $seconds, $isClear);
		}
	
		$length = count($rewardId);
		for ( $i=0; $i<$length; $i++ ) {
			$type = $rewardId[$i];
			if ( $type == 5005 ) {  //medal
				$rewardPercent[$i] += (int)($param["bonus3"]);
			}
		}

		########## REWARD SET ##############################
		// 방안 1
		// 증가 보상 구간에 같이 합산을 하는 로직을 넣던가.
		// 보상을 주는 구간에서 getBonus 에서 증가 합산 로직을 추가 한다.
		// getBonus 구간에서 모든 보상을 주게 하고
		// 이전에 클라에서 올려주던 bonus 123 을 서버에서 검증하여 미리 증가하게 해두는 걸로 변경한다.
		// 이건 스테이지보상에서도 동일하게 주게 변경한다.
		// 다만 그러하다면. getBonus의 로직내에서 
		// 각 스테이지 별로 또는 고탑과의 구분을 두어 증가 구간에 대한 로직을 변경 하여야한다

		$rewardItemIdxPointer=0;
		$lastRIPointer= $clearConditionIdx[0];
		$resultRewardType = null;
		$length = count($rewardId);
		for ( $i=0; $i<$length; $i++ ) {
			$conditionIdx = $clearConditionIdx[$i];
			if (!$isSuccess[$conditionIdx]) {
				continue;	
			}

			$tempSuc = rand(0,999);
			$tempDiff = $rewardPercent[$i];
			if ($tempSuc > $tempDiff) {
				continue;	
			}

			$rewardType = $rewardId[$i];
			$rewardCnt = $rewardCount[$i];
			if ( $rewardType > 500 && $rewardType < 507 ) { //random soulstone
				$key_name = $rewardType - 500;
				$apc_key = 'CharacIndex_'.$key_name;
				$charData = apc_fetch($apc_key);
				$charMin = $charData[0];
				$charMax = $charData[1];
				$rewardType = rand($charMin, $charMax);
			}
			else if ( $rewardType > 1500 && $rewardType < 1508 ) {  //random weapon
				$array_pos = $rewardType - 1500;
				$apc_key = 'WeaponInform_WeaponMinIdx';
				$weaponData = apc_fetch($apc_key);
				$minTemp = explode('/', $weaponData[1]);
				$weaponMin = $minTemp[$array_pos] + 1000;

				$apc_key = 'WeaponInform_WeaponMaxIdx';
				$weaponData = apc_fetch($apc_key);
				$maxTemp = explode('/', $weaponData[1]);
				$weaponMax = $maxTemp[$array_pos] + 1000;

				while ( $rewardCnt > 1 ) {
					$rewardType = rand($weaponMin, $weaponMax);
					$resultRewardType[] = $rewardType;
				
					$resultRewardCount[] = 1;
					$rewardCnt--;
				}
				$rewardType = rand($weaponMin, $weaponMax);
			}
			else if ( $rewardType > 10500 && $rewardType < 10508 ) {  //random magic
				$key_name = $rewardType - 10500;
				$apc_key = 'MagicPowderValue_'.$key_name;
				$magicData = apc_fetch($apc_key);
				$magicMin = $magicData[2] + 10000;
				$magicMax = $magicData[3] + 10000;
				if (!$magicMin) {
					$this->logger->logError(__FUNCTION__.':  error userId : '.$userId. ' rewardType : '.$rewardType);
					$this->logger->logError(__FUNCTION__.':  error userId : '.$userId. ' key_name : '.$key_name);
				}

				while ( $rewardCnt > 1 ) {
					$rewardType = rand($magicMin, $magicMax);
					$resultRewardType[] = $rewardType;
					$resultRewardCount[] = 1;
					$rewardCnt--;
				}
				$rewardType = rand($min, $max);
			}
			else if ( $rewardType == 5000 ) {    // gold
				if ( $conditionIdx == 0 ){
					$rewardCnt *= $lastWave;
				}
				$rewardCount = $rewardCount;
			}
			else if ( $rewardType == 5001 ) {    // exp
				if ( $conditionIdx == 0 ) {
					$rewardCnt *= $lastWave;
				}
				$rewardCount = $rewardCount;
			}
			else if ( $rewardType == 5005 ) {    // medal
			}
			if ( @in_array($rewardType, $resultRewardType)
			 && !($rewardType>=1000&&$rewardType<2000) 
			 && !($rewardType>=10000&&$rewardType<11000)) {
				$tempIdx = array_search($rewardType, $resultRewardType);
				$resultRewardCount[$tempIdx] += $rewardCnt;
			} else {
				$resultRewardType[] = $rewardType;
				$resultRewardCount[] = $rewardCnt;
			}
		}

		$result['resultRewardType'] = $resultRewardType;
		$result['resultRewardCount'] = $resultRewardCount;
		return $result;
	}
	*/

	function GetRankNew ($param) {
		if (isset($param['name'])) {
			$name = $param["name"];
		} else {
			$name = null;
		}
		$min = $param["min"];
		$max = $param["max"];

		$result['ResultCode'] = 100;

		$rankRedis = new RedisServer();
		
		$climbName = $this->getRedisName();
		$data = null;
		if ( $name !== null ) {
			if ( !($data['myRank']['rank'] = $rankRedis->zRevRank($climbName, $name)) ) {
				$result['Data'] = null;
				return $result;
			}
			else {
				if ( $min < 0) {
					$min = $data['myRank']['rank']-2;
					if ( $min < 1) {
						$min = 1;
					}
					$max = $min+5;
				}

				$data['myRank']['totalUser'] = $rankRedis->zCard($climbName)-1;

				$climbScoreDatas = json_decode($rankRedis->hGet('climbScoreDatas', $name));
				$data['myRank']['climbScoreDatas'] = $climbScoreDatas;
			}
		}

		$data['otherRanks']["names"] = null;
		$names = $rankRedis->zRevRange($climbName, $min, $max, null);
		if ($names) {
			$data['otherRanks']["names"] = $names;
		}

		$length = count($data['otherRanks']["names"]);
		$data['otherRanks']["score"] = null;
		$data['otherRanks']["climbScoreDatas"] = null;
		$data['otherRanks']["climbSpecDatas"] = null;
		for ( $i=0; $i<$length; $i++ ) {
			$name = $data['otherRanks']["names"][$i];

			$score = $rankRedis->zScore($climbName, $name);
			if ($score)
				$data['otherRanks']["score"][] = $score;

			$data['otherRanks']['climbScoreDatas'][] = json_decode($rankRedis->hGet('climbScoreDatas', $name));
			$data['otherRanks']['climbSpecDatas'][] = json_decode($rankRedis->hGet('climbSpecDatas', $name));
		}

		$result['Protocol'] = 'ResGetRankNew';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;
		return $result;

	}

	function SearchRank($param) {
		$name = $param["name"];

		$result['ResultCode'] = 100;

		$rankRedis = new RedisServer();

		$climbName = $this->getRedisName();

		$data = null;
		$targetRank = $rankRedis->zRevRank($climbName, $name);
		if ( !($targetRank = $rankRedis->zRevRank($climbName, $name)) ) {
			// not process
		} else {

			$min= $targetRank-2;
			if ( $min < 1)
				$min = 1;

			$data["min"] = $min;
			$data["names"] = $rankRedis->zRevrange($climbName, $min, $targetRank+2);

			$length = count($data["names"]);
			for ( $i=0; $i<$length; $i++ ) {
				$data["score"][] = $rankRedis->zScore($climbName, $data["names"][$i]);
				$data["climbScoreDatas"][] = json_decode($rankRedis->hGet('climbScoreDatas', $data["names"][$i]));
				$data["climbSpecDatas"][] = json_decode($rankRedis->hGet('climbSpecDatas', $data["names"][$i]));

			}

		}
		$result['Protocol'] = 'ResSearchRank';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;
		return $result;

	}

	function NewClimbData($param) {
		$userId = $param["userId"];

		$db = new DB();
		$sql = "
   select * from frdClimbTopData where userId = :userId";

		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!isset($row) || is_null($row) || $row == 0) { 
			$data["bestRank"] = 0;
			$data["bestTotalUserCount"] = 0;
			$data["playCount"] = 0;
			$data["averageRank"] = 0;
			$data["avrgTotalUserCount"] = 0;
			$data["lastRank"] = 0;
			$data["lastTotalUserCount"] = 0;
		} else {
			$data["bestRank"] = $row["bestRank"];
			$data["bestTotalUserCount"] = $row["bestTotalUserCount"];
			$data["playCount"] = $row["playCount"];
			$data["averageRank"] = $row["averageRank"];
			$data["avrgTotalUserCount"] = $row["avrgTotalUserCount"];
			$data["lastRank"] = $row["lastRank"];
			$data["lastTotalUserCount"] = $row["lastTotalUserCount"];
		}

		$result['Protocol'] = 'ResSearchRank';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;
		return $result;

	}


}
?>
