<?php
include_once './common/DB.php';
include_once './common/LOGIN_DB.php';
require_once './lib/Logger.php';

class CouponManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

	function Coupon($param) {
		$resultFail['ResultCode'] = 300;
		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResCoupon';

		$userId = $param["userId"];
		$coupon = $param["coupon"];

		// 쿠폰 체크 
		$login_db = new LOGIN_DB();
		$sql = <<<SQL
		SELECT * FROM frdCoupon 
		WHERE id LIKE '%$coupon%' 
SQL;
        $login_db -> prepare($sql);
        $login_db -> execute();
        $row = $login_db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.': not thing no userId : '.$userId.", sql : ".$sql);
			$data['error'] = 1;
			$result['Data'] = $data;
			return $result;
		} 
		if ($row['isGet'] > 0 ) {
			$this->logger->logError(__FUNCTION__.': already use  userId : '.$userId.", sql : ".$sql);
			$data['error'] = 2;
			$result['Data'] = $data;
			return $result;
		}

		$couponType = (int)$row["type"];

		if ( $couponType >= 100 ) {
			$sql = "SELECT type FROM frdUsedPublicCoupon where id = :userId AND type = :type";
			$login_db -> prepare($sql);
			$login_db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$login_db -> bindValue(':type', $couponType, PDO::PARAM_INT);
			$login_db -> execute();
			$row = $login_db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if ($row) {
				$this->logger->logError(__FUNCTION__.': already use  userId : '.$userId.", sql : ".$sql);
				$data['error'] = 2;
				$result['Data'] = $data;
				return $result;
			}
		} else {
			$sql = "select isGet from frdCoupon where getId = :userId and type= :type";
			$login_db -> prepare($sql);
			$login_db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$login_db -> bindValue(':type', $couponType, PDO::PARAM_INT);
			$login_db -> execute();
			$row = $login_db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if ($row) {
				$this->logger->logError(__FUNCTION__.': already use  userId : '.$userId.", sql : ".$sql);
				$data['error'] = 2;
				$result['Data'] = $data;
				return $result;
			}
		}

		$db = new DB();

		switch($couponType) {
			case 0:   // top ten
				$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 111, 10, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5004, 100, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5002, 10, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
				break;

			case 16:  //게임부킹 일반
				$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 10, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5000, 3000, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5003, 30, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
				break;

              case 17:  //모비 스페셜
             	 $sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 50, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5002, 10, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5000, 5000, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5003, 40, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
                break;

              case 18:  //모비&루팅 일반
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 10, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5000, 3000, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5003, 30, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
                break;

              case 19:  //루팅 D1
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 20, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5006, 100, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 100003, 1, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
                break;

              case 20:  //루팅 D2
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 20, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5002, 5, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 100003, 1, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
                break;

              case 21:  //루팅 D3
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 20, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5005, 50, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 100003, 1, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
                break;

              case 22:  //루팅 유니크1
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 80, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5005, 100, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5006, 200, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5003, 40, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
                break;

              case 23:  //루팅 유니크2
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 50, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5002, 10, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5000, 5000, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5003, 40, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
                break;

              case 24:  //찌
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 500, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5005, 400, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5002, 30, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 100000, 1, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5006, 500, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
                break;


              case 49:  //171220 new coupon 루팅 모비 일반
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 10, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5000, 3000, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5003, 30, DATE_ADD(now(), INTERVAL 3 DAY ), now()) ";
                break;
              case 50:  //171220 new coupon 루팅 소규모 1
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 20, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5006, 100, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 100003, 1, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
                break;
              case 51:  //171220 new coupon 루팅 소규모2
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 20, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5002, 5, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 100003, 1, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
                break;
              case 52:  //171220 new coupon 루팅 소규모 3
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 20, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5005, 50, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 100003, 1, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
                break;

              case 53:  //171220 new coupon 루팅 유니크 
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 80, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5005, 100, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5006, 200, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5003, 40, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
                break;
              case 54:  //171220 new coupon 루팅 플러스 
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 50, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5002, 10, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5000, 5000, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5003, 40, DATE_ADD(now(), INTERVAL 3 DAY ), now()) ";
                break;
              case 55:  //171220 new coupon 모비 스폐셜 
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 50, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5002, 10, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5000, 5000, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5003, 40, DATE_ADD(now(), INTERVAL 3 DAY ), now()) ";
                break;
              case 56:  //171220 new coupon 찌 
              	$sql = "INSERT INTO frdUserPost
					(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
				$sql .= "($userId, 4, 5004, 500, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5005, 400, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5002, 30, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 100000, 1, DATE_ADD(now(), INTERVAL 3 DAY ), now()), ";
				$sql .= "($userId, 4, 5006, 500, DATE_ADD(now(), INTERVAL 3 DAY ), now())  ";
                break;
            
			default :  
				$this->logger->logError(__FUNCTION__.': not Type userId : '.$userId.", sql : ".$sql);
				$data['error'] = 1;
				$result['Data'] = $data;
				return $result;
		}
		$db -> prepare($sql);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

		if ( $couponType >= 100 ) {
			$sql = "insert into frdUsedPublicCoupon ( type, id)  values (:couponType, :userId)";
			$login_db -> prepare($sql);
			$login_db -> bindValue(':couponType', $couponType, PDO::PARAM_INT);
			$login_db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		} else {
			$sql = "update frdCoupon set isGet = 1, getId = :userId where id LIKE '%$coupon%' ";
			$login_db -> prepare($sql);
			$login_db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		}
		$row = $login_db->execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}
		$result['Protocol'] = 'ResCoupon';
		$result['ResultCode'] = 100;
		$data['error'] = 0;
		$result['Data'] = $data;
		return $result;
	}


}
?>
