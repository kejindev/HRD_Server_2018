<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
require_once './lib/RandManager.php';


class DailyQuestManager {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function isBonusHeartTime($time) {
		if ( ( $time >= 12 && $time < 14 ) || ( $time >= 18 && $time < 20 ) ) {
			return true;
		}
		return false;
	}

	function DailyQuestComplete($param) {
		$resultFail['Protocol'] = 'ResDailyQuestComplete';
		$resultFail['ResultCode'] = 300;

		$userId = $param["userId"];
		$questIdx = $param["questIdx"];
		$type = $param["type"];
		$cnt = $param["cnt"];

		// apc 에서 가져오는 걸로 변경 한다. 
		$questMax = 15;
		$clearCondtion = array( 1,1,1,1,1,1,1,100,1,1,1,1,1,1 );
		$questRewardType = array(5002,5000,5004,5006,5004,5006,5000,5004,5006,5000,5000,5002,5000);
		$questRewardCount = array(3,1500,5,100,5,100,1500,5,100,100,1500,3,1500);

		$db = new DB();
		$sql = "
			SELECT DATEDIFF(now(), attendDate)as attendDiff, attendCount,
			questIdx0, progress0,
			questIdx1, progress1,
			questIdx2, progress2,
			questIdx3, progress3,
			questIdx4, progress4,
			freeH1, freeH2, tripleCrown
			FROM frdMonthlyCard WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			
			$arr = $this-> UniqueRandomNumbersWithinRange(0, $questMax, 5);

			$questIdxs = null;
			$progress = null;
			$questIdxs[] = $arr[0];
			$questIdxs[] = $arr[1];
			$questIdxs[] = $arr[2];
			$questIdxs[] = $arr[3];
			$questIdxs[] = $arr[4];
			$progress[]  = 0;
			$progress[]  = 0;
			$progress[]  = 0;
			$progress[]  = 0;
			$progress[]  = 0;

			$freeH1 = 0;
			$freeH2 = 0;
			$tripleCrown = 0;

			$sql = "INSERT INTO frdMonthlyCard 
				( userId, attendDate, attendCount, startTime, recvCnt, duration, 
				  questIdx1, progress1,
				  questIdx2, progress2,
				  questIdx3, progress3,
				  questIdx4, progress4,
				  questIdx5, progress5,
				  freeH1, freeH2, 
				tripleCrown,
				  update_date 
				) 
				VALUES ( :userId, now(), 1,  now(), 0, 0, 
					:q1,0,
					:q2,0,
					:q3,0,
					:q4,0,
					:q5,0,
					0,0,
					0,
					now()) ";
			$db -> prepare($sql);
			$db -> bindValue(':q1', $arr[0], PDO::PARAM_INT);
			$db -> bindValue(':q2', $arr[1], PDO::PARAM_INT);
			$db -> bindValue(':q3', $arr[2], PDO::PARAM_INT);
			$db -> bindValue(':q4', $arr[3], PDO::PARAM_INT);
			$db -> bindValue(':q5', $arr[4], PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError(__FUNCTION__.': fail userId :'.$userId.' sql: '.$sql );
				return $resultFail;
			}

			$attendDiff  = 1;
			$attendanceCount = 1;
			$everydayQuest = 0;

			// 신규 유입이 완료를 보내다니. 이건 반란이다.
			// 초기값을보내서 널 종료 한다. 
		} else {
			$attendDiff  = $row['attendDiff'];
			$attendanceCount = $row["attendCount"];

			$questIdxs  = null;
			$progress  = null;


			$questIdxs[] = $row['questIdx0'];
			$questIdxs[] = $row['questIdx1'];
			$questIdxs[] = $row['questIdx2'];
			$questIdxs[] = $row['questIdx3'];
			$questIdxs[] = $row['questIdx4'];
			$progress[]  = $row['progress0'];
			$progress[]  = $row['progress1'];
			$progress[]  = $row['progress2'];
			$progress[]  = $row['progress3'];
			$progress[]  = $row['progress4'];

			$freeH1 = $row['freeH1'];
			$freeH2 = $row['freeH2'];
			$tripleCrown = $row['tripleCrown'];
		}

		// 현재progress 와 데이터의 max 를 비교 
		/*
		0	스테이지 클리어			영광의 승리
		1	강림 던전 클리어		이계의 영웅
		2	고난의 탑 플레이		탑을 오르는 자
		3	정수 추출				정수 수집가
		4	티켓 사용 스테이지 클리어	강행 돌파
		5	유물 진화				진화하는 유물
		6	유물 강화				강해지는 비법
		7	특성 획득				특성을 위하여!
		8	특정 스테이지 클리어	해결사
		9	마법 강화				마법 전문가
		10	마법 합성				강력한 마법
		11	퀘스트 아이템 획득		여신 공물
		12	룬 획득					여신의 힘
		13	룬 조합					조합가
		14	신전 스테이지 플레이	신전 수호자
		31	일퀘 3개 달성			여신의 축복
		*/
		for($i = 0; $i < 15; $i++) {
			// 퀘스트가 존재 하는지 확인 
			if (@$questIdxs[$i] == $questIdx) {
				// apc에서 데이터 긁어옮 MAX 
				$apc_key = 'DailyMission_'.$questIdx;
				$apc_data = apc_fetch($apc_key);
				if($progress[$i] < $apc_data[1]) {
					$data["missionShouldRefreshed"] = true;
					$data['questIdx'] = $questIdx;
					$result['Data'] = $data;
					$result['ResultCode'] = 801;
					return $resultFail;
				} else {
					break;
				}
			}
		}

		if ( ($questIdx == 29) || ($questIdx == 30) || ($questIdx == 31) ) {  
			$result['Protocol'] = 'ResDailyQuestComplete';
			$data = null;

			if ($questIdx == 29 && $freeH1 != 0) {
				$this -> logger -> logError(__FUNCTION__.': fail userId :'.$userId.' 29 ');
				$data["missionShouldRefreshed"] = true;
				$data['questIdx'] = $questIdx;
				$result['Data'] = $data;
				$result['ResultCode'] = 801;
				return $result;
			}

			if ($questIdx == 30 && $freeH2 != 0) {
				$this -> logger -> logError(__FUNCTION__.': fail userId :'.$userId.' 30 ');
				$data["missionShouldRefreshed"] = true;
				$data['questIdx'] = $questIdx;
				$result['Data'] = $data;
				$result['ResultCode'] = 801;
				return $result;
			}

			if ($questIdx == 31 && $tripleCrown != 0) {
				$this -> logger -> logError(__FUNCTION__.': fail userId :'.$userId.' 31 ');
				$data["missionShouldRefreshed"] = true;
				$data['questIdx'] = $questIdx;
				$result['Data'] = $data;
				$result['ResultCode'] = 801;
				return $result;
			}

			$freeResult = $this->freeHeartCheck($db, $userId, $questIdx);
			if ($freeResult['ResultCode'] != 100 ) {
				$this->logger->logError(__FUNCTION__.": FAIL userId : ".$userId." sql : ".$sql);
				return $resultFail;
			}

			$addedQuery = $freeResult['add'];
			$sql = "UPDATE frdMonthlyCard SET update_date = now() $addedQuery WHERE userId = :userId";
			$db->prepare($sql);
			$db->bindValue(':userId',$userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.": FAIL userId : ".$userId." sql : ".$sql);
				return $resultFail;
			}
//			$data = $freeResult['Data'];
			$data['questIdx'] = $questIdx;
			$result['Data'] = $data;
			$result['ResultCode'] = 100;
			return $result;
		}

		$missionData["questIdxs"] = $questIdxs;
		$missionData["progress"] = $progress;

		$isCheck = false;
		if($questRewardType[$questIdx] != $type) {
			$result['Protocol'] = 'ResDailyQuestComplete';
			$result['ResultCode'] = 802;
			$result['Data'] = null;
			return $result;
		}

		for ($rowPos = 0; $rowPos < count($questIdxs); $rowPos++) { 
			if ($questIdxs[$rowPos] == $questIdx) {
				$isCheck = true;
				break;
			}
		}

		// false 는 미존재 
		if ($isCheck == false){
			$result['Protocol'] = 'ResDailyQuestComplete';
			$result['ResultCode'] = 800;
			$result['Data'] = null;
			return $result;
		}

		// -1 은 수령 완료  
		if ( $progress[$rowPos] == -1) {
			$result['Protocol'] = 'ResDailyQuestComplete';
			$result['ResultCode'] = 801;
			$result['Data'] = null;
			return $result;
		}
			

		$rewardType = $questRewardType[$questIdx];
		$rewardCount = $questRewardCount[$questIdx];

		$sql = "
			INSERT INTO frdUserPost 
			(recvUserId, sendUserId, type, count, expire_time, reg_date)
			values (:userId, 1, :type, :count, DATE_ADD(now(), INTERVAL 6 DAY), now()) ";
		$db->prepare($sql);
		$db->bindValue(':userId',$userId, PDO::PARAM_INT);
		$db->bindValue(':type',$rewardType, PDO::PARAM_INT);
		$db->bindValue(':count',$rewardCount, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.": FAIL userId : ".$userId.' sql :'.$sql);
			return $resultFail;
		}
		$addedQuery = ", progress$rowPos = -1";

		$sql = "UPDATE frdMonthlyCard SET update_date = now() $addedQuery WHERE userId = :userId";
		$db->prepare($sql);
		$db->bindValue(':userId',$userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.": FAIL userId : ".$userId." sql : ".$sql);
			return $resultFail;
		}

		$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo 
			where userId=:userId and  achieveId > 4400 and achieveId < 4500";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$achieveId = $row['achieveId'];
			if ($row['cnt'] != -1 ) {
				require_once './classes/AchieveManager.php';
				$AchieveManager = new AchieveManager();
				$AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
			} 
		}	

		$result['Protocol'] = 'ResDailyQuestComplete';
		$result['ResultCode'] = 100;
		$data = null;
		$data['questIdx'] = $questIdx;
		$result['Data'] = $data;
		return $result;
	}

	function updateDayMission($db, $userId, $questIdx, $val) {
        $resultFail['ResultCode'] = 300;

		$sql = "
			SELECT DATEDIFF(now(), attendDate)as attendDiff, attendCount,
				   questIdx0, progress0,
				   questIdx1, progress1,
				   questIdx2, progress2,
				   questIdx3, progress3,
				   questIdx4, progress4,
				   freeH1, freeH2, tripleCrown
					   FROM frdMonthlyCard WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

		$questIdxs  = null;
		$progress  = null;

		$questIdxs[] = $row['questIdx0'];
		$questIdxs[] = $row['questIdx1'];
		$questIdxs[] = $row['questIdx2'];
		$questIdxs[] = $row['questIdx3'];
		$questIdxs[] = $row['questIdx4'];
		$progress[]  = $row['progress0'];
		$progress[]  = $row['progress1'];
		$progress[]  = $row['progress2'];
		$progress[]  = $row['progress3'];
		$progress[]  = $row['progress4'];

		$dCheck = false;
        for($i = 0; $i < 5; $i++) {
            // 퀘스트가 존재 하는지 확인
			if ($questIdxs[$i] == $questIdx) {
				$progressVal = $progress[$i];
				$dCheck = true;
				break;
			}
        }

		if ($dCheck == false) {
//			$this->logger->logError(__FUNCTION__.' : Not exsit userId :'.$userId.' qid : '.$questIdx);
			$result['ResultCode'] = 100;
			return $result;
		}

		if ($progressVal == -1 ) {
//			$this->logger->logError(__FUNCTION__.' : allready clear userId :'.$userId.' qid : '.$questIdx);
			$result['ResultCode'] = 100;
			return $result;
		}

		// 특포 (메달) 은 누적 기록 
		if ($questIdx == 7 ) {
			$val = $val + $progressVal;
		}
		
		$sql = "UPDATE frdMonthlyCard SET progress".$i." = :val 	where userId=:userId";
		$db->prepare($sql);
		$db->bindValue(':val', $val, PDO::PARAM_INT);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

        $result['ResultCode'] = 100;
        return $result;
    }


    function UniqueRandomNumbersWithinRange($min, $max, $quantity) {
        $numbers = range($min, $max);
        shuffle($numbers);
        return array_slice($numbers, 0, $quantity);
    }

	function freeHeartCheck($db, $userId, $questIdx) {
		// 무료 하트 지급 타입
		$now = time();
		$nowTime = intval(date("H", $now));

		$addedQuery = null;
		// 무료 하트 시간 체크
		$result["add"] = null;
		$data = null;
/*		if (!($this->isBonusHeartTime($nowTime))) { 
		$data["missionShouldRefreshed"] = true;
		$result['ResultCode'] = 100;
		$result['Data'] = $data;
			return $result;	
		}
	*/
		switch($questIdx) {
			case 29:
				$type = 5003;	
				$count = 15;
				$add = ", freeH1 = -1 ";
				break;
			case 30:
				$type = 5003;	
				$count = 15;
				$add = ", freeH2 = -1 ";
				break;
			case 31:
				$type = 110000;	
				$count = 1;
				$add = ", tripleCrown = -1";
				break;
			default:
				$result['ResultCode'] = 300;
				return $result;
		}

		// 선행 if 절에서 29,30,31 일때만 진입한다. else 가 없다. 
		$sql = "
			INSERT INTO frdUserPost 
			(recvUserId, sendUserId, type, count, expire_time, reg_date)
			values (:userId, 1, :type, :count, DATE_ADD(now(), INTERVAL 6 DAY), now()) ";
		$db->prepare($sql);
		$db->bindValue(':userId',$userId, PDO::PARAM_INT);
		$db->bindValue(':type',$type, PDO::PARAM_INT);
		$db->bindValue(':count',$count, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.": FAIL userId : ".$userId.' sql :'.$sql);
			$result['ResultCode'] = 300;
			return $result;
		}

		$result["add"] = $add;

		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}
}

?>
