<?php
include_once './common/define.php';
require_once './lib/Logger.php';

class EquipWeaponEffect {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function getEquipWeapon($db, $userId) {

		$sql = "SELECT *
			FROM frdEquipWeapons WHERE userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$resultList = null;
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			if ($row['slotNo'] == 0) {
				$idxs = $row['eq0'].','.$row['eq1'].','.$row['eq2'].','.$row['eq3'].','.$row['eq4'].','.$row['eq5'].','.$row['eq6'].','.$row['eq7'];
			} else {
				$idxs = $row['eq8'].','.$row['eq9'].','.$row['eq10'].','.$row['eq11'].','.$row['eq12'].','.$row['eq13'].','.$row['eq14'].','.$row['eq15'];
			}
			$sub  =  $row['eq16'].','.$row['eq17'].','.$row['eq18'].','.$row['eq19'].','.$row['eq20'].','.$row['eq21'].','.$row['eq22'].','.$row['eq23'];
		} else {
			$result = null;
			return $result;
		}

		$sql = "SELECT weaponTableId, weaponId, level, exp,
			statId0, statId1, statId2, statId3, statId4,
			statVal0, statVal1, statVal2, statVal3, statVal4
				FROM frdHavingWeapons 
				WHERE userId=:userId AND weaponTableId IN ($idxs)";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$resultList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$resultList[] = $row;
		}

		return $resultList;
	}

	function getEquipWeaponForEffect($db, $userId) {

		$sql = "SELECT *
			FROM frdEquipWeapons WHERE userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$resultList = null;
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		$mainSlotNo = null;
		$subSlotNo = null;
		if ($row) {
			if ($row['slotNo'] == 0) {
				$mainSlotNo[] = $row['eq0'];
				$mainSlotNo[] = $row['eq1'];
				$mainSlotNo[] = $row['eq2'];
				$mainSlotNo[] = $row['eq3'];

				$mainSlotNo[] = $row['eq4'];
				$mainSlotNo[] = $row['eq5'];
				$mainSlotNo[] = $row['eq6'];
				$mainSlotNo[] = $row['eq7'];

				$idxs = $row['eq0'].','.$row['eq1'].','.$row['eq2'].','.$row['eq3'].','.$row['eq4'].','.$row['eq5'].','.$row['eq6'].','.$row['eq7'];
			} else {
				$mainSlotNo[] = $row['eq8'];
				$mainSlotNo[] = $row['eq9'];
				$mainSlotNo[] = $row['eq10'];
				$mainSlotNo[] = $row['eq11'];

				$mainSlotNo[] = $row['eq12'];
				$mainSlotNo[] = $row['eq13'];
				$mainSlotNo[] = $row['eq14'];
				$mainSlotNo[] = $row['eq15'];

				$idxs = $row['eq8'].','.$row['eq9'].','.$row['eq10'].','.$row['eq11'].','.$row['eq12'].','.$row['eq13'].','.$row['eq14'].','.$row['eq15'];
			}
			$subSlotNo[] = $row['eq16'];
			$subSlotNo[] = $row['eq17'];
			$subSlotNo[] = $row['eq18'];
			$subSlotNo[] = $row['eq19'];

			$subSlotNo[] = $row['eq20'];
			$subSlotNo[] = $row['eq21'];
			$subSlotNo[] = $row['eq22'];
			$subSlotNo[] = $row['eq23'];

			$sub  =  $row['eq16'].','.$row['eq17'].','.$row['eq18'].','.$row['eq19'].','.$row['eq20'].','.$row['eq21'].','.$row['eq22'].','.$row['eq23'];
		} else {
			$result = null;
			return $result;
		}

		$sql = "SELECT weaponTableId, weaponId, level, exp, transcend,
			statId0, statId1, statId2, statId3, statId4,
			statVal0, statVal1, statVal2, statVal3, statVal4
				FROM frdHavingWeapons 
				WHERE userId=:userId AND weaponTableId IN ($idxs)";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$resultList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$resultList[] = $row;
		}

		$sql = "SELECT weaponTableId, weaponId, level, exp, transcend,
			statId0, statId1, statId2, statId3, statId4,
			statVal0, statVal1, statVal2, statVal3, statVal4
				FROM frdHavingWeapons 
				WHERE userId=:userId AND weaponTableId IN ($sub)";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$subList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$subList[] = $row;
		}

		$result['main'] = $resultList;
		$result['mainSlot'] = $mainSlotNo;
		$result['sub'] = $subList;
		$result['subSlot'] = $subSlotNo;
		return $result;
	}

	function getEquipWeaponEffect($db, $userId) {
		// 착용하고 있는 유물 정보를 가져 온다.
		$eResult = $this->getEquipWeaponForEffect($db, $userId);
		if ($eResult == null) {
			return null;
		}
	
		$data = null;	

		$mainList = $eResult['main'];
		$subList = $eResult['sub'];
		$mainSlot = $eResult['mainSlot'];
		$subSlot = $eResult['subSlot'];

		$data['addGold'] = 0;
		$data['addExp'] = 0;
		$data['addMedal'] = 0;

		$mainBonus = $this->sumEff($mainList, $mainSlot,$userId);
		$mData = $mainBonus['Data'];
		$data['addGold'] = $mData['addGold'];
		$data['addExp'] = $mData['addExp'];
		$data['addMedal'] = $mData['addMedal'];


		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}	
			
	function sumEff($list, $mainSlot,$userId) {	
		// apc 를 통해 착용하고 있는 유물의 효과를 가져 온다.

		$addGold = 0;
		$addExp = 0;
		$addMedal = 0;
		if (count($list) < 1) {
			$data['addGold'] = $addGold;
			$data['addExp'] = $addExp;
			$data['addMedal'] = $addMedal;
			$result['ResultCode'] = 100;
			$result['Data'] = $data;

			return $result;
		}

		foreach($list as $row) {

			$tableId = $row['weaponTableId'];
			$WTID = null;
			for($slotI = 0; $slotI < count($mainSlot); $slotI++) {
				if ($tableId == $mainSlot[$slotI] ) {
					$WTID = $slotI;
					break;
				}
			}

			$weaponId = $row['weaponId'];
			$statId0 = $row['statId0'];
			$statId1 = $row['statId1'];
			$statId2 = $row['statId2'];
			$statId3 = $row['statId3'];
			$level = $row['level'];

			$apc_key = 'WeaponOption_'.$weaponId;
			$apc_data = apc_fetch($apc_key);

			// 항목 두개 
			// 보조유물의 퍼센트를 합산 하다.
			// 클래스를 비교하여 맞는 슬롯에 장착하였을때만 효과를 더한다. 


			if ($statId0 == 11) { 
				if ($level > 9 ) {
					if (isset($apc_data[3]) && $apc_data[3] != "") {
						if ($WTID != $apc_data[3]) {
							continue;
						}
					}
					if ($apc_data[1] == 101) {
						$rand_key = 'RandomOption_'.$statId0;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[1];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else if  ($apc_data[1] == 102) {
						$rand_key = 'RandomOption_'.$statId0;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else {
						$vals = $apc_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					}
					$addGold +=  $min + (($max - $min) * ($row['statVal0'] *4/1000));
				}
			}	

			if ($statId1 == 11) { 
				if ($level > 19 ) {
					if (isset($apc_data[6]) && $apc_data[6] != "") {
						if ($WTID != $apc_data[6]) {
							continue;
						}
					}
					$statId1 = $statId1 -1;
					if ($apc_data[4] == 101) {
						$rand_key = 'RandomOption_'.$statId1;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[1];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else if  ($apc_data[4] == 102) {
						$rand_key = 'RandomOption_'.$statId1;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else {
						$vals = $apc_data[5];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					}
					$addGold +=  $min + (($max - $min) * ($row['statVal1'] *4/1000));
				}
			}	
			if ($statId2 == 11) { 
				if ($level > 29 ) {
					if (isset($apc_data[9]) && $apc_data[9] != "") {
						if ($WTID != $apc_data[9]) {
							continue;
						}
					}
					if ($apc_data[7] == 101) {
						$rand_key = 'RandomOption_'.$statId2;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[1];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else if  ($apc_data[7] == 102) {
						$rand_key = 'RandomOption_'.$statId2;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else {
						$vals = $apc_data[8];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					}
					$addGold +=  $min + (($max - $min) * ($row['statVal2'] *4/1000));
				}
			}	
			if ($statId3 == 11) { 
				if ($level > 39 ) {
					if (isset($apc_data[12]) && $apc_data[12] != "") {
						if ($WTID != $apc_data[12]) {
							continue;
						}
					}
					if ($apc_data[10] == 101) {
						$rand_key = 'RandomOption_'.$statId3;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[1];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else if  ($apc_data[10] == 102) {
						$rand_key = 'RandomOption_'.$statId3;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else {
						$vals = $apc_data[11];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					}
					$addGold +=  $min + (($max - $min) * ($row['statVal3'] *4/1000));
				}
			}	
			// 12 : add exp
			if ($statId0 == 12) { 
				if ($level > 9 ) {
					if (isset($apc_data[3]) && $apc_data[3] != "") {
						if ($WTID != $apc_data[3]) {
							continue;
						}
					}
					if ($apc_data[1] == 101) {

						$rand_key = 'RandomOption_'.$statId0;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[1];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else if  ($apc_data[1] == 102) {
						$rand_key = 'RandomOption_'.$statId0;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else {
						$vals = $apc_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					}
					$addExp +=  $min + (($max - $min) * ($row['statVal0'] *4/1000));
				}
			}	
			if ($statId1 == 12) { 
				if ($level > 19 ) {
					if (isset($apc_data[6])  && $apc_data[6] != "") {
						if ($WTID != $apc_data[6]) {
							continue;
						}
					}
					if ($apc_data[4] == 101) {
						$rand_key = 'RandomOption_'.$statId1;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[1];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else if  ($apc_data[4] == 102) {
						$rand_key = 'RandomOption_'.$statId1;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else {
						$vals = $apc_data[5];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					}
					$addExp +=  $min + (($max - $min) * ($row['statVal1'] *4/1000));
				}
			}	
			if ($statId2 == 12) { 
				if ($level > 29 ) {
					if (isset($apc_data[9])  && $apc_data[9] != "") {
						if ($WTID != $apc_data[9]) {
							continue;
						}
					}
					if ($apc_data[7] == 101) {
						$rand_key = 'RandomOption_'.$statId2;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[1];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else if  ($apc_data[7] == 102) {
						$rand_key = 'RandomOption_'.$statId2;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else {
						$vals = $apc_data[8];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					}
					$addExp +=  $min + (($max - $min) * ($row['statVal2'] *4/1000));
				}
			}	
			if ($statId3 == 12) { 
				if ($level > 39 ) {
					if (isset($apc_data[12]) && $apc_data[12] != "") {
						if ($WTID != $apc_data[12]) {
							continue;
						}
					}

					if ($apc_data[10] == 101) {
						$rand_key = 'RandomOption_'.$statId3;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[1];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else if  ($apc_data[10] == 102) {
						$rand_key = 'RandomOption_'.$statId3;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else {
						$vals = $apc_data[11];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					}
					$addExp +=  $min + (($max - $min) * ($row['statVal3'] *4/1000));
				}
			}	

			// 19 : add medal per
			if ($statId0 == 19) { 
				if ($level > 9 ) {
					if (isset($apc_data[3]) && $apc_data[3] != "") {
						if ($WTID != $apc_data[3]) {
							continue;
						}
					}
					if ($apc_data[1] == 101) {
						$rand_key = 'RandomOption_'.$statId0;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[1];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else if  ($apc_data[1] == 102) {
						$rand_key = 'RandomOption_'.$statId0;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else {
						$vals = $apc_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
						;
					}
					$addMedal +=  $min + (($max - $min) * ($row['statVal0'] *4/1000));
				}
			}	
			if ($statId1 == 19) { 
				if ($level > 19 ) {
					if (isset($apc_data[6]) && $apc_data[6] != "") {
						if ($WTID != $apc_data[6]) {
							continue;
						}
					}
					if ($apc_data[4] == 101) {
						$rand_key = 'RandomOption_'.$statId1;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[1];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else if  ($apc_data[4] == 102) {
						$rand_key = 'RandomOption_'.$statId1;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else {
						$vals = $apc_data[5];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					}
					$addMedal +=  $min + (($max - $min) * ($row['statVal1'] *4/1000));
				}
			}	
			if ($statId2 == 19) { 
				if ($level > 29 ) {
					if (isset($apc_data[9]) && $apc_data[9] != "") {
						if ($WTID != $apc_data[9]) {
							continue;
						}
					}
					if ($apc_data[7] == 101) {
						$rand_key = 'RandomOption_'.$statId2;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[1];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else if  ($apc_data[7] == 102) {
						$rand_key = 'RandomOption_'.$statId2;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else {

						$vals = $apc_data[8];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					}
					$addMedal +=  $min + (($max - $min) * ($row['statVal2'] *4/1000));
				}
			}	
			if ($statId3 == 19) { 
				if ($level < 39 ) {
					if (isset($apc_data[12]) && $apc_data[12] != "") {
						if ($WTID != $apc_data[12]) {
							continue;
						}
					}
					if ($apc_data[10] == 101) {
						$rand_key = 'RandomOption_'.$statId3;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[1];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else if  ($apc_data[10] == 102) {
						$rand_key = 'RandomOption_'.$statId3;
						$rand_data = apc_fetch($rand_key);
						$vals = $rand_data[2];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					} else {

						$vals = $apc_data[11];
						$val = explode('/',$vals);
						$min = $val[0];
						$max = $val[1];
					}
					$addMedal +=  $min + (($max - $min) * ($row['statVal3'] *4/1000));
				}
			}	
		}	

		$this->logger->logError(__FUNCTION__.' : Weapon Bonus  gold :'.$addGold);
		$this->logger->logError(__FUNCTION__.' : Weapon Bonus  medal :'.$addMedal);
		$this->logger->logError(__FUNCTION__.' : Weapon Bonus  exp :'.$addExp);

		$data['addGold'] = $addGold;
		$data['addExp'] = $addExp;
		$data['addMedal'] = $addMedal;

		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}

}
?>
