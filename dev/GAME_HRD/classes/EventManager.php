<?php
require_once './common/DB.php';
require_once './common/define.php';
require_once './lib/Logger.php';
require_once './send_data/SendDataManager.php';

class EventManager {

	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function IsActiveEvent($event_Key) {
		$SendDataManager = new SendDataManager();
        $apc_key =  "Event_".$GLOBALS['COUNTRY'].'_'.$event_Key;
        $eventType = apc_fetch($apc_key);
        if (!$eventType)
            return false;

        $event_now = date('ymdH');
        $eventResult = $SendDataManager->getEventData($event_Key+1, $eventType[1],$event_now);
        if (!$eventResult)
            return false;

        return $eventResult;
	}

	// StageManager - 핫타임
	function getHottimeBonus (&$bonusData, $stageId) {
		$result = array();
		$eventCheck = $this->IsActiveEvent(EVENT_HOTTIME);
		if ( false == $eventCheck ) {
			$result['eventCheck'] = null;
			return $result;
		}

		$hour = (int)date('H');
		$isAM = false;
		$isPM = false;
		if ( $hour>=12 && $hour<14 )
			$isAM = true;
		else if ( $hour>=20 && $hour<22 )
			$isPM = true;
		else {
			$result['eventCheck'] = null;
			return $result;
		}

		$arr_eventCheck = explode(",", $eventCheck[0]);
		$start_ymdH = $arr_eventCheck[0];
		$startDate = new DateTime('20'.$start_ymdH.'0000');
		$targetDate = new DateTime(date('YmdHmi'));
		$interval = $startDate->diff($targetDate);
		$rewardDay = (int)$interval->days + 1;

		$bonusArr = null;
		switch ($rewardDay) {
			case 1:
				if ( $isAM && $stageId<500 )	
					$bonusData['amount']['type'][(string)Type_Medal] += 0.5;//메달 획득량 1.5배
				if ( $isPM && ($stageId < 500 || $stageId > 20000 || ($stageId>2000 && $stageId<5000)) )
					$bonusData['runeAddPer'] += (150000 - 100000);			//룬 획득 량 1.5배
				break;
			
			case 2:
				if ( $isAM && $stageId<500 )
					$bonusData['amount']['type'][(string)Type_Gold] += 0.5;	//골드 획득량 1.5배
				if ( $isPM && $stageId<500 )
					$bonusData['amount']['type'][(string)Type_Exp] += 0.5;	//경험치 획득량 1.5배
				break;

			case 3:
				if ( $isAM && ($stageId > 20000 || ($stageId>2000 && $stageId<5000)) )
					$bonusData['questItemGetPer'] += 0.5;					//퀘템 획득 확률 1.5배 (룬제외)
				if ( $isPM && ($stageId > 20000 || ($stageId>2000 && $stageId<5000)) )
					$bonusData['questItemGetPer'] += 0.5;					//퀘템 획득 확률 1.5배 (룬제외)
				break;

			case 4:
				if ( $isAM && $stageId<500 )
					$bonusData['amount']['type']['0_506'] += 1.0;			//영혼석 획득량 2배 (강림제외)
				if ( $isPM && $stageId<500 )
					$bonusData['amount']['type']['0_506'] += 1.0;			//영혼석 획득량 2배 (강림제외)
				break;

			case 5:
				if ( $isAM && $stageId<500 )	
					$bonusData['amount']['type'][(string)Type_Medal] += 0.5;//메달 획득량 1.5배
				if ( $isPM && ($stageId < 500 || $stageId > 20000 || ($stageId>2000 && $stageId<5000)) )
					$bonusData['runeAddPer'] += (150000 - 100000);			//룬 획득 량 1.5배
				break;

			case 6:
				if ( $isAM && $stageId<500 )
					$bonusData['amount']['type'][(string)Type_Gold] += 0.5;	//골드 획득량 1.5배
				if ( $isPM && $stageId<500 )
					$bonusData['amount']['type'][(string)Type_Exp] += 0.5;	//경험치 획득량 1.5배
				break;

			case 7:
				if ( $isAM && ($stageId > 20000 || ($stageId>2000 && $stageId<5000)) )
					$bonusData['questItemGetPer'] += 0.5;					//퀘템 획득 확률 1.5배 (룬제외)
				if ( $isPM && ($stageId > 20000 || ($stageId>2000 && $stageId<5000)) )
					$bonusData['questItemGetPer'] += 0.5;					//퀘템 획득 확률 1.5배 (룬제외)
				break;

			default:
				break;
		}
		
		//	룬 획득 확률 1.5배
		//	$bonusData['runeGetPer'] += 0.5;

		// 	퀘템 획득 량 2배 (룬제외)
		// 	$bonusData['questItemAddPer'] += (200000 - 100000);

		$result['eventCheck'] = $eventCheck;
		return $result;
	}

	// StageManager - 고탑 입장 이벤트
	function getClimbEnterReward(&$db, $userId) {
        $curYMD = (int)date('ymd');
       
       	$eventCheck = $this->IsActiveEvent(EVENT_ENTER_CLIMB_REWARD);
       	if ( false == $eventCheck )
       		return;

        $sql = "SELECT lastYMD, totalChk FROM Event_ClimbEnterReward WHERE userId=:userId ";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {

			$sql = "INSERT INTO Event_ClimbEnterReward ( userId, startYMD, lastYMD, totalChk )
											VALUES (:userId, :startYMD, :lastYMD, 0)";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':startYMD', (int)date('ymd'), PDO::PARAM_INT);
			$db -> bindValue(':lastYMD', (int)date('ymd'), PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql: '.$sql);
				return;
			}

			$lastYMD = 0;
			$totalChk = 0;
		}
		else {
			$lastYMD = (int)$row['lastYMD'];
			$totalChk = (int)$row['totalChk'];
		}
		
		if ( $curYMD <= $lastYMD )
			return;

		$lastYMD = $curYMD;
		$totalChk++;
		$sql = "UPDATE Event_ClimbEnterReward SET lastYMD = :lastYMD, totalChk = :totalChk WHERE userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':lastYMD', $lastYMD, PDO::PARAM_INT);
		$db -> bindValue(':totalChk', $totalChk, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql : '.$sql);
			return;
		}

		$rewardType = array();
		$rewardCount = array();

		$arr_eventCheck = explode(",", $eventCheck[0]);
		$start_ymdH = $arr_eventCheck[0];
		$startDate = new DateTime('20'.$start_ymdH.'0000');
		$targetDate = new DateTime(date('YmdHmi'));
		$interval = $startDate->diff($targetDate);
		$rewardDay = (int)$interval->days + 1;
		switch ($rewardDay) {
			case 1: $rewardType[] = 5000; $rewardCount[] = 10000; break;
			case 2: $rewardType[] = 5006; $rewardCount[] = 150; break;
			case 3: $rewardType[] = 5004; $rewardCount[] = 50; break;
			case 4: $rewardType[] = 5002; $rewardCount[] = 5; break;
			case 5: $rewardType[] = 100003; $rewardCount[] = 3; break;
			case 6: $rewardType[] = 100000; $rewardCount[] = 1; break;
			case 7: 	//각종 찬룬 10개
				for ( $i=115000; $i<115005; $i++ ) {
					$rewardType[] = $i; $rewardCount[] = 10;
				}
				break;
			default: return;
		}
	

		$sql = "INSERT INTO frdUserPost (recvUserId, sendUserId, type, count, expire_time, reg_date) values ";
		for ( $ii=0; $ii<count($rewardType); $ii++ ) {
			$rt = $rewardType[$ii];
			$rc = $rewardCount[$ii];
			if ( $ii != 0 )
				$sql .= ", ";
			$sql .= "($userId, 5, $rt, $rc, DATE_ADD(now(), INTERVAL 7 DAY ), now()) ";
		}
		$db -> prepare($sql);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			return;
		}
	}


	// GetRewardManager - 369이벤트
	function get369Bonus (&$db, $userId, $stageId, &$resultRewardType, &$resultRewardCount) {

		$eventCheck = $this->IsActiveEvent(EVENT_369);
       	if ( false == $eventCheck )
       		return;

       	if ( $stageId > 210 && $stageId < 20000 )	//일반, 히든스테이지만
       		return;

		$sql = "SELECT dailyCount, accuCount, DATEDIFF(now(), update_date) as attendDiff FROM Event_369 WHERE userId=:userId ";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$dailyCount = $row['dailyCount'];
			$accuCount = $row['accuCount'];
			if ( (int)$row['attendDiff'] > 0 )
				$dailyCount = 0;
		}
		else {
			$sql = "INSERT INTO Event_369 ( userId, dailyCount, accuCount, update_date ) values ($userId, 0, 0, now())";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql: '.$sql);
				return;
			}
			$dailyCount = 0;
			$accuCount = 0;
		}
		$dailyCount++;
		$accuCount++;

		if ( $dailyCount == 3 || $dailyCount == 6 || $dailyCount == 9 ) {

			for ( $i=0; $i<count($resultRewardCount); $i++ )
				$resultRewardCount[$i] *= 2;

			switch ($dailyCount) {
				case 3: $resultRewardType[] = 135; break;
				case 6: $resultRewardType[] = 136; break;
				case 9: $resultRewardType[] = 137; break;
				default: break;
			}
			$resultRewardCount[] = 1;
		}

		$sql = "UPDATE Event_369 SET dailyCount=$dailyCount, accuCount=$accuCount, update_date=now() WHERE userId=$userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql : '.$sql);
			return;
		}
	}



	// GetRewardManager - 매일 스테이지 일정 횟수 클리어시 보상 지급 이벤트
	function getDailyBonus_ClearCount (&$db, $userId, $stageId, &$resultRewardType, &$resultRewardCount) {

		$eventCheck = $this->IsActiveEvent(EVENT_DAILY_CLEARCOUNT);
       	if ( false == $eventCheck )
       		return;

		$sql = "SELECT dailyCount, accuCount, DATEDIFF(now(), update_date) as attendDiff FROM Event_Daily_ClearCount WHERE userId=:userId ";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$dailyCount = $row['dailyCount'];
			$accuCount = $row['accuCount'];
			if ( (int)$row['attendDiff'] > 0 )
				$dailyCount = 0;
		}
		else {
			$sql = "INSERT INTO Event_Daily_ClearCount ( userId, dailyCount, accuCount, update_date ) values ($userId, 0, 0, now())";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql: '.$sql);
				return;
			}
			$dailyCount = 0;
			$accuCount = 0;
		}
		$dailyCount++;
		$accuCount++;

		$apcContents = apc_fetch('EventData'.$GLOBALS['COUNTRY'].EVENT_DAILY_CLEARCOUNT);
		if ( $apcContents == false || $apcContents == null ) {
			$this->logger->logError(__FUNCTION__.' : There is no apc_fetch '.'EventData'.$GLOBALS['COUNTRY'].EVENT_DAILY_CLEARCOUNT);
			return false;
		}
		if (is_object($apcContents))	//stdClass -> array
			$apcContents = get_object_vars($apcContents);
		
		$needCount = (int)$apcContents['needCount'];

		if ( $dailyCount == $needCount ) {
			$rewardArrJson = $apcContents['rewardArrJson'];
			$rewardArr = json_decode($rewardArrJson);
			
			for ( $i=0; $i<count($rewardArr[0]); $i++ ) {
				$resultRewardType[] = $rewardArr[0][$i];
				$resultRewardCount[] = $rewardArr[1][$i];
			}
		}

		$sql = "UPDATE Event_Daily_ClearCount SET dailyCount=$dailyCount, accuCount=$accuCount, update_date=now() WHERE userId=$userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql : '.$sql);
			return;
		}
	}



	// AbyssManager - 심던 5의 배수 스테이지 클리어 보상 2배
	function getDoubleBonus_Abyss ($stageId, &$resultRewardType, &$resultRewardCount) {
		$eventCheck = $this->IsActiveEvent(EVENT_DOUBLE_ABYSS);
       	if ( false == $eventCheck )
       		return;

		if ( (int)$stageId % 5 !== 0 )
			return;

		for ( $i=0; $i<count($resultRewardCount); $i++ )
			$resultRewardCount[$i] = (int)$resultRewardCount[$i] * 2;
	}


	// 히든 스테이지 퀘스트 아이템 확률 증가 
	function getEventClearCntBonus ($db, $userId, $stageId) {
		$eventCheck = false;
		$eventRunePer = false;
		$eventQuestPer = false;
		$eventMedalAdd = false;

		$eventRealCheck = false;
		// 히든 스테이지 클리어 퀘스트 아이템 두배 이벤트
		$event_now = date('ymdH');
		$event_Key = EVENT_STAGE_CLEAR;
		$apc_key =  "Event_".$GLOBALS['COUNTRY'].$event_Key;
		$eventType = apc_fetch($apc_key);
		if ($eventType) {
			$SendDataManager = new SendDataManager();
			$eventCheck = $SendDataManager->getEventData($event_Key+1, $eventType[1],$event_now);
			if ($eventCheck) {
				$sql = "SELECT progress FROM frdClearEvent WHERE userId=:userId ";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				if ($row) {
					$cnt = $row['progress'];
					if ($cnt < 5) {
						if ( ($event_now > 17100300) && ($event_now < 17100400) ) {
							if ($stageId > 19999 && $stageId < 20011) {
								$eventRealCheck = true;
								$eventCheck = true;
								$eventRunePer = true;
							}
						} else if ( ($event_now > 17100400) && ($event_now < 17100500) ) {
							if ($stageId > 19999 && $stageId < 20011) {
								$eventRealCheck = true;
								$eventCheck = true;
								$eventQuestPer = true;
							}
						} else if ( ($event_now > 17100500) && ($event_now < 17100600) ) {
							if ($stageId < 210 ) {
								$eventRealCheck = true;
								$eventCheck = true;
								$eventMedalAdd = true;
							}
						}

					}
				
					if ($eventRealCheck) {
						$sql = "UPDATE frdClearEvent
							SET progress = progress+1
							WHERE userId=:userId";
						$db -> prepare($sql);
						$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
						$row = $db -> execute();
						if (!isset($row) || is_null($row) || $row == 0) {
							$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql : '.$sql);
						}
					}
				} else {
					if ( ($event_now > 17100300) && ($event_now < 17100400) ) {
						if ($stageId > 19999 && $stageId < 20011) {
							$eventRealCheck = true;
							$eventCheck = true;
							$eventRunePer = true;
						}
					} else if ( ($event_now > 17100400) && ($event_now < 17100500) ) {
						if ($stageId > 19999 && $stageId < 20011) {
							$eventRealCheck = true;
							$eventCheck = true;
							$eventQuestPer = true;
						}
					} else if ( ($event_now > 17100500) && ($event_now < 17100600) ) {
						if ($stageId < 210 ) {
							$eventRealCheck = true;
							$eventCheck = true;
							$eventMedalAdd = true;
						}
					}

					if ($eventRealCheck) {
						$sql = "INSERT INTO frdClearEvent ( userId, progress, update_date)
							VALUES (:userId, 1, now())";
						$db -> prepare($sql);
						$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
						$row = $db -> execute();
						if (!isset($row) || is_null($row) || $row == 0) {
							$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql: '.$sql);
						}
					}
				}
			}
		}

		$result['eventCheck'] = $eventCheck;
		$result['eventRunePer'] = $eventRunePer;
		$result['eventQuestPer'] = $eventQuestPer;
		$result['eventMedalAdd'] = $eventMedalAdd;

		return $result;
	}

	

	// 일반, 신전, 히든클리어, 고탑플레이시 웹뷰 아이템 지급 
	function getEventWebviewStageReward($db, $userId ) {
		$event_now = date('ymdH');
		$event_Key = EVENT_WEBVIEW;
		$apc_key =  "Event_".$GLOBALS['COUNTRY'].$event_Key;
		$eventType = apc_fetch($apc_key);
		if ($eventType) {
			$SendDataManager = new SendDataManager();
			$eventCheck = $SendDataManager->getEventData($event_Key+1, $eventType[1],$event_now);
			if ($eventCheck) {
				$WebviewManager = new WebviewManager();
				$WebviewManager->WebviewEventAddItem($db, $userId);
			}
		}

		return true;
	}

	


}
?>
