<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
require_once "./common/define.php";
require_once "./lib/RandManager.php";
require_once './classes/AssetManager.php';
require_once './classes/AddManager.php';

class GShopManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

	function OpenCheckGShop($param){
		$resultFail['ResultCode']  = 300;
		$resultFail['Protocol'] = 'ResOpenCheckGShop';;
		$userId = $param["userId"];

		$db = new DB();
		$sql = "SELECT
			shopId,tierWeight,
			TIMESTAMPDIFF(SECOND, startTime, NOW()) as startTime,
			UNIX_TIMESTAMP( endTime) as endTime
			FROM frdGuerrillaShop
			WHERE
			TIMESTAMPDIFF(SECOND, startTime, NOW()) > 0 AND
			TIMESTAMPDIFF(SECOND, NOW(), endTime) > 0
			";
		$db -> prepare($sql);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$resultFail['ResultCode']  = 100;
			$resultFail['Data'] = null;
			return $resultFail;
		}

		$shopId = $row['shopId'];
		$endTime = $row['endTime'];
		$tierWeight = $row['tierWeight'];

		/*
		기존 frdGuerrillaShopUserData itemList 처리 내용.
		"itemId":100001,
		"itemCount":1,
		"resourceType":3,
		"discountRate":19,
		"itemCost":1620,
		"isBuy":false
		*/

		$sql = "
			select * from frdGuerrillaShopUserData where userId = $userId AND shopId = $shopId	
			";
		$db->prepare($sql);
		$db->execute();
		$row = $db->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$itemList = $this->getGShopItemList($db, $userId, $tierWeight);
			$resetCount = 0;

			$shopItemIds = $itemList['shopItemIds'];
			$itemIds = $itemList['itemIds'];
			$itemCounts = $itemList['itemCounts'];
			$discountRates = $itemList['discountRates'];
			$resourceTypes = $itemList['resourceTypes'];
			$itemCosts = $itemList['itemCost'];
			$isBuy = $itemList['isBuy'];

			$tempShopIds = null;
			for($tempI=0; $tempI < count($shopItemIds); $tempI++) {
				if ($tempShopIds == null) {
					$tempShopIds = $shopItemIds[$tempI];
				} else {
					$tempShopIds = $tempShopIds.','.$shopItemIds[$tempI];
				}
			}	

			$tempIds = null;
			for($tempI=0; $tempI < count($itemIds); $tempI++) {
				if ($tempIds == null) {
					$tempIds = $itemIds[$tempI];
				} else {
					$tempIds = $tempIds.','.$itemIds[$tempI];
				}
			}	

			$tempCnt = null;
			for($tempI=0; $tempI < count($itemCounts); $tempI++) {
				if ($tempCnt == null) {
					$tempCnt = $itemCounts[$tempI];
				} else {
					$tempCnt = $tempCnt.','.$itemCounts[$tempI];
				}
			}	

			$tempRate = null;
			for($tempI=0; $tempI < count($discountRates); $tempI++) {
				if ($tempRate == null) {
					$tempRate = $discountRates[$tempI];
				} else {
					$tempRate = $tempRate.','.$discountRates[$tempI];
				}
			}	

			$tempCost = null;
			for($tempI=0; $tempI < count($itemCosts); $tempI++) {
				if ($tempCost == null) {
					$tempCost = $itemCosts[$tempI];
				} else {
					$tempCost = $tempCost.','.$itemCosts[$tempI];
				}
			}	

			$tempBuy = null;
			for($tempI=0; $tempI < count($isBuy); $tempI++) {
				if ($tempI == 0) {
					$tempBuy = $isBuy[$tempI];
				} else {
					$tempBuy = $tempBuy.','.$isBuy[$tempI];
				}
			}	

			$tempType = null;
			for($tempI=0; $tempI < count($resourceTypes); $tempI++) {
				if ($tempType == null) {
					$tempType = $resourceTypes[$tempI];
				} else {
					$tempType = $tempType.','.$resourceTypes[$tempI];
				}
			}	

			$sql = "INSERT INTO frdGuerrillaShopUserData 
						(userId, shopId, resetCount,shopItemIds, itemIds, itemCounts,resourceTypes, 
						discountRates, itemCosts, isBuy, reg_date)
						VALUES (:userId, :shopId, 0,:shopItemIds, :itemIds,:itemCounts,:resourceTypes,
						:discountRates,:itemCosts,:isBuy,now())";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':shopId', $shopId, PDO::PARAM_INT);
			$db -> bindValue(':shopItemIds', $tempShopIds, PDO::PARAM_STR);
			$db -> bindValue(':itemIds', $tempIds, PDO::PARAM_STR);
			$db -> bindValue(':itemCounts', $tempCnt, PDO::PARAM_STR);
			$db -> bindValue(':resourceTypes', $tempType, PDO::PARAM_STR);
			$db -> bindValue(':discountRates', $tempRate, PDO::PARAM_STR);
			$db -> bindValue(':itemCosts', $tempCost, PDO::PARAM_STR);
			$db -> bindValue(':isBuy', $tempBuy, PDO::PARAM_STR);
			$row = $db -> execute();

			/*
			var_dump($userId);
			var_dump($shopId);
			var_dump($tempShopIds);
			var_dump($itemIds);
			var_dump($itemCounts);
			var_dump($resourceTypes);
			var_dump($discountRates);
			var_dump($itemCosts);
			var_dump($isBuy);
			*/

			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
				return $resultFail;
			}
			$itemList = null;
			$itemList['itemIds'] = $tempIds;
			$itemList['itemCounts'] = $tempCnt;
			$itemList['resourceTypes'] = $tempType;
			$itemList['itemCosts'] = $tempCost;
			$itemList['discountRates'] = $tempRate;
			$itemList['isBuy'] = $tempBuy;

		} else {
			$resetCount = intval($row["resetCount"]);

			$itemList['itemIds'] = $row['itemIds'];
			$itemList['itemCounts'] = $row['itemCounts'];
			$itemList['resourceTypes'] = $row['resourceTypes'];
			$itemList['itemCosts'] = $row['itemCosts'];
			$itemList['discountRates'] = $row['discountRates'];
			$itemList['isBuy'] = $row['isBuy'];
		}

		$data["endTime"] = $endTime;
		$data["resetCount"] = $resetCount;

		$data["itemList"] = $itemList;

		$result['Protocol'] = 'ResOpenCheckGShop';;
		$result['ResultCode'] = 100;

		$result['Data'] = $data;
		
		return $result;
	}

	function getGShopItemList($db, $userId, $tierWeight){
		$resultFail['ResultCode'] = 300;		

		$type =  ABILL_Sale_BlackMarket;

		$plusDisVal = 0;
		$sql = "select val from frdEffectForEtc where userId = :userId and type=:type";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_STR);
		$db -> bindValue(':type', $type, PDO::PARAM_STR);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$plusDisVal = $row['val'];
		}  

		// tierwheight 에서 가중치를 배열화
		$tierWeightList = explode('/',$tierWeight);
		$tierList = array(1,2,3,4,5,6);	

		// 가중치로 랜덤을 이용해. 어떤티어가 나올지 뽑늗다.
		// 8개의 아이템 티어를 구한다. 
		$RandManager = new RandManager();
		for ($i = 0; $i<8; $i++ ) {
			$tierResult = $RandManager->Rate($tierList, $tierWeightList);
			switch ($tierResult) {
				case 1:
					$min = 101;
					$max = 112;
					break;
				case 2:
					$min = 201;
					$max = 214;
					break;
				case 3:
					$min = 301;
					$max = 312;
					break;
				case 4:
					$min = 401;
					$max = 413;
					break;
				case 5:
					$min = 501;
					$max = 507;
					break;
				case 6:
					$min = 601;
					$max = 602;
					break;
				default:
					//error
					break;
			}
			$tempKey = rand($min,$max);
			$apc_key = 'BlackMarket_'.$tempKey;
			$apc_data = apc_fetch($apc_key);
			if (!isset($apc_data) || $apc_data == null || $apc_data == "" || !$apc_data ) {
				$i--;
				$this->logger->logError(__FUNCTION__.' :  ------ continue!!!!!  i :'.$i);
				continue;
			}


			/*
			0  	Idx,
			1	ItemType,
			2	ItemCount,
			3	Price,
			4	ResourceType,
			5	Rate,
			6	Random		
			*/
			
			// 확정 랜덤 아이템 
			$this->logger->logError(__FUNCTION__.' :  ----------------------------- apc6 : '.$apc_data[6]. ' $i :'.$i);
			if (!isset($apc_data[1]) || $apc_data[1] == null || $apc_data[1] == "") {
				$i--;
				$this->logger->logError(__FUNCTION__.' :  ------ continueId : '.$apc_data[1]. ' i :'.$i);
				continue;
			}

			if ($apc_data[6] == 1) {
				$itemIds[] = $apc_data[1];	
				$this->logger->logError(__FUNCTION__.' :  ------ itemIds : '.$apc_data[1]);
			} else {
				$fixRandItemId = $this->fixRandItem($apc_data[1]);
				$itemIds[] = $fixRandItemId;	
				$this->logger->logError(__FUNCTION__.' :  ------ fix itemIds : '.$fixRandItemId);
			}

			$shopItemIds[] = $apc_data[1];	

			// 아이템 지급 개수 랜덤
			$itemCount = explode('/',$apc_data[2]);
			if (count($itemCount) > 2) {
				$cntMax = count($itemCount);
				$cntPos = rand(0, $cntMax-1);
				$itemCounts[] = $itemCount[$cntPos];	
			} else {
				$itemCounts[] = $itemCount[0];	
			}

			// 구매 재화 타입이 여러개인지 체크 하여 프로세스
			$payType = explode('/',$apc_data[4]);
			if (count($payType) > 2) {
				$payTypeRate = explode('/',$apc_data[5]);
				$randResult = $RandManager->Rate_N_Idx($payType, $payTypeRate);
				$resourceTypes[] = $randResult['val'];
				$resourceType = $randResult['val'];
				$pos = $randResult['pos'];
			} else {
				$resourceTypes[] = $payType[0];
				$resourceType = $payType[0];
			}
		
	
			// 할인률 로직 
			$discountRate = rand(0, 20);
			if ( $plusDisVal > 0 ) {
				$tPlusDisVal = rand($plusDisVal*0.666666, $plusDisVal*1.333333);
				$discountRate += ($tPlusDisVal * 0.001);
			}
			if ( $discountRate > 95 ) {
				$discountRate = 95;
			}
			if ($discountRate < 1) {
				$discountRate = 1;
			}
			$discountRates[] = ceil($discountRate);

			// 아이템 지급 개수에 관련한 구매 비용 계산 
			$itemPrice = explode('/',$apc_data[3]);
			if (count($itemCount) > 2) {
				$itemCost[] = $this->getItemCost($itemPrice[$cntPos], $resourceType, $discountRates[$i]);
			} else {
				$itemCost[] = $this->getItemCost($itemPrice[0], $resourceType, $discountRates[$i]);
			}

			// 데이터가 있다면 이 함수 상위에서 업데이트 된다.
			// 이건 디폴트를 위해 넣었다.
			$isBuy = array(0,0,0,0,0,0,0,0,0);	 

		}

		$itemList['shopItemIds'] = $shopItemIds;
		$itemList['itemIds'] = $itemIds;
		$itemList['itemCounts'] = $itemCounts;
		$itemList['resourceTypes'] = $resourceTypes;
		$itemList['itemCost'] = $itemCost;
		$itemList['discountRates'] = $discountRates;
		$itemList['isBuy'] = $isBuy;

		
			
		return $itemList;
	}

	function fixRandItem($rewardType) {
		$this->logger->logError(__FUNCTION__.' : test rewardType : '.$rewardType);

		if ( $rewardType < 1000 ) {        //고정 캐릭터 소울스톤 지급
			if ( $rewardType > 500 ) {
				$key_name = $rewardType-500;
				$apc_key = 'CharacIndex_'.$key_name;
				$charApc = apc_fetch($apc_key);
				$charMin = $charApc[0];
				$charMax = $charApc[1];
				$rewardType = rand($charMin, $charMax);
			}
		} else if ( $rewardType < 2000 ) {    //보구 지급
			if ( $rewardType > 1500 ) {
				$array_pos = $rewardType - 1500;
				$apc_key = 'WeaponInform_WeaponMinIdx';
				$weaponData = apc_fetch($apc_key);
				$minTemp = explode('/', $weaponData[1]);
				$weaponMin = $minTemp[$array_pos];

				$apc_key = 'WeaponInform_WeaponMaxIdx';
				$weaponData = apc_fetch($apc_key);
				$maxTemp = explode('/', $weaponData[1]);
				$weaponMax = $maxTemp[$array_pos];

				$rewardType = rand($weaponMin, $weaponMax);
				$rewardType = $rewardType + 1000;
			}
		} else if (  $rewardType >= 10000 && $rewardType < 10510  ) {  //마법 지급
			if ( $rewardType > 10500 ) {
				$key_name = $rewardType - 10500;
				$apc_key = 'MagicPowderValue_'.$key_name;
				$magicData = apc_fetch($apc_key);;
				$magicMin = $magicData[2] + 10000;
				$magicMax = $magicData[3] + 10000;	

				$rewardType = rand($magicMin, $magicMax);
			}
		} else if ( $rewardType > 100000 && $rewardType < 200000 )  {
			if ($rewardType > 100999 && $rewardType < 110000 ) {
				$tempVal = rand(1, 10);
			} else if  ($rewardType > 110000 && $rewardType < 120000 ) {
				$tempVal = rand(1, 5);
			}  else {
				$tempVal = 0;
			}
			$rewardType  = $rewardType -$tempVal;
		} else {
			$this->logger->logError(__FUNCTION__.' :dont do that FAIL rewardType : '.$rewardType);
		}

		return $rewardType;
	}

	function getItemCost($itemValue, $resourceType, $discountRate) {
		switch($resourceType) {
			case Type_Gold:
				$valueRate = 2000;
				break;
			case Type_Jewel:
				$valueRate = 1;
				break;
			case Type_Powder:
				$valueRate = 40;
				break;
			default:
				return 100000000;
		}
		$discountValue = (100 - $discountRate) / 100;

		$itemCost = $itemValue * $valueRate * $discountValue;
		return intval($itemCost);
	}

	function BuyGShop($param){
		$resultFail['ResultCode']  = 300;
		$userId = $param["userId"];

		$index = $param['index'];

		$recvItemCost = (int)$param["itemCost"];
		$recvResourceType = (int)$param["resourceType"];

		// shop open check 
		$db = new DB();
		$sql = "
			SELECT
            shopId,tierWeight,
            TIMESTAMPDIFF(SECOND, startTime, NOW()) as startTime,
            TIMESTAMPDIFF(SECOND, NOW(), endTime) as endTime
            FROM frdGuerrillaShop
            WHERE
            TIMESTAMPDIFF(SECOND, startTime, NOW()) > 0 AND
            TIMESTAMPDIFF(SECOND, NOW(), endTime) > 0

				";
		$db -> prepare($sql);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$resultFail['ResultCode']  = 700;
			$resultFail['Data'] = null;
			return $resultFail;
		}

		$shopId = $row['shopId'];

		$sql = "select * from frdGuerrillaShopUserData where userId = :userId AND shopId = :shopId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':shopId', $shopId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$resultFail['ResultCode']  = 700;
			$resultFail['Data'] = null;
			return $resultFail;
		}

		$itemIds = $row['itemIds'];
		$tempIds = explode(',', $itemIds);	

		$resourceTypes = $row['resourceTypes'];
		$tempType = explode(',', $resourceTypes);	

		$itemCosts = $row['itemCosts'];
		$tempCost = explode(',', $itemCosts);	

		$itemCounts = $row['itemCounts'];
		$tempCounts = explode(',', $itemCounts);	

		$isBuy = $row['isBuy'];
		$tempBuy = explode(',', $isBuy);	

		if ( ($tempType[$index] != $recvResourceType) ||
			 ($tempCost[$index] != $recvItemCost) )
		{
			$result['ResultCode']  = 702;
			$result['Data'] = null;
			return $result;
		}

		if ($tempBuy[$index] == 1)  {
			// 이미 구입한 항목 
			$result['ResultCode']  = 701;
			$result['Data'] = null;
			return $result;
		}
		$tempBuy[$index] = 1;

		$price = $recvItemCost;

		$AssetManager = new AssetManager();
		switch($recvResourceType) {
			case 5000:
				$AssetResult = $AssetManager->useGold($db, $userId, $price);
				if ($AssetResult['ResultCode'] != 100 ) {
					$resultFail['ResultCode'] = $AssetRestult['ResultCode'];
					return $resultFail;
				}
				$gold['curGold'] = $AssetResult['gold'];
				$gold['addGold'] = 0 - $price;
				break;
			case 5004:
				$AssetResult = $AssetManager->useJewel($db, $userId, $price);
				if ($AssetResult['ResultCode'] != 100 ) {
					$resultFail['ResultCode'] = $AssetRestult['ResultCode'];
					return $resultFail;
				}
				$jewel['curJewel'] = $AssetResult['jewel'];
				$jewel['addJewel'] = 0 - $price;
				break;
			case 5003:
				$AssetResult = $AssetManager->useHeart($db, $userId, $price);
				if ($AssetResult['ResultCode'] != 100 ) {
					$resultFail['ResultCode'] = $AssetRestult['ResultCode'];
					return $resultFail;
				}
				$heart['curHeart'] = $AssetResult['curHeart'];
				$heart['addHeart'] = 0 - $price;
				$heart['maxHeart'] = $AssetResult['maxHeart'];
				$heart['passTime'] = $AssetResult['passTime'];
				break;
			case 5006:
				$AssetResult = $AssetManager->usePowder($db, $userId, $price);
				if ($AssetResult['ResultCode'] != 100 ) {
					$resultFail['ResultCode'] = $AssetRestult['ResultCode'];
					return $resultFail;
				}
				$powder['curPowder'] = $AssetResult['powder'];
				$powder['addPowder'] = 0 - $price;
				break;

			default :
				$this->logger->logError(__FUNCTION__.' : FAIL userId : '.$userId.' type  2: '.$priceType);
				$resultFail['ResultCode']  = 702;
				$resultFail['Data'] = null;
				return $resultFail;
		}


		if (isset($jewel)) {
			$temp['use']['jewel'] = $jewel;
		}
		if (isset($gold)) {
			$temp['use']['gold'] = $gold;
		}
		if (isset($heart)) {
			$temp['use']['heart'] = $heart;
		}
		if (isset($powder)) {
			$temp['use']['powder'] = $powder;
		}

		$rewardId = null;
		$rewardId[] = $tempIds[$index];	
		$rewardCount = null;
		$rewardCount[] = $tempCounts[$index];	

		if ( $tempIds[$index] >= 5000 && $tempIds[$index] < 5007 ) {
			$addResult = $this->addAssetItem($db, $userId, $tempIds[$index], $tempCounts[$index], $AssetManager);
			$data['rewards'] = $addResult;
		} else {
			$addResult = $this->addItem($db, $userId, $rewardId, $rewardCount);
			$data['rewards'] = $addResult['data'];
		}

		$resultBuy = null;
		for($tempI=0; $tempI < count($tempBuy); $tempI++) {
			if ($tempI == 0) {
				$resultBuy = $tempBuy[$tempI];
			} else {
				$resultBuy = $resultBuy.','.$tempBuy[$tempI];
			}
		}	

		$sql = "UPDATE frdGuerrillaShopUserData  SET
			isBuy = :isBuy, update_date = now() 
			where userId = :userId and shopId = :shopId
			";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':shopId', $shopId, PDO::PARAM_INT);
		$db -> bindValue(':isBuy', $resultBuy, PDO::PARAM_STR);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			return $resultFail;
		}


		$Data = $data;

		$Data['use'] = $temp['use'];

		$result['Protocol'] = 'ResBuyGShop';;
		$result['ResultCode'] = 100;
		$result['Data'] = $Data;

		return $result;
	}


	function ResetGShop($param){
		$resultFail['ResultCode']  = 300;

		$userId = $param["userId"];

		// shop open check 
		$db = new DB();

		$sql = "SELECT
			shopId,tierWeight,
			TIMESTAMPDIFF(SECOND, startTime, NOW()) as startTime,
			TIMESTAMPDIFF(SECOND, NOW(), endTime) as endTime
			FROM frdGuerrillaShop
			WHERE
			TIMESTAMPDIFF(SECOND, startTime, NOW()) > 0 AND
			TIMESTAMPDIFF(SECOND, NOW(), endTime) > 0
			";
		$db -> prepare($sql);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$resultFail['ResultCode']  = 700;
			$resultFail['Data'] = null;
			return $resultFail;
		}

		$shopId = $row['shopId'];
		$endTime = $row['endTime'];
		$tierWeight = $row['tierWeight'];

		$sql = "
			select * from frdGuerrillaShopUserData where userId = $userId AND shopId = $shopId
			";
		$db->prepare($sql);
		$db->execute();
		$row = $db->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {

		}
		$resetCount = $row['resetCount'];

		switch ($resetCount) {
			case 0:          
				$price = 50;
				break;
			case 1:         
				$price = 100;
				break;
			case 2:          
				$price = 200;
				break;
			case 3: 
			default:  
				$price = 300;
				break;
		}

		$AssetManager = new AssetManager();
		$AssetResult = $AssetManager->useJewel($db, $userId, $price);
		if ($AssetResult['ResultCode'] != 100 ) {
			return $resultFail;
		}
		$jewel['curJewel'] = $AssetResult['jewel'];
		$jewel['addJewel'] = 0 - $price;

		$itemList = $this->getGShopItemList($db, $userId, $tierWeight);
		$resetCount++;

		$shopItemIds = $itemList['shopItemIds'];
		$itemIds = $itemList['itemIds'];
		$itemCounts = $itemList['itemCounts'];
		$discountRates = $itemList['discountRates'];
		$resourceTypes = $itemList['resourceTypes'];
		$itemCosts = $itemList['itemCost'];
		$isBuy = $itemList['isBuy'];

		$tempShopIds = null;
		for($tempI=0; $tempI < count($shopItemIds); $tempI++) {
			if ($tempShopIds == null) {
				$tempShopIds = $shopItemIds[$tempI];
			} else {
				$tempShopIds = $tempShopIds.','.$shopItemIds[$tempI];
			}
		}	
		$tempIds = null;
		for($tempI=0; $tempI < count($itemIds); $tempI++) {
			if ($tempIds == null) {
				$tempIds = $itemIds[$tempI];
			} else {
				$tempIds = $tempIds.','.$itemIds[$tempI];
			}
		}	

		$tempCnt = null;
		for($tempI=0; $tempI < count($itemCounts); $tempI++) {
			if ($tempCnt == null) {
				$tempCnt = $itemCounts[$tempI];
			} else {
				$tempCnt = $tempCnt.','.$itemCounts[$tempI];
			}
		}	

		$tempRate = null;
		for($tempI=0; $tempI < count($discountRates); $tempI++) {
			if ($tempRate == null) {
				$tempRate = $discountRates[$tempI];
			} else {
				$tempRate = $tempRate.','.$discountRates[$tempI];
			}
		}	

		$tempCost = null;
		for($tempI=0; $tempI < count($itemCosts); $tempI++) {
			if ($tempCost == null) {
				$tempCost = $itemCosts[$tempI];
			} else {
				$tempCost = $tempCost.','.$itemCosts[$tempI];
			}
		}	

		$tempBuy = null;
		for($tempI=0; $tempI < count($isBuy); $tempI++) {
			if ($tempI == 0) {
				$tempBuy = $isBuy[$tempI];
			} else {
				$tempBuy = $tempBuy.','.$isBuy[$tempI];
			}
		}	

		$tempType = null;
		for($tempI=0; $tempI < count($resourceTypes); $tempI++) {
			if ($tempType == null) {
				$tempType = $resourceTypes[$tempI];
			} else {
				$tempType = $tempType.','.$resourceTypes[$tempI];
			}
		}	

		$sql = "UPDATE frdGuerrillaShopUserData  SET
			resetCount = resetCount+1, 
			shopItemIds = :shopItemIds, 
			itemIds = :itemIds, 
			itemCounts = :itemCounts,
			resourceTypes = :resourceTypes, discountRates = :discountRates, 
			itemCosts = :itemCosts, isBuy = :isBuy, update_date = now() 
			where userId = :userId and shopId = :shopId
			";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':shopId', $shopId, PDO::PARAM_INT);
		$db -> bindValue(':shopItemIds', $tempShopIds, PDO::PARAM_STR);
		$db -> bindValue(':itemIds', $tempIds, PDO::PARAM_STR);
		$db -> bindValue(':itemCounts', $tempCnt, PDO::PARAM_STR);
		$db -> bindValue(':resourceTypes', $tempType, PDO::PARAM_STR);
		$db -> bindValue(':discountRates', $tempRate, PDO::PARAM_STR);
		$db -> bindValue(':itemCosts', $tempCost, PDO::PARAM_STR);
		$db -> bindValue(':isBuy', $tempBuy, PDO::PARAM_STR);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			return $resultFail;
		}
		$itemList = null;
		$itemList['itemIds'] = $tempIds;
		$itemList['itemCounts'] = $tempCnt;
		$itemList['resourceTypes'] = $tempType;
		$itemList['itemCosts'] = $tempCost;
		$itemList['discountRates'] = $tempRate;
		$itemList['isBuy'] = $tempBuy;

		$data["endTime"] = $endTime;
		$data["resetCount"] = $resetCount;

		$data["itemList"] = $itemList;

		$data['use']['jewel'] = $jewel;

		$result['Protocol'] = 'ResResetGShop';;
		$result['ResultCode'] = 100;

		$result['Data'] = $data;
	
		return $result;	
	}

	function addItem ($db, $userId, $typeList, $amount) {

		$AddManager = new AddManager();

		$chestCount  = count($typeList);
		for ( $idx = 0; $idx < $chestCount; $idx++ ) {
			$rewardType = (int)$typeList[$idx];
			$rewardCnt = (int)$amount[$idx];
				
			if ( $rewardType < 1000 ) {        //고정 캐릭터 소울스톤 지급
				if ( $rewardType > 500 ) {
					$key_name = $rewardType - 500;
					$apc_key = 'CharacIndex_'.$key_name;
					$cd = apc_fetch($apc_key);
					$charMin = $cd[0];
					$charMax = $cd[1];
					$rewardType = rand($charMin, $charMax);
					$this->logger->logError(__FUNCTION__.': userId : '.$userId.' rewardType: '.$rewardType);
					$this->logger->logError(__FUNCTION__.': userId : '.$userId.' min: '.$charMin);
					$this->logger->logError(__FUNCTION__.': userId : '.$userId.' max: '.$charMax);
				}				

				$CharResult = $AddManager->setAddHero($db, $userId, $rewardType, $rewardCnt);
				if ($CharResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.': userId : '.$userId.' rewardType: '.$rewardType);
					break;
				}
				$charTemp = null;
				$charTemp['charTableId'] = $CharResult['charTableId'];
				$charTemp['charId'] = $CharResult['charId'];
				$charTemp['exp'] = $CharResult['curExp'];

				$charData[] = $charTemp;
			} else if ( $rewardType < 2000 ) {    //보구 지급
				if ( $rewardType > 1500 ) {
					$array_pos = $rewardType - 1500;
					$apc_key = 'WeaponInform_WeaponMinIdx';
					$wd1 = apc_fetch($apc_key);
					$minTemp = explode('/', $wd1[1]);
					$weaponMin = $minTemp[$array_pos];

					$apc_key = 'WeaponInform_WeaponMaxIdx';
					$wd2 = apc_fetch($apc_key);
					$maxTemp = explode('/', $wd2[1]);
					$weaponMax = $maxTemp[$array_pos];

					$rewardType = rand($weaponMin, $weaponMax);
					$rewardType = $rewardType + 1000;
					$this->logger->logError(__FUNCTION__.__LINE__.': FAIL weapon min : '.$weaponMin);
					$this->logger->logError(__FUNCTION__.__LINE__.': FAIL weapon max : '.$weaponMax);

				}
				$rewardType = $rewardType - 1000;
				$WeaponResult = $AddManager->addWeapon($db, $userId, $rewardType, $rewardCount);
				if ($WeaponResult['ResultCode'] != 100) {
					$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
					break;
				}
				$weaponTemp = null;
				$weaponTemp['weaponTableId'] = $WeaponResult['weaponTableId'];
				$weaponTemp['weaponId'] = $WeaponResult['weaponId'];
				$weaponTemp['statIds'] = $WeaponResult['statIds'];
				$weaponTemp['statVals'] = $WeaponResult['statVals'];
				$weaponTemp['exp'] = $WeaponResult['exp'];

				$weaponData[] = $weaponTemp;

			} else if ( $rewardType  < 12000 ) {  //마법 지급
				if ( $rewardType > 10500 ) {
					$key_name = $rewardType - 10500;
					$apc_key = 'MagicPowderValue_'.$key_name;
					$md = apc_fetch($apc_key);
					$magicMin = $md[2] + 10000;
					$magicMax = $md[3] + 10000;

					$rewardType = rand($magicMin, $magicMax);
				}
				$rewardType = $rewardType - 10000;
				$MagicResult = $AddManager->addMagic($db, $userId, $rewardType);
				if ($MagicResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
					break;
				}
				$magicTemp = null;
				$magicTemp['magicTableId'] = $MagicResult['magicTableId'];
				$magicTemp['magicId'] = $MagicResult['magicId'];
				$magicTemp['exp'] = 0;

				$magicData[] = $magicTemp;
			} else if ( $rewardType >= 99999 && $rewardType < 200000 ) {   //아이템 지급
				$itemResult = $AddManager->addItem($db, $userId, $rewardType,$rewardCnt);
				if ($itemResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
					break;
				}
				$itemTemp = null;
				$itemTemp['itemTableId'] = $itemResult['itemTableId'];
				$itemTemp['itemId'] = $itemResult['itemId'];
				$itemTemp['cur'] = $itemResult['cnt'];
				$itemTemp['add'] = $rewardCnt;

				$itemData[] = $itemTemp;

			} else {
				$this->logger->logError(__FUNCTION__.' :dont do that FAIL userId : '.$userId);
			}

		}
		$dataTotal = null;

        if (isset($weaponData)) {
            $dataTotal['weapons'] = $weaponData;
        }
        if (isset($magicData)) {
            $dataTotal['magics'] = $magicData;
        }
        if (isset($itemData)) {
            $dataTotal['items'] = $itemData;
        }
        if (isset($charData)) {
            $dataTotal['characs'] = $charData;
        }

		$result['data'] = $dataTotal;	
		return $result;
	}
		
	function addAssetItem($db, $userId, $itemId, $rewardCount, $AssetManager) {
		$total = null;
		switch ($itemId) {
			case 5000: // 골드 지급
				$gold_result = $AssetManager->addGold($db, $userId, $rewardCount);
				if ($gold_result['ResultCode'] != 100 ) {
					return $gold_result;
				}
				$data["curGold"] = $gold_result['gold'];
				$data["addGold"] = $rewardCount;
				$total['gold'] = $data;
				break;
			case 5002: //티켓 지급
				$AssetResult = $AssetManager->addTicket($db, $userId, $rewardCount);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $AssetResult;
				}
				$data["curTicket"] = $AssetResult['curTicket'];
				$data["addTicket"] = $rewardCount;
				$total['ticket'] = $data;
				break;
			case 5003: //행동력 지급
				$AssetResult = $AssetManager->addHeart($db, $userId, $rewardCount);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $AssetResult;
				}
				$data["curHeart"] = $AssetResult['curHeart'];
				$data["addHeart"] = $rewardCount;
				$data["maxHeart"] = $AssetResult['maxHeart'];
				$data["passTime"] = $AssetResult['passTime'];
				$total['heart'] = $data;
				break;
			case 5004: // 보석 지급
				$AssetResult = $AssetManager->addJewel($db, $userId, $rewardCount);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $AssetResult;
				}
				$data["curJewel"] = $AssetResult['jewel'];
				$data["addJewel"] = $rewardCount;
				$total['jewel'] = $data;
				break;
			case 5005: // 메달 지급
				$AssetResult = $AssetManager->addMedal($db, $userId, $rewardCount);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $AssetResult;
				}
				$data["curMedal"] = $AssetResult['skillPoint'];
				$data["addMedal"] = $rewardCount;
				$total['medal'] = $data;
				break;
			case 5006: // 메달 지급
				$AssetResult = $AssetManager->addPowder($db, $userId, $rewardCount);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $AssetResult;
				}
				$data["curPowder"] = $AssetResult['powder'];
				$data["addPowder"] = $rewardCount;
				$total['powder'] = $data;
				break;

		}

		return $total;
	}
}
?>
