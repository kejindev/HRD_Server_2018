<?php
require_once './classes/AbillEffectReward.php';
require_once "./classes/EquipWeaponEffect.php";
require_once './classes/SkillEffect.php';
require_once './classes/EventManager.php';
require_once './classes/RewardManager.php';
require_once './common/define.php';
require_once './lib/Logger.php';

class GetRewardManager {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	//public
	function GetReward(
		&$result,
		&$db, 
		$userId,
		$userLevel,
		$stageId,
		$lastWave,
		$isClear,
		$seconds,
		$curRewardIds,
		$difficulty,
		$isClimb, 
		$firstReward,
		$weaponIdxs,
		$subWeaponIdxs
		) {

		$EventManager = new EventManager();

		$bonusData = array();
		$bonusData['percent']['type'][(string)Type_Medal] += $difficulty*0.1;
	
		// ABIL ADD BONUS
		$AbillEffectReward = new AbillEffectReward();
		$abEfResult = $AbillEffectReward->getAbillEffectReward($db, $userId);

		$bonusArr = array();	
		$bonusData['percent']['type'][(string)Type_Medal] += $abEfResult['medalGetPer']/100;
		if ($abEfResult['medalAddPer']) {
			$bonusData['amount_ab']['type'][(string)Type_Medal] += $abEfResult['medalAddPer']/100000;
		}
		if ($abEfResult['expAddPer']) {
			$bonusData['amount_ab']['type'][(string)Type_Exp] += $abEfResult['expAddPer']/100000;
		}
		if ($abEfResult['addGold']) {
			$bonusData['amount_ab']['type'][(string)Type_Gold] += $abEfResult['addGold']/100000;
		} 

		if ( is_null($EquipWeaponEffect) )
			$EquipWeaponEffect = new EquipWeaponEffect();

		if ($difficulty > -1) {
			$weaponBonusAdd = $EquipWeaponEffect->getEquipWeaponEffect($db, $userId);
			$wData	= $weaponBonusAdd['Data'];

			$bonusData['amount_wpsp']['type'][(string)Type_Gold] += $wData['addGold']/100;
			$bonusData['amount_wpsp']['type'][(string)Type_Exp] += $wData['addExp']/100;
			$bonusData['percent_wpsp']['type'][(string)Type_Medal] += $wData['addMedal']*10;	//addMedal -> getMedal로 명명 필요
		}

		// 부스트 아이템 체크
		$sql = "SELECT resourceType, mulPercent, maxUserLv FROM frdBoost where userId = :userId AND now() < endTime";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$resourceType = (int)$row['resourceType'];
			$result['boostCond'][] = $resourceType.", ".$userLevel.", ".(int)$row['maxUserLv'];
			if ( $resourceType >= 5000 && $resourceType < 5010 && $userLevel <= (int)$row['maxUserLv'] ) {
				$bonusData['amount_boost']['type'][(string)$resourceType] += $row['mulPercent']*0.01;
			}
		}

		// SKILL (MEDAL) ADD BONUS
		$SkillEffect = new SkillEffect();
		$SkillBonusAdd = $SkillEffect->getSkillEffect($db, $userId, $isClear);

		// WEAPON + SKILL ADD BONUS
		$bonusData['amount_wpsp']['type'][(string)Type_Exp] += $SkillBonusAdd['expPer']/100000;
		$bonusData['percent_wpsp']['type'][(string)Type_Ticket] += $SkillBonusAdd['ticketPer'];

		if (rand(0,99999) < ($SkillBonusAdd['medalDoublePer']))	$bonusData['amount_3']['type'][(string)Type_Medal] += 1.0;
		if ( $SkillBonusAdd['medalDouble'] )					$bonusData['amount_4']['type'][(string)Type_Medal] += 1.0;
		if ( $SkillBonusAdd['runeDouble'] )						$bonusData['runeAddPer'] += 100000;
		if ( $SkillBonusAdd['questItemDouble'] )				$bonusData['questItemAddPer'] += 100000;
		if ( $SkillBonusAdd['clearAssetDouble'] ) {
																$bonusData['amount_5']['type']['0_120000'] += 1.0;
																$bonusData['questItemAddPer'] += 100000;
																$bonusData['runeAddPer'] += 100000;
		}
		if ( $SkillBonusAdd['soulStoneDouble'] )				$bonusData['amount_wpsp']['type']['0_506'] += 1.0;
		

		$rewardResultArray = $this->setStageRewardArray($bonusData,$stageId,$lastWave,$isClear,$seconds,$curRewardIds,$difficulty,$isClimb);
		if ( $rewardResultArray['bonusData'] ) {
			$result['bonusData'] = $rewardResultArray['bonusData'];
			$this->logger->logError(__FUNCTION__.' : Event :'.$userId.' str :'.json_encode($rewardResultArray['event']));
		}

		$resultRewardType = array();
		$resultRewardCount = array();
		$resultRewardType = $rewardResultArray['resultRewardType'];
		$resultRewardCount = $rewardResultArray['resultRewardCount'];
		$needHeart = $rewardResultArray['needHeart'];
		if ( $EventManager->IsActiveEvent(EVENT_HALF_HEART) ) {
			if ( $stageId >= 500 && $stageId < 600 ) {
				$needHeart = round($needHeart/2);
			}
		}

		if ($firstReward) {
			if ($stageId == 1) {
				$resultRewardType[] = 10501;
				$resultRewardCount[] = 1;
			}
			$resultRewardType[] = $firstReward['resultRewardType'];
			$resultRewardCount[] = $firstReward['resultRewardCount'];
		}
		

		// 이벤트 체크 로직 
		$nowDay = date('ymdH', time()); 

		// 이벤트 기간 동안 제한된 횟수 만큼 클리어 보너스를 제공   
		// 리턴 true false
		// $clearEventResult = $EventManager->getEventClearCntBonus($db, $userId,$stageId);
		// if ($clearEventResult['eventCheck']) {
		// 	$totalData['eventMedalAdd'] = $clearEventResult['eventMedalAdd'];
		// }

		// Rune & QuestItem
		if ($isClear && !$isClimb) {
			$runeAddPer = $rewardResultArray['runeAddPer']+$abEfResult['runeAddPer'];
			$runeGetPer = (1+$rewardResultArray['runeGetPer']+$abEfResult['runeGetPer']/100000);
			$questItemAddPer = $rewardResultArray['questItemAddPer']+$abEfResult['questItemAddPer'];
			$questItemGetPer = (1+$rewardResultArray['questItemGetPer']+$abEfResult['questItemGetPer']/100000);
			$result['bonusData']['runeAddPer'] = $runeAddPer;
			$result['bonusData']['runeGetPer'] = $runeGetPer;
			$result['bonusData']['questItemAddPer'] = $questItemAddPer;
			$result['bonusData']['questItemGetPer'] = $questItemGetPer;

			$diffLevel = $stageId%100;
			$runeResult = $this->setReaward_Rune_QuestItem($stageId, $needHeart, $diffLevel, $runeGetPer, $runeAddPer, $questItemGetPer, $questItemAddPer);
			$result['bonusData']['test'] = $runeResult['test'];
			$runeRewardType = $runeResult['resultRewardType'];
			$runeRewardCount = $runeResult['resultRewardCount'];
			if (count($runeRewardCount)) {
				$resultRewardType = array_merge($resultRewardType, $runeRewardType);
				$resultRewardCount = array_merge($resultRewardCount, $runeRewardCount);
			}
		}
		

		// type 1100 = 특수퀘스트 
		$sql = "SELECT type, subType, val FROM frdEffectForPreCheckRewards WHERE userId=:userId AND type = 1100 AND isSuccess=1";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$type = (int)$row["type"];
			$subType = (int)$row["subType"];
			$val = (int)$row["val"];

			$resultRewardType[] = 5004;
			$resultRewardCount[] = $val;

			$sql = "UPDATE frdEffectForPreCheckRewards 
				SET val=0, isSuccess=0 
				WHERE userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql : '.$sql);
				return $resultFail;
			}
		}

		// clearJewel10 Add Check 
        if ($SkillBonusAdd['clearJewel10']) {
			$resultRewardType[] = 5004;
			$resultRewardCount[] = (int)($needHeart/3);
		}

		//  Add Check 
		if ($SkillBonusAdd['clearHeart30PerReturn']) {
			$resultRewardType[] = 5003;
			$resultRewardCount[] = (int)($needHeart/3);
		}

		if ( $difficulty >= 0 ) {
			if ( empty($weaponIdxs) ) {
				$Weapon_Main_Sub = $EquipWeaponEffect->getEquipWeaponForEffect($db,$userId);
				$WeaponResult = $Weapon_Main_Sub['main'];
				if ($WeaponResult) {
					foreach($WeaponResult as $WR) {
						$wTemp[] = (int)$WR["weaponId"];
						$weaponIdxs[] = (int)$WR["weaponTableId"];
						$wTemp[] = (int)$WR["statId0"];
						$wTemp[] = (int)$WR["statId1"];
						$wTemp[] = (int)$WR["statId2"];
						$wTemp[] = (int)$WR["statId3"];
						$wTemp[] = (int)$WR["statVal0"];
						$wTemp[] = (int)$WR["statVal1"];
						$wTemp[] = (int)$WR["statVal2"];
						$wTemp[] = (int)$WR["statVal3"];
					}
				}
				$subWeaponIdxs = null;
				$subResult = $Weapon_Main_Sub['sub'];
				if ($subResult) {
					foreach($subResult as $SR) {
						$subWeaponIdxs[] = (int)$SR["weaponTableId"];
					}
				}
			}
		}

		if ( $isClear ) {
			$EventManager->get369Bonus($db, $userId, $stageId, $resultRewardType, $resultRewardCount);
			$EventManager->getDailyBonus_ClearCount($db, $userId, $stageId, $resultRewardType, $resultRewardCount);
		}

		$RewardManager = new RewardManager();
		$rew = $RewardManager->getReward($db, $resultRewardType, $resultRewardCount, $userId,$weaponIdxs, $subWeaponIdxs);
		if ( $rew['ResultCode'] != 100 ) {
			$this -> logger -> logError(__FUNCTION__.' : fail reawrd userId :' . $userId );
			return $resultFail;
		}
		return $rew['Data'];
	}



	//private
	function IsSuccess($conditionType, $lastWave, $std, $seconds, $difficulty, $isClear) {

		switch($conditionType) {
			case 0:
				$value = (int)$seconds;
				if ( $std < $value || !$isClear )
					return false;
				else
					return true;

			case 4:
				if ( $lastWave >= $std )
					return true;
				else
					return false;

			case 5:
				if ( $isClear > 0 )
					return true;
				else
					return false;

			case 6:
				return true;

			case 10:      // Life difficulty 0
				if ( $difficulty >= 0 && $isClear > 0 )
					return true;
				return false;

			case 11:      // Life difficulty 1
				if ( $difficulty >= 1 && $isClear > 0 )
					return true;
				return false;

			case 12:      // Life difficulty 2
				if ( $difficulty >= 2 && $isClear > 0 )
					return true;
				return false;

			case 13:      // Life difficulty 3
				if ( $difficulty >= 3 && $isClear > 0 )
					return true;
				return false;

			default:
				return false;
		}
	}

	//private
	function setStageRewardArray(&$bonusData, $stageId, $lastWave, $isClear,$seconds, $curRewardIds, $difficulty, $isClimb) {
		// APC DATA GET 
		$apc_key = 'stageDataTotal_'.$stageId;
		$stageData = apc_fetch($apc_key);
#0 StageId
#1 TotalWave/MaxMon/monCount/intervalTimer
#2 StartMamber
#3 MakeChaPosition
#4 NeedHeart
#5 MonWay
#6 ClearType
#7 ClearTemp
#8 RewardId
#9 RewardAmount
#10 RewardPercent
#11 NeedClearIdx
#12 GimicType
#13 Gimic
		$resultRewardType = null;
		$resultRewardCount = null;

		$arrHeart= explode('/',$stageData[4]);
		$needHeart = $arrHeart[0];
		
		$clearType = $stageData[6];
		$clearConditionType = explode('/',$clearType);
		
		$clearTemp = $stageData[7];
		$clearConditionVal = explode('/',$clearTemp);

		$rewardIds = $stageData[8];
		$rewardId = explode('/',$rewardIds);

		$rewardCounts = $stageData[9];
		$rewardCount = explode('/',$rewardCounts);

		$rewardPSs = $stageData[10];
		$rewardPercent = explode('/',$rewardPSs);

		$NeedClearIdx = $stageData[11];
		$clearConditionIdx = explode('/',$NeedClearIdx);

		// reward ticket Per ADD
		$rewardId[] = 5002;
		$rewardCount[] = rand(1,3);
		$rewardPercent[] = 40;
		$clearConditionIdx[] = 0;	//clear & difficulty>0

		if ($isClimb) {
		//	$curRewardIds = array();
		}
		else {
			$curRewardIds = str_replace('[','',$curRewardIds);
			$curRewardIds = str_replace(']','',$curRewardIds);

			$curRewardIds = explode(',',$curRewardIds);
			// reward ticket Per ADD
			$curRewardIds[] = 5002;
		}

		$isSuccess = null;
		$length = count($clearConditionType);
		for ( $i=0; $i<$length; $i++ ) {
			$isSuccess[$i] = $this->IsSuccess($clearConditionType[$i], $lastWave, $clearConditionVal[$i], $seconds, $difficulty,$isClear);
		}
		$length = count($rewardId);

		$rewardItemIdxPointer=0;
		$resultRewardType = null;

		$EventManager = new EventManager();


		//event
	 	$hottimeEventResult = $EventManager->getHottimeBonus($bonusData, $stageId);
		if ($hottimeEventResult['eventCheck']) {
			$result['bonusData']['eventCheck'] = $hottimeEventResult;
		}

		$result['questItemGetPer'] = 0;
		$result['questItemAddPer'] = 0;
		$result['runeGetPer'] = 0;
		$result['runeAddPer'] = 0;

		$isAction = false;
		$resultLen = count($rewardId);
		foreach ($bonusData as $key => $var) {
			switch ($key) {
				case 'questItemGetPer':case 'questItemAddPer':
				case 'runeGetPer':case 'runeAddPer':
					$result['bonusData'][$key][] = $var;
					$result[$key] += $var;
					break;

				default:
					$typeLen = count($var['type']);
					$types = $var['type'];
					foreach ( $types as $subKey => $amount ) {
						if ( $amount == 0 )
							continue;

						$pieces = explode ("_", $subKey);
						$minType = (int)$pieces[0];
						$maxType = (int)(count($pieces) > 1 ? $pieces[1] : $minType);
					    for ( $ii=0; $ii<$resultLen; $ii++ ) {
					    	
					    	$rewardType = (int)$rewardId[$ii];
					    	if ( $rewardType >= $minType && $rewardType <= $maxType ) {

					    		$isAction = true;
					    		//only percent or amount
					    		if ( strpos($key, "amount") !== false ) {
				    				$result['bonusData']['preCnt'][] = "Type=".$rewardType.", Cnt=".$rewardCount[$ii].", Amt=".$amount;
				    				$rewardCount[$ii] += ($rewardCount[$ii] * $amount);
				    				$result['bonusData']['curCnt'][] = "Type=".$rewardType.", Cnt=".$rewardCount[$ii];
				    			}
				    			else {
				    				$result['bonusData']['prePer'][] = "Type=".$rewardType.", Per=".$rewardPercent[$ii].", Amt=".$amount;
					    			$rewardPercent[$ii] += ($rewardPercent[$ii] * $amount);
					    			$result['bonusData']['curPer'][] = "Type=".$rewardType.", Per=".$rewardPercent[$ii];
					    		}
					    		
					    	}

					    }
					}
					break;
			}
		}
		if ( $isAction ) {
			for ( $ii=0; $ii<$resultLen; $ii++ )
				$rewardCount[$ii] = round($rewardCount[$ii]);
		}
		

		for ( $i=0; $i<$length; $i++ ) {
			$conditionIdx = $clearConditionIdx[$i];
			if (!$isSuccess[$conditionIdx])
				continue;	

			$rewardType = $rewardId[$i];
			$rewardCnt = $rewardCount[$i];

			$tempSuc = rand(0,999);
			$tempDiff = $rewardPercent[$i];

			if ($tempSuc > $tempDiff ) {
				continue;	
			}

			if ( $rewardType > 500 && $rewardType < 507 ) { //random soulstone
				//if (isset($curRewardIds)) {
				if ($curRewardIds[$i]) {
					$rewardType = $curRewardIds[$i];
				}
				if ( is_null($rewardType) ) {
					$key_name = $rewardType - 500;
					$apc_key = 'CharacIndex_'.$key_name;
					$charData = apc_fetch($apc_key);
					$charMin = $charData[0];
					$charMax = $charData[1];
					$rewardType = rand($charMin, $charMax);
				}
			}
			else if ( $rewardType > 1500 && $rewardType < 1508 ) {  //random weapon
				if ($curRewardIds[$i]) {
					$rewardType = $curRewardIds[$i];
				}
				if ( is_null($rewardType) ) {

					$array_pos = $rewardType - 1500;
					$apc_key = 'WeaponInform_WeaponMinIdx';
					$weaponData = apc_fetch($apc_key);
					$minTemp = explode('/', $weaponData[1]);
					$weaponMin = $minTemp[$array_pos] + 1000;

					$apc_key = 'WeaponInform_WeaponMaxIdx';
					$weaponData = apc_fetch($apc_key);
					$maxTemp = explode('/', $weaponData[1]);
					$weaponMax = $maxTemp[$array_pos] + 1000;

					while ( $rewardCnt > 1 ) {
						$rewardType = rand($weaponMin, $weaponMax);
						$resultRewardType[] = $rewardType;

						$resultRewardCount[] = 1;
						$rewardCnt--;
					}
					$rewardType = rand($weaponMin, $weaponMax);
				}
			}
			else if ( $rewardType > 10500 && $rewardType < 10508 ) {  //random magic
				if ($curRewardIds[$i]) {
					$rewardType = $curRewardIds[$i];
				}
				if ( is_null($rewardType) ) {
					$key_name = $rewardType - 10500;
					$apc_key = 'MagicPowderValue_'.$key_name;
					$magicData = apc_fetch($apc_key);
					$magicMin = $magicData[2] + 10000;
					$magicMax = $magicData[3] + 10000;

					while ( $rewardCnt > 1 ) {
						$rewardType = rand($magicMin, $magicMax);
						$resultRewardType[] = $rewardType;
						$resultRewardCount[] = 1;
						$rewardCnt--;
					}
					$rewardType = rand($min, $max);
				}
			}
			else if ( $rewardType == 5000 ) {    // gold
				if ( $conditionIdx == 0 ){
					$rewardCnt *= $lastWave;
				}
			}
			else if ( $rewardType == 5001 ) {    // exp
				if ( $conditionIdx == 0 ) {
					$rewardCnt *= $lastWave;
				}
			}
			if ( @in_array($rewardType, $resultRewardType)
					&& !($rewardType>=1000&&$rewardType<2000) 
					&& !($rewardType>=10000&&$rewardType<11000)) {
				$tempIdx = array_search($rewardType, $resultRewardType);
				$resultRewardCount[$tempIdx] += $rewardCnt;
			} else {
				$resultRewardType[] = $rewardType;
				$resultRewardCount[] = $rewardCnt;
			}
		}
		$result['resultRewardType'] = $resultRewardType;
		$result['resultRewardCount'] = $resultRewardCount;
		$result['needHeart'] = $needHeart;
		return $result;
	}

	//private
	function setReaward_Rune_QuestItem($stageId, $needHeart, $diffLevel, $runeGetPer, $runeAddPer, $questItemGetPer, $questItemAddPer) {
		
		$questCountVal = 1;
		$runeCountVal = 1;


		$runeCountVal += (int)( ($runeCountVal)*(round($runeAddPer/100000)) );
		$questCountVal += (int)( ($questCountVal)*(round($questItemAddPer/100000)) );

		$resultRewardCount = null;
		$resultRewardType = null;

		if ( ($stageId > 0 && $stageId < 210) || ($stageId > 20000) ) {   //normal, hidden

			if ($stageId > 20000) {
				$mulVal = 2;
				$chapter = (int)($stageId-20000-1);
			} else {
				$mulVal = 1;
				$chapter = (int)(($stageId-1)/10);
			}

			if ($stageId > 20000) {
				if ( rand(0,99) < $needHeart*0.1*$mulVal*$questItemGetPer) { 
					$resultRewardType[] = 106000 + $chapter;   
					$resultRewardCount[] = 1*$questCountVal; 
				}
				if ( rand(0,99) < $needHeart*0.3*$mulVal*$questItemGetPer) { 
					$resultRewardType[] = 105000 + $chapter;   
					$resultRewardCount[] = 1*$questCountVal; 
				}

				if ( rand(0,99) < $needHeart*0.6*$mulVal*$questItemGetPer) { 
					$resultRewardType[] = 104000 + $chapter;   
					$resultRewardCount[] = 1*$questCountVal; 
				}
				if ( rand(0,99) < $needHeart*1.25*$mulVal*$questItemGetPer) { 
					$resultRewardType[] = 103000 + $chapter;   
					$resultRewardCount[] = 1*$questCountVal; 
				}
				if ( rand(0,99) < $needHeart*2*$mulVal*$questItemGetPer) {
					$resultRewardType[] = 102000 + $chapter;   
					$resultRewardCount[] = 1*$questCountVal; 
				}
				if ( rand(0,99) < $needHeart*2.5*$mulVal*$questItemGetPer) { 
					$resultRewardType[] = 101000 + $chapter;   
					$resultRewardCount[] = 1*$questCountVal; 
				}
			}

			if ( rand(0,99) < $needHeart*0.2*$mulVal*$runeGetPer) {
				$resultRewardType[] = 115000 + rand(0,4);   
				$resultRewardCount[] = 1*$runeCountVal; 
			}
			if ( rand(0,99) < $needHeart*0.6*$mulVal*$runeGetPer) { 
				$resultRewardType[] = 114000 + rand(0,4);   
				$resultRewardCount[] = floor(rand(1,3)*$runeCountVal); 
			}

			if ( rand(0,99) < $needHeart*1*$mulVal*$runeGetPer) { 
				$resultRewardType[] = 113000 + rand(0,4);   
				$resultRewardCount[] = floor(rand(2,4)*$runeCountVal); 
			}

			if ( rand(0,99) < $needHeart*1.25*$mulVal*$runeGetPer) { 
				$resultRewardType[] = 112000 + rand(0,4);
				$resultRewardCount[] = floor(rand(2,6)*$runeCountVal); 
			}

			if ( rand(0,99) < $needHeart*1.65*$mulVal*$runeGetPer) { 
				$resultRewardType[] = 111000 + rand(0,4);
				$resultRewardCount[] = floor(rand(3,7)*$runeCountVal); 
			}

			//		return $result;
		} else if ( $stageId > 2000 && $stageId < 4000 ){
			$templeId = (int)round(($stageId-2000) / 100);
			$chapter = $templeId-1;
			$result['test']['diffLevel'] = $diffLevel;
			switch($diffLevel) {
				case 0:
					if ( rand(0,99) < 20*$questItemGetPer) { 
						$resultRewardType[] = 106000 + $chapter;   
						$resultRewardCount[] = 1*$questCountVal; 
					}

					if ( rand(0,99) < 40*$questItemGetPer) { 
						$resultRewardType[] = 105000 + $chapter;   
						$resultRewardCount[] = 1*$questCountVal; 	
					}

					if ( rand(0,99) < 55*$questItemGetPer) {
						$resultRewardType[] = 104000 + $chapter;   
						$resultRewardCount[] = 1*$questCountVal; 
					}

					if ( rand(0,99) < 75*$questItemGetPer) { 
						$resultRewardType[] = 103000 + $chapter;   
						$resultRewardCount[] = 1*$questCountVal; 
					}

					if ( rand(0,99) < 85*$questItemGetPer) {
						$resultRewardType[] = 102000 + $chapter;   
						$resultRewardCount[] = 1*$questCountVal; 
					}

					if ( rand(0,99) < 100*$questItemGetPer) {
						$resultRewardType[] = 101000 + $chapter;   
						$resultRewardCount[] = 1*$questCountVal; 
					}

					if ( rand(0,99) < 5*$runeGetPer) { 
						$resultRewardType[] = 115000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   
						$resultRewardCount[] = floor(rand(1,3)*$runeCountVal); 
					}
					if ( rand(0,99) < 10*$runeGetPer) { 
						$resultRewardType[] = 114000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   
						$resultRewardCount[] = floor(rand(2,6)*$runeCountVal); 
					}
					if ( rand(0,99) < 25*$runeGetPer) { 
						$resultRewardType[] = 113000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   
						$resultRewardCount[] = floor(rand(4,8)*$runeCountVal); 
					}
					if ( rand(0,99) < 33*$runeGetPer) { $resultRewardType[] = 112000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(6,10)*$runeCountVal); }
					if ( rand(0,99) < 50*$runeGetPer) { $resultRewardType[] = 111000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(7,13)*$runeCountVal); }
					break;

				case 1:
					if ( rand(0,99) < 35*$questItemGetPer) { $resultRewardType[] = 106000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 50*$questItemGetPer) { $resultRewardType[] = 105000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 65*$questItemGetPer) { $resultRewardType[] = 104000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 80*$questItemGetPer) { $resultRewardType[] = 103000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 100*$questItemGetPer) { $resultRewardType[] = 102000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 90*$questItemGetPer) { $resultRewardType[] = 101000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }

					if ( rand(0,99) < 10*$runeGetPer) { $resultRewardType[] = 115000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(1,3)*$runeCountVal); }
					if ( rand(0,99) < 15*$runeGetPer) { $resultRewardType[] = 114000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(2,6)*$runeCountVal); }
					if ( rand(0,99) < 32*$runeGetPer) { $resultRewardType[] = 113000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(4,8)*$runeCountVal); }
					if ( rand(0,99) < 41*$runeGetPer) { $resultRewardType[] = 112000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(6,10)*$runeCountVal); }
					if ( rand(0,99) < 60*$runeGetPer) { $resultRewardType[] = 111000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(7,13)*$runeCountVal); }
					break;

				case 2:
					if ( rand(0,99) < 50*$questItemGetPer) { $resultRewardType[] = 106000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 65*$questItemGetPer) { $resultRewardType[] = 105000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 75*$questItemGetPer) { $resultRewardType[] = 104000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 100*$questItemGetPer) { $resultRewardType[] = 103000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 90*$questItemGetPer) { $resultRewardType[] = 102000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 80*$questItemGetPer) { $resultRewardType[] = 101000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }

					if ( rand(0,99) < 15*$runeGetPer) { $resultRewardType[] = 115000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(1,3)*$runeCountVal); }
					if ( rand(0,99) < 20*$runeGetPer) { $resultRewardType[] = 114000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(2,6)*$runeCountVal); }
					if ( rand(0,99) < 39*$runeGetPer) { $resultRewardType[] = 113000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(4,8)*$runeCountVal); }
					if ( rand(0,99) < 49*$runeGetPer) { $resultRewardType[] = 112000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(6,10)*$runeCountVal); }
					if ( rand(0,99) < 70*$runeGetPer) { $resultRewardType[] = 111000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(7,13)*$runeCountVal); }
					break;
				case 3:
					if ( rand(0,99) < 65*$questItemGetPer) { $resultRewardType[] = 106000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 75*$questItemGetPer) { $resultRewardType[] = 105000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 100*$questItemGetPer) { $resultRewardType[] = 104000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 90*$questItemGetPer) { $resultRewardType[] = 103000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 80*$questItemGetPer) { $resultRewardType[] = 102000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 70*$questItemGetPer) { $resultRewardType[] = 101000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }

					if ( rand(0,99) < 20*$runeGetPer) { $resultRewardType[] = 115000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(1,3)*$runeCountVal); }
					if ( rand(0,99) < 25*$runeGetPer) { $resultRewardType[] = 114000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(2,6)*$runeCountVal); }
					if ( rand(0,99) < 46*$runeGetPer) { $resultRewardType[] = 113000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(4,8)*$runeCountVal); }
					if ( rand(0,99) < 57*$runeGetPer) { $resultRewardType[] = 112000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(6,10)*$runeCountVal); }
					if ( rand(0,99) < 80*$runeGetPer) { $resultRewardType[] = 111000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(7,13)*$runeCountVal); }
					break;

				case 4:
					if ( rand(0,99) < 75*$questItemGetPer) { $resultRewardType[] = 106000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 100*$questItemGetPer) { $resultRewardType[] = 105000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 90*$questItemGetPer) { $resultRewardType[] = 104000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 80*$questItemGetPer) { $resultRewardType[] = 103000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 70*$questItemGetPer) { $resultRewardType[] = 102000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 60*$questItemGetPer) { $resultRewardType[] = 101000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 30*$runeGetPer) { $resultRewardType[] = 115000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(1,3)*$runeCountVal); }
					if ( rand(0,99) < 41*$runeGetPer) { $resultRewardType[] = 114000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(2,6)*$runeCountVal); }
					if ( rand(0,99) < 53*$runeGetPer) { $resultRewardType[] = 113000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(4,8)*$runeCountVal); }
					if ( rand(0,99) < 65*$runeGetPer) { $resultRewardType[] = 112000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(6,10)*$runeCountVal); }
					if ( rand(0,99) < 90*$runeGetPer) { $resultRewardType[] = 111000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(7,13)*$runeCountVal); }
					break;

				case 5:
					if ( rand(0,99) < 100*$questItemGetPer) { $resultRewardType[] = 106000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 90*$questItemGetPer) { $resultRewardType[] = 105000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 80*$questItemGetPer) { $resultRewardType[] = 104000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 70*$questItemGetPer) { $resultRewardType[] = 103000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 60*$questItemGetPer) { $resultRewardType[] = 102000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
					if ( rand(0,99) < 50*$questItemGetPer) { $resultRewardType[] = 101000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }

					if ( rand(0,99) < 40*$runeGetPer) { $resultRewardType[] = 115000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(1,3)*$runeCountVal); }
					if ( rand(0,99) < 50*$runeGetPer) { $resultRewardType[] = 114000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(2,6)*$runeCountVal); }
					if ( rand(0,99) < 70*$runeGetPer) { $resultRewardType[] = 113000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(4,8)*$runeCountVal); }
					if ( rand(0,99) < 80*$runeGetPer) { $resultRewardType[] = 112000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(6,10)*$runeCountVal); }
					if ( rand(0,99) < 100*$runeGetPer) { $resultRewardType[] = 111000 + ((rand(0,100)<40) ? $this->GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(7,13)*$runeCountVal); }
					break;

				default:
					break;
			}
		}

		$result['resultRewardCount'] = $resultRewardCount;
		$result['resultRewardType'] = $resultRewardType;

		return $result;
	}

	function GetGoddessAttribute($id) {
        switch($id) {
            case 1:return 4;
            case 2:return 2;
            case 3:return 3;
            case 4:return 1;
            case 5:return 0;
            case 6:return 2;
            case 7:return 1;
            case 8:return 2;
            case 9:return 3;
            case 10:return 4;
            default:return 0;
        }
    }
	
}
?>