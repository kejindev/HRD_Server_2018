<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
require_once './classes/RewardManager.php';
require_once './classes/AbilityManager.php';
require_once './classes/ConsumeManager.php';
require_once './classes/AchieveManager.php';
require_once './classes/LogDBSendManager.php';

class GoddessManager {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function getAbillity($db, $userId, $questIdx, $goddessId, $data) {
		$resultFail['ResultCode'] = 300;

        $privateIdx = ($goddessId-1)*5+$questIdx;

        if ( $privateIdx < 63 ) {
            $bundleIdx = 0;
            $mask = 1 << $privateIdx;
            $bundleName = "bundle0";
        }
        else {
            $bundleIdx = 1;
            $mask = 1 << ($privateIdx-64);
            $bundleName = "bundle1";
        }

		$AbilityManager = new AbilityManager();
		$defultValue = $AbilityManager->GetCantUnlockSlotValue();

        $sql = "SELECT bundle0, bundle1 FROM frdAbillity WHERE userId=:userId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
//			$this -> logger -> logError('getAbiliity : fail SELECT userId :' . $userId );
            if ( $bundleIdx == 0 ) {
                $bundle0 = $mask;
				$bundle1 = 0;
				$sql = "INSERT INTO frdAbillity
					(userId, openedSlot, canUnlockSlot, bundle0, bundle1, usingBundle0,usingBundle1, reg_date )
					values (:userId, 0, :defaultValue, :mask, 0, 0, 0,now())";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':defaultValue', $defaultValue, PDO::PARAM_INT);
				$db -> bindValue(':mask', $mask, PDO::PARAM_STRING);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this -> logger -> logError('getAbillity : fail UPDATE userId :' . $userId );
					$this -> logger -> logError('sql : ' . $sql );
					return $resultFail;
				}
            }
            else {
                $bundle0 = 0;
                $bundle1 = $mask;
				$sql = "INSERT INTO frdAbillity
					(userId, openedSlot, canUnlockSlot, bundle0, bundle1, usingBundle0,usingBundle1, reg_date )
					values (:userId, 0, :defaultValue, 0, :mask, 0, 0,now())";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':defaultValue', $defaultValue, PDO::PARAM_INT);
				$db -> bindValue(':mask', $mask, PDO::PARAM_STRING);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this -> logger -> logError('getAbillity : fail  1 UPDATE userId :' . $userId );
					$this -> logger -> logError('sql : ' . $sql );
					return $resultFail;
				}

            }
		} else {
            $resultMask = $row[$bundleName] | $mask;

            $sql = "UPDATE frdAbillity SET ".$bundleName."=:resultMask where userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':resultMask', $resultMask, PDO::PARAM_STR);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('getAbillity : fail UPDATE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}

			$sql = "SELECT bundle0, bundle1 FROM frdAbillity WHERE userId=:userId";
			$db->prepare($sql);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db->execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				$this -> logger -> logError('getAbillity : fail UPDATE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}
            $bundle0 = $row["bundle0"];
            $bundle1 = $row["bundle1"];

        }
		$result['ResultCode'] = 100;
		$result['bundle0'] = $bundle0;
		$result['bundle1'] = $bundle1;

		return $result;
	}

	function GodPresent ($param) {
		$resultFail['ResultCode'] = 300;

		$userId = $param["userId"];
		$goddessId = $param["goddessId"];

		$db = new DB();

		$sql = "select exp from frdGoddess where userId=:userId and goddessId=:goddessId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':goddessId', $goddessId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this -> logger -> logError('sendPresent : fail SELECT userId :' . $userId );
			$this -> logger -> logError('sql : ' . $sql );
			return $resultFail;
		}


		$preExp = (int)$row["exp"];
		$ids = null;
		for ( $matCount=0; $matCount<8; $matCount++ ) {
			if ( @is_null( $param["runeId".$matCount] ) )
				break;

			$runeIds[] = $param["runeId".$matCount];
			$runeAmounts[] = $param["runeAmount".$matCount];
			if ($ids == null) {
				$ids = $param["runeId".$matCount];
			} else {
				$ids = $ids.','.$param["runeId".$matCount];
			}
		}

		$sql = "SELECT itemTableId, itemId, itemCount FROM frdHavingItems WHERE userId= :userId";
		if ($ids != null ) {
			$sql  = $sql." AND  itemId IN (".$ids.")";
		}
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$ResultList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$ResultList[] = $row;
		}
		if ($matCount != count($ResultList) ) {
			$this->logger->logError('sendPresent : error userId: '.$userId.'matCount:'.$matCount.' result:'.count($ResultList) );
			return $resultFail;
		}
		$i = 0;
		$totalExp = 0;
		$deleteItemId = null;
		$updateItemId = null;

		$logDBSendManager = new LogDBSendManager();

		foreach($ResultList as $RL ) {
			$itemId = $RL["itemId"];
			$itemCount = $RL["itemCount"];

			$itemCount = abs($itemCount);

			for($tempI = 0; $tempI < count($runeIds); $tempI++ ){
				$tempId = $runeIds[$tempI];
				if ($tempId == $itemId) {
					break;
				}
			}

			$resultItemCount = $itemCount - $runeAmounts[$tempI];
			if ( $resultItemCount <= 0 ) {
				$deleteItemId[] = $RL['itemId'];
			}
			$tempId = null;
			$tempId['itemId'] = $RL["itemId"];
			$tempId['cur'] = $resultItemCount;
			$tempId['add'] = 0 - $runeAmounts[$tempI];

			$data['use']['items'][] = $tempId;

			$updateItemId[] = $itemId;
			$updateItemCount[] = $resultItemCount;

			$runeExp = $this->GetRuneExpForGoddess($goddessId, $runeIds[$tempI]);

			$totalExp += (int)($runeExp * $runeAmounts[$tempI]);
//			$data["itemCount"][] = $resultItemCount;

			$logDBSendManager->sendItem($userId, $RL['itemId'], $itemCount, $resultItemCount, $runeAmounts[$tempI]);
		}

		if ( !is_null($deleteItemId) ) {
			$ids = null;
			for ( $i=0; $i<count($deleteItemId); $i++ ) {
				if ($ids == null) {
					$ids = $deleteItemId[$i];
				}
					$ids = $ids.','.$deleteItemId[$i];
			}

			$sql = "delete from frdHavingItems where userId=:userId AND itemId IN (".$ids.")";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('sendPresent : fail DELETE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}
		}

		if ( !is_null($updateItemId) ) {
			$sql = "update frdHavingItems set itemCount = case ";
			for ( $i=0; $i<count($updateItemId); $i++ ) {
				$sql .= " when itemId=".$updateItemId[$i]." then ".$updateItemCount[$i];
			}
			$sql .= " else itemCount end where userId = :userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('sendPresent : fail DELETE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}
		}

		$resultExp = $preExp+$totalExp;
		$sql = "update frdGoddess set exp=:resultExp where userId=:userId and goddessId=:goddessId";
		$db -> prepare($sql);
		$db -> bindValue(':resultExp', $resultExp, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':goddessId', $goddessId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('sendPresent : fail DELETE userId :' . $userId );
			$this -> logger -> logError('sql : ' . $sql );
			return $resultFail;
		}


		$goddessExp["curExp"] = $resultExp;
		$goddessExp["goddessId"] = $goddessId;

		$data['rewards']['goddessExp'] = $goddessExp;

		$result['Protocol'] = 'ResGodPresent';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;
		return $result;
	}

	function GodInform ($param) {
		$resultFail['ResultCode'] = 300;

		$userId = $param["userId"];

		$db = new DB();
  		$sql = "select goddessId, level, exp from frdGoddess where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$ResultList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$ResultList[] = $row;
		}
		$data["goddess"] = $ResultList;

		$result['Protocol'] = 'ResGodInform';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;
		return $result;
	}

	function GodQuest($param) {
		$resultFail['ResultCode'] = 300;

		$userId = $param["userId"];
		$goddessId = (int)$param["goddessId"];
		$data = null;
		$db = new DB();
  		$sql = "select level, exp from frdGoddess where userId=:userId and goddessId=:goddessId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':goddessId', $goddessId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql'.$sql);
			return $resultFail;
		}

		$gExp = $this->GetGoddessNeedExp($goddessId, $row["level"]);
		if ($row['exp'] < $gExp ) {
			$this->logger->logError('setGoddessQuest : EXP WRONG userId.' . $userId);
			$this->logger->logError(' row EXP :' . $row['exp'].' gExp : '.$gExp);
			return $resultFail;
		}


		$mats = $this->GetNeedQuestMats($goddessId, (int)$row["level"]);
		$matCount = (int)(count($mats)/2);
		$ConsumeManager = new ConsumeManager();
		for ( $i=0; $i<$matCount; $i++ ) {
			$rewardType = $mats[$i*2];
			$rewardCount = $mats[$i*2+1];
				$this->logger->logError('setGoddessQuest val  :' . $rewardType);
				$this->logger->logError('setGoddessQuest val  :' . $rewardCount);
			//  echo $rewardType.", ".$rewardCount."\n";
			$resultVal = $ConsumeManager->setConsumeResource($db, $userId, $i, $rewardType, $rewardCount);
			$temps[] = $resultVal;

//			$data["consumeType"][] = $rewardType;
//			$data["consumeCount"][] = $rewardCount;
			if ( $resultVal == 0) {
				$this->logger->logError('setGoddessQuest val  :' . $resultVal.' userId : '.$userId);
				return $resultFail;
			}
			else if ( $resultVal == -1 ) {
				$this->logger->logError('setGoddessQuest val  :' . $resultVal.' userId : '.$userId);
				return $resultFail;
			}
		}

		$questIdx = (int)$row["level"];
		$resultLevel = $questIdx+1;
		$abil_result = $this->getAbillity( $db, $userId, $questIdx, $goddessId, $data );
		if ( $abil_result['ResultCode'] != 100 ) {
			$this->logger->logError('setGoddessQuest val  :' . $resultVal.' userId : '.$userId);
			return $resultFail;
		}
		$data['bundle0'] = $abil_result["bundle0"];
		$data['bundle1'] = $abil_result["bundle1"];

		$sql = "UPDATE frdGoddess SET level=:level, exp=0 where userId=:userId and goddessId=:goddessId";
		$db -> prepare($sql);
		$db -> bindValue(':level', $resultLevel, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':goddessId', $goddessId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('sendPresent : fail DELETE userId :' . $userId );
			$this -> logger -> logError('sql : ' . $sql );
			return $resultFail;
		}



		$goddessExp["goddessId"] = $goddessId;
		$goddessExp["curExp"] = 0;
		$goddessExp["curLevel"] = $resultLevel;
		$data['rewards']['goddessLevel'] = $goddessExp;

		foreach($temps as $tp){
			if (isset($tp['gold'])) {
				$data['use']['gold'] = $tp['gold'];
			}
			if (isset($tp['jewel'])) {
				$dataTotal['jewel'] = $tp['jewel'];
			}
			if (isset($tp['medal'])) {
				$data['use']['medal'] = $tp['medal'];
			}
			if (isset($tp['heart'])) {
				$data['use']['heart'] = $tp['heart'];
			}

			if (isset($tp['weapons'])) {
				$data['use']['weapons']['delTableIds'][] = $tp['weapons']['weaponTableId'];
			}
			if (isset($tp['magics'])) {
				$data['use']['magics'][] = $tp['magics'];
			}
			if (isset($tp['items'])) {
				$data['use']['items'][] = $tp['items'];
			}
		}

		$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
			where userId=:userId and achieveId > 4300 and achieveId < 4400";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$achieveId = $row['achieveId'];
			if ($row['cnt'] != -1 ) {
				$AchieveManager = new AchieveManager();
				$AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
			}
		}


		$result['Data'] = $data;
		$result['Protocol'] = 'ResGodQuest';
		$result['ResultCode'] = 100;

		return $result;
	}

	function GetNeedQuestMats($goddessId, $level) {
		switch($goddessId) {
		case 1:
			switch($level) {
			case 1:
				return array(101006,30,102000,20,102007,15,103000,10,113004,30);
			case 2:
				return array(104000,10,105001,5,114004,10,115002,5,1118,1,5000,100000);
			case 3:
				return array(104007,25,105000,10,115004,10,1129,1,1215,1,110000,5);
			case 4:
				return array(106000,10,115004,20,1200,1,1225,1,1202,1,110000,10);
			default:
				return array(100000, 2, 5002, 100, 110000, 10);
			}
		case 2:
			switch($level) {
			case 1:
				return array(101001,25,102001,20,103001,15,104001,10,114000,5);
			case 2:
				return array(102007,25,104001,15,115002,5,5000,100000,5006,3000,5005,500);
			case 3:
				return array(102005,25,104003,20,105001,10,115002,12,5000,120000,110000,5);
			case 4:
				return array(106001,10,115002,20,5000,300000,5006,10000,1204,1,110000,10);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}

		case 3:
			switch($level) {
			case 1:
				return array(101002,25,102002,15,103002,10,104002,5,113003,40);
			case 2:
				return array(102001,30,104005,15,113001,10,113003,30,114003,25,5006,3000);
			case 3:
				return array(104002,25,105002,10,105007,5,114003,30,115000,5,110000,5);
			case 4:
				return array(103007,25,104009,20,105007,10,106002,10,115003,25,110000,10);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}

		case 4:
			switch($level) {
			case 1:
				return array(102003,20,103003,15,113001,30,114001,10,10205,1);
			case 2:
				return array(101003,30,114000,15,114001,15,114003,15,10207,1,5000,100000);
			case 3:
				return array(105003,10,114001,30,115001,10,10304,1,5000,200000,110000,5);
			case 4:
				return array(104003,15,104007,20,106003,10,115001,20,10400,1,110000,10);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 5:
			switch($level) {
			case 1:
				return array(101000,30,101009,20,113003,20,114000,10,5006,1000);
			case 2:
				return array(101004,30,104004,10,104007,10,104009,10,114000,30,5000,120000);
			case 3:
				return array(105004,10,105005,10,105006,10,105009,10,115004,10,110000,5);
			case 4:
				return array(106004,10,115000,25,1211,1,1217,1,1218,1,110000,10);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 6:
			switch($level) {
			case 1:
				return array(103005,15,105009,1,113000,15,113002,20,113004,15);
			case 2:
				return array(101005,30,102005,20,103008,15,104006,10,105002,5,114002,30);
			case 3:
				return array(104005,20,113002,30,114002,25,115004,10,5006,5000,110000,5);
			case 4:
				return array(106008,3,106005,8,114000,25,115002,15,115004,10,110000,10);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 7:
			switch($level) {
			case 1:
				return array(101006,20,102004,10,102008,20,113004,25,114000,10);
			case 2:
				return array(102006,20,103004,15,103006,20,103008,15,106005,1,114001,25);
			case 3:
				return array(105006,10,105007,10,105008,10,114003,35,115001,10,110000,5);
			case 4:
				return array(106006,8,106009,3,115001,15,115002,10,5000,300000,110000,10);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 8:
			switch($level) {
			case 1:
				return array(101007,30,102007,25,104007,5,106008,1,113004,30);
			case 2:
				return array(103007,20,104001,15,106001,1,114002,30,5006,3000,5000,150000);
			case 3:
				return array(102003,25,103000,30,104009,20,114004,30,115000,10,110000,5);
			case 4:
				return array(104008,25,106003,3,106007,8,115001,10,115004,15,110000,10);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 9:
			switch($level) {
			case 1:
				return array(103003,10,104000,5,104004,5,113000,30,5006,1000);
			case 2:
				return array(101008,20,103001,25,103009,25,104005,20,114000,25,5006,3000);
			case 3:
				return array(101008,30,102008,25,103008,20,114001,25,115000,10,110000,5);
			case 4:
				return array(106008,8,115001,8,115002,8,115003,8,115004,8,110000,10);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 10:
			switch($level) {
			case 1:
				return array(101004,20,103002,15,104008,5,113003,25,5000,100000);
			case 2:
				return array(101009,30,102009,30,113004,30,114002,15,114003,15,5006,2500);
			case 3:
				return array(103009,20,105005,10,106007,3,114003,20,115004,10,110000,5);
			case 4:
				return array(102004,25,105009,10,106009,10,114001,15,115003,25,110000,10);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		default:
			return array(5000,50);
		}
		/*	새로운 기획 데이터
		switch($goddessId) {
			case 1:
				switch($level) {
					case 1:
						return array(103000, 5, 101006, 10, 102000, 10, 102007, 5, 113004, 20);
					case 2:
						return array(105000, 10, 106000, 5,  115004, 5, 115002, 5, 1118, 1);
					case 3:
						return array(106000, 5,  103009, 8, 115004, 10, 1129, 1, 1117, 1, 1215, 1);
					case 4:
						return array(1200, 1, 1225, 1, 1202, 1, 1201, 1, 1208, 1, 110000, 10);
					default:
						return array(100000, 2, 5002, 100, 110000, 10);
				}
			case 2:
				switch($level) {
					case 1:
						return array(101001, 30, 102001, 20, 103001, 15, 104001, 10, 114002, 20);
					case 2:
						return array(5000, 50000, 115002, 10,  105001, 10, 5006, 3000, 104008, 2);
					case 3:
						return array(5000, 80000,  106001, 5, 102005, 6, 103003, 3, 105008, 1, 110000, 5);
					case 4:
						return array(5000, 120000, 5006, 5000, 1204, 1, 1204, 1, 1204, 1, 101001, 100);
					default:
						return array(100000, 2, 5002, 100, 110000, 5);
				}
			case 3:
				switch($level) {
					case 1:
						return array(101002, 12, 102002, 10, 103002, 8, 104002, 5, 115002, 3);
					case 2:
						return array(105002, 10, 106002, 5,  111003, 50, 112003, 30, 113003, 20);
					case 3:
						return array(114003, 35, 115003, 10, 103007, 15, 103006, 15, 103005, 15, 110000, 5);
					case 4:
						return array(115003, 30, 103002, 20, 103003, 20, 103004, 20, 106002, 10, 110000, 10);
					default:
						return array(100000, 2, 5002, 100, 110000, 5);
				}
			case 4:
				switch($level) {
					case 1:
						return array(111001, 100, 102003, 20, 103003, 15, 112001, 20, 10205, 1);
					case 2:
						return array(101003, 50, 113001, 20,  113003, 20, 113000, 20, 10207, 1);
					case 3:
						return array(114001, 40, 115001, 15, 104003, 20, 105003, 10, 10304, 1, 5000, 200000);
					case 4:
						return array(115001, 10, 106003, 5, 106002, 5, 106001, 5, 10400, 1, 110000, 10);
					default:
						return array(100000, 2, 5002, 100, 110000, 5);
				}
			case 5:
				switch($level) {
					case 1:
						return array(101000, 50, 106003, 3, 102004, 30, 112000, 50, 5006, 3000);
					case 2:
						return array(101004, 50, 104004, 10, 104007, 10, 104009, 10, 114000, 30);
					case 3:
						return array(105004, 10, 105005, 5, 105006, 10, 105007, 10, 105009, 10, 110000, 10);
					case 4:
						return array(115000, 30, 1211, 1, 1217, 1, 1218, 1, 106004, 20, 110000, 15);
					default:
						return array(100000, 2, 5002, 100, 110000, 5);
				}
			case 6:
				switch($level) {
					case 1:
						return array(103005, 5, 106000, 5, 111002, 50, 113000, 40, 113004, 20);
					case 2:
						return array(101005, 30, 102005, 20, 103008, 10, 104006, 8, 105002, 5, 106005, 5);
					case 3:
						return array(104005, 20, 106002, 10, 113002, 50, 114002, 25, 115004, 15, 5005, 3000);
					case 4:
						return array(106008, 10, 104001, 20, 115004, 15, 115002, 30, 114000, 40, 110000, 15);
					default:
						return array(100000, 2, 5002, 100, 110000, 5);
				}
        case 7:
  			switch($level) {
    			case 1:
    				return array(101006, 40, 102008, 30, 106007, 5, 113004, 30, 114000, 20);
    			case 2:
    				return array(102006, 40, 101007, 30, 103008, 30, 106005, 10, 104006, 40, 101008, 40);
    			case 3:
    				return array(105006, 25, 105007, 20, 105008, 20, 114003, 35, 115001, 20, 110000, 5);
    			case 4:
    				return array(106006, 15, 106009, 10, 115002, 15, 115001, 35, 5000, 300000, 110000, 15);
    			default:
    				return array(100000, 2, 5002, 100, 110000, 5);
  			}
  		case 8:
  			switch($level) {
    			case 1:
    				return array(101007, 45, 104007, 20, 106008, 5, 102002, 35, 111004, 70);
    			case 2:
    				return array(103007, 30, 106001, 10, 104001, 30, 114002, 40, 5005, 1000, 110000, 5);
    			case 3:
    				return array(103002, 35, 102003, 40, 104009, 30, 115004, 25, 115000, 15, 110000, 10);
    			case 4:
    				return array(106007, 10, 106003, 10, 102009, 40, 115001, 20, 115004, 30, 110000, 15);
    			default:
    				return array(100000, 2, 5002, 100, 110000, 5);
  			}
  		case 9:
  			switch($level) {
    			case 1:
    				return array(104000, 40, 104004, 30, 105001, 20, 106001, 5, 5005, 2000);
    			case 2:
    				return array(103001, 30, 103007, 30, 103009, 25, 106005, 10, 113000, 50, 5006, 5000);
    			case 3:
    				return array(101008, 30, 102008, 30, 103008, 20, 106007, 10, 115000, 20, 110000, 5);
    			case 4:
    				return array(115001, 15, 115003, 15, 115002, 15, 115004, 15, 106008, 10, 110000, 15);
    			default:
    				return array(100000, 2, 5002, 100, 110000, 5);
  			}
  		case 10:
  			switch($level) {
    			case 1:
    				return array(104009, 25, 104008, 25, 104004, 25, 104001, 25, 5000, 200000);
    			case 2:
    				return array(101009, 50, 102009, 40, 104006, 25, 112003, 50, 113002, 30, 5005, 3000);
    			case 3:
    				return array(103009, 20, 105005, 10, 106004, 10, 114003, 30, 115004, 20, 110000, 5);
    			case 4:
    				return array(105009, 20, 103005, 20, 106009, 10, 115003, 25, 115001, 15, 110000, 15);
    			default:
    				return array(100000, 2, 5002, 100, 110000, 5);
  			}
			default:
				return array(5000,50);
		}
		*/
	}


	function GetGoddessNeedExp($goddessId, $level) {
		switch($goddessId) {
			case 1:
				switch($level) {
					case 1:return 20000;
					case 2:return 50000;
					case 3:return 100000;
					case 4:return 280000;
					default:return 1;
				}
			case 2:
				switch($level) {
					case 1:return 13333;
					case 2:return 33333;
					case 3:return 66667;
					case 4:return 186667;
					default:return 1;
				}
			case 3:
				switch($level) {
					case 1:return 30000;
					case 2:return 60000;
					case 3:return 120000;
					case 4:return 290000;
					default:return 1;
				}
			case 4:
				switch($level) {
					case 1:return 25000;
					case 2:return 60000;
					case 3:return 150000;
					case 4:return 260000;
					default:return 1;
				}
			case 5:
				switch($level) {
					case 1:return 30000;
					case 2:return 70000;
					case 3:return 140000;
					case 4:return 300000;
					default:return 1;
				}
			case 6:
				switch($level) {
					case 1:return 25000;
					case 2:return 60000;
					case 3:return 150000;
					case 4:return 300000;
					default:return 1;
				}
      case 7:
  			switch($level) {
    			case 1:return 35000;
    			case 2:return 70000;
    			case 3:return 160000;
    			case 4:return 285000;
    			default:return 1;
  			}
  		case 8:
  			switch($level) {
    			case 1:return 30000;
    			case 2:return 75000;
    			case 3:return 155000;
    			case 4:return 290000;
    			default:return 1;
  			}
  		case 9:
  			switch($level) {
    			case 1:return 32000;
    			case 2:return 65000;
    			case 3:return 170000;
    			case 4:return 285000;
    			default:return 1;
  			}
  		case 10:
  			switch($level) {
    			case 1:return 25000;
    			case 2:return 65000;
    			case 3:return 180000;
    			case 4:return 280000;
    			default:return 1;
  			}
			default:
				return 0;
		}
	}




	function GetRuneAttribute($runeId) {
		if ( $runeId == 110000 )
			return 10;
		$val = $runeId % 1000;
		return $val;
	}

	function GetRuneLevel( $runeId) {
		$val = (int)((int)($runeId - 110000)/1000);
		if ( $val < 0 )
			return 9;   //rainbow
		return $val;
	}

	function GetGoddessAttribute($id) {
		switch($id) {
			case 1:return 4;
			case 2:return 2;
			case 3:return 3;
			case 4:return 1;
			case 5:return 0;
			case 6:return 2;
			case 7:return 1;
			case 8:return 4;
			case 9:return 0;
			case 10:return 3;
			default:return 0;
		}
	}
	function GetRuneExp($level) {
		switch($level) {
			case 1:return 160;
			case 2:return 290;
			case 3:return 720;
			case 4:return 1200;
			case 5:return 2800;
			default:return 9999;
		}
	}

	function GetRuneExpForGoddess($goddessId, $runeId) {
		$level = $this->GetRuneLevel($runeId);
		$runeExp = $this->GetRuneExp($level);
		$goddessAtt = $this->GetGoddessAttribute($goddessId);
		$runeAtt = $this->GetRuneAttribute($runeId);
		if ( $goddessAtt == $runeAtt || $runeAtt == 10 )
			return (int)round($runeExp * 1.5);
		return $runeExp;
	}
}
?>
