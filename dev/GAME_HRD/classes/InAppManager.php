<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
require_once "./common/define.php";
require_once './classes/AddManager.php';
require_once './classes/LogDBSendManager.php';
require_once './classes/LobbyEventManager.php';

class InAppManager {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function getCurl($fUrl,$fMethod,$fParam) {
		$sUrl = $fUrl.(($fParam && strtolower($fMethod)=="get") ? "?$fParam": "");
		$sMethod = (strtolower($fMethod)=="get") ? "0" : "1" ;
		$sParam = (strtolower($fMethod)=="get") ? "" : $fParam ;

		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL,"$sUrl"); //접속할 URL 주소
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // 인증서 체크같은데 true 시 안되는 경우가 많다.
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		// default 값이 true 이기때문에 이부분을 조심 (https 접속시에 필요)
		curl_setopt ($ch, CURLOPT_SSLVERSION,4); // SSL 버젼 (https 접속시에 필요)
		curl_setopt ($ch, CURLOPT_HEADER, 0); // 헤더 출력 여부
		curl_setopt ($ch, CURLOPT_POST, $sMethod); // Post Get 접속 여부
		curl_setopt ($ch, CURLOPT_POSTFIELDS, "$fParam"); // Post 값  Get 방식처럼적는다.
		curl_setopt ($ch, CURLOPT_TIMEOUT, 30); // TimeOut 값
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); // 결과값을 받을것인지
		$result = curl_exec ($ch);
		curl_close ($ch);
		return $result;
	}

	function verifyGoogle($packageName, $productId, $purchaseToken  ) {

		// token 파일을 apc 로 옮긴다. 
		$sFileName = dirname(__FILE__)."/token.txt";
		$sResult = json_decode(file_get_contents($sFileName),true);
		$refresh_token = addslashes($sResult["refresh_token"]);
		$regdate = $sResult["regdate"];

		if ((time()-$regdate)>1800) {
			$sParam = "";
			$sParam .= "refresh_token=".$refresh_token;
			//    $sParam .= "&client_id=1068641320169-00n3c9s58jt1n5351ida22l3i5o3jejv.apps.googleusercontent.com";
			//    $sParam .= "&client_secret=W3JEfPszL36KxCFQqtgqD7O-";
			$sParam .= "&client_id=29383828223-32013hh36f3e182t3pgddg07b8gjrc09.apps.googleusercontent.com";
			$sParam .= "&client_secret=JtatfRM15QX0SVZEvav4xj47";
			$sParam .= "&grant_type=refresh_token";
			$rResult = $this->getCurl("https://accounts.google.com/o/oauth2/token","post","$sParam");

			$sResult = json_decode($rResult,true);
			$sResult["regdate"] = "".time();
			$sResult["refresh_token"] = $refresh_token;

			$rResult = json_encode($sResult);
			$fp=fopen($sFileName,'w');
			fwrite($fp,$rResult);
			fclose($fp);
		}

		$sUrl = "https://www.googleapis.com/androidpublisher/v1.1/";
		$sUrl .= "applications/";
		$sUrl .= $packageName;
		$sUrl .= "/inapp/";
		$sUrl .= $productId;
		$sUrl .= "/purchases/";
		$sUrl .= $purchaseToken;
		$sUrl .= "/?access_token=";
		$sUrl .= $sResult['access_token'];

		//		sleep(2);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $sUrl);
		// Set so curl_exec returns the result instead of outputting it.
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// Get the response and close the channel.
		$response = curl_exec($ch);
		curl_close($ch);

		$sResult = json_decode($response);
		$data["str"] = $sResult;
		$purchaseDate = $sResult->{'purchaseTime'};

		if ( $purchaseDate == null ) {
			$data['ResultCode'] = 200;
			$data['purchaseDate'] = $purchaseDate;
			return $data;
		} else {
			$data['ResultCode'] = 100;
			$data['purchaseDate'] = $purchaseDate;
			return $data;
		}
	}


	function verifyIOS($receipt) {
		$endpoint = "";

		// 샌드박스일 경우
		if($GLOBALS['REAL'] != 5) {
			$endpoint = 'https://sandbox.itunes.apple.com/verifyReceipt';
		} else {// 실결제일 경우
			$endpoint = 'https://buy.itunes.apple.com/verifyReceipt';
		}
		$postData = json_encode(array('receipt-data' => $receipt));

		$ch = curl_init($endpoint);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

		$response = curl_exec($ch);
		$errno    = curl_errno($ch);
		$errmsg   = curl_error($ch);
		curl_close($ch);

		$verifyResult = json_decode($response);
		if(!is_object($verifyResult) || !isset($verifyResult->status) || $verifyResult->status != 0) {
			if ( $GLOBALS['REAL'] == 5 ) {
				$endpoint = 'https://sandbox.itunes.apple.com/verifyReceipt';
				$postData = json_encode(array('receipt-data' => $receipt));

				$ch = curl_init($endpoint);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

				$response = curl_exec($ch);
				$errno    = curl_errno($ch);
				$errmsg   = curl_error($ch);
				curl_close($ch);

				$verifyResult = json_decode($response);
				if(!is_object($verifyResult) || !isset($verifyResult->status) || $verifyResult->status != 0) {
					$result['ResultCode'] = 200;
					$result['purchaseDate'] = $purchaseDate;
					return $result;
				}
			}
		}

		$data['ResultCode'] = 100;
		$data['Data'] = "";

		return $data;
	}

	function getPostCurl($fUrl,$postData) {
		$ch = curl_init($fUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  Application/json"));
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		$result = curl_exec ($ch);
		curl_close ($ch);
		return $result;
	}

	function verifyOneStore($txId, $appId, $receipt, $purchaseToken) {
		$postData = json_encode(array('txid' => $txId,
					'appid' => $appId,
					'signdata' => $receipt));
		// 라이브 환경 검증
		$rResult = $this->getPostCurl("https://iap.tstore.co.kr/digitalsignconfirm.iap", $postData);
		$resData = json_decode($rResult);
		if (!is_object($resData) || !isset($resData->status) || $resData->status != 0) {
			// 개발 환경 검증
			$rResult = getPostCurl("https://iapdev.tstore.co.kr/digitalsignconfirm.iap", $postData);
			$resData = json_decode($rResult);
			if (!is_object($resData) || !isset($resData->status) || $resData->status != 0) {
				$data["error"] = 1;
				$data["status"] = $resData->status;
				$data['ResultCode'] = 200;
				return $data;
			}
		}


		if (strcmp($resData->product[0]->tid, $purchaseToken) != 0) {
			// 다른 tid!
			$data["error"] = 1;
			$data["message"] = "Different TID";
			$data['ResultCode'] = 200;
			return $data;
		}

		$data['ResultCode'] = 100;
		$data['Data'] = "";

		return $data;

	}

	public function BuyCashGoogle($param) {
		$resultFail['Protocol'] = 'ResBuyCashGoogle';
		$resultFail['ResultCode'] = 300;

		$userId = $param['userId'];
		if (isset($param["packageName"] ) ) {
			$packageName = $param["packageName"];
		}
		$orderId = $param["orderId"];
		$productId = $param["productId"];

		if (isset($param["purchaseTime"] ) ) {
			$purchaseTime = $param["purchaseTime"];
		} else {
			$purchaseTime = 0;
		}

		$purchaseState = $param["purchaseState"];
		$purchaseToken = $param["purchaseToken"];
		$signature = $param["signature"];
		$isTest = $param["isTest"] == 1 ? true : false;

		$marketType = $param["marketType"];

		$db = new DB();
		$sql = "SELECT orderId from frdLogBuy where orderId = :orderId";
		$db -> prepare($sql);
		$db -> bindValue(':orderId', $orderId, PDO::PARAM_STR);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$this->logger->logError(__FUNCTION__.' : EXISIT userId : '.$userId.' orderId :'. $orderId);
			return $resultFail;
		}

		// 영수증 무결성 체크 
		if ($GLOBALS['REAL'] == 5) {
			switch($marketType) {
				case 'AS':
					$vResult = $this->verifyIOS($signature);
					$packageName = "IOS";
					break;
				case 'PS':
					$vResult = $this->verifyGoogle($packageName, $productId, $purchaseToken);
					break;
				case 'OS':
					$vResult = $this->verifyOneStore($orderId, $packageName, $signature, $purchaseToken);
					break;
				default:
					$this->logger->logError(__FUNCTION__.' : userId : '.$userId.' marketType :'. $marketType);
					return $resultFail;
			}
		} else {
			$vResult['ResultCode'] = 100;
		}

		if ($vResult['ResultCode'] != 100) {
			$this->logger->logError(__FUNCTION__.' :FAIL VERYFI userId : '.$userId.' orderId :'.$orderId);

			if ($marketType != 'AS') {	
				$data["url"] = $sUrl;
				$data["str"] = $sResult;
			}

			$data["error"] = 2;

			$sql = "INSERT INTO frdLogBuy 
				(userId, packageName, orderId, productId, purchaseTime,purchaseState, purchaseToken, signature, isComplete, reg_date ) 
				values 
				(:userId, :packageName, :orderId, :productId, now(),:purchaseState, :purchaseToken, :signature, 0, now() )";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':packageName', $packageName, PDO::PARAM_STR);
			$db -> bindValue(':orderId', $orderId, PDO::PARAM_STR);
			$db -> bindValue(':productId', $productId, PDO::PARAM_STR);
			$db -> bindValue(':purchaseState', $purchaseState, PDO::PARAM_INT);
			$db -> bindValue(':purchaseToken', $purchaseToken, PDO::PARAM_STR);
			$db -> bindValue(':signature', $signature, PDO::PARAM_STR);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
				return $resultFail;
			}
			$resultFail['ResultCode'] = 200;
			return $resultFail;
		} else {
			$sql = "SELECT shopItemId, itemId, itemCount,price FROM frdShopData 
				WHERE productId = :productId";
			$db -> prepare($sql);
			$db -> bindValue(':productId', $productId, PDO::PARAM_STR);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				// 존재 하지 않는 상품 
				$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
				return $resultFail;
			}

			$shopItemId = $row['shopItemId'];
			$itemId = $row['itemId'];
			$itemCount = $row['itemCount'];
			$price = $row['price'];

			// 패키지 
			if ( $shopItemId > 6000 && $shopItemId != 6005 ) {
				$tempTypes = explode('/',$itemId);	
				$tempCnts = explode('/',$itemCount);	

				$addResult = $this->addItemForPackage($db, $userId, $tempTypes, $tempCnts);
				$data['rewards'] = $addResult;
				// 보석 
			} else if ( $shopItemId > 1000 && $shopItemId < 2000) {
				if ( $itemCount < 0 || $itemCount > 1000000 ) {
					$this->logger->logError(__FUNCTION__.' : over range data userId.'.$userId.', amount : '.$itemCount);
					return $resultFail;
				}

				$AssetManager = new AssetManager();
				$AssetResult = $AssetManager->addJewel($db, $userId, $itemCount);
				if ($AssetResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.' : fail add userId.'.$userId);
					return $resultFail;
				}

				$jewel['curJewel'] = $AssetResult['jewel'];
				$jewel['addJewel'] = $itemCount;

				$data['rewards']['jewel'] = $jewel;

			} else if ( $shopItemId == 6005  ) {	
				
				$sql = "UPDATE frdMonthlyCard SET startTime = now(), duration = 30, recvCnt = 0
					WHERE UserId = :userId";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
					return $resultFail;
				}
				
				$LobbyEventManager = new LobbyEventManager();
				if ( false == $LobbyEventManager->sendReaPackReward($db, $userId, 101) ) {
					$this->logger->logError(__FUNCTION__.' : INSERT FAIL userId :'.$userId.' sql : '.$sql);
					return $resultFail;
				}
				$data['good'] = 1;
			}

			$sql = "select orderId from frdLogBuy where orderId = :orderId";
			$db -> prepare($sql);
			$db -> bindValue(':orderId', $orderId, PDO::PARAM_STR);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				$sql = "INSERT INTO frdLogBuy 
					(userId, packageName, orderId, productId, purchaseTime,purchaseState, purchaseToken, signature, isComplete, reg_date ) 
					values 
					(:userId, :packageName, :orderId, :productId, now(),:purchaseState, :purchaseToken, :signature, 0, now() )";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':packageName', $packageName, PDO::PARAM_STR);
				$db -> bindValue(':orderId', $orderId, PDO::PARAM_STR);
				$db -> bindValue(':productId', $productId, PDO::PARAM_STR);
				$db -> bindValue(':purchaseState', $purchaseState, PDO::PARAM_INT);
				$db -> bindValue(':purchaseToken', $purchaseToken, PDO::PARAM_STR);
				$db -> bindValue(':signature', $signature, PDO::PARAM_STR);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
					return $resultFail;
				}
			}

			/*
			// 로그 사용 이유를 확인 하여 기록할 것인지 체크 한다. 
			include_once("../LogingPurchaseProcess1.php");
			SaveLog($id, "ipSe:{ $productId,$orderId }");
			 */
		}

		// EVENT CHECK 시작 
		$this->checkEventAccu($db,$userId,$price);
		$this->checkEventFirst($db, $userId, $price, $LobbyEventManager);

		$result['Protocol'] = 'ResBuyCashGoogle';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}		

	function checkEventFirst(&$db, $userId, $price, $LobbyEventManager) {
		if ( $LobbyEventManager == null )
			$LobbyEventManager = new LobbyEventManager();

		if ( false == $LobbyEventManager->IsActiveEvent(EVENT_SELL_PACKAGE) ) {//EVENT_PAY_FIRST 300에 합침
			$this->logger->logError(__FUNCTION__.' : There is no event '.'EventData'.EVENT_PAY_FIRST);
        	return false;
        }
     
        $event = null;
        $event_now = date('ymdH');
        
		$rewardTypes = array(
			5002,
			5003,
			5006,
			10503,
			1504
			);
		$rewardCnts = array(
			10,
			100,
			100,
			1,
			1
			);

		$sql = "select * from Event_Buy_First where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$sql = "INSERT INTO Event_Buy_First 
				(userId, price, state, reg_date ) 
				values 
				(:userId, :price, :state, now())";
			$db -> prepare($sql);
			$db -> bindValue(':price', $price, PDO::PARAM_INT);
			$db -> bindValue(':state', 1, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
				return false;
			}
		} else {
			return true;
		}

		$sql = "INSERT INTO frdUserPost (recvUserId, sendUserId, type, count, expire_time, reg_date) values ";
		for ( $i=0; $i<count($rewardTypes); $i++ ) {
			if ( $i !== 0 )
				$sql .=", ";
			$sql .= "($userId, 5, ".$rewardTypes[$i].", ".$rewardCnts[$i].", DATE_ADD(now(), INTERVAL 30 DAY ), now()) ";
		}
		$db -> prepare($sql);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			return $resultFail;
		}
		else {
			$this->logger->logError(__FUNCTION__.' : Success Send Post userId.'.$userId.', sql : '.$sql);
		}

        return true;
    }

	function checkEventAccu($db, $userId, $price) {
        $event = null;
        $event_now = date('ymdH');

        if ( $LobbyEventManager == null )
			$LobbyEventManager = new LobbyEventManager();

		if ( false == $LobbyEventManager->IsActiveEvent(EVENT_SELL_PACKAGE) ) {//EVENT_PAY_ACCU 100, 300, 1000 통합
			$this->logger->logError(__FUNCTION__.' : There is no event '.'EventData'.EVENT_SELL_PACKAGE);
        	return false;
        }

		$sql = "select * from Event_Buy_Accu where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			
			$lastAccuPayment = 0;
			$curAccuPayment = (int)$price;
			$sql = "INSERT INTO Event_Buy_Accu (userId, AmountOfPayment, update_date, reg_date ) 
				values ($userId, $price, now(), now())";
			
		} else {

			$lastAccuPayment = (int)$row['AmountOfPayment'];
			$curAccuPayment = (int)$price + (int)$row['AmountOfPayment'];
			$sql = "UPDATE Event_Buy_Accu SET AmountOfPayment = $curAccuPayment, update_date = now() where userId = $userId";

		}
		$db -> prepare($sql);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			return $resultFail;
		}


		$eventPayArray = array(5000,10000,50000,100000,200000,300000);
		$preIdx = 0;
		for ( $preIdx=0; $preIdx<count($eventPayArray); $preIdx++ ) {
			if ( $lastAccuPayment < $eventPayArray[$preIdx] )
				break;
		}
		$curIdx = 0;
		for ( $curIdx=0; $curIdx<count($eventPayArray); $curIdx++ ) {
			if ( $curAccuPayment < $eventPayArray[$curIdx] )
				break;
		}
		// $this->logger->logError(__FUNCTION__.' : lastAccuPayment = '.$lastAccuPayment.", curAccuPayment = ".$curAccuPayment);
		// $this->logger->logError(__FUNCTION__.' : preIdx = '.$preIdx.", curIdx = ".$curIdx);
		if ( $preIdx >= $curIdx )
			return true;


		$rewardTypes = array();	
		$rewardCnts = array();	
		for ( $i=$preIdx; $i<$curIdx; $i++ ) {

			switch ($i) {
				case 0:// 5000
					$accType = "5002,5005,5003";
					$accCnt = "5,50,50";
					break;

				case 1:// 10000
					$accType = "5002,5005,5006,5003,5003";
					$accCnt= "10,100,300,50,50";
					break;

				case 2:// 50000
					$accType = "1505,	110000,	5002,	5005,	5006,	5003,	5003,	5003,	5003";
					$accCnt =  "1,		1,		20,		300,	500,	50,		50,		50,		50";
					break;

				case 3:// 100000
					$accType = "10504, 	100000, 5006, 	110000, 100004,	100003";
					$accCnt =  "1, 		2,		1000,	3,		1,		10";
					break;

				case 4:// 200000
					$accType = "10505,	100000,	100004,	5006,5002,	110000,	5005";
					$accCnt =  "1,		3,		3,		2000,30,	5,		1000";
					break;

				case 5:// 300000
					$accType = "10505,	1505,	100004,	5006,5002, 	110000,	100000, 5005";
					$accCnt =  "1,		1,		6,		3000,100,	10,		6,		1500";
					break;
				
				default:
					$accType = "";
					$accCnt = "";
					break;
			}

			$realType = explode(',',$accType);
			$realCnt = explode(',',$accCnt);

			$rewardTypes = array_merge($rewardTypes, $realType);		
			$rewardCnts = array_merge($rewardCnts, $realCnt);	

		//	$this->logger->logError(__FUNCTION__.' : rewardTypes = '.json_encode($rewardTypes));	

		}
		if ( count($rewardTypes) > 0 ) {
			$sql = "INSERT INTO frdUserPost (recvUserId, sendUserId, type, count, expire_time, reg_date) values ";
			for ($i = 0; $i < count($rewardTypes); $i++) {
				$rType = $rewardTypes[$i];
				$rCnt = $rewardCnts[$i];
				if ($i !== 0)
					$sql .= ", ";
				
				$sql .= "($userId, 5, $rType, $rCnt, DATE_ADD(now(), INTERVAL 30 DAY ), now())";
			}
			$db -> prepare($sql);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
				return $resultFail;
			}
		}
        return true;
    }




	function addItemForPackage($db, $userId, $typeList, $amount) {
		$AssetManager =  new AssetManager();
		$AddManager =  new AddManager();

		$chestCount  = count($typeList);
		$total = null;
		$magicTemp = null;
		$itemTemp = null;
		$charTemp = null;
		$weaponTemp = null;
		$weaponData = null;
		$magicData = null;

		for ( $idx = 0; $idx < $chestCount; $idx++ ) {
			$rewardType = (int)$typeList[$idx];
			$rewardCount = (int)$amount[$idx];
			switch($rewardType) {
				case 5000: // 골드 지급
					$gold_result = $AssetManager->addGold($db, $userId, $rewardCount);
					if ($gold_result['ResultCode'] != 100 ) {
						return $gold_result;
					}
					$data = null;
					$data["curGold"] = $gold_result['gold'];
					$data["addGold"] = $rewardCount;
					$total['gold'] = $data;
					break;
				case 5001: //티켓 지급
					$AssetResult = $AssetManager->addUserExp($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$exp = $AssetResult['exp'];
					$total['exp'] = $exp;
					break;
				case 5002: //티켓 지급
					$AssetResult = $AssetManager->addTicket($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$data = null;
					$data["curTicket"] = $AssetResult['curTicket'];
					$data["addTicket"] = $rewardCount;
					$total['ticket'] = $data;
					break;
				case 5003: //행동력 지급
					$AssetResult = $AssetManager->addHeart($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$data = null;
					$data["curHeart"] = $AssetResult['curHeart'];
					$data["addHeart"] = $rewardCount;
					$data["maxHeart"] = $AssetResult['maxHeart'];
					$data["passTime"] = $AssetResult['passTime'];
					$total['heart'] = $data;
					break;
				case 5004: // 보석 지급
					$AssetResult = $AssetManager->addJewel($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$data = null;
					$data["curJewel"] = $AssetResult['jewel'];
					$data["addJewel"] = $rewardCount;
					$total['jewel'] = $data;
					break;
				case 5005: // 메달 지급
					$AssetResult = $AssetManager->addMedal($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$data = null;
					$data["curMedal"] = $AssetResult['skillPoint'];
					$data["addMedal"] = $rewardCount;
					$total['medal'] = $data;
					break;
				case 5006: // 메달 지급
					$AssetResult = $AssetManager->addPowder($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$data = null;
					$data["curPowder"] = $AssetResult['powder'];
					$data["addPowder"] = $rewardCount;
					$total['powder'] = $data;
					break;
				default :
					if ( $rewardType < 1000 ) {        //고정 캐릭터 소울스톤 지급
						if ( $rewardType > 500 ) {
							$key_name = $rewardType-500;
							$apc_key = 'CharacIndex_'.$key_name;
							$charData = apc_fetch($apc_key);
							$charMin = $charData[0];
							$charMax = $charData[1];
							$charId = rand($charMin, $charMax);
						} else {
							$charId = $rewardType;
						}
						$CharResult = $AddManager->setAddHero($db, $userId, $charId, $rewardCount);
						if ($CharResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.' :Weapon FAIL userId : '.$userId);
							continue;
						}
						$charTemp['charTableId'] = $CharResult['charTableId'];
						$charTemp['charId'] = $CharResult['charId'];
						$charTemp['exp'] = $CharResult['curExp'];

						$charData[] = $charTemp;
					} else if ( $rewardType < 2000 ) {    //보구 지급
						$rewardType = $rewardType - 1000;
						if ( $rewardType > 500 ) {
							$array_pos = $rewardType - 500;
							$apc_key = 'WeaponInform_WeaponMinIdx';
							$weaponDataApc = apc_fetch($apc_key);
							$minTemp = explode('/', $weaponDataApc[1]);
							$weaponMin = $minTemp[$array_pos];

							$apc_key = 'WeaponInform_WeaponMaxIdx';
							$weaponDataApc2 = apc_fetch($apc_key);
							$maxTemp = explode('/', $weaponDataApc2[1]);
							$weaponMax = $maxTemp[$array_pos];

							$rewardType = rand($weaponMin, $weaponMax);
						}

						// 아이템 지급 	
						$WeaponResult =	$AddManager->addWeapon($db, $userId, $rewardType, null);
						if ($WeaponResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.' :Weapon FAIL userId : '.$userId);
							continue;
						}
						$weaponTemp['weaponTableId'] = $WeaponResult['weaponTableId'];
						$weaponTemp['weaponId'] = $WeaponResult['weaponId'];
						$weaponTemp['level'] = $WeaponResult['level'];
						$weaponTemp['statIds'] = $WeaponResult['statIds'];
						$weaponTemp['statVals'] = $WeaponResult['statVals'];
						$weaponTemp['exp'] = $WeaponResult['exp'];

						$weaponData[] = $weaponTemp;
					} else if ( $rewardType  < 12000 ) {  //마법 지급
						$resultId = $rewardType-10000;
						if ( $resultId > 500 ) {
							$key_name = $resultId - 500;
							$this->logger->logError(__FUNCTION__.' :Magic key userId : '.$key_name);
							$apc_key = 'MagicPowderValue_'.$key_name;
							$magicData = apc_fetch($apc_key);
							$magicMin = $magicData[2];
							$magicMax = $magicData[3];	

							$this->logger->logError(__FUNCTION__.' :Magic min userId : '.$magicMin);
							$this->logger->logError(__FUNCTION__.' :Magic max userId : '.$magicMax);
							$resultId = rand($magicMin, $magicMax);
						}

						$MagicResult = $AddManager-> addMagic($db, $userId, $resultId);
						if ($MagicResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.' :Magic FAIL userId : '.$userId);
							continue;
						}
						$magicTemp['magicTableId'] = $MagicResult['magicTableId'];
						$magicTemp['magicId'] = $MagicResult['magicId'];
						$magicTemp['exp'] = 0;

						$magicData[] = $magicTemp;
					} else 	if ( $rewardType >= 99999 && $rewardType < 200000 ) {   //아이템 지급
						$itemResult = $AddManager->addItem($db, $userId, $rewardType,$rewardCount);
						if ($itemResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
							break;
						}
						$itemTemp['itemTableId'] = $itemResult['itemTableId'];
						$itemTemp['itemId'] = $itemResult['itemId'];
						$itemTemp['cur'] = $itemResult['cnt'];
						$itemTemp['add'] = $rewardCount;

						$total['items'][] = $itemTemp;
					} else {
						$this->logger->logError(__FUNCTION__.' :dont do that FAIL userId : '.$userId);
					}
					break;
			}
		}

		if (isset($weaponData)) {
			$total['weapons'] = $weaponData;
		}
		if (isset($magicData)) {
			$total['magics'] = $magicData;
		}
		if (isset($charData)) {
			$total['characs'] = $charData;
		}
		return $total;
	}

	function CheckBuy($param){ 
		
		$userId = $param["userId"];
		$productId = $param["productId"];
		
		$db = new DB();
		$ResultList[] = null;
		$sql = "select productId, purchaseTime from frdLogBuy where productId = :productId and userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':productId', $productId, PDO::PARAM_STR);
		$db -> bindValue(':userId', $userId, PDO::PARAM_STR);
		$db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$data["error"] = 1;
		} else {
			$data["error"] = 0;
		}
		
		$result['Protocol'] = 'ResCheckBuy';	
		$result['ResultCode'] = 100;	
		$result['Data'] = $data;	

		return $result;
	}
	function addItem ($db, $userId, $typeList, $amount) {
		$AddManager =  new AddManager();

		$chestCount  = count($typeList);
		$weaponData = null;
		for ( $idx = 0; $idx < $chestCount; $idx++ ) {
			$rewardType = (int)$typeList[$idx];
			if ( $rewardType < 1000 ) {        //고정 캐릭터 소울스톤 지급
				if ( $rewardType > 500 ) {
					$key_name = $rewardType-500;
					$apc_key = 'CharacIndex_'.$key_name;
					$charData = apc_fetch($apc_key);
					$charMin = $charData[0];
					$charMax = $charData[1];
					$charId = rand($charMin, $charMax);
				} else {
					$charId = $rewardType;
				}
				$CharResult = $AddManager->setAddHero($db, $userId, $charId, null);
				if ($CharResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.' :Weapon FAIL userId : '.$userId);
					continue;
				}
				$charTemp = null;
				$charTemp['charTableId'] = $CharResult['charTableId'];
				$charTemp['charId'] = $CharResult['charId'];
				$charTemp['exp'] = $CharResult['curExp'];
//				$charTemp['addExp'] = $CharResult['addExp'];

				$charData[] = $charTemp;
			} else if ( $rewardType < 2000 ) {    //보구 지급
				$rewardType = $rewardType - 1000;
				if ( $rewardType > 500 ) {
					$array_pos = $rewardType - 500;
					$apc_key = 'WeaponInform_WeaponMinIdx';
					$weaponDataApc = apc_fetch($apc_key);
					$minTemp = explode('/', $weaponDataApc[1]);
					$weaponMin = $minTemp[$array_pos];

					$apc_key = 'WeaponInform_WeaponMaxIdx';
					$weaponDataApc2 = apc_fetch($apc_key);
					$maxTemp = explode('/', $weaponDataApc2[1]);
					$weaponMax = $maxTemp[$array_pos];

					$rewardType = rand($weaponMin, $weaponMax);
				}

				// 아이템 지급 	
				$WeaponResult =	$AddManager->addWeapon($db, $userId, $rewardType, null);
				if ($WeaponResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.' :Weapon FAIL userId : '.$userId);
					continue;
				}
				$weaponT = null;
				$weaponT['weaponTableId'] = $WeaponResult['weaponTableId'];
				$weaponT['weaponId'] = $WeaponResult['weaponId'];
				$weaponT['level'] = $WeaponResult['level'];
				$weaponT['statIds'] = $WeaponResult['statIds'];
				$weaponT['statVals'] = $WeaponResult['statVals'];
				$weaponT['exp'] = $WeaponResult['exp'];

				$weaponData[] = $weaponT;
			} else if ( $rewardType  < 12000 ) {  //마법 지급
				$resultId = $rewardType-10000;
				if ( $resultId > 500 ) {
					$key_name = $resultId - 500;
					$apc_key = 'MagicPowderValue_'.$key_name;
							$magicData = apc_fetch($apc_key);
					$magicMin = $magicData[2];
					$magicMax = $magicData[3];	

					$resultId = rand($magicMin, $magicMax);
				}

				$MagicResult = $AddManager-> addMagic($db, $userId, $resultId);
				if ($MagicResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.' :Magic FAIL userId : '.$userId);
					continue;
				}
				$magicTemp = null;
				$magicTemp['magicTableId'] = $MagicResult['magicTableId'];
				$magicTemp['magicId'] = $MagicResult['magicId'];
				$magicTemp['exp'] = 0;

				$magicData[] = $magicTemp;
			} else {
				$this->logger->logError(__FUNCTION__.' :dont do that FAIL userId : '.$userId);
			}

		}
		$dataTotal = null;

        if (isset($weaponData)) {
            $dataTotal['weapons'] = $weaponData;
        }
        if (isset($magicData)) {
            $dataTotal['magics'] = $magicData;
        }
        if (isset($itemData)) {
            $dataTotal['items'] = $itemData;
        }
        if (isset($charData)) {
            $dataTotal['characs'] = $charData;
        }

		$result['data'] = $dataTotal;	
		return $result;
	}
}
?>
