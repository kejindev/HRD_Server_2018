<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
require_once "./common/define.php";
require_once './classes/AddManager.php';
require_once './classes/LogDBSendManager.php';

class InAppManager_YK {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function BuyCashYK($param) {
		$userId 	= $param['userId'];

		$result['Protocol'] = 'ResBuyCashYK';
		$result['ResultCode'] = 100;
		$tempResult = array();
		
		$db = new DB();
		$sql = "SELECT productId, orderId from frdLogBuy where userId = :userId and purchaseState = 100";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$tempResult['ResultCode'] = 200;
		}
		else {
			$tempResult['ResultCode'] = 100;
		}

		$result['Data'] = $tempResult;
		return $result;
	}		

	function addItemForPackage($db, $userId, $typeList, $amount) {
		$AssetManager =  new AssetManager();
		$AddManager =  new AddManager();

		$chestCount  = count($typeList);
		$total = null;
		$magicTemp = null;
		$itemTemp = null;
		$charTemp = null;
		$weaponTemp = null;
		$weaponData = null;
		$magicData = null;

		for ( $idx = 0; $idx < $chestCount; $idx++ ) {
			$rewardType = (int)$typeList[$idx];
			$rewardCount = (int)$amount[$idx];
			switch($rewardType) {
				case 5000: // 골드 지급
					$gold_result = $AssetManager->addGold($db, $userId, $rewardCount);
					if ($gold_result['ResultCode'] != 100 ) {
						return $gold_result;
					}
					$data = null;
					$data["curGold"] = $gold_result['gold'];
					$data["addGold"] = $rewardCount;
					$total['gold'] = $data;
					break;
				case 5001: //티켓 지급
					$AssetResult = $AssetManager->addUserExp($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$exp = $AssetResult['exp'];
					$total['exp'] = $exp;
					break;
				case 5002: //티켓 지급
					$AssetResult = $AssetManager->addTicket($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$data = null;
					$data["curTicket"] = $AssetResult['curTicket'];
					$data["addTicket"] = $rewardCount;
					$total['ticket'] = $data;
					break;
				case 5003: //행동력 지급
					$AssetResult = $AssetManager->addHeart($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$data = null;
					$data["curHeart"] = $AssetResult['curHeart'];
					$data["addHeart"] = $rewardCount;
					$data["maxHeart"] = $AssetResult['maxHeart'];
					$data["passTime"] = $AssetResult['passTime'];
					$total['heart'] = $data;
					break;
				case 5004: // 보석 지급
					$AssetResult = $AssetManager->addJewel($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$data = null;
					$data["curJewel"] = $AssetResult['jewel'];
					$data["addJewel"] = $rewardCount;
					$total['jewel'] = $data;
					break;
				case 5005: // 메달 지급
					$AssetResult = $AssetManager->addMedal($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$data = null;
					$data["curMedal"] = $AssetResult['skillPoint'];
					$data["addMedal"] = $rewardCount;
					$total['medal'] = $data;
					break;
				case 5006: // 메달 지급
					$AssetResult = $AssetManager->addPowder($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$data = null;
					$data["curPowder"] = $AssetResult['powder'];
					$data["addPowder"] = $rewardCount;
					$total['powder'] = $data;
					break;
				default :
					if ( $rewardType < 1000 ) {        //고정 캐릭터 소울스톤 지급
						if ( $rewardType > 500 ) {
							$key_name = $rewardType-500;
							$apc_key = 'CharacIndex_'.$key_name;
							$charData = apc_fetch($apc_key);
							$charMin = $charData[0];
							$charMax = $charData[1];
							$charId = rand($charMin, $charMax);
						} else {
							$charId = $rewardType;
						}
						$CharResult = $AddManager->setAddHero($db, $userId, $charId, $rewardCount);
						if ($CharResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.' :Weapon FAIL userId : '.$userId);
							continue;
						}
						$charTemp['charTableId'] = $CharResult['charTableId'];
						$charTemp['charId'] = $CharResult['charId'];
						$charTemp['exp'] = $CharResult['curExp'];

						$charData[] = $charTemp;
					} else if ( $rewardType < 2000 ) {    //보구 지급
						$rewardType = $rewardType - 1000;
						if ( $rewardType > 500 ) {
							$array_pos = $rewardType - 500;
							$apc_key = 'WeaponInform_WeaponMinIdx';
							$weaponDataApc = apc_fetch($apc_key);
							$minTemp = explode('/', $weaponDataApc[1]);
							$weaponMin = $minTemp[$array_pos];

							$apc_key = 'WeaponInform_WeaponMaxIdx';
							$weaponDataApc2 = apc_fetch($apc_key);
							$maxTemp = explode('/', $weaponDataApc2[1]);
							$weaponMax = $maxTemp[$array_pos];

							$rewardType = rand($weaponMin, $weaponMax);
						}

						// 아이템 지급 	
						$WeaponResult =	$AddManager->addWeapon($db, $userId, $rewardType, null);
						if ($WeaponResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.' :Weapon FAIL userId : '.$userId);
							continue;
						}
						$weaponTemp['weaponTableId'] = $WeaponResult['weaponTableId'];
						$weaponTemp['weaponId'] = $WeaponResult['weaponId'];
						$weaponTemp['level'] = $WeaponResult['level'];
						$weaponTemp['statIds'] = $WeaponResult['statIds'];
						$weaponTemp['statVals'] = $WeaponResult['statVals'];
						$weaponTemp['exp'] = $WeaponResult['exp'];

						$weaponData[] = $weaponTemp;
					} else if ( $rewardType  < 12000 ) {  //마법 지급
						$resultId = $rewardType-10000;
						if ( $resultId > 500 ) {
							$key_name = $resultId - 500;
							$this->logger->logError(__FUNCTION__.' :Magic key userId : '.$key_name);
							$apc_key = 'MagicPowderValue_'.$key_name;
							$magicData = apc_fetch($apc_key);
							$magicMin = $magicData[2];
							$magicMax = $magicData[3];	

							$this->logger->logError(__FUNCTION__.' :Magic min userId : '.$magicMin);
							$this->logger->logError(__FUNCTION__.' :Magic max userId : '.$magicMax);
							$resultId = rand($magicMin, $magicMax);
						}

						$MagicResult = $AddManager-> addMagic($db, $userId, $resultId);
						if ($MagicResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.' :Magic FAIL userId : '.$userId);
							continue;
						}
						$magicTemp['magicTableId'] = $MagicResult['magicTableId'];
						$magicTemp['magicId'] = $MagicResult['magicId'];
						$magicTemp['exp'] = 0;

						$magicData[] = $magicTemp;
					} else 	if ( $rewardType >= 99999 && $rewardType < 200000 ) {   //아이템 지급
						$itemResult = $AddManager->addItem($db, $userId, $rewardType,$rewardCount);
						if ($itemResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
							break;
						}
						$itemTemp['itemTableId'] = $itemResult['itemTableId'];
						$itemTemp['itemId'] = $itemResult['itemId'];
						$itemTemp['cur'] = $itemResult['cnt'];
						$itemTemp['add'] = $rewardCount;

						$total['items'][] = $itemTemp;
					} else {
						$this->logger->logError(__FUNCTION__.' :dont do that FAIL userId : '.$userId);
					}
					break;
			}
		}

		if (isset($weaponData)) {
			$total['weapons'] = $weaponData;
		}
		if (isset($magicData)) {
			$total['magics'] = $magicData;
		}
		if (isset($charData)) {
			$total['characs'] = $charData;
		}
		return $total;
	}

	function addItem ($db, $userId, $typeList, $amount) {
		$AddManager =  new AddManager();

		$chestCount  = count($typeList);
		$weaponData = null;
		for ( $idx = 0; $idx < $chestCount; $idx++ ) {
			$rewardType = (int)$typeList[$idx];
			if ( $rewardType < 1000 ) {        //고정 캐릭터 소울스톤 지급
				if ( $rewardType > 500 ) {
					$key_name = $rewardType-500;
					$apc_key = 'CharacIndex_'.$key_name;
					$charData = apc_fetch($apc_key);
					$charMin = $charData[0];
					$charMax = $charData[1];
					$charId = rand($charMin, $charMax);
				} else {
					$charId = $rewardType;
				}
				$CharResult = $AddManager->setAddHero($db, $userId, $charId, null);
				if ($CharResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.' :Weapon FAIL userId : '.$userId);
					continue;
				}
				$charTemp = null;
				$charTemp['charTableId'] = $CharResult['charTableId'];
				$charTemp['charId'] = $CharResult['charId'];
				$charTemp['exp'] = $CharResult['curExp'];
//				$charTemp['addExp'] = $CharResult['addExp'];

				$charData[] = $charTemp;
			} else if ( $rewardType < 2000 ) {    //보구 지급
				$rewardType = $rewardType - 1000;
				if ( $rewardType > 500 ) {
					$array_pos = $rewardType - 500;
					$apc_key = 'WeaponInform_WeaponMinIdx';
					$weaponDataApc = apc_fetch($apc_key);
					$minTemp = explode('/', $weaponDataApc[1]);
					$weaponMin = $minTemp[$array_pos];

					$apc_key = 'WeaponInform_WeaponMaxIdx';
					$weaponDataApc2 = apc_fetch($apc_key);
					$maxTemp = explode('/', $weaponDataApc2[1]);
					$weaponMax = $maxTemp[$array_pos];

					$rewardType = rand($weaponMin, $weaponMax);
				}

				// 아이템 지급 	
				$WeaponResult =	$AddManager->addWeapon($db, $userId, $rewardType, null);
				if ($WeaponResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.' :Weapon FAIL userId : '.$userId);
					continue;
				}
				$weaponT = null;
				$weaponT['weaponTableId'] = $WeaponResult['weaponTableId'];
				$weaponT['weaponId'] = $WeaponResult['weaponId'];
				$weaponT['level'] = $WeaponResult['level'];
				$weaponT['statIds'] = $WeaponResult['statIds'];
				$weaponT['statVals'] = $WeaponResult['statVals'];
				$weaponT['exp'] = $WeaponResult['exp'];

				$weaponData[] = $weaponT;
			} else if ( $rewardType  < 12000 ) {  //마법 지급
				$resultId = $rewardType-10000;
				if ( $resultId > 500 ) {
					$key_name = $resultId - 500;
					$apc_key = 'MagicPowderValue_'.$key_name;
							$magicData = apc_fetch($apc_key);
					$magicMin = $magicData[2];
					$magicMax = $magicData[3];	

					$resultId = rand($magicMin, $magicMax);
				}

				$MagicResult = $AddManager-> addMagic($db, $userId, $resultId);
				if ($MagicResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.' :Magic FAIL userId : '.$userId);
					continue;
				}
				$magicTemp = null;
				$magicTemp['magicTableId'] = $MagicResult['magicTableId'];
				$magicTemp['magicId'] = $MagicResult['magicId'];
				$magicTemp['exp'] = 0;

				$magicData[] = $magicTemp;
			} else {
				$this->logger->logError(__FUNCTION__.' :dont do that FAIL userId : '.$userId);
			}

		}
		$dataTotal = null;

        if (isset($weaponData)) {
            $dataTotal['weapons'] = $weaponData;
        }
        if (isset($magicData)) {
            $dataTotal['magics'] = $magicData;
        }
        if (isset($itemData)) {
            $dataTotal['items'] = $itemData;
        }
        if (isset($charData)) {
            $dataTotal['characs'] = $charData;
        }

		$result['data'] = $dataTotal;	
		return $result;
	}

	function YKReceive($param) {
		$resultFail['Protocol'] = 'ResYKReceive';
		$resultFail['ResultCode'] = 300;
		$userId = $param['userId'];
		$productId = $param['productId'];

		$db = new DB();
		$sql = "SELECT * from frdYKBuyLog where userId = :userId and  state = 100";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
		// 	$this->logger->logError(__FUNCTION__.' : EXISIT userId : '.$userId.' orderId :'. $orderId);
		// 	return $resultFail;
		// }

		// if ($row['state'] != 100) {
			$this->logger->logError(__FUNCTION__.' : NOT YET userId : '.$userId.' orderId :'. $orderId);
			$resultFail['ResultCode'] = 101;

			return $resultFail;
		}

		
		$data = json_decode($row['log']);	

		$sql = "UPDATE frdYKBuyLog SET state = 200 where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			return $resultFail;
		}

		$sql = "UPDATE frdLogBuy SET purchaseState = 200 where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			return $resultFail;
		}


		$result['Protocol'] = 'ResYKReceive';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;

	}

	function YKReceive_back($param) {
		$resultFail['Protocol'] = 'ResYKReceive';
		$resultFail['ResultCode'] = 300;
		$userId = $param['userId'];
		$orderId = $param['orderId'];
		$productId = $param['productId'];

		$db = new DB();
		$sql = "SELECT orderId, purchaseState from frdLogBuy where orderId = :orderId and userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':orderId', $orderId, PDO::PARAM_STR);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$this->logger->logError(__FUNCTION__.' : EXISIT userId : '.$userId.' orderId :'. $orderId);
			return $resultFail;
		}

		if ($row['purchaseState'] != 100) {
			$this->logger->logError(__FUNCTION__.' : NOT YET userId : '.$userId.' orderId :'. $orderId);
			$resultFail['ResultCode'] = 101;

			return $resultFail;
		}
		
	
		$sql = "SELECT shopItemId, itemId, itemCount,price FROM frdShopData 
			WHERE productId = :productId";
		$db -> prepare($sql);
		$db -> bindValue(':productId', $productId, PDO::PARAM_STR);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			// 존재 하지 않는 상품 
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			return $resultFail;
		}

		$shopItemId = $row['shopItemId'];
		$itemId = $row['itemId'];
		$itemCount = $row['itemCount'];
		$price = $row['price'];

		// 패키지 
		if ( $shopItemId > 6000 && $shopItemId != 6005 ) {
			$tempTypes = explode('/',$itemId);	
			$tempCnts = explode('/',$itemCount);	

			$addResult = $this->addItemForPackage($db, $userId, $tempTypes, $tempCnts);
			// 보석 
		} else if ( $shopItemId > 1000 && $shopItemId < 2000) {

			$AssetManager = new AssetManager();
			$AssetResult = $AssetManager->addJewel($db, $userId, $itemCount);

		} else if ( $shopItemId == 6005  ) {	

			$sql = "UPDATE frdMonthlyCard SET startTime = now(), duration = 30, recvCnt = 1
				WHERE UserId = :userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
				return $resultFail;
			}

			$sql = "INSERT INTO frdUserPost
				(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
			$sql .= "($userId, 101, 5004, 20, DATE_ADD(now(), INTERVAL 30 DAY ), now()), ";  // jewel
			$sql .= "($userId, 101, 5003, 20, DATE_ADD(now(), INTERVAL 30 DAY ), now())  ";  // heart
			$db -> prepare($sql);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
				return $resultFail;
			}
		}

		$sql = "UPDATE frdLogBuy SET purchaseState = 200 where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			return $resultFail;
		}

		$result['Protocol'] = 'ResYKReceive';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;

	}
}

?>
