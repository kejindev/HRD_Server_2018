<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
require_once './classes/AssetManager.php';
require_once './classes/AddManager.php';
require_once "./classes/DailyQuestManager.php";
require_once './classes/LogDBSendManager.php';

class ItemManager {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function SellItem ($param ) {
		$resultFail['ResultCode'] = 300;

		$userId 	= $param["userId"];
		$itemId 	= $param["itemId"];
		$itemCount 	= $param["itemCount"];

		$itemCount = abs($itemCount);

		$db = new DB();

		$sql = "select gold, session from frdUserData where userId = :userId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError('sql : ' . $sql. ' userId: ' .$userId );
			return $resultFail;
		}

		$myGold = $row['gold'];

		$sql = "SELECT itemTableId, itemCount FROM frdHavingItems WHERE userId = :userId and itemId=:itemId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':itemId', $itemId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError('sql : ' . $sql. ' userId: ' .$userId );
			return $resultFail;
		}
		$itemTableId = $row['itemTableId'];
		$myCount = $row['itemCount'];

		if ( $itemCount > $myCount ) {
			$this->logger->logError('Fail SellItem userId : ' . $userId. ' Cnt: ' .$itemCount. ' need : '.$row["itemCount"] );
			return $resultFail;
		}

		$price = $this->GetItemPrice($itemId);
		$resultItemCount = $myCount - $itemCount;
		$resultGold = $price*$itemCount;

		if ( $resultItemCount <= 0 ) {
			$sql = "DELETE FROM frdHavingItems WHERE userId = :userId AND itemId=:itemId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':itemId', $itemId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('selItem : fail DELETE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}
		} else {
			$sql = "UPDATE frdHavingItems SET itemCount=:resultItemCount WHERE userId = :userId and itemId=:itemId";
			$db -> prepare($sql);
			$db -> bindValue(':resultItemCount', $resultItemCount, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':itemId', $itemId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('selItem : fail UPDATE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}
		}

		$AssetManager = new AssetManager();	
		$AssetResult = $AssetManager->addGold($db,$userId, $resultGold);
		if ($AssetResult['ResultCode'] != 100) {
			$resultFail['ResultCode'] = $AssetResult['ResultCode'];
			return $resultFail;
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendItem($userId, $itemId, $resultItemCount + $itemCount, $resultItemCount, 0 - $itemCount);

		$item = null;
		$item['itemTableId'] = $itemTableId;
		$item['itemId'] = $itemId;
		$item['add'] = 0 - $itemCount;
		$item['cur'] = $resultItemCount;

		$data['use']['items'][] = $item;

		$gold = null;
		$gold['addGold'] = $resultGold;
		$gold['curGold'] = $AssetResult['gold'];

		$data['rewards']["gold"] = $gold;

		$result['Data'] = $data;
		$result['Protocol'] = 'ResSellItem';
		$result['ResultCode'] = 100;
		return $result;
	}

	function ChangeWeaponStat ($param ) {
		$resultFail['ResultCode'] = 309;

		$userId	= $param["userId"];
		$weaponTableId  = $param["weaponTableId"];

		// HARD CODE
		$itemId	= 100000;
		$itemCount 	= 1;

		$db = new DB();
		$sql = "
		SELECT itemCount FROM frdHavingItems WHERE userId = :userId AND itemId = :itemId";
		$db->prepare($sql);
		$db->bindValue(':itemId', $itemId, PDO::PARAM_INT);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError('sql : '.$sql.' userId: '.$userId);
			return $resultFail;
		}

		$myCount = $row['itemCount'];
		if ( $itemCount > $myCount ) {
			$this->logger->logError('chgWeaponStat Fail userId : ' . $userId. ' Cnt: ' .$itemCount. ' need : '.$row["itemCount"] );
			return $resultFail;
		}

		$resultItemCount = $myCount - $itemCount;

		if ( $resultItemCount <= 0 ) {
			$sql = "DELETE FROM frdHavingItems WHERE userId = :userId AND itemId = :itemId";
			$db -> prepare($sql);
			$db->bindValue(':itemId', $itemId, PDO::PARAM_INT);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);

			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('chgWeaponStat : fail DELETE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}
		} else {
			$sql = "UPDATE frdHavingItems SET itemCount=:resultItemCount
				 WHERE userId = :userId AND itemId = :itemId";
			$db -> prepare($sql);
			$db -> bindValue(':resultItemCount', $resultItemCount, PDO::PARAM_INT);
			$db->bindValue(':itemId', $itemId, PDO::PARAM_INT);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('chgWeaponStat : fail UPDATE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendItem($userId, $itemId, $myCount, $resultItemCount, 0 - $itemCount);

		$sql = "
		SELECT weaponId FROM frdHavingWeapons 
			WHERE weaponTableId = :weaponTableId and  userId = :userId ";
		$db->prepare($sql);
		$db->bindValue(':weaponTableId', $weaponTableId, PDO::PARAM_INT);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError('sql : '.$sql.' userId: '.$userId);
			return $resultFail;
		}
		$weaponId = $row['weaponId'];

		$apc_key = 'WeaponOption_'.$weaponId;
		$apc_data = apc_fetch($apc_key);
		if (!$apc_data) {
			$this->logger->logError(__FUNCTION__.' : APC Wrong userId : '.$userId.", weaponId : ".$weaponId);
			return $resultFail;
		}


		$statId0 = 0;
		$statVal0 = 0;
		$statId1 = 0;
		$statVal1 = 0;
		$statId2 = 0;
		$statVal2 = 0;
		$statId3 = 0;
		$statVal3 = 0;
		$statId4 = 0;
		$statVal4 = 0;


		$idNo0 = (int)$apc_data[1];
		$idNo1 = (int)$apc_data[4];
		$idNo2 = (int)$apc_data[7];
		$idNo3 = (int)$apc_data[10];

		if (isset($apc_data[13])) {
			$idNo4 = (int)$apc_data[13];
		} else {
			$idNo4 = 0;
		}

		$No101Max = 14; // 0 ~ 16
		$No102Max = 14; // 0 ~ 16
		$No103Max = 21; // 0 ~ 16

		if ($idNo0) {
			if ($idNo0 == 101) {
				$statId0 = rand(0,$No101Max);
			} else if ($idNo0 == 102) {
				$statId0 = rand(0,$No102Max);
			} else {
				$statId0 = $idNo0;
			}
		}
		$statVal0 = rand(0, 250);

		if ($idNo1 ) {
			if ($idNo1 == 101) {
				$statId1 = rand(0,$No101Max);
			} else if ($idNo1 == 102) {
				$statId1 = rand(0,$No102Max);
			} else {
				$statId1 = $idNo1;
			}
		}
		$statVal1 = rand(0, 250);

		if ($idNo2 ) {
			if ($idNo2 == 101) {
				$statId2 = rand(0,$No101Max);
			} else if ($idNo2 == 102) {
				$statId2 = rand(0,$No102Max);
			} else {
				$statId2 = $idNo2;
			}
		}
		$statVal2 = rand(0, 250);

		if ($idNo3 ) {
			if ($idNo3 == 101) {
				$statId3 = rand(0,$No101Max);
			} else if ($idNo3 == 102) {
				$statId3 = rand(0,$No102Max);
			} else {
				$statId3 = $idNo3;
			}
		}
		$statVal3 = rand(0, 250);

		if ($idNo4) {
			if ($idNo4 == 103) {
				$statId4 = rand(0,$No103Max);
				// 14, 15 미사용. 
				if ($statId4 == 14 || $statId4 == 15) {
					$statId4 += 2;
				}
			} else {
				$statId4 = $idNo4;
			}
		}
		$statVal4 = rand(0, 250);


		$sql = "UPDATE frdHavingWeapons 
		SET 
		statId0=:statId0,
		statId1=:statId1,
		statId2=:statId2,
		statId3=:statId3,
		statId4=:statId4,
		statVal0=:statVal0,
		statVal1=:statVal1,
		statVal2=:statVal2,
		statVal3=:statVal3,
		statVal4=:statVal4
		 WHERE  weaponTableId = :weaponTableId AND userId = :userId ";
		$db -> prepare($sql);
        $db -> bindValue(':statId0', $statId0, PDO::PARAM_INT);
        $db -> bindValue(':statId1', $statId1, PDO::PARAM_INT);
        $db -> bindValue(':statId2', $statId2, PDO::PARAM_INT);
        $db -> bindValue(':statId3', $statId3, PDO::PARAM_INT);
        $db -> bindValue(':statId4', $statId4, PDO::PARAM_INT);
        $db -> bindValue(':statVal0', $statVal0, PDO::PARAM_INT);
        $db -> bindValue(':statVal1', $statVal1, PDO::PARAM_INT);
        $db -> bindValue(':statVal2', $statVal2, PDO::PARAM_INT);
        $db -> bindValue(':statVal3', $statVal3, PDO::PARAM_INT);
        $db -> bindValue(':statVal4', $statVal4, PDO::PARAM_INT);
		$db->bindValue(':weaponTableId', $weaponTableId, PDO::PARAM_INT);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError('chgWeaponStat : fail UPDATE userId :' . $userId );
			$this -> logger -> logError('sql : ' . $sql );
			return $resultFail;
		}
		$statIds = null;
		$statIds[] = (int)$statId0;
		$statIds[] = (int)$statId1;
		$statIds[] = (int)$statId2;
		$statIds[] = (int)$statId3;
		$statIds[] = (int)$statId4;

		$statVals = null;	
		$statVals[] = (int)$statVal0;
		$statVals[] = (int)$statVal1;
		$statVals[] = (int)$statVal2;
		$statVals[] = (int)$statVal3;
		$statVals[] = (int)$statVal4;

		$data["itemCount"] = $resultItemCount;
		
		$result['Protocol'] = 'ResChangeWeaponStat';
		$result['ResultCode'] = 100;
		$data['statIds'] = $statIds;
		$data['statVals'] = $statVals;
		$result['Data'] = $data;

		return $result;
	}

	function ComposeRune ($param ) {
		$resultFail['ResultCode'] = 300;

		$userId = $param["userId"];
		$matRuneId = (int)$param["matRuneId"];
		if ( $matRuneId < 100000 )
    		$resultRuneId = 100004;
 		else
   			$resultRuneId = $matRuneId + 1000;

		$resultAddCount = (int)$param["resultAddCount"];

		$db = new DB();
		$sql = "select powder  from frdUserData where userId = :userId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError('sql : ' . $sql. ' userId: ' .$userId );
			return $resultFail;
		}

		$myPowder = $row['powder'];
		$needPowder = $this->GetNeedPowderWhenComposeRune($matRuneId) * $resultAddCount;
		if ( $myPowder < $needPowder ) {
			$this->logger->logError('composeRune userId : '.$userId.' Powder Cnt: '.$myPowder.' need : '.$needPowder);
			return $resultFail;
		}

		$sql = "SELECT itemCount FROM frdHavingItems 
			WHERE userId = :userId and itemId=:matRuneId";
		$db->prepare($sql);
		$db->bindValue(':matRuneId', $matRuneId, PDO::PARAM_INT);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError('sql : ' . $sql. ' userId: ' .$userId );
			return $resultFail;
		}
		$myCount = $row['itemCount'];

		$sql = "SELECT itemCount FROM frdHavingItems 
			WHERE userId = :userId and itemId=:resultRuneId";
		$db->prepare($sql);
		$db->bindValue(':resultRuneId', $resultRuneId, PDO::PARAM_INT);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$addItemCount = 0;
		} else {
			$addItemCount = $row['itemCount'];
		}

		$needRune = ($this->GetNeedRuneWhenComposeRune($matRuneId)) * $resultAddCount;
		if ( $needRune > $myCount ) {
			$this->logger->logError('composeRune userId : '.$userId.'R Cnt: '.$myCount.'need : '.$reddRune);
			return $resultFail;
		}

		$resultPowder = $myPowder - $needPowder;
		$resultMatRune = $myCount - $needRune;
		$resultResultRune = $addItemCount + $resultAddCount;


		if ( $resultMatRune <= 0 ) {
			$sql = "DELETE FROM frdHavingItems WHERE userId = :userId AND itemId=:itemId";
			$db -> prepare($sql);
			$db -> bindvalue(':userId', $userId, pdo::PARAM_INT);
			$db -> bindvalue(':itemId', $matRuneId, pdo::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logerror('selitem : fail delete userId :' . $userId );
				$this -> logger -> logerror('sql : ' . $sql );
				return $resultFail;
			}
		} else {
			$sql = "UPDATE frdHavingItems SET itemCount=:resultMatRune
			 WHERE userId =:userId and itemId=:itemId";
			$db -> prepare($sql);
			$db -> bindValue(':resultMatRune', $resultMatRune, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':itemId', $matRuneId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('selItem : fail UPDATE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendItem($userId, $matRuneId, $myCount, $resultMatRune, 0 - $needRune);
		

		$AddManager = new AddManager();
		$AddResult = $AddManager->addItem($db, $userId, $resultRuneId, $resultAddCount);

		$AssetManager = new AssetManager();	
		$AssetResult = $AssetManager->usePowder($db,$userId, $needPowder);

		$DailyQuestManager = new DailyQuestManager();
		$DailyQuestManager->updateDayMission($db, $userId, 11, 1);


		$powder["curPowder"] = $AssetResult['powder'];
		$powder["addPowder"] = 0-$needPowder;
		$data['use']['powder'] = $powder;

		$matItem['itemId'] = $matRuneId;
		$matItem['add'] = 0- $needRune;
		$matItem['cur'] = $resultMatRune;
		$data['use']['items'][] = $matItem;

		$addItem['itemId'] = $resultRuneId;
		$addItem['add'] = $resultAddCount;
		$addItem['cur'] = $resultResultRune;
		$data['rewards']["items"][] = $addItem;

		$result['Data'] = $data;
		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResComposeRune';
	
		return $result;
	}
	function SpecialExpStone($param) {
		$resultFail['ResultCode'] = 300;
		$userId = $param["userId"];
		$charTableId = $param["charTableId"];

		$db = new DB();
		$pass_SpecialExpStone = 100004;
		$addExpCount = 50;

		$sql = "select charId, exp from frdCharInfo
			where charTableId = :charTableId and userId=:userId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':charTableId', $charTableId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.'sql : ' . $sql. ' userId: ' .$userId.' char :'.$charTableId );
			return $resultFail;
		}

		$exp = $row['exp'];
		$charId = $row['charId'];

		$sql = "select itemCount from frdHavingItems where userId = :userId and itemId=:pass_SpecialExpStone";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':pass_SpecialExpStone', $pass_SpecialExpStone, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.'sql : ' . $sql. ' userId: ' .$userId );
			return $resultFail;
		}
		$resultItemCount = $row["itemCount"]-1;


		if ( $resultItemCount <= 0 ) {
			$sql = "DELETE from frdHavingItems where userId=:userId and itemId=:pass_SpecialExpStone";
			$db -> prepare($sql);
			$db -> bindvalue(':userId', $userId, pdo::PARAM_INT);
			$db -> bindvalue(':pass_SpecialExpStone', $pass_SpecialExpStone, pdo::PARAM_INT);
		} else {
			$sql = "update frdHavingItems set itemCount = itemCount -1, update_date = now()
				where userId=:userId and itemId=:pass_SpecialExpStone";	
			$db -> prepare($sql);
			$db -> bindvalue(':userId', $userId, pdo::PARAM_INT);
			$db -> bindvalue(':pass_SpecialExpStone', $pass_SpecialExpStone, pdo::PARAM_INT);

		}
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logerror(__FUNCTION__.' : fail userId :' . $userId );
			$this -> logger -> logerror('sql : ' . $sql );
			return $resultfail;
		}


		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendItem($userId, $pass_SpecialExpStone, $resultItemCount-1, $resultItemCount, -1 );
			
		$AddManager = new AddManager();
		$AddResult = $AddManager->setAddHero($db, $userId, $charId, $addExpCount);
		if ($AddResult['ResultCode'] != 100 ) { 
			$this->logger->logerror(__FUNCTION__.' : fail userId :' . $userId );
			return $resultfail;
		}

		$data['charTableId'] = $AddResult['charTableId'];
		$data['charId'] = $AddResult['charId'];
		$data['curExp'] = $AddResult['curExp'];
		$data['addExp'] = $AddResult['addExp'];
	
		$result['Protocol'] = 'ResSpecialExpStone';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;
		return $result;
	}

	function UseItem ($param ) {
		$resultFail['ResultCode'] = 300;

		$userId 	= $param["userId"];
		$itemId 	= (int)$param["itemId"];
		
		$db = new DB();


		$sql = "SELECT itemTableId, itemCount FROM frdHavingItems WHERE userId = :userId and itemId=:itemId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':itemId', $itemId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError('sql : ' . $sql. ' userId: ' .$userId );
			return $resultFail;
		}
		$itemTableId = $row['itemTableId'];
		$myCount = $row['itemCount'];
		if ( $myCount < 1 ) {
			$this->logger->logError('Fail SellItem userId : ' . $userId. ' Cnt: ' .$itemCount. ' need : '.$row["itemCount"] );
			return $resultFail;
		}

		switch ($itemId) {

		case 100006:		// 경험치 부스트 티켓. 7일간 2배
			$sql = "SELECT endTime FROM frdBoost WHERE userId = :userId and itemId=:itemId";
			$db->prepare($sql);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db->bindValue(':itemId', $itemId, PDO::PARAM_INT);
			$db->execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if ($row) {
				$existEndTime = strtotime($row["endTime"]);
				$timenow = date("Y-m-d H:i:s");
				$curTime = strtotime($timenow);
				if ( $curTime < $existEndTime ) {
					$data['exist'] = $row['endTime'];
					$result['Data'] = $data;
					$result['Protocol'] = 'ResUseItem';
					$result['ResultCode'] = 100;
					return $result;
				}

				$sql = "DELETE FROM frdBoost WHERE userId = :userId AND itemId=:itemId";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':itemId', $itemId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this -> logger -> logError('selItem : fail DELETE userId :' . $userId. ' sql : ' . $sql );
					return $resultFail;
				}
			}

			$date = new DateTime();
			switch ($itemId) {
				case 100006:		// 경험치 부스트 티켓. 1일간 2배
					$data['boost']['type'] 			= 5001;
					$data['boost']['mulPercent'] 	= 200 - 100;
					$data['boost']['maxUserLv'] 	= 65;
	    			$date->add(new DateInterval('P1D'));
					$data['boost']['endTime'] 		= $date->format("Y-m-d H:i:s");
					break;
				
				default:
					$data['boost']['type'] 			= 5000;
					$data['boost']['mulPercent'] 	= 100 - 100;
					$data['boost']['maxUserLv'] 	= 65;
					$date->add(new DateInterval('P1D'));
					$data['boost']['endTime'] 		= $date->format("Y-m-d H:i:s");
					break;
			}
			$data['boost']['itemId']		 		= $itemId;
		
			// Boost Table에 추가
			$sql = "INSERT INTO frdBoost (userId, itemId, maxUserLv, resourceType, mulPercent, recvCnt, startTime, endTime, update_date) 
					VALUES (			:userId, :itemId, :maxUserLv, :resourceType, :mulPercent, 0, now(), :endTime, now()
							)";
			$db -> prepare($sql);
			$db -> bindValue(':userId',			$userId, 						PDO::PARAM_INT);
			$db -> bindValue(':itemId',			$itemId, 						PDO::PARAM_INT);
			$db -> bindValue(':maxUserLv',		$data['boost']['maxUserLv'],	PDO::PARAM_INT);
			$db -> bindValue(':resourceType',	$data['boost']['type'], 		PDO::PARAM_INT);
			$db -> bindValue(':mulPercent',		$data['boost']['mulPercent'],	PDO::PARAM_INT);
			$db -> bindValue(':endTime', 		$data['boost']['endTime'], 		PDO::PARAM_STR);
			
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('sql : ' . $sql. ' userId: ' .$userId. ' itemId: ' .$itemId );
				return $resultFail;
			}
			break;

		case 190008:	//선물 상자
			$rewards = array(
				array(1, array(5004, 5)),
				array(1, array(5004, 10)),
				array(1, array(5004, 15)),
				array(1, array(5000, 1000)),
				array(1, array(5000, 3000)),
				array(1, array(5000, 5000)),
				array(1, array(5006, 50)),
				array(1, array(5006, 100)),
				array(1, array(5006, 200)),
				array(1, array(5003, 10)),
				array(1, array(5003, 20)),
				array(1, array(5003, 30)),
				array(1, array(5002, 2)),
				array(1, array(5002, 4)),
				array(1, array(5002, 6)),
				array(2, array(100000, 1)),
				array(1, array(100003, 1)),
				array(1, array(100003, 2)),
				array(2, array(100003, 3)),
				array(1, array(99999,  rand(1,3))),
				array(1, array(1495, 1, 1495, 1)),
				array(2, array(1495, 1, 1495, 1, 1495, 1, 1495, 1)),
				array(2, array(1495, 1, 1495, 1, 1495, 1, 1495, 1, 1495, 1, 1495, 1)),
				array(1, array( rand(105000, 105009), 1) ),
				array(1, array( rand(105000, 105009), 3) ),
				array(2, array( rand(105000, 105009), 5) ),
				array(2, array( rand(106000, 106009), 1) ),
				array(2, array( rand(106000, 106009), 2) ),
				array(3, array( rand(106000, 106009), 3) ),

				array(1, array( rand(115000, 115004), 1) ),
				array(2, array( rand(115000, 115004), 2) ),
				array(2, array( rand(115000, 115004), 3) ),

				array(3, array( 115000,2, 115001,2, 115002,2, 115003,2, 115004,2 )),
				array(2, array(110000, 1)),
				array(3, array(110000, 3)),

				array(3, array(10504, 1)),
				array(5, array(10505, 1)),
				array(4, array(1505,  1)),

				array(5, array(98,  10)),
				array(5, array(283, 10))
			);
			while(true) {
				$rewardIdx = rand(0, count($rewards)-1);
				$rewards[$rewardIdx][0]--;
				if ( $rewards[$rewardIdx][0] <= 0 )
					break;
			}
			$arr_Rewards = $rewards[$rewardIdx][1];
			$sql = "INSERT INTO frdUserPost (recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
			for ( $i=0; $i<count($arr_Rewards); $i+=2 ) {
				$rewardType = $arr_Rewards[$i];
				$rewardCount = $arr_Rewards[$i+1];
				if ( $i !== 0 )
					$sql .= ", ";
				$sql .= "($userId, 5, $rewardType, $rewardCount, DATE_ADD(now(), INTERVAL 30 DAY ), now())"; 
			}
			
			$db -> prepare($sql);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError('sql : ' . $sql. ' userId: ' .$userId. ' itemId: ' .$itemId );
				return $resultFail;
			}

			$data['presentChest'] = 1;
			break;

		default:
			break;
		}





		$resultItemCount = $myCount - 1;
		if ( $resultItemCount <= 0 ) {
			$sql = "DELETE FROM frdHavingItems WHERE userId = :userId AND itemId=:itemId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':itemId', $itemId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('selItem : fail DELETE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}
		} else {
			$sql = "UPDATE frdHavingItems SET itemCount=:resultItemCount WHERE userId = :userId and itemId=:itemId";
			$db -> prepare($sql);
			$db -> bindValue(':resultItemCount', $resultItemCount, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':itemId', $itemId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('selItem : fail UPDATE userId :' . $userId );
				$this -> logger -> logError('sql : ' . $sql );
				return $resultFail;
			}
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendItem($userId, $itemId, $resultItemCount+1, $resultItemCount, -1);

		$item = null;
		$item['itemTableId'] = $itemTableId;
		$item['itemId'] = $itemId;
		$item['add'] = -1;
		$item['cur'] = $resultItemCount;

		$data['use']['items'][] = $item;

		$result['Data'] = $data;
		$result['Protocol'] = 'ResUseItem';
		$result['ResultCode'] = 100;
		return $result;
	}







	function GetNeedRuneWhenComposeRune($matRuneId) {
		if ( $matRuneId < 100000 )
			return 10;

		$runeGrade = (int)round(($matRuneId - 110000)/1000);
		switch ($runeGrade) {
			case 1:
				return 3;
			case 2:
				return 5;
			case 3:
				return 7;
			case 4:
				return 9;
			default:
				return 10;
		}
	}

	function GetNeedPowderWhenComposeRune($matRuneId) {
		if ( $matRuneId < 100000 )
			return 500;

		$runeGrade = (int)round(($matRuneId - 110000)/1000);
		switch ($runeGrade) {
			case 1:
				return 5;
			case 2:
				return 10;
			case 3:
				return 50;
			case 4:
				return 100;
			default:
				return 100;
		}
	}

	function GetItemPrice($itemId) {
		$itemVal = floor(($itemId-100000)/1000);
		switch ($itemVal) {
			case -1:
				return 500;
				
			case 0:

				switch($itemId) {
					case 100000:    //유물 변경권
						return 1500;
					case 100001:    //특포 초기화권
						return 2500;
					case 100002:    // 닉변
						return 2500;
					case 100005:    // 닉변
						return 2500;
					default:
						return 2000;
				}

				break;

			case 1:             //1티어 잡동사니
				return 100;

			case 2:
				return 200;

			case 3:
				return 400;

			case 4:
				return 800;

			case 5:
				return 1500;

			case 6:             //6티어 잡동사니
				return 3000;

			case 10:
				return 3333;

			case 11:                //1티어 Rune
				return 44;

			case 12:
				return 98;

			case 13:
				return 220;

			case 14:
				return 538;

			case 15:
				return 1412;

			case 20:        //히든 잡동사니
				return 50000;

			case 90:    //event item
				return 1;
			default:
				return 50000;
		}

	}




}
?>
