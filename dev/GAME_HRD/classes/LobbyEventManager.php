<?php
require_once './common/DB.php';
require_once './common/define.php';
require_once './lib/Logger.php';
require_once './send_data/SendDataManager.php';

class LobbyEventManager {

	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function IsActiveEvent($event_Key) {
		$SendDataManager = new SendDataManager();
        $apc_key =  "Event_".$GLOBALS['COUNTRY'].'_'.$event_Key;
        $eventType = apc_fetch($apc_key);
        if (!$eventType)
            return false;

        $event_now = date('ymdH');
        $eventResult = $SendDataManager->getEventData($event_Key+1, $eventType[1],$event_now);
        if (!$eventResult)
            return false;

        return $eventResult;
	}

	//LobbyManager, InAppManager, InAppManager_YK 레아패키지
	public function sendReaPackReward(&$db,$userId, $postSendId) {
		$jewelCount = 20;
		$heartCount = 20;
		if ( $this->IsActiveEvent(EVENT_DOUBLE_LEAPACKAGE) ) {
			$jewelCount *= 2;
			$heartCount *= 2;
		}

		$sql = "INSERT INTO frdUserPost 
			(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
		$sql .= "($userId, $postSendId, 5004, $jewelCount, DATE_ADD(now(), INTERVAL 30 DAY ), now()), ";  
		$sql .= "($userId, $postSendId, 5003, $heartCount, DATE_ADD(now(), INTERVAL 30 DAY ), now())  "; 
		$db -> prepare($sql);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return false;
		}

		$sql = "UPDATE frdMonthlyCard SET recvCnt = recvCnt + 1 where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return false;
		}

		return true;
	}

	function clear_frdClearEvent(&$db, $userId) {
		$sql = "UPDATE frdClearEvent SET progress = 0, update_date = now() WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' :  FAIL userId :'.$userId.' sql : '.$sql);

			$sql = "INSERT INTO frdClearEvent ( userId, progress, update_date)
				VALUES (:userId, 0, now())";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql: '.$sql);
			}
		}
	}

	//LobbyManager - 출첵
	function getChkAttendanceReward(&$db, $userId) {
        $curYMD = (int)date('ymd');
       
        $SendDataManager = new SendDataManager();

        $eventCheck = $this->IsActiveEvent(EVENT_CHK_ATTENDANCE);
       	if ( false == $eventCheck )
       		return;

        $sql = "SELECT lastYMD, totalChk FROM Event_ChkAttendance WHERE userId=:userId ";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {

			$sql = "INSERT INTO Event_ChkAttendance ( userId, startYMD, lastYMD, totalChk )
											VALUES (:userId, :startYMD, :lastYMD, 0)";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':startYMD', (int)date('ymd'), PDO::PARAM_INT);
			$db -> bindValue(':lastYMD', (int)date('ymd'), PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql: '.$sql);
				return;
			}

			$lastYMD = 0;
			$totalChk = 0;
		}
		else {
			$lastYMD = (int)$row['lastYMD'];
			$totalChk = (int)$row['totalChk'];
		}
			
		if ( $curYMD <= $lastYMD )
			return;

		$lastYMD = $curYMD;
		$totalChk++;
		$sql = "UPDATE Event_ChkAttendance SET lastYMD = :lastYMD, totalChk = :totalChk WHERE userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':lastYMD', $lastYMD, PDO::PARAM_INT);
		$db -> bindValue(':totalChk', $totalChk, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql : '.$sql);
			return;
		}

		$rewardType = array();
		$rewardCount = array();

		$arr_eventCheck = explode(",", $eventCheck[0]);
		$start_ymdH = $arr_eventCheck[0];
		$startDate = new DateTime('20'.$start_ymdH.'0000');
		$targetDate = new DateTime(date('YmdHmi'));
		$interval = $startDate->diff($targetDate);
		$rewardDay = (int)$interval->days + 1;
		switch ($rewardDay) {
			case 1: $rewardType[] = 190008; $rewardCount[] = 2; break;
			case 2: $rewardType[] = 190008; $rewardCount[] = 2; break;
			case 3: $rewardType[] = 190008; $rewardCount[] = 2; break;
			case 4: $rewardType[] = 190008; $rewardCount[] = 2; break;
			case 5: $rewardType[] = 190008; $rewardCount[] = 2; break;
			case 6: $rewardType[] = 190008; $rewardCount[] = 2; break;
			case 7: $rewardType[] = 190008; $rewardCount[] = 2; break;
			case 8: $rewardType[] = 190008; $rewardCount[] = 2; break;
			case 9: $rewardType[] = 190008; $rewardCount[] = 2; break;
			case 10: $rewardType[] = 190008; $rewardCount[] = 1; break;
			// case 7: 
			// 	for ( $i=115000; $i<115005; $i++ ) {
			// 		$rewardType[] = $i; $rewardCount[] = 5;
			// 	}
			// 	break;
			// case 8: $rewardType[] = 110000; $rewardCount[] = 1; break;

			// 	$runes = array();
			// 	for ( $i=0; $i<5; $i++ )
			// 		$runes[rand(115000, 115004)] += 1;
			// 	foreach ( $runes as $id => $count ) {
			// 		$rewardType[] = $id; $rewardCount[] = $count; 
			// 	}
			// 	break;

			// case 6:	//4티어 아이템 렌덤 10개
			// $questItems = array();
			// for ( $i=0; $i<10; $i++ )
			// 	$questItems[rand(104000, 104009)] += 1;
			// foreach ( $questItems as $id => $count ) {
			// 	$rewardType[] = $id; $rewardCount[] = $count; 
			// }
			// break;
			// case 7: $rewardType[] = 100000; $rewardCount[] = 2; break;
			default:break;
		}
		// if ( $totalChk == 8 ) {
		// 	$rewardType[] = 110000; $rewardCount[] = 1;
		// 	$rewardType[] = 1505; $rewardCount[] = 1;
		// 	$rewardType[] = 10504; $rewardCount[] = 1;
		// 	$rewardType[] = 100003; $rewardCount[] = 5;
		// }
		

		$sql = "INSERT INTO frdUserPost (recvUserId, sendUserId, type, count, expire_time, reg_date) values ";
		for ( $ii=0; $ii<count($rewardType); $ii++ ) {
			$rt = $rewardType[$ii];
			$rc = $rewardCount[$ii];
			if ( $ii != 0 )
				$sql .= ", ";
			$sql .= "($userId, 5, $rt, $rc, DATE_ADD(now(), INTERVAL 7 DAY ), now()) ";
		}
		$db -> prepare($sql);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			return $resultFail;
		}
	}


	//LobbyManager - 복귀이벤트
	function getWelcomebackReward(&$db, $userId) {
        

        $SendDataManager = new SendDataManager();
        
        $eventCheck = $this->IsActiveEvent(EVENT_RETURN_USER);
       	if ( false == $eventCheck )
       		return;

       	$this->logger->logError(__FUNCTION__.' : HiHiHiHi');
			
        $apcContents = apc_fetch('EventData'.$GLOBALS['COUNTRY'].EVENT_RETURN_USER);
		if ( $apcContents == false || $apcContents == null ) {
			$this->logger->logError(__FUNCTION__.' : There is no apc_fetch '.'EventData'.$GLOBALS['COUNTRY'].EVENT_RETURN_USER);
			return false;
		}

		if (is_object($apcContents))	//stdClass -> array
			$apcContents = get_object_vars($apcContents);
		
		$rewardArrJson = $apcContents['rewardArrJson'];

		$rewardArr = json_decode($rewardArrJson);
		$returnDate = $apcContents['returnDate'];
		switch (strlen($returnDate)) {
			case 6:	$returnDate = '20'.$returnDate.'000000';	break;
			case 8:	$returnDate = '20'.$returnDate.'0000';		break;
			default:
				$this->logger->logError(__FUNCTION__.' : returnDate is error : '.$returnDate);
				return false;
		}
		
		$str_returnDate = date('Y-m-d H:i:s', strtotime($returnDate));


        $sql = "SELECT attendDate FROM frdMonthlyCard WHERE userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (
			($row && strtotime($row['attendDate']) < strtotime($returnDate))
			|| empty($row) || empty($row['attendDate'])
		) {

			$sql = "INSERT INTO frdUserPost (recvUserId, sendUserId, type, count, expire_time, reg_date) values ";
			for ( $i=0; $i<count($rewardArr[0]); $i++ ) {
				if ( $i !== 0 )
					$sql .=", ";
				$sql .= "($userId, 10, ".$rewardArr[0][$i].", ".$rewardArr[1][$i].", DATE_ADD(now(), INTERVAL 7 DAY ), now()) ";
			}

			$db -> prepare($sql);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			}

		}
	}

	// LobbyManager - 핫타임
	function getHottimeBonus () {
		$result = array();
		$eventCheck = $this->IsActiveEvent(EVENT_HOTTIME);
		if ( false == $eventCheck ) {
			$result['eventCheck'] = null;
			return $result;
		}

		$hour = (int)date('H');
		$isAM = false;
		$isPM = false;
		if ( $hour>=12 && $hour<14 )
			$isAM = true;
		else if ( $hour>=20 && $hour<22 )
			$isPM = true;
		else {
			$result['eventCheck'] = null;
			return $result;
		}

		$arr_eventCheck = explode(",", $eventCheck[0]);
		$start_ymdH = $arr_eventCheck[0];
		$startDate = new DateTime('20'.$start_ymdH.'0000');
		$targetDate = new DateTime(date('YmdHmi'));
		$interval = $startDate->diff($targetDate);
		$rewardDay = (int)$interval->days + 1;

		$bonusArr = null;
		switch ($rewardDay) {
			case 1:
				if ( $isAM ) {	
					$result['str']="핫타임 이벤트|일반 스테이지 클리어시 메달 획득량이 1.5배 증가합니다.";
					$result['obj']="icon_buf_Medal_o";
				}
				if ( $isPM ) {
					$result['str']="핫타임 이벤트|일반/히든/신전 스테이지 클리어시 룬 획득 량이 1.5배 증가합니다.";
					$result['obj']="icon_buf_Roon_o";
				}	
				break;
			
			case 2:
				if ( $isAM ) {	
					$result['str']="핫타임 이벤트|일반 스테이지 클리어시 골드 획득량이 1.5배 증가합니다.";
					$result['obj']="icon_buf_Gold_o";
				}
				if ( $isPM ) {
					$result['str']="핫타임 이벤트|일반 스테이지 클리어시 경험치 획득량이 1.5배 증가합니다.";
					$result['obj']="icon_buf_Exp_o";
				}	
				break;

			case 3:
				if ( $isAM ) {
					$result['str']="핫타임 이벤트|히든/신전 스테이지 클리어시 퀘스트 아이템 획득 확률이 1.5배 증가합니다.";
					$result['obj']="icon_buf_QuestItem_o";
				}
				if ( $isPM ) {
					$result['str']="핫타임 이벤트|히든/신전 스테이지 클리어시 퀘스트 아이템 획득 확률이 1.5배 증가합니다.";
					$result['obj']="icon_buf_QuestItem_o";
				}
				break;

			case 4:
				if ( $isAM ) {
					$result['str']="핫타임 이벤트|일반 스테이지 클리어시 영혼석 획득 량이 2배 증가합니다.";
					$result['obj']="icon_buf_Soulstone_o";	
				}
				if ( $isPM ) {
					$result['str']="핫타임 이벤트|일반 스테이지 클리어시 영혼석 획득 량이 2배 증가합니다.";
					$result['obj']="icon_buf_Soulstone_o";
				}
				break;

			case 5:
				if ( $isAM ) {	
					$result['str']="핫타임 이벤트|일반 스테이지 클리어시 메달 획득량이 1.5배 증가합니다.";
					$result['obj']="icon_buf_Medal_o";
				}
				if ( $isPM ) {
					$result['str']="핫타임 이벤트|일반/히든/신전 스테이지 클리어시 룬 획득 량이 1.5배 증가합니다.";
					$result['obj']="icon_buf_Roon_o";
				}	
				break;

			case 6:
				if ( $isAM ) {	
					$result['str']="핫타임 이벤트|일반 스테이지 클리어시 골드 획득량이 1.5배 증가합니다.";
					$result['obj']="icon_buf_Gold_o";
				}
				if ( $isPM ) {
					$result['str']="핫타임 이벤트|일반 스테이지 클리어시 경험치 획득량이 1.5배 증가합니다.";
					$result['obj']="icon_buf_Exp_o";
				}	
				break;

			case 7:
				if ( $isAM ) {
					$result['str']="핫타임 이벤트|히든/신전 스테이지 클리어시 퀘스트 아이템 획득 확률이 1.5배 증가합니다.";
					$result['obj']="icon_buf_QuestItem_o";
				}
				if ( $isPM ) {
					$result['str']="핫타임 이벤트|히든/신전 스테이지 클리어시 퀘스트 아이템 획득 확률이 1.5배 증가합니다.";
					$result['obj']="icon_buf_QuestItem_o";
				}
				break;

			default:
				break;
		}
		$result['eventCheck'] = $eventCheck;
		return $result;
	}

}

/*
@PostTitle0,운영자님으로부터 받은 선물
@PostTitle1,일일퀘스트 보상
@PostTitle2,출석 보상
@PostTitle3,고난의 탑 보상
@PostTitle4,쿠폰 보상
@PostTitle5,이벤트 보상
@PostTitle6,업적 보상
@PostTitle10,복귀 수호자님을 위한 선물,
@PostTitle200,스타터 패키지,
@PostTitle201,허니잼 패키지,
@PostTitle202,히랜디 패키지,
@PostTitle203,어빌리티 패키지,
@PostTitle204,영혼 각인석 패키지1,
@PostTitle205,영혼 각인석 패키지2,
@PostTitle206,엑스칼리버 패키지2,
@PostTitle207,일로케이터 패키지2,
@PostTitle208,갓핸드 패키지2,
@PostTitle209,일곱장막전승자 패키지,
@PostTitle210,이벤트 어빌리티 패키지,
@PostTitle211,메달 패키지,
@PostTitle212,메달 묶음 패키지,
@PostTitle213,영혼각인석 패키지,
@PostTitle214,영혼각인석 묶음 패키지,
@PostTitle215,설날 패키지 1,
@PostTitle216,설날 패키지 2,
@PostTitle217,설날 패키지 3,
@PostTitle218,설날 패키지 4,
*/
?>

