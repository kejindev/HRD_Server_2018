<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
require_once './classes/LobbyEventManager.php';

class LobbyManager {

    public function __construct() {
        $this -> logger = Logger::get();
    }

	function UniqueRandomNumbersWithinRange($min, $max, $quantity) {
		$numbers = range($min, $max);
		shuffle($numbers);
		return array_slice($numbers, 0, $quantity);
	}
	function isBonusHeartTime($time) {
		if ( ( $time >= 12 && $time < 14 ) || ( $time >= 18 && $time < 20 ) ) {
			return true;
		}
		return false;
	}

	function GetEnterLobbyCheck ($param) {
		$LobbyEventManager = new LobbyEventManager();
		$resultFail['ResultCode'] = 300;

		$userId = $param['userId'];	

		$questMax = 13;

		// 30일 패키지 지급 로직 ---> 
		// 지급 조건 시작일로 부터. 30일의 기간동안만 지급
		// 중간에 로그인 하지 못한다면. 지급 하지 않는다.
		$attendanceReward = null;
		$data = null;
		$db = new DB();
		$sql = " 
			SELECT DATEDIFF(now(), startTime) as datediff, duration, recvCnt,
			DATEDIFF(now(), attendDate) as attendDiff, attendCount, 
			questIdx0,progress0,questIdx1,progress1,
			questIdx2,progress2,questIdx3,progress3,
			questIdx4,progress4,freeH1, freeH2,tripleCrown
			FROM frdMonthlyCard WHERE userId = :userId";
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {

        	// 이벤트 처리 매니저
			$LobbyEventManager->clear_frdClearEvent($db, $userId);
			$LobbyEventManager->getChkAttendanceReward($db, $userId);
			$LobbyEventManager->getWelcomebackReward($db, $userId);
		 	
			$newResult = $this->newUserSetMonth($db,$userId);
			$attendanceCount = 1;
			$questData = $newResult['questData'];

			// 일일 출석 보상 지급 
			$attendResult = $this->attendRewardSend($db, $userId, $attendanceCount);
			if (!$attendResult) {
				return $resultFail;
			}
			$questData['attendanceRewardData'] = $attendResult['attendanceReward'];

			$monthlyCardData["hasCard"] = false;
			$data["monthlyCardData"] = $monthlyCardData;
        }
        else {
			$datediff 	= $row["datediff"];  // 레아패키지 시작일 
			$lastRecvDay = $row["recvCnt"];  // 마지막 수령일. 

			// 레아패키지 시작일부터 30일이지 지나면 지급 종료 체크를 위해 
			$duration	 = $row["duration"];
			// 마지막 로그인과 체크 시간의 경과일 
			$attendDiff = $row["attendDiff"];

			// 출석 카운트 
			$attendanceCount = (int)$row["attendCount"];

			//################# NEW ATTEND START ######################
			// 현재에서 시작 일을 빼서 얼마나 경과 했는지 일수로 계산
			// 진행 일수가 30일 보다 크면 hasCard 가 false 이다.

			// 금일처음 출석 
			if ($attendDiff > 0 ) {

				// 레아 패키지 지급 --->
				$monthlyCardData["hasCard"] = false; 

				if ( ( $datediff >  0) && ($datediff <= $duration)) {
					// 진행 일수가 마지막 지급일 보다 크면 아이템을 지급
					if ($datediff+1 > $lastRecvDay) {
						// 레아 패키지 지급 
						$postSendId = $datediff + 101; 
						$reaResult = $LobbyEventManager->sendReaPackReward($db, $userId, $postSendId);
						if (!$reaResult) {
							return $resultFail;
						}
						// 레아 지급 정보 
						$monthlyCardData["GiftCount"] = 2;
						$monthlyCardData["hasCard"] = true; 
						$data["monthlyCardData"] = $monthlyCardData;
					}
				}
				// <----- 레아 패키지 지급 완료 


				// 이벤트 처리 매니저
				
				$LobbyEventManager->clear_frdClearEvent($db, $userId);
				$LobbyEventManager->getChkAttendanceReward($db, $userId);
				$LobbyEventManager->getWelcomebackReward($db, $userId);

				if ($attendanceCount > 6) {
					$attendanceCount = 1;
				} else {
					$attendanceCount += 1;
				}

				// 일일 미션 초기화 					
				$resetDaily = $this->updateDailyMission($db, $userId, $attendanceCount);
				if (!$resetDaily) {
					return $resultFail;
				}
				$questData = $resetDaily;

				// 일일 출석 보상 지급 
				$attendResult = $this->attendRewardSend($db, $userId, $attendanceCount);
				if (!$attendResult) {
					return $resultFail;
				}
				// 일일 퀘스트, 출석 정보 입력 	 
				$questData["attendanceCount"] = $attendanceCount;
				$questData["attendanceRewardData"] = $attendResult['attendanceReward'];
				$questData["isInitialize"] = true;

				// 신전 클리어 카운트 초기화 
				$templeInit = $this->resetTempleClearCount($db, $userId);
				if (!$templeInit) {
					return $resultFail;
				}

				
				

				// 업적 체크 
				$this->achieveCheck($db, $userId);
			//################# NEW ATTEND END ######################
			} 
			else {
				$monthlyCardData["hasCard"] = false;
				$data["monthlyCardData"] = $monthlyCardData;

				$questIdxs = null;
				$progress = null;
				$questIdxs[] = $row['questIdx0'];
				$questIdxs[] = $row['questIdx1'];
				$questIdxs[] = $row['questIdx2'];
				$questIdxs[] = $row['questIdx3'];
				$questIdxs[] = $row['questIdx4'];
				$progress[]  = $row['progress0'];
				$progress[]  = $row['progress1'];
				$progress[]  = $row['progress2'];
				$progress[]  = $row['progress3'];
				$progress[]  = $row['progress4'];

				$now = time();
				$nowTime = intval(date("H", $now));
				$tripleCnt = 0;
				foreach($progress as $pr) {
					if ($pr == -1 ) {
						$tripleCnt++;
					}
				}

				if ($this->isBonusHeartTime($nowTime))  {
					if (  $nowTime >= 12 && $nowTime < 14   ) {
						if ( $row['freeH1'] == 0 )  {
							$questIdxs[] = 29;
							$progress[]  = 0;
						} else {
							$questIdxs[] = 29;
							$progress[]  = -1;
						}
					} else {
						if ( $row['freeH2'] == 0 )  {
							$questIdxs[] = 30;
							$progress[]  = 0;
						} else {
							$questIdxs[] = 30;
							$progress[]  = -1;
						}
					}
				}

				$questIdxs[] = 31;
				if ($row['tripleCrown'] == -1) {
					$progress[]  = -1;
				} else {
					$progress[]  = $tripleCnt;
				}

				$questData['questIdxs'] = $questIdxs;
				$questData['progress'] = $progress;

				
			}
		}

		$hottimeEventResult = $LobbyEventManager->getHottimeBonus();
		if ($hottimeEventResult['eventCheck']) {
			$tData = array();
			$tData['str'] = $hottimeEventResult['str'];
			$tData['obj'] = $hottimeEventResult['obj'];
			$data['eventprefab'][] = $tData;
		}
		

		//부스트 아이템 체크
		$sql = "SELECT itemId, resourceType, mulPercent, maxUserLv, endTime FROM frdBoost WHERE userId = :userId and now()<endTime";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {

			$boost = null;
			$boost['itemId'] 		= $row['itemId'];
			$boost['type'] 			= $row['resourceType'];
			$boost['mulPercent'] 	= $row['mulPercent'];
			$boost['maxUserLv'] 	= $row['maxUserLv'];
			$boost['endTime'] 		= $row['endTime'];
			$data['boost'][] = $boost;

        }
		


		// 로비갱신 아이디 발급. 
		$curPIdResult = $this->checkCurPId($db, $userId);
		$data['curPId'] = $curPIdResult['curPId'];
		$data["nowTime"] = $curPIdResult['nowTime'];
	
		$data['questData'] = $questData;

		$result['Protocol'] = 'ResGetEnterLobbyCheck';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}

	function newUserSetMonth($db, $userId) {
		$resetQuest = $this->getResetDailyQuestData();

		$questData = $resetQuest['questData'];
		$questIdxs = $questData['questIdxs'];

		$sql = "INSERT INTO frdMonthlyCard
			( userId, attendDate, attendCount, startTime, recvCnt, duration,
			  questIdx0, progress0,
			  questIdx1, progress1,
			  questIdx2, progress2,
			  questIdx3, progress3,
			  questIdx4, progress4,
			  freeH1, freeH2,
			  update_date
			)
			VALUES ( :userId, now(), 1,  now(), 0, 0,
					:q1,0,
					:q2,0,
					:q3,0,
					:q4,0,
					:q5,0,
					0,0,
					now()) ";

		$db -> prepare($sql);
		$db -> bindValue(':q1', $questIdxs[0], PDO::PARAM_INT);
		$db -> bindValue(':q2', $questIdxs[1], PDO::PARAM_INT);
		$db -> bindValue(':q3', $questIdxs[2], PDO::PARAM_INT);
		$db -> bindValue(':q4', $questIdxs[3], PDO::PARAM_INT);
		$db -> bindValue(':q5', $questIdxs[4], PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$questIdxs[0]);
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$questIdxs[1]);
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$questIdxs[2]);
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$questIdxs[3]);
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$questIdxs[4]);
			return false;
		}

		$result = null;
		$result['questData'] = $questData;

		return $result;
	}

	

	function getResetDailyQuestData() {

		$questMax = 8;

		$arr = $this->UniqueRandomNumbersWithinRange(0, $questMax, 5);

		$questIdxs = null;
		$progress = null;
		$questIdxs[] = $arr[0];
		$questIdxs[] = $arr[1];
		$questIdxs[] = $arr[2];
		$questIdxs[] = $arr[3];
		$questIdxs[] = $arr[4];
		$progress[]  = 0;
		$progress[]  = 0;
		$progress[]  = 0;
		$progress[]  = 0;
		$progress[]  = 0;

		$now = time();
		$nowTime = intval(date("H", $now));

		if ($this->isBonusHeartTime($nowTime))  {
			if (  $nowTime >= 12 && $nowTime < 14   ) {
				$questIdxs[] = 29;
				$progress[]  = 0;
			} else {
				$questIdxs[] = 30;
				$progress[]  = 0;
			}
		}

		$questIdxs[] = 31;
		$progress[]  = 0;

		$questData["questIdxs"] = $questIdxs;
		$questData["progress"] = $progress;

		$result = null;
		$result['questData'] = $questData;

		return $result;
	}

	function updateDailyMission($db, $userId, $attendanceCount) {
		$resetQuest = $this->getResetDailyQuestData();

		$questData = $resetQuest['questData'];
		$questIdxs = $questData['questIdxs'];

		$sql = "UPDATE frdMonthlyCard SET 
			attendDate = now(), attendCount = :attendanceCount,
					   questIdx0 = :q0,  progress0 = 0,
					   questIdx1 = :q1,  progress1 = 0,
					   questIdx2 = :q2,  progress2 = 0,
					   questIdx3 = :q3,  progress3 = 0,
					   questIdx4 = :q4,  progress4 = 0,
					   freeH1 = 0, freeH2 = 0, tripleCrown = 0
						   WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':attendanceCount', $attendanceCount, PDO::PARAM_INT);
		$db -> bindValue(':q0', $questIdxs[0], PDO::PARAM_INT);
		$db -> bindValue(':q1', $questIdxs[1], PDO::PARAM_INT);
		$db -> bindValue(':q2', $questIdxs[2], PDO::PARAM_INT);
		$db -> bindValue(':q3', $questIdxs[3], PDO::PARAM_INT);
		$db -> bindValue(':q4', $questIdxs[4], PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return false;
		}

		return $questData;
	}

	function attendRewardSend($db, $userId, $attendanceCount) {
		$attendanceReward["type"] = array( 5000, 5005, 5004, 5002, 5003, 5004, 110000 );
		$attendanceReward["count"] = array( 1000, 50, 20, 3, 30, 50, 1 );

		$rewardType 	= $attendanceReward["type"][$attendanceCount - 1];
		$rewardCount 	= $attendanceReward["count"][$attendanceCount - 1];

		if ( ($rewardType>=1000&&$rewardType<1500) || ($rewardType>=10000&&$rewardType<10500) ) {
			$sql = "INSERT INTO frdUserPost (recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
			for ( $ii=0; $ii<$rewardCount-1; $ii++ )
				$sql .= "($userId, 2, $rewardType, 1, DATE_ADD(now(), INTERVAL 3 DAY),now()),";
			$sql .= "($userId, 2, $rewardType, 1, DATE_ADD(now(), INTERVAL 3 DAY), now())";
			$db -> prepare($sql);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : 1000~ FAIL userId :'.$userId.' sql : '.$sql);
				return false ;
			}
		} else {
			$sql = "INSERT INTO frdUserPost (recvUserId, sendUserId, type, count, expire_time, reg_date) 
				VALUES (:userId, 2, :rewardType, :rewardCount, DATE_ADD(now(), INTERVAL 3 DAY), now() )";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':rewardType', $rewardType, PDO::PARAM_INT);
			$db -> bindValue(':rewardCount', $rewardCount, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
				return false ;
			}
		}

		$result['attendanceReward'] = $attendanceReward;
		return $result;	
	}
		
	function resetTempleClearCount($db, $userId) {
		// 신전 일일 클리어 횟수 초기화
		$sql = "UPDATE frdTempleClearCount SET dailyCount = 0, update_date = now() WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' :  FAIL userId :'.$userId.' sql : '.$sql);
			return false ;
		}


		return true;	
	}

	function achieveCheck($db, $userId) {
		// 업적 시작 
		$sql = "select achieveId, cnt from frdAchieveInfo where userId=:userId and achieveId > 1600 and achieveId < 1700";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$achieveId = $row['achieveId'];

			require_once './classes/AchieveManager.php';
			$AchieveManager = new AchieveManager();
			$AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
		}
	
		return true;	
	}

	function checkCurPId($db, $userId) {
		$result = null;
		$sql = "
				SELECT 	UNIX_TIMESTAMP(NOW()) as nowTime, curPId FROM frdUserData
				WHERE userId = :userId ";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.': FAIL userId :'.$userId.' sql : '.$sql);
			return false;
		}

		$result['curPId'] = $row['curPId']+1;
		$result["nowTime"] = $row['nowTime'];

		$sql = "UPDATE frdUserData SET curPId = curPId + 1 WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.': FAIL userId :'.$userId.' sql : '.$sql);
			return false;
		}

		return $result;
	}
}
?>
