<?php
include_once './common/log_DB.php';
require_once './lib/Logger.php';

class LogDBSendManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

    function sendAsset($userId, $type, $pre, $cur, $cnt ) {

        /* Common Device Info Create Ready */
        $db = new log_DB();
        $sql = <<<SQL
INSERT INTO asset_data (userId, type, pre, cur, cnt, protocol, reg_date)
 VALUES ( :userId, :type, :pre,  :cur, :cnt, :protocol, now())
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':type', $type, PDO::PARAM_INT);
        $db -> bindValue(':pre', $pre, PDO::PARAM_INT);
        $db -> bindValue(':cur', $cur, PDO::PARAM_INT);
        $db -> bindValue(':cnt', $cnt, PDO::PARAM_INT);
        $db -> bindValue(':protocol', $GLOBALS['protocol'], PDO::PARAM_STR);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__.': FAIL userId.' . $userId);
            $result['ResultCode'] = 300;
            return false;
        }

		return true;
    }

    function sendChar($userId, $charId, $pre, $cur, $cnt ) {

        /* Common Device Info Create Ready */
        $db = new log_DB();
        $sql = <<<SQL
INSERT INTO char_data (userId, charId, pre, cur, cnt, protocol, reg_date)
 VALUES ( :userId, :charId, :pre,  :cur, :cnt, :protocol, now())
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':charId', $charId, PDO::PARAM_INT);
        $db -> bindValue(':pre', $pre, PDO::PARAM_INT);
        $db -> bindValue(':cur', $cur, PDO::PARAM_INT);
        $db -> bindValue(':cnt', $cnt, PDO::PARAM_INT);
        $db -> bindValue(':protocol', $GLOBALS['protocol'], PDO::PARAM_STR);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__.': FAIL userId.' . $userId);
            $result['ResultCode'] = 300;
            return false;
        }

		return true;
    }

    function sendItem($userId, $itemId, $pre, $cur, $cnt ) {
        $db = new log_DB();
        $sql = <<<SQL
INSERT INTO item_data (userId, itemId, pre, cur, cnt, protocol, reg_date)
 VALUES ( :userId, :itemId, :pre,  :cur, :cnt, :protocol, now())
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':itemId', $itemId, PDO::PARAM_INT);
        $db -> bindValue(':pre', $pre, PDO::PARAM_INT);
        $db -> bindValue(':cur', $cur, PDO::PARAM_INT);
        $db -> bindValue(':cnt', $cnt, PDO::PARAM_INT);
        $db -> bindValue(':protocol', $GLOBALS['protocol'], PDO::PARAM_STR);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__.': FAIL userId.' . $userId);
            $result['ResultCode'] = 300;
            return false;
        }

		return true;
    }

    function sendMagic($userId, $magicId, $type, $level) {
        $db = new log_DB();
        $sql = <<<SQL
INSERT INTO magic_data (userId, magicId, type, level, protocol, reg_date)
 VALUES ( :userId, :magicId, :type, :level, :protocol, now())
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':magicId', $magicId, PDO::PARAM_INT);
        $db -> bindValue(':type', $type, PDO::PARAM_INT);
        $db -> bindValue(':level', $level, PDO::PARAM_INT);
        $db -> bindValue(':protocol', $GLOBALS['protocol'], PDO::PARAM_STR);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__.': FAIL userId.' . $userId);
            $result['ResultCode'] = 300;
            return false;
        }

		return true;
    }

    function sendStage($userId, $stageId, $type) {
        $db = new log_DB();
        $sql = <<<SQL
INSERT INTO stage_data (userId, stageId, type, reg_date)
 VALUES ( :userId, :stageId, :type, now())
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
        $db -> bindValue(':type', $type, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__.': FAIL userId.' . $userId);
            $result['ResultCode'] = 300;
            return false;
        }

		return true;
    }

    function sendWeapon($userId, $weaponId, $type, $level, $exp, $statId0, $statId1, $statId2, $statId3, $statVal0, $statVal1, $statVal2, $statVal3) {

        $db = new log_DB();
        $sql = <<<SQL
INSERT INTO weapon_data (
			userId, weaponId, type, level,  exp, 
			statId0, statId1, statId2, statId3, 
			statVal0, statVal1, statVal2, statVal3, 
			protocol, reg_date )
 VALUES ( 	:userId, :weaponId, :type, :level, :exp, 
			:statId0, :statId1, :statId2, :statId3, 
			:statVal0, :statVal1, :statVal2, :statVal3,
			:protocol, now()
		)
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':weaponId', $weaponId, PDO::PARAM_INT);
        $db -> bindValue(':type', $type, PDO::PARAM_INT);
        $db -> bindValue(':level', $level, PDO::PARAM_INT);
        $db -> bindValue(':exp', $exp, PDO::PARAM_INT);

        $db -> bindValue(':statId0', $statId0, PDO::PARAM_INT);
        $db -> bindValue(':statId1', $statId1, PDO::PARAM_INT);
        $db -> bindValue(':statId2', $statId2, PDO::PARAM_INT);
        $db -> bindValue(':statId3', $statId3, PDO::PARAM_INT);

        $db -> bindValue(':statVal0', $statVal0, PDO::PARAM_INT);
        $db -> bindValue(':statVal1', $statVal1, PDO::PARAM_INT);
        $db -> bindValue(':statVal2', $statVal2, PDO::PARAM_INT);
        $db -> bindValue(':statVal3', $statVal3, PDO::PARAM_INT);

        $db -> bindValue(':protocol', $GLOBALS['protocol'], PDO::PARAM_STR);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__.': FAIL userId.' . $userId.' sql '. $sql);
            $result['ResultCode'] = 300;
            return false;
        }

		return true;
    }

    function sendWeapon_bulk($userId, $bulkInsert) {

        $db = new log_DB();
        $sql = "INSERT INTO weapon_data (
			userId, weaponId, type, level,  exp, 
			statId0, statId1, statId2, statId3, 
			statVal0, statVal1, statVal2, statVal3, 
			protocol, reg_date )  VALUES ".$bulkInsert;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__.': FAIL userId.' . $userId.' sql '. $sql);
            return false;
        }
		return true;
    }
}
?>

