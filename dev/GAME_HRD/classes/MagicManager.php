<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
require_once './classes/AssetManager.php';
require_once './classes/AchieveManager.php';
require_once './classes/DailyQuestManager.php';
require_once './classes/LogDBSendManager.php';

class MagicManager {

	public function __construct() {
		$this -> logger = Logger::get();
	}

	public function GetMagicsInfo ($param) {
		$resultFail['ResultCode'] = 300;

		$db = new DB();

		$userId = $param['userId'];	
		require_once './classes/UserInfoManager.php';

		$UserInfoManager = new UserInfoManager();

		$U_Result = $UserInfoManager->getHavingMagics($db, $userId);
		
		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResGetMagicsInfo';
		$result['Data'] = $U_Result;

		return $result;	
	}

	public function BuyMana ($param) {
		$resultFail['ResultCode'] = 300;

		$userId = $param['userId'];	
		$buyPrice = $param['price'];	

		$db = new DB();

		$sql = "SELECT gold, manaLevel, session FROM frdUserData WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logerror(__FUNCTION__.' : fail userId :'.$userId.' sql:'.$sql);
			return $resultFail;
		}  
		$myGold = $row['gold'];
		$myManaLevel = $row["manaLevel"]+1;

		$price = $this->GetManaLevelUpPrice($row["manaLevel"]);
		if ( $price != $buyPrice) {
			$this -> logger -> logerror('setBuyMana : NOT MATCH userId :' . $userId);
			$this -> logger -> logerror('setBuyMana : price .'.$price.' buy:'.$buyPrice);
			return $resultFail;
		}

		if ($price > $myGold) {
			$this -> logger -> logerror('setBuyMana : NOT ENOUGH GOLD userId :' . $userId);
			return $resultFail;
		}

		$AssetManager = new AssetManager();
		$AssetResult = $AssetManager->useGold($db, $userId, $price);
		if ( $AssetResult['ResultCode'] != 100) {
			$this -> logger -> logerror('setBuyMana : FAIL use userId :' . $userId );
			return $resultFail;
		}
		
		$gold['curGold'] = $AssetResult['gold'];
		$gold['addGold'] = 0 - $price;


		$sql = "UPDATE frdUserData SET manaLevel=:myManaLevel WHERE userId=:userId";
		$db -> prepare($sql);
		$db -> bindvalue(':myManaLevel', $myManaLevel, PDO::PARAM_INT);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logerror(__FUNCTION__.' : fail userId :'.$userId.' sql:'.$sql);
			return $resultFail;
		}

		require_once './classes/AchieveManager.php';
		$sql = "select achieveId, cnt from frdAchieveInfo where userId=:userId and achieveId > 1500 and achieveId < 1600";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$achieveId = $row['achieveId'];
			$AchieveManager = new AchieveManager();
			$AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
		}

		$data['use']['gold'] = $gold;

		$data["resultLevel"] = $myManaLevel;

		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResBuyMana';
		$result['Data'] = $data;

		return $result;
	}

	public function ComposeMagicGold($param){
		$resultFail['ResultCode'] = 300;

		$userId		= $param["userId"];
		$privateIdx = $param["privateIdx"];
		$matPrivateIdx = $param["matPrivateIdx"];

		$db = new DB();
		
		$sql = "SELECT gold FROM frdUserData WHERE userId = :userId";
        $db -> prepare($sql);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logerror('setComposeArti : select fail userId :' . $userId);
            return $resultFail;
        }
        $myGold = $row['gold'];

		$sql = "SELECT * FROM frdHavingMagics WHERE  
				( magicTableId=:privateIdx    and userId=:userId ) or 
				( magicTableId=:matPrivateIdx and userId=:userId )";

		$db -> prepare($sql);
		$db -> bindvalue(':privateIdx', $privateIdx, PDO::PARAM_INT);
		$db -> bindvalue(':matPrivateIdx', $matPrivateIdx, PDO::PARAM_INT);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$resultList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$resultList[] = $row;	
		} 

		
		foreach ($resultList as $rs) {
			$curPIdx = (int)$rs["magicTableId"];
			if ( $curPIdx == $privateIdx ) {  // desktop
				$deskGrade = (int)$this->GetGrade($rs["magicId"]);
				$deskLevel = (int)$rs["level"];
			}
			else {                            // material
				$matGrade = (int)$this->GetGrade($rs["magicId"]);
				$matLevel = (int)$rs["level"];
				$matId = (int)$rs["magicId"];
			}
		}
		if ( (int)$matGrade !== (int)$deskGrade ) {
            $this -> logger -> logerror('setComposeArti : GRADE err userId :' . $userId);
            return $resultFail;
		}
		
		if ( ($matLevel !== $this->GetMaxLevel()) || ($deskLevel !== $this->GetMaxLevel()) ) {
            $this -> logger -> logerror('setComposeArti : Level err userId :' . $userId);
            return $resultFail;
		}

		$price = $this->GetGradeUpPrice($deskGrade);
		if ($myGold < $price ) {
            $this -> logger -> logerror('setComposeArti : not enought gold userId :'.$userId);
            $this -> logger -> logerror('setComposeArti : desk :'.$price.'my '.$myGold);
            return $resultFail;
		}

		$AssetManager = new AssetManager();
		$AssetResult = $AssetManager->useGold($db, $userId, $price);
		if ($AssetResult['ResultCode'] != 100) {
			$this -> logger -> logerror(__FUNCTION__.' : FAIL use userId :' . $userId );
			return $resultFail;
		}

		$gold['curGold'] = $AssetResult['gold'];
		$gold['addGold'] = 0 - $price;

		$sql = "DELETE FROM frdHavingMagics WHERE magicTableId=:matPrivateIdx AND userId=:userId";
		$db -> prepare($sql);
		$db -> bindvalue(':matPrivateIdx', $matPrivateIdx, PDO::PARAM_INT);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
        $row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			$this->logger->logError(__FUNCTION__.' : FAIL matPrivateIdx :'.$matPrivateIdx);
			return $resultFail;
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendMagic($userId, $matId, 2, $matLevel);

		$deskGrade = $deskGrade +1;

		if ( $deskGrade >= $this->GetMaxGrade() ) {
			$deskGrade = $this->GetMaxGrade();
		}

		$apc_key = 'MagicPowderValue_'.$deskGrade;
		$md = apc_fetch($apc_key);
		$magicMin = $md[2];
		$magicMax = $md[3];

		$resultId = rand($magicMin, $magicMax);

		$sql = "UPDATE frdHavingMagics SET magicId=:magicId, level=0, exp=0 
				WHERE magicTableId=:privateIdx AND userId=:userId";
		$db -> prepare($sql);
		$db -> bindvalue(':magicId', $resultId, PDO::PARAM_INT); // HARDj
		$db -> bindvalue(':privateIdx', $privateIdx, PDO::PARAM_INT);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

		$AchieveManager = new AchieveManager();

		$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
			where userId=:userId and achieveId > 4200 and achieveId < 4300";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
			$achieveId = $row['achieveId'];
			if ($row['cnt'] != -1 ) {
				$AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
			}
		}
		$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
			where userId=:userId and achieveId > 3200 and achieveId < 3300";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$achieveId = $row['achieveId'];
			if ($row['cnt'] != -1 ) {
				$AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
			}
		}

		$DailyQuestManager = new DailyQuestManager();
		$DailyQuestManager->updateDayMission($db, $userId, 9, 1);


		$data["resultId"] = $resultId;

		$data['use']["gold"] = $gold;
		$data['use']["magics"]['delTableIds'][] = $matPrivateIdx;

		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResComposeMagicGold';
		$result['Data'] = $data;
				
		return $result;	

	}

	public function MagicExtraction($param) {
		$resultFail['ResultCode'] = 300;

		$userId		= $param["userId"];
		$magicTableId 	= $param["objectIdx"];

		$db = new DB();

		$sql = "SELECT * FROM frdHavingMagics WHERE magicTableId=:magicTableId and userId=:userId ";
		$db -> prepare($sql);
		$db -> bindvalue(':magicTableId', $magicTableId, PDO::PARAM_INT);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql.' obj :'.$magicTableId);
			return $resultFail;
		}
		$magicId = $row['magicId'];
		$level = $row['level'];

		/*
		   $amountValue = $redis->lindex('magicPowder', $grade);
		   $amountPercent = $redis->get('magicPowderValue');
		*/

		//HARD
		$amountPercent = 80;
		$grade = $this->GetGrade($row["magicId"]);
		$apc_key = "MagicPowderValue_".$grade;
		$magic_data = apc_fetch($apc_key);

		$amountValue = $magic_data[1];

		$sql = "SELECT val FROM frdEffectForEtc WHERE userId = :userId AND type=:val";
		$db -> prepare($sql);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindvalue(':val', ABILL_Bonus_Extraction, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$val = 0;
		} else {
			$val = $row['val']; 
		}

		$this -> logger -> logerror(__FUNCTION__.' : val :' . $val );

        $percent = $val / 100000;
        $amountValue += round($amountValue * $percent);

		$min = $amountValue - ceil($amountValue*$amountPercent/100);
		$max = $amountValue + (int)($amountValue*$amountPercent/100);

		$this -> logger -> logerror(__FUNCTION__.' : min :' . $min );
		$this -> logger -> logerror(__FUNCTION__.' : max :' . $max );

		$resultAmount = rand($min, $max) + $row["powder"];

		$AssetManager = new AssetManager();
		$AssetResult = $AssetManager->addPowder($db, $userId, $resultAmount);
		if ( $AssetResult['ResultCode'] != 100) {
			$this -> logger -> logerror('setExtrantion : FAIL use userId :' . $userId );
			return $resultFail;
		}
		$powder = null;	
		$powder["curPowder"] = $AssetResult['powder'];
		$powder["addPowder"] = $resultAmount;

		$sql = "DELETE FROM frdHavingMagics where magicTableId=:magicTableId and userId=:userId";
		$db -> prepare($sql);
		$db -> bindvalue(':magicTableId', $magicTableId, PDO::PARAM_INT);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError('setExtrantion : UPDATE FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendMagic($userId, $magicId, 2, $level);

		$data['use']['magics']['delTableIds'][] = $magicTableId;
		$data['rewards']['powder'] = $powder;

	
		// 일일 퀘스트 	
		$DailyQuestManager = new DailyQuestManager();
		$DailyQuestManager->updateDayMission($db, $userId, 3, 1);


		$result['Protocol'] = 'ResMagicExtraction';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}

	public function SellMagic($param) {
		$resultFail['ResultCode'] = 300;

		$userId	= $param["userId"];
		$isEx 	= $param["isExtraction"];

		$db = new DB();

		// 소스의 로직에 관하여, 수정이 필요 하다.
		// 해당 소스를 선택하여 판매 하는 형태?
		$sql = "SELECT gold, powder FROM frdUserData WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this -> logger -> logerror('sellMagic : select fail userId :' . $userId);
			return $resultFail;
		}  
		$myPowder = $row['powder'];
		$mygold = $row['gold'];

		$sellCount =0;
		for ( $idx = 0; $idx<9; $idx++ ) {
			$val = $param["matPId".$idx];
			if ( !is_null($val) && $val >= 0 ) {
				$idxArr[$sellCount++] = $val;
			}
		}
		$sql = "select magicTableId, magicId, level from frdHavingMagics where ";

		for ( $idx=0; $idx<$sellCount; $idx++ ) {
			if ( $idx < $sellCount-1 )
				$sql .= sprintf("(magicTableId='%s' and userId='%s') or ", $idxArr[$idx], $userId);
			else
				$sql .= sprintf("(magicTableId='%s' and userId='%s')", $idxArr[$idx], $userId);
		}

		$db -> prepare($sql);
		$db -> execute();
		$resultList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$resultList[] = $row;	
		}

		if (!$isEx) {
			$addGold = 0;
			foreach ($resultList as $rs ) {
				$addGold += $this->GetSellPrice($this->GetGrade((int)$rs["magicId"]), (int)$rs["level"]);

					$logDBSendManager = new LogDBSendManager();
					foreach ($resultList as $rs ) {
						$logDBSendManager->sendMagic($userId, $rs['magicId'], 2, $rs['level']);
					}
				
			}

			for ( $idx=0; $idx<$sellCount; $idx++ ) {
				$sql = "delete from frdHavingMagics where magicTableId=:privateId and userId=:userId";
				$db -> prepare($sql);
				$db -> bindvalue(':privateId', 	$idxArr[$idx], PDO::PARAM_INT);
				$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError('sellMagic :  FAIL userId :'.$userId.' sql : '.$sql);
					return $resultFail;
				}
			}


			$AssetManager = new AssetManager();
			$AssetResult = $AssetManager->addGold($db,$userId, $addGold);

			$gold['addGold'] = $addGold;
			$gold['curGold'] = $AssetResult['gold'];

		} else {

			$amountPercent = 80;
			$addPowder = 0;
			foreach ($resultList as $rs ) {
				$grade = $this->GetGrade($rs["magicId"]);

				// apc get 
//				$amountValue = $redis->lindex('magicPowder', $grade);
				$apc_key = "MagicPowderValue_".$grade;
				$magic_data = apc_fetch($apc_key);
				// test 
				$amountValue = $magic_data[1];

				$GLOBALS['$ABILL_Bonus_Extraction'] = 1017; // 정리가 필요
				$val = $GLOBALS['$ABILL_Bonus_Extraction'];

				$sql = "SELECT val FROM frdEffectForEtc WHERE userId = :userId AND type=:val";
				$db -> prepare($sql);
				$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindvalue(':val', $val, PDO::PARAM_INT);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				if (!$row) {
					$row["val"] = 1;
				} else {
					$row["val"] = 0;
				}

				$percent = $row["val"] / 100000;
				$amountValue += round($amountValue * $percent);

				$min = $amountValue - ceil($amountValue*$amountPercent/100);
				$max = $amountValue + (int)($amountValue*$amountPercent/100);

				$this -> logger -> logerror(__FUNCTION__.' : min :' . $min );
				$this -> logger -> logerror(__FUNCTION__.' : max :' . $max );

				$resultAmount = rand($min, $max);
				$addPowder += $resultAmount;

				$logDBSendManager = new LogDBSendManager();
				foreach ($resultList as $rs ) {
					$logDBSendManager->sendMagic($userId, $rs['magicId'], 2, $rs['level']);
				}
				
			}

			for ( $idx=0; $idx<$sellCount; $idx++ ) {
				$sql = "DELETE FROM frdHavingMagics 
					WHERE magicTableId=:tableId AND userId=:userId";
				$db -> prepare($sql);
				$db -> bindvalue(':tableId', $idxArr[$idx], PDO::PARAM_INT);
				$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError('sellMagic : DELETE FAIL userId :'.$userId.' sql : '.$sql);
					return $resultFail;
				}

			}



			$AssetManager = new AssetManager();
			$AssetResult = $AssetManager->addPowder($db,$userId, $addPowder);

			$powder['curPowder'] = $AssetResult['powder'];
			$powder['addPowder'] = $addPowder;

			// 일일 퀘스트 	
			$DailyQuestManager = new DailyQuestManager();
			$DailyQuestManager->updateDayMission($db, $userId, 3, 1);
		}

		$data['use']['magics']['delTableIds'] = $idxArr;

		if (isset($gold)) {
			$data['rewards']["gold"] = $gold;
		}

		if (isset($powder)) {
			$data['rewards']["powder"] = $powder;
		}

		$result['Protocol'] = 'ReqSellMagic';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;

	}

	public function UpgradeMagicGold($param) {
		$resultFail['ResultCode'] = 300;

		$userId	= $param["userId"];
		$privateIdx = $param["privateIdx"];
		$matPrivateIdx = $param["matPrivateIdx"];


		$db = new DB();

		$sql = "SELECT gold FROM frdUserData WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this -> logger -> logerror('upgradeArti : select fail userId :' . $userId);
			return $resultFail;
		}  
		$myGold = $row['gold'];

		$sql = sprintf("select * from frdHavingMagics where ");
		$sql .= sprintf("(magicTableId='%s' and userId='%s') or ", $privateIdx, $userId);
		$sql .= sprintf("(magicTableId='%s' and userId='%s')", $matPrivateIdx, $userId);
		$db -> prepare($sql);
		$db -> execute();
		$resultList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$resultList[] = $row;	
		}
		if (count($resultList) < 2 ) {
			$this->logger->logError(__FUNCTION__.' : not match   userId :'.$userId.'sql'.$sql);
			return $resultFail;
		}

		foreach ($resultList as $rs) {
			$curPIdx = (int)$rs["magicTableId"];
			if ( $curPIdx == $privateIdx ) {  // desktop
				$deskGrade = $this->GetGrade($rs["magicId"]);
				$magicId = $rs['magicId'];
				$deskExp = $rs["exp"];
				$deskLevel = $rs["level"];
			}
			else {                            // material
				$matGrade = $this->GetGrade($rs["magicId"]);
				$matId = $rs["magicId"];
				$matLevel = $rs["level"];
			}
		}

		$matExp = $this->GetHavingExp($matGrade);
		$percent = $this->GetLevelUpPercent($db, $userId, $deskGrade, $matExp);
		$bonusPercent = $this->GetLevelUpPercent($db, $userId, $deskGrade, $deskExp);
		$price = $this->GetLevelUpPrice($deskGrade, $deskLevel);

		if ($price > $myGold ) {
			$this->logger->logError('upgradeArti : not enough gold userId :'.$userId);
			return $resultFail;
		}
		
		$AssetManager = new AssetManager();
		$AssetResult = $AssetManager->useGold($db, $userId, $price);
		if($AssetResult['ResultCode'] != 100 ) {
			$this->logger->logError('upgradeArti : err gold userId :'.$userId);
			return $resultFail;
		}
		
		$gold['curGold'] = $AssetResult['gold'];
		$gold['addGold'] = 0 - $price;
		$data['use']['gold'] = $gold;
		$data['use']['delTableIds'][] = $matPrivateIdx;

		$sql = "DELETE FROM frdHavingMagics WHERE magicTableId=:matIdx and userId=:userId";
		$db -> prepare($sql);
		$db -> bindvalue(':matIdx', $matPrivateIdx, PDO::PARAM_INT);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendMagic($userId, $matId, 2, $matLevel);


		$itemNeedExp = $this->GetNeedExp($deskGrade);
		$itemExp = $deskExp + $this->GetHavingExp($matGrade);
		if ( rand(0, $itemNeedExp) <= $itemExp ) {
			$data["isSuccess"] = 1;
			$resultLevel = $deskLevel+1;
			$sql = "UPDATE frdHavingMagics set level=:resultLevel, exp=0 
				WHERE magicTableId=:privateIdx AND userId=:userId";
			$db -> prepare($sql);
			$db -> bindvalue(':resultLevel', $resultLevel, PDO::PARAM_INT);
			$db -> bindvalue(':privateIdx', $privateIdx, PDO::PARAM_INT);
			$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
				return $resultFail;
			}
			$data["level"] = $resultLevel;
			$data["exp"] = 0;
		}
		else {
			$data["isSuccess"] = 0;
			$resultExp = $deskExp + (int)($matExp/5);
			$sql = "UPDATE frdHavingMagics SET exp =:resultExp 
				where magicTableId =:privateIdx and userId=:userId";
			$db -> prepare($sql);
			$db -> bindvalue(':resultExp', $resultExp, PDO::PARAM_INT);
			$db -> bindvalue(':privateIdx', $privateIdx, PDO::PARAM_INT);
			$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
				return $resultFail;
			}

			$data["exp"] = $resultExp;
			$data["level"] = $deskLevel;
			
		}

		$DailyQuestManager = new DailyQuestManager();
		$DailyQuestManager->updateDayMission($db, $userId, 8, 1);

		$data["magicId"] = $magicId;
		$result['Protocol'] = 'ResUpgradeMagicGold';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;

	}

	function SetEquipMagic ($param) {
		$resultFail['ResultCode'] = 300;

		$userId	= $param["userId"];
		$equipIdx = $param["equipIdx"]; // slot no 
		$magicTableId = $param["magicTableId"];
	
		$sql = "SELECT itemTableId,equipIdx 
		FROM frdHavingMagics 
		WHERE userId = :userId AND (equipIdx = :equipIdx OR magicTableId = :magicTableId)";
		$db -> prepare($sql);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindvalue(':equipIdx', $equipIdx, PDO::PARAM_INT);
		$db -> bindvalue(':magicTableId', $magicTableId, PDO::PARAM_INT);
		$db -> execute();
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		$resultList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$resultList[] = $row;	
		}  
	
		$isExisit = 0;	
		$isEquip = 0;
		foreach($resultList as $rs) {
			if ($rs['equipIdx'] == $equipIdx) {
				$tempId = $rs['itemItableId'];
				$isEquip = 1;
			}

			if ($rs['magicTableId'] == $magicTableId) {
				$isExisit = 1; 	
			}
		}

		// 존재 하지 않는 아이템이다 실패 
		if (!$isExisit){
			$this->logger->logError(__FUNCTION__.' : NOT EXISIT  userId :'.$userId);
			return $resultFail;
		}


		// 동일한 종류의 마법을 착용하고 있는지 검증 하여야 한다. 
		// 동일한 종류의 마법을 착용하고 있는지 검증 하여야 한다. 
		// 동일한 종류의 마법을 착용하고 있는지 검증 하여야 한다. 
		// 동일한 종류의 마법을 착용하고 있는지 검증 하여야 한다. 
		// 동일한 종류의 마법을 착용하고 있는지 검증 하여야 한다. 
		// 동일한 종류의 마법을 착용하고 있는지 검증 하여야 한다. 



		// 착용중이다 해제한다 
		if ($isEquip) {
			$sql = "UPDATE frdHavingMagics 
				SET equipIdx = 0 
				WHERE magicTableId=:magicTableId AND userId=:userId";
			$db -> prepare($sql);
			$db -> bindvalue(':magicTableId', $magicTableId, PDO::PARAM_INT);
			$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
				return $resultFail;
			}

			$sql = "UPDATE frdHavingMagics 
				SET equipIdx = :equipIdx 
				WHERE magicTableId=:magicTableId AND userId=:userId";
			$db -> prepare($sql);
			$db -> bindvalue(':magicTableId', $magicTableId, PDO::PARAM_INT);
			$db -> bindvalue(':equipIdx', $equipIdx, PDO::PARAM_INT);
			$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
				return $resultFail;
			}
		} else {
			$sql = "UPDATE frdHavingMagics 
				SET equipIdx = 0 
				WHERE magicTableId=:magicTableId AND userId=:userId";
			$db -> prepare($sql);
			$db -> bindvalue(':magicTableId', $magicTableId, PDO::PARAM_INT);
			$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
				return $resultFail;
			}
		}

		$data['magicTableId'] = $magicTableId;
		$data['equipIdx'] = $equipIdx;

		$result['Protocol'] = 'ResSetEquipMagic';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}

	function SetUnEquipMagic_ ($param) {
		$resultFail['ResultCode'] = 300;

		$userId	= $param["userId"];
		$equipIdx = $param["equipIdx"]; // slot no 
		$magicTableId = $param["magicTableId"];
	
		$sql = "SELECT itemTableId,equipIdx 
		FROM frdHavingMagics 
		WHERE magicTableId = :magicTableId AND userId = :userId";
		$db -> prepare($sql);
		$db -> bindvalue(':magicTableId', $magicTableId, PDO::PARAM_INT);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			$this->logger->logError(__FUNCTION__.' : NOT EXISIT  magicTableId:'.$magicTableId);
			return $resultFail;
		}

		$sql = "UPDATE frdHavingMagics 
			SET equipIdx = 0 
			WHERE magicTableId=:magicTableId AND userId=:userId";
		$db -> prepare($sql);
		$db -> bindvalue(':magicTableId', $magicTableId, PDO::PARAM_INT);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			return $resultFail;
		}

		$data['magicTableId'] = $magicTableId;
		$data['unEquipIdx'] = $equipIdx;

		$result['Protocol'] = 'ResSetUnEquipMagic';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}


	function EquipMagic ($param) {
		$resultFail['ResultCode'] = 300;

		$userId = $param['userId'];
		$equipIdx = $param['equipIdx'];
		$magicTableId = $param['magicTableId'];

		if ($equipIdx < 0 || $equipIdx > 2 ) {
			$this->logger->logError(__FUNCTION__.' : wrong  userId : '.$userId.", equipIdx : ".$equipIdx);
			$resultFail['ResultCode'] = 300;
			return $resultFail;
		}

		$db = new DB();
		$sql = "SELECT * FROM frdEquipMagics WHERE userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$resultList = null;
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' :  userId : ' . $userId . ", sql : " . $sql);
			return $resultFail;
		}

		$data = null;
		$data['equipIdx'] = $equipIdx;
		$data['magicTableId'] = $row['eq'.$equipIdx];

		$sql = "UPDATE frdEquipMagics SET eq".$equipIdx." = :magicTableId
			WHERE userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':magicTableId', $magicTableId, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' :  userId : ' . $userId . ", sql : " . $sql);
			return $resultFail;
		}

		$result['Protocol'] = 'ResEquipMagic';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}

	function GetGrade($magicId) {
		return (int)($magicId/100)+1;
	}


	function GetMaxLevel() {
		return 5;
	}

	function GetMaxGrade() {
		return 5;
	}

	function GetHavingExp($grade) {
		switch($grade) {
			case 0:
				return 100;
			case 1:
				return 200;
			case 2:
				return 400;
			case 3:
				return 800;
			case 4:
				return 1600;
			case 5:
				return 3200;
			default:
				return 3200;
		}
	}

	function GetNeedExp($grade) {
		switch($grade) {
			case 0:
				return 100;
			case 1:
				return 200;
			case 2:
				return 400;
			case 3:
				return 800;
			case 4:
				return 1600;
			case 5:
				return 3200;
			default:
				return 3200;
		}
	}

	function GetGradeUpPrice($grade) {
		switch($grade) {
			case 1:
				return 1000;
			case 2:
				return 2000;
			case 3:
				return 5000;
			case 4:
				return 27000;
			default:
				return 120000;
		}
	}

	function GetLevelUpPrice($grade, $level) {
		$price=0;
		$intervalPrice=0;
		switch($grade) {
			case 1:
				$price = 300;
				$intervalPrice = 100;
				break;
			case 2:
				$price = 500;
				$intervalPrice = 200;
				break;
			case 3:
				$price = 1000;
				$intervalPrice = 500;
				break;
			case 4:
				$price = 2000;
				$intervalPrice = 1000;
				break;
			case 5:
				$price = 4000;
				$intervalPrice = 2000;
				break;
			default:
				break;
		}

		return $price + $intervalPrice*$level;
	}

	function GetLevelUpPercent($db, $userId, $targetGrade, $matExp) {
		$resultPercent = (int)(($matExp*100) / $this->GetNeedExp($targetGrade));

		$sql = "SELECT val FROM frdEffectForEtc 
			WHERE userId=:userId and type=:type";
		$db -> prepare($sql);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindvalue(':type', ABILL_Bonus_MagicLvUpPercent, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$resultPercent += round($resultPercent*$row["val"]*0.00001);
		}

		return $resultPercent;
	}

	function GetSellPrice($deskGrade, $deskLevel) {
		$price=0;
		switch($deskGrade) {
			case 1:
				$price = 75;
				for ( $i=1; $i<=$deskLevel; $i++ )
					$price += 100 +50*$i;
				break;
			case 2:
				$price = 125;
				for ( $i=1; $i<=$deskLevel; $i++ )
					$price += 150 +100*$i;
				break;
			case 3:
				$price = 250;
				for ( $i=1; $i<=$deskLevel; $i++ )
					$price += 250 +250*$i;
				break;
			case 4:
				$price = 500;
				for ( $i=1; $i<=$deskLevel; $i++ )
					$price += 500 +500*$i;
				break;
			case 5:
				$price = 1000;
				for ( $i=1; $i<=$deskLevel; $i++ )
					$price += 1000 +1000*$i;
				break;
			default:
				break;
		}
		return $price;
	}

	function GetManaLevelUpPrice($curLevel) {
		$sum=0;
		$level = $curLevel;
		$sum += 200 + $level*200;

		while($level>0) {
			$level -= 10;
			if ( $level >= 0 ) {
				$sum += 100*($level+1);
			}
		}
		return $sum;
	}









}
?>
