<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
include_once './classes/AssetManager.php';
include_once './classes/AddManager.php';

class MailManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

    public function MailList($param) {
        $resultFail['Protocol'] = 'ResMailList';
        $resultFail['ResultCode'] = 300;

		$userId = $param["userId"];

        $db = new DB();
        $sql = <<<SQL
		SELECT postTableId, sendUserId, type, count, expire_time,
        TIMESTAMPDIFF(second,now(),expire_time) AS expire_second
		FROM frdUserPost 
		WHERE recvUserId = :userId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();

        $mail_info_array = array();
        $mail_del_array = array();
        $mail_del_id = null;

        while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            $mail_info = array();
            $mail_info['pId'] = $row['postTableId'];
            $mail_info['sendUserId'] = $row['sendUserId'];
            $mail_info['type'] = $row['type'];
            $mail_info['count'] = $row['count'];
            $mail_info['expire_time'] = $row['expire_second'];

            $expire_time = null;
            $expire_time = $row['expire_time'];

			// 삭제 백업용 메일 정보 
            if ($mail_info['expire_time'] <= 0) {
                $mail_info['expire_time'] = $expire_time;
                array_push($mail_del_array, $mail_info);
                if ($mail_del_id == null) {
                    $mail_del_id = $mail_info['pId'];
                } else {
                    $mail_del_id = trim($mail_del_id . "," . $mail_info['pId']);
                }

            } else {
                array_push($mail_info_array, $mail_info);
            }
        }

        $result['Protocol'] = "ResMailList";
        $result['ResultCode'] = 100;
		$data = null;
		$data['post'] = $mail_info_array;
        $result['Data'] = $data;

        return $result;
    }

	public function MailReceive($param) {

		$resultFail['Protocol'] = 'ResMailReceive';
		$resultFail['ResultCode'] = 300;

		$pId = $param["pId"];
		$userId = $param["userId"];

		$data = null;
		$db = new DB();

######################################################
		/*
		require_once './classes/TransactionManager.php';
		$TransactionManager = new TransactionManager();
		$check_transaction = $TransactionManager -> init_transaction($db, $userId);
		if ($check_transaction['ResultCode'] != 100) {
			$this -> logger -> logError(__FUNCTION__ . ',: init_transaction fail userId : ' . $userId);
			return $resultFail;
		}
		*/
######################################################

		$resultList = null;
		// get item all
		if ( ($pId == -1) || ($pId == '-1') ) {
			$sql = "
				SELECT *
				FROM frdUserPost
				WHERE recvUserId = :userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> execute();
			while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
				$resultList[] = $row;
			}
		} else {
			$sql = " SELECT * FROM frdUserPost
				WHERE postTableId = :pId AND recvUserId = :userId";
			$db->prepare($sql);
			$db->bindValue(':pId', $pId, PDO::PARAM_INT);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db->execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if ($row) {
				$resultList[] = $row;
			}
		}

        $dataTotal = null;
        $gold = null;
        $jewel = null;
        $heart = null;
        $ticket = null;
		$resultExp = 0;

		if ($resultList == null) {
			$this->logger->logError(__FUNCTION__.__LINE__.': userId : '.$userId.' sql '.$sql. ' pId'.$pId);
			$result['ResultCode'] = 100;
			$result['Data'] = null;
			return $result;
		}

		$AssetManager = new AssetManager();
		foreach ($resultList as $ma) {
			$rewardType = $ma['type'];
			$rewardCount = $ma['count'];
			
			$rewardCount = abs($rewardCount);
			$GLOBALS['protocol'] = $GLOBALS['protocol'].','.$ma['sendUserId'];
			switch ($rewardType) {
				case 5000: // 골드 지급
					$gold_result = $AssetManager->addGold($db, $userId, $rewardCount);
					if ($gold_result['ResultCode'] != 100 ) {
						return $gold_result;
					}
                    $gold["curGold"] = $gold_result['gold'];
                    if (isset($gold['addGold'])) {
                        $gold["addGold"] = $gold['addGold'] + $rewardCount;
                    } else {
                        $gold["addGold"] = $rewardCount;
                    }
					break;
				case 5001: //경험치 지급
					$resultExp += (int)$rewardCount;
					break;
				case 5002: //티켓 지급
					$AssetResult = $AssetManager->addTicket($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$ticket["curTicket"] = $AssetResult['curTicket'];
                    if (isset($ticket["addTicket"])) {
						$ticket["addTicket"] +=  $rewardCount;
                    } else {
						$ticket["addTicket"] =  $rewardCount;
                    }

					break;
				case 5003: //행동력 지급
					$AssetResult = $AssetManager->addHeart($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$heart["curHeart"] = $AssetResult['curHeart'];
					$heart["maxHeart"] = $AssetResult['maxHeart'];
					$heart["passTime"] = $AssetResult['passTime'];
					if (isset($heart['addHeart'])) {
						$heart["addHeart"] = $heart['addHeart'] + $rewardCount;
					} else {
						$heart["addHeart"] = $rewardCount;
					}
					break;
				case 5004: // 보석 지급
					$AssetResult = $AssetManager->addJewel($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$jewel["curJewel"] = $AssetResult['jewel'];
					if (isset($jewel['addJewel'])) {
						$jewel["addJewel"] = $jewel['addJewel'] + $rewardCount;
					} else {
						$jewel["addJewel"] = $rewardCount;
					}
					break;
				case 5005: // 메달 지급
					$AssetResult = $AssetManager->addMedal($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$skillPoint["curMedal"] = $AssetResult['skillPoint'];
					if (isset($skillPoint['addMedal'])) {
						$skillPoint["addMedal"] = $skillPoint['addMedal'] + $rewardCount;
					} else {
						$skillPoint["addMedal"] = $rewardCount;
					}
					break;
				case 5006: // 파우더 마법 가루 
					$AssetResult = $AssetManager->addPowder($db, $userId, $rewardCount);
					if ($AssetResult['ResultCode'] != 100 ) {
						return $AssetResult;
					}
					$powder["curPowder"] = $AssetResult['powder'];
					if (isset($skillPoint['addPowder'])) {
						$powder["addPowder"] = $powder['addMedal'] + $rewardCount;
					} else {
						$powder["addPowder"] = $rewardCount;
					}
					break;
				default:
					$AddManager = new AddManager();
					if (  $rewardType < 1000 ) { //캐릭터 소울스톤 지급
						if ( $rewardType > 500 ) {
							$key_name = $rewardType - 500;
							$apc_key = 'CharacIndex_'.$key_name;
							$cd = apc_fetch($apc_key);
							$charMin = $cd[0];
							$charMax = $cd[1];
							$rewardType = rand($charMin, $charMax);
							$this->logger->logError(__FUNCTION__.': userId : '.$userId.' rewardType: '.$rewardType);
							$this->logger->logError(__FUNCTION__.': userId : '.$userId.' min: '.$charMin);
							$this->logger->logError(__FUNCTION__.': userId : '.$userId.' max: '.$charMax);
						}				

						$CharResult = $AddManager->setAddHero($db, $userId, $rewardType, $rewardCount);
						if ($CharResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.': userId : '.$userId.' rewardType: '.$rewardType);
							break;
						}
						$charTemp = null;
						$charTemp['charTableId'] = $CharResult['charTableId'];
						$charTemp['charId'] = $CharResult['charId'];
						$charTemp['exp'] = $CharResult['curExp'];
//						$charTemp['addExp'] = $CharResult['addExp'];

						$charData[] = $charTemp;
					}
					else if ( $rewardType < 2000 ) { // 보구 지급
                        if ( $rewardType > 1500 ) {
                            $array_pos = $rewardType - 1500;
                            $apc_key = 'WeaponInform_WeaponMinIdx';
                            $wd1 = apc_fetch($apc_key);
                            $minTemp = explode('/', $wd1[1]);
                            $weaponMin = $minTemp[$array_pos];

                            $apc_key = 'WeaponInform_WeaponMaxIdx';
                            $wd2 = apc_fetch($apc_key);
                            $maxTemp = explode('/', $wd2[1]);
                            $weaponMax = $maxTemp[$array_pos];

                            $rewardType = rand($weaponMin, $weaponMax);
							$rewardType = $rewardType + 1000;
							$this->logger->logError(__FUNCTION__.__LINE__.': FAIL weapon min : '.$weaponMin);
							$this->logger->logError(__FUNCTION__.__LINE__.': FAIL weapon max : '.$weaponMax);

                        }
                        $rewardType = $rewardType - 1000;
						$WeaponResult = $AddManager->addWeapon($db, $userId, $rewardType, $rewardCount);
						if ($WeaponResult['ResultCode'] != 100) {
							$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
							break;
						}
						$weaponTemp = null;
						$weaponTemp['weaponTableId'] = $WeaponResult['weaponTableId'];
						$weaponTemp['weaponId'] = $WeaponResult['weaponId'];
                        $weaponTemp['statIds'] = $WeaponResult['statIds'];
                        $weaponTemp['statVals'] = $WeaponResult['statVals'];
						$weaponTemp['exp'] = $WeaponResult['exp'];

						$weaponData[] = $weaponTemp;
					} 
					else if ( $rewardType >= 10000 && $rewardType < 10510 ) {  //마법 지급
						if ( $rewardType > 10500 ) {
							$key_name = $rewardType - 10500;
							$apc_key = 'MagicPowderValue_'.$key_name;
							$md = apc_fetch($apc_key);
							$magicMin = $md[2] + 10000;
							$magicMax = $md[3] + 10000;

							$rewardType = rand($magicMin, $magicMax);
						}
						$rewardType = $rewardType - 10000;
						$MagicResult = $AddManager->addMagic($db, $userId, $rewardType);
						if ($MagicResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
							break;
						}
						$magicTemp = null;
						$magicTemp['magicTableId'] = $MagicResult['magicTableId'];
						$magicTemp['magicId'] = $MagicResult['magicId'];
						$magicTemp['exp'] = 0;

						$magicData[] = $magicTemp;
					}
					else if ( ( $rewardType >= 99999 && $rewardType < 200000 )  || ( $rewardType > 1000 &&  $rewardType < 1500 ))  {
						$ItemResult = $AddManager->addItem($db, $userId, $rewardType, $rewardCount );
						if ($ItemResult['ResultCode'] != 100 ) {
							$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
							break;
						}
						$itemTemp = null;
						$itemTemp['itemTableId'] = $ItemResult['itemTableId'];
						$itemTemp['itemId'] = $ItemResult['itemId'];
						$itemTemp['cur'] = $ItemResult['cnt'];
						$itemTemp['add'] = $rewardCount;

						$itemData[] = $itemTemp;
					} else {
						$this->logger->logError(__FUNCTION__.__LINE__.': ERROR userId : '.$userId);
						$this->logger->logError(__FUNCTION__.__LINE__.': ERROR type : '.$rewardType);
					}
			}
		}



		if ( ($pId !== -1) && ($pId !== "-1") ) {
			$sql = "delete from frdUserPost where recvUserId = :userId and postTableId = :postTableId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':postTableId', $pId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': userId : '.$userId.' sql: '.$sql);
				return $resultFail;
			}

		}
		if ( ($pId == -1) || ($pId == '-1') ) {
			$sql = "delete from frdUserPost where recvUserId = :userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.': userId : '.$userId.' sql: '.$sql);
				return $resultFail;
			}
		}

		if ($resultExp > 0 ) {
		$AssetResult = $AssetManager->addUserExp($db, $userId, $resultExp);
			if ($AssetResult['ResultCode'] != 100) {
				$this->logger->logError(__FUNCTION__.': userId : '.$userId.' error exp : '.$resultExp);
			}
			$data['exp'] = $AssetResult["exp"];
		}


######################################################

//		$check_transaction = $TransactionManager -> end_transaction($db, $UID);

######################################################
        if (isset($gold)) {
            $data['gold'] = $gold;
        }
        if (isset($jewel)) {
            $data['jewel'] = $jewel;
        }
        if (isset($skillPoint)) {
            $data['medal'] = $skillPoint;
        }
        if (isset($heart)) {
            $data['heart'] = $heart;
        }
        if (isset($ticket)) {
            $data['ticket'] = $ticket;
        }
        if (isset($powder)) {
            $data['powder'] = $powder;
        }



		if (isset($charData)) {
			$data['characs'] = $charData;
		}
		if (isset($magicData)) {
			$data['magics'] = $magicData;
		}
		if (isset($itemData)) {
			$data['items'] = $itemData;
		}
		if (isset($weaponData)) {
			$data['weapons'] = $weaponData;
		}
		$temp['rewards'] = $data;
		$result['Protocol'] = "ResMailReceive";
		$result['ResultCode'] = 100;
		$result['Data'] = $temp;

		return $result;
	}

    ######################################################################################################

    function expire_date_check($UID) {
        $resultFail['Protocol'] = 'expire_date_check';
        $resultFail['ResultCode'] = 300;

        $db = new DB();

        $sql = <<<SQL
        SELECT MailID, sender_UID, receiver_UID, sender_name, reg_date, expire_time, item_type, value,
        TIMESTAMPDIFF(second,now(),expire_time) as expire_second
        FROM Mail_Info
        WHERE receiver_UID = '{$UID}'
SQL;

        $db -> prepare($sql);
        $db -> execute();

        $mail_info_array = array();
        $mail_info_id = null;

        while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {

            $mail_info = array();
            $mail_info['MailID'] = $row['MailID'];
            $mail_info['sender_UID'] = $row['sender_UID'];
            $mail_info['receiver_UID'] = $row['receiver_UID'];
            $mail_info['sender_name'] = $row['sender_name'];
            $mail_info['reg_date'] = $row['reg_date'];
            $mail_info['expire_time'] = $row['expire_time'];
            $mail_info['item_type'] = $row['item_type'];
            $mail_info['value'] = $row['value'];

            $time_check = $row['expire_second'];

            if ($time_check <= 0) {
                array_push($mail_info_array, $mail_info);

                if ($mail_info_id == null) {
                    $mail_info_id = $mail_info['MailID'];
                } else {
                    $mail_info_id = trim($mail_info_id . "," . $mail_info['MailID']);
                }
            }
        }

        if (count($mail_info_array) > 0) {
            // delete mail
            $sql = <<<SQL
    DELETE FROM Mail_Info
    WHERE MailID IN ({$mail_info_id})
SQL;

            $db -> prepare($sql);
            $row = $db -> execute();
            if (!isset($row) || is_null($row) || $row == 0) {
                $this -> logger -> logError('expire_date_check : Qeury DELETE  FAIL UID : ' . $UID);
                return $resultFail;
            }

            for ($i = 0; $i < count($mail_info_array); $i++) {

                // add Mail_Back
                $sql = <<<SQL
            INSERT INTO Mail_Back (MailID, sender_UID, receiver_UID, sender_name, reg_date, expire_time, item_type, value, mail_back_time)
            VALUES ({$mail_info_array[$i]['MailID']}, {$mail_info_array[$i]['sender_UID']}, {$mail_info_array[$i]['receiver_UID']}, "{$mail_info_array[$i]['sender_name']}", '{$mail_info_array[$i]['reg_date']}', '{$mail_info_array[$i]['expire_time']}', {$mail_info_array[$i]['item_type']}, {$mail_info_array[$i]['value']}, now())
SQL;
                $db -> prepare($sql);
                $row = $db -> execute();
                if (!isset($row) || is_null($row) || $row == 0) {
                    $this -> logger -> logError('expire_date_check : Qeury INSERT  FAIL UID : ' . $UID . ", sql : " . $sql);
                    return $resultFail;
                }
            }
        }

        $result['ResultCode'] = 100;

        return $result;
    }

    function del_mail($db, $UID, $mail_info_array, $del_mail_id) {
        $resultFail['Protocol'] = 'del_mail';
        $resultFail['ResultCode'] = 300;

        require_once './lib/BackUpManager.php';

        $BackUpManager = new BackUpManager();

        $backUp_result = $BackUpManager -> mail_del($db, $UID, $mail_info_array, $del_mail_id, $GLOBALS['AccessNo']);
        if ($backUp_result['ResultCode'] != 100) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', MailManager::del_mail mail_del fail UID : ' . $UID);
            return $resultFail;
        }

        $result['ResultCode'] = 100;

        return $result;
        #############################################################################################
        // delete mail
        $sql = <<<SQL
            DELETE FROM Mail_Info
            WHERE MailID IN ({$del_mail_id}) AND receiver_UID = '{$UID}'
SQL;
        $db -> prepare($sql);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError('del_mail : Qeury DELETE  FAIL UID : ' . $UID . ", sql : " . $sql);
            return $resultFail;
        }

        $a = $mail_info_array;

        for ($i = 0; $i < count($a); $i++) {
            // add Mail_Back
            $sql = <<<SQL
            INSERT INTO Mail_Back (MailID, sender_UID, receiver_UID, sender_name, sender_type, reg_date, expire_time, item_type, value, mail_back_time)
            VALUES (:MailID, :sender_UID, :receiver_UID, :sender_name, :sender_type, :reg_date, :expire_time, :item_type, :value, now())
SQL;
            $db -> prepare($sql);
            $db -> bindValue(':MailID', $a[$i]['MailID'], PDO::PARAM_INT);
            $db -> bindValue(':sender_UID', $a[$i]['sender_UID'], PDO::PARAM_INT);
            $db -> bindValue(':receiver_UID', $a[$i]['receiver_UID'], PDO::PARAM_INT);
            $db -> bindValue(':sender_name', $a[$i]['sender_name'], PDO::PARAM_STR);
            $db -> bindValue(':sender_type', $a[$i]['sender_type'], PDO::PARAM_STR);
            $db -> bindValue(':reg_date', $a[$i]['reg_date'], PDO::PARAM_STR);
            $db -> bindValue(':expire_time', $a[$i]['expire_time'], PDO::PARAM_STR);
            $db -> bindValue(':item_type', $a[$i]['item_type'], PDO::PARAM_INT);
            $db -> bindValue(':value', $a[$i]['value'], PDO::PARAM_INT);
            $row = $db -> execute();
            if (!isset($row) || is_null($row) || $row == 0) {
                $this -> logger -> logError('del_mail : Qeury INSERT  FAIL UID : ' . $UID . ", sql : " . $sql);
                return $resultFail;
            }
        }

        $result['ResultCode'] = 100;

        return $result;
    }

    function mail_status_change($mail_info_array) {
        $resultFail['Protocol'] = 'mail_status_change';
        $resultFail['ResultCode'] = 300;

        $db = new DB();

        for ($i = 0; $i < count($mail_info_array); $i++) {

            $sql = <<<SQL
            UPDATE Mail_Info
            SET mail_status = 2
            WHERE MailID = {$mail_info_array[$i]['MailID']}
SQL;

            $db -> prepare($sql);
            $row = $db -> execute();
            if (!isset($row) || is_null($row) || $row == 0) {
                $this -> logger -> logError('mail_status_change : Qeury UPDATE  FAIL UID : ' . $UID . ", sql : " . $sql);
                return $resultFail;
            }
        }

        $result['ResultCode'] = 100;

        return $result;
    }

}
?>
