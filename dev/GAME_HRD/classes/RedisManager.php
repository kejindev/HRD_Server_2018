<?php
include_once './common/RedisDB.php';
require_once './lib/Logger.php';

class RedisManager
{
	public function set_zAdd($key, $score, $member)
	{
		/*  데이터를 추가 하거나 이미 존재 하는 경우에는 스코어를 업데이트 
			key = ranking
			score = score
			member = UserNo  
		*/
		$r = new RedisServer();
		$re = $r->zAdd($key, $score , $member);
		unset($r);
		return $re;
	}
	public function set_zCard($key)
	{
		/*  총 유저수 반환  
			key = ranking
			member = UserNo  
		*/
		$r = new RedisServer();
		$get_value = $r->zCard($key);

		unset($r);
		return $get_value;
	}
    public function set_hDel($key, $field) {
		$r = new RedisServer();
		$get_value = $r->hDel($key,$field);

		unset($r);
		return $get_value;
    }

	public function set_zRevRange($key, $start, $stop, $with)
	{
		/*   스코어 높은 순 순위와 UserNo 반환   
			key = ranking
			member = UserNo  
			0 이 1등 
		*/
		$r = new RedisServer();
		$get_value = $r->zRevRange($key, $start, $stop, $with);
		unset($r);
		return $get_value;
	}

	public function set_zRevRangeByScore($key, $start, $stop)
	{
		/*   스코어 높은 순 순위와 UserNo 반환   
			key = ranking
			member = UserNo  
			0 이 1등 
		*/
		$r = new RedisServer();
		$get_value = $r->zRevRangeByScore($key, $start, $stop);

		unset($r);
		return $get_value;
	}

	public function set_zRank($member)
	{
		/*  특정 멤버 스코어 낮은 순위
			key = ranking
			member = UserNo  
		*/
		$key = "ranking";
		$r = new RedisServer();
		$get_value = $r->zRank($key, $member);

		unset($r);
		return $get_value;
	}

	public function set_zRevRank($key, $member)
	{
		/*  특정 멤버 스코어 높은 순위  
			key = ranking
			member = UserNo  
		*/
		$r = new RedisServer();
		$get_value = $r->zRevRank($key, $member);
		unset($r);
		return $get_value;
	}

	public function set_zScore($key,$member)
	{
		/*  특정 멤버  의 스코어 반환  
			key = ranking
			member = UserNo  
		*/
		$r = new RedisServer();
		$get_value = $r->zScore($key, $member);

		unset($r);
		return $get_value;
	}

	public function set_zRem($key, $member)
	{
		/*  특정 멤버 삭제   
			key = ranking
			member = UserNo  
		*/
		$r = new RedisServer();
		$get_value = $r->zRem($key, $member);

		unset($r);
		return $get_value;
	}

	public function set_zRange($start, $stop)
	{
		/*  특정 범위 랭킹 가져 오기   
			key = ranking
			start 시작
		*/
		$key = "ranking";
		$r = new RedisServer();
		$get_value = $r->zRange($key, $start, $stop);

		unset($r);
		return $get_value;
	}

	/* 아이템 획득 알림 용  값 추가 하기 */
	public function set_Set($string)
	{
		$r = new RedisServer();
		/* 몇번째 입력할 차례인지 가져 온다 */
		$key 	= "GainSec";	
		$Sec 	= $r->Get($key);
		switch($Sec) {
			case 1:
				$setkey	= "gain2";
				$value 	= 2;
				break;
			case 2:
				$setkey	= "gain3";
				$value 	= 3;
				break;
			case 3:
				$setkey	= "gain4";
				$value 	= 4;
				break;
			case 4:
				$setkey	= "gain1";
				$value 	= 1;
				break;
			default :
				$setkey	= "gain1";
				$value 	= 1;
				break;
		}
		/* 다음 차례 값을 입력 */
		$key 		= "GainSec";	
		$get_value 	= $r->Set($key, $value); 

		/* 가져온 알림 메세지를 셋팅  */ 
		$get_value 	= $r->Set($setkey, $string); 

		unset($r);
		return $get_value;
	}

	/* 아이템 획득 알림 용  값 가져 기 */
	public function set_Get()
	{
		$key = "gain";
		$r = new RedisServer();
		$get_value = $r->Get($key);

		unset($r);
		return $get_value;
	}

	/* 아이템 획득 알림 용  값 지우기 */
	/* 지울 필요는 없다. set 할때 그냥 엎어 친다. */ 
	public function set_Del($string)
	{
		$key = "gain";
		$r = new RedisServer();
		$result_exists = $r->Exists($key); 
		if ($result_exists) {
			$get_value = $r->Del($key);
		}
		unset($r);
		return $get_value;
	}

	/* 멀티 겟 배열 형식으로 요청 하면 다준다 */ 
	public function redis_mGet()
	{
		$key = array("gain1", "gain2", "gain3", "gain4");

		$r = new RedisServer();
		$get_value = $r->mGet($key); 

		unset($r);
		return $get_value;
	}

	public function hSet($key, $field, $value)
	{

		$r = new RedisServer();
		$get_value = $r->hSet($key, $field, $value); 
		unset($r);
		return $get_value;
	}

	public function hGet($key, $field)
	{

		$r = new RedisServer();
		$get_value = $r->hGet($key, $field); 
		unset($r);
		return $get_value;
	}

}
?>
