<?php
include_once './common/DB.php';
require_once './lib/Logger.php';

class SendMailManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

	function sendMail($db,$userId, $sendUserId, $type, $cnt) {
		$resultFail['ResultCode'] = 300;

		$sql = "INSERT INTO frdUserPost
			(recvUserId, sendUserId, type, count, expire_time,  reg_date)
			values (:userId, :sendUserId, :type, :cnt, DATE_ADD(now(), INTERVAL 6 DAY), now())";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':sendUserId', $sendUserId, PDO::PARAM_INT);
		$db -> bindValue(':type', $type, PDO::PARAM_INT);
		$db -> bindValue(':cnt', $cnt, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
			return $resultFail;
		}
		
		$result['ResultCode'] = 100;
		return  $result;
	}
}
?>
