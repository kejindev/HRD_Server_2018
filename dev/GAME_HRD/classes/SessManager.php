<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
date_default_timezone_set('Asia/Seoul');

class SessManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

    public function set_make_sessionkey($db, $userId) {
        // Make Session Key Logic
        // # Using Data
        //  - DeviceID
        //  - Last Login Time ( Timestamp )
        //  - userId

        $sql = <<<SQL
SELECT lastLoginDate FROM frdUserData where userId = ?
SQL;
        $db -> prepare($sql);
        $db -> bindValue(1, $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $result['ResultCode'] = 300;
            //FAIL CODE
            $this -> logger -> logError('check_sessionkey : not Exist userId : ' . $userId);
            return $result;
        }
        $LastAccess = $row['lastLoginDate'];

        $AccessTime = SessManager::date_to_timestamp($LastAccess);

        // Make Session Key
        $Sess = SessManager::data_to_asc($AccessTime, $userId);

        return $Sess;
    }

    public function check_sessionkey($userId, $Sess, $ApiName, $param) {

		$db = new DB();
		$sql = "SELECT lastLoginDate FROM frdUserData where userId = ? ";
		$db -> prepare($sql);
		$db -> bindValue(1, $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$result['ResultCode'] = 300;
			//FAIL CODE
			$this -> logger -> logError($GLOBALS['AccessNo'] . ', SessManager::check_sessionkey : not Exist userId : ' . $userId);
			return $result;
		}

		$LastAccess = $row['lastLoginDate'];

		/*
		$sql = " SELECT state FROM Check_Server_State ";
		$db -> prepare($sql);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$result['ResultCode'] = 300;
			$this -> logger -> logError($GLOBALS['AccessNo'] . ', SessManager::check_sessionkey : fail state userId : ' . $userId);
			return $result;
		}

		$server_state = $row['state'];
		*/

        // SessionKey Decode
        $resultTime = SessManager::asc_to_data($Sess, $userId);
        $convertTime = SessManager::timestamp_to_date($resultTime);

        // error for diffrent data
        if ($convertTime != $LastAccess) {
			/*
            if ($server_state == 2) {
                $result['ResultCode'] = 119;
            } else {
                $result['ResultCode'] = 367;
            }
			*/

            //FAIL CODE
            $ReturnName = $ApiName;
            $result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
			$result['ResultCode'] = 367;
            $this -> logger -> logError('check_sessionkey : check Sess FAIL  Sess        : ' . $Sess);
            $this -> logger -> logError('check_sessionkey : check Sess FAIL  userId    	 : ' . $userId);
            $this -> logger -> logError('check_sessionkey : check Sess FAIL  ConvertTime : ' . $convertTime);
            $this -> logger -> logError('check_sessionkey : check Sess FAIL  LastAccess  : ' . $LastAccess);
            return $result;
        }

		$result['ResultCode'] = 100;
        return $result;
    }

    // Session Key Check
    // Receive Data  Compare to DB Last Login Time

    // Convert Device ID, userId to Asc code
    public function data_to_asc($AccessTime, $DeviceID) {
        $result = '';
        for ($i = 0; $i < strlen($AccessTime); $i++) {
            $char = substr($AccessTime, $i, 1);
            @$keychar = substr($DeviceID, ($i % strlen($DeviceID)) - 1, 1);
            $char = chr(ord($char) + ord($keychar));
            $result .= $char;
        }
        return base64_encode($result);
    }

    // date convert timestamp
    public function date_to_timestamp($date) {
        $arg = explode(' ', $date);
        // 날짜 와 시간을 분리
        $ymd = explode('-', $arg[0]);
        // 날짜 부분
        $hms = explode(':', $arg[1]);
        // 시간 부분
        $time = mktime($hms[0], $hms[1], $hms[2], $ymd[1], $ymd[2], $ymd[0]);
        return $time;
    }

    // decode Base64 and asc to string
    public function asc_to_data($SessionKey, $DeviceID) {
        $result = '';
        $encryptdata = base64_decode($SessionKey);

        for ($i = 0; $i < strlen($encryptdata); $i++) {
            $char = substr($encryptdata, $i, 1);
            @$keychar = substr($DeviceID, ($i % strlen($DeviceID)) - 1, 1);
            $char = chr(ord($char) - ord($keychar));
            $result .= $char;
        }
        return $result;
    }

    // date convert for  timestamp
    public function timestamp_to_date($nowtime) {

        $nowdate = date("Y-m-d H:i:s", $nowtime);

        return $nowdate;
    }

}
?>
