<?php
include_once './common/DB.php';
require_once "./common/define.php";
require_once './lib/Logger.php';
require_once "./lib/RandManager.php";
require_once "./send_data/SendDataManager.php";
require_once './classes/AchieveManager.php';
require_once './classes/UserInfoManager.php';
require_once './classes/AddManager.php';
require_once './classes/LogDBSendManager.php';
require_once './classes/EventManager.php';

class ShopManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

    function BuyChest($param) {
		$resultFail['ResultCode']  = 300;
		$userId = $param["userId"];
		$goodsId = $param["goodsId"];
		$isTest = $param["isTest"] == 1 ? true : false;

		$db = new DB();
		$sql = "SELECT priceType, price, chestCount FROM frdShopData 
				WHERE shopItemId = :shopItemId";
		$db -> prepare($sql);
		$db -> bindValue(':shopItemId', $goodsId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			// 존재 하지 않는 상품 
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			return $resultFail;
		}

		$priceType = $row['priceType'];
		$price = $row['price'];
		$chestCount = $row['chestCount'];

		$isUseFreePass = false;

		// 상자 뽑기권 체크  

		$isUseFreePass = false;
		if ($priceType == "5004") {
			$sql = "select itemCount from frdHavingItems where userId = :userId AND itemId = 100003";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_STR);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if ($row) {
				$freePremiumChestPassCount = $row["itemCount"];
				$usePassCount = $price / 30;
				if ( ( $freePremiumChestPassCount - $usePassCount ) > -1 ) {
					$isUseFreePass = true;
				}
				if ($isUseFreePass) {
					$restPassCount = $freePremiumChestPassCount - $usePassCount;
					$data["freePremiumChestPassCount"] = $restPassCount;
					if ($restPassCount == 0) {
						$sql = "DELETE FROM frdHavingItems WHERE userId = :userId AND itemId = 100003";
						$db -> prepare($sql);
						$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					} else {
						$sql = "UPDATE frdHavingItems SET itemCount = :restPassCount 
							WHERE userId = :userId AND itemId = 100003";
						$db -> prepare($sql);
						$db -> bindValue(':restPassCount', $restPassCount, PDO::PARAM_INT);
						$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					}
					$row = $db -> execute();
					if (!isset($row) || is_null($row) || $row == 0) {
						$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
						return $resultFail;
					}

					$logDBSendManager = new LogDBSendManager();
					$logDBSendManager->sendItem($userId, 100003, $restPassCount+1 , $restPassCount, -1);

				}
			}
		}
		// 뽑기권 체크 종료 

		// 소모 자원 보유체크 , 차감 
		$sql = "SELECT gold, jewel, heart FROM frdUserData WHERE userId = :userId";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			return $resultFail;
		}

		$myGold = $row['gold'];
		$myJewel = $row['jewel'];
		$myHeart = $row['heart'];

		switch($priceType) {
			case 5000:
				if ($myGold < $price) {
					$this->logger->logError(__FUNCTION__.' : userId : '.$userId.' myGold:'.$myGold.' price:'.$price );
					return $resultFail;	
				}
				break;
			case 5004:
				// 뽑기권으로 위에서 이미 체크 하였다면 필요 없다. 
				if ($isUseFreePass) {
					break;
				}
				if ($myJewel < $price) {
					$this->logger->logError(__FUNCTION__.' : userId : '.$userId.' myJewel:'.$myJewel.' price:'.$price );
					return $resultFail;	
				}
				break;
			case 5003:
				if ($myHeart < $price) {
					$this->logger->logError(__FUNCTION__.' : userId : '.$userId.' myHeart:'.$myHeart.' price:'.$price );
					return $resultFail;	
				}
				break;
			default :
				$this->logger->logError(__FUNCTION__.' : FAIL userId : '.$userId.' type : '.$priceType );
				return $resultFail;	
		}
		// 소모 자원 체크 종료 

		if ( $priceType == 5004 ) {
			$typeList = array();
			$arr_goods = $this->checkEventChest($goodsId);
			if (false !== $arr_goods) {
				for ( $i=0; $i<count($arr_goods); $i++ ) {
					$chestCount--;
					$typeList[] = $arr_goods[$i];
				}
			}
			
			$this->setPremiumReward($db, $userId, $priceType, $chestCount, $goodsId, $typeList);
			$this->logger->logError(__FUNCTION__.' :  userId : '.$userId.' count : '.count($typeList));
			
			// 업적 체크 
			if ($goodsId >= 3004 && $goodsId < 3012) {
				$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
					where userId=:userId and achieveId > 4500 and achieveId < 4600";
				$db->prepare($sql);
				$db->bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				if ($row) {
					$achieveId = $row['achieveId'];
					if ($row['cnt'] != -1 ) {
						$AchieveManager = new AchieveManager();
						if ($chestCount == 9 ) {
							$chestCount = 10;
						}
						$AchieveManager->addAchieveData($db, $userId, $achieveId, $chestCount); 
					}
				}
			}
		} else {
			$typeList = $this->setNormalReward($db, $userId, $priceType, $chestCount,$goodsId);
			if ($goodsId != 3001) {
				$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
					where userId=:userId and achieveId > 4600 and achieveId < 4700";
				$db->prepare($sql);
				$db->bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				if ($row) {
					$achieveId = $row['achieveId'];
					if ($row['cnt'] != -1 ) {
						$AchieveManager = new AchieveManager();
						$AchieveManager->addAchieveData($db, $userId, $achieveId, $chestCount);
					}
				}
			}
		}
		// 상자의 배율을 달리한 로직을 사용하여야 한다 가중치 적용 로직 
		// 황금 상자도 같은 로직을 탈수 있게 라이브러리를 구성한다. 
		// max Inven 을 체크 할것인가..?

		$addResult = $this->addItem($db, $userId, $typeList, $chestCount);

		// 차감 로직 시작 
		$AssetManager = new AssetManager();
		switch($priceType) {
			case 5000:
				$AssetResult = $AssetManager->useGold($db, $userId, $price);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $resultFail;
				}
				$gold['curGold'] = $AssetResult['gold'];
				$gold['addGold'] = 0 - $price;
				break;
			case 5004:
				// 뽑기권으로 위에서 이미 체크 하였다면 필요 없다. 
				if ($isUseFreePass) {
					break;
				}
				$AssetResult = $AssetManager->useJewel($db, $userId, $price);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $resultFail;
				}
				$jewel['curJewel'] = $AssetResult['jewel'];
				$jewel['addJewel'] = 0 - $price;
				break;
			case 5003:
				$AssetResult = $AssetManager->useHeart($db, $userId, $price);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $resultFail;
				}
				$heart['curHeart'] = $AssetResult['curHeart'];
				$heart['addHeart'] = 0 - $price;
				$heart['maxHeart'] = $AssetResult['maxHeart'];
				$heart['passTime'] = $AssetResult['passTime'];
				break;
			default :
				$this->logger->logError(__FUNCTION__.' : FAIL userId : '.$userId.' type  2: '.$priceType);
				return $resultFail;
		}

		$result['Protocol'] = 'ResBuyChest';;
		$result['ResultCode'] = 100;

		$temp['rewards'] = $addResult['data'];

		if ($isUseFreePass) {
			$item['itemId'] = 100003;
			$item['add'] = 0 - $usePassCount;
			$item['cur'] = $restPassCount;
			$temp['use']['items'][] = $item;
		}
        if (isset($jewel)) {
            $temp['use']['jewel'] = $jewel;
        }
        if (isset($gold)) {
            $temp['use']['gold'] = $gold;
        }
        if (isset($heart)) {
            $temp['use']['heart'] = $heart;
		}

		$result['Data'] = $temp;
		return $result;
	}

	function checkEventChest($goodsId) {
		$EventManager = new EventManager();
		if ( false == $EventManager->IsActiveEvent(EVENT_SELL_PACKAGE) )//EVENT_CHEST) ) 100,300 통합
			return false;


		$addValId = array();
		switch ($goodsId) {
			case 3004: //일반 골드 상자
				if (rand(0,999) < 12) {
					$rewardCharac = array(98, 235, 236, 237, 282, 283);
					$addValId[] = $rewardCharac[rand(0, 5)];
				}
				break;
			case 3005: //일반 골드 상자 10
				if (rand(0,999) < 120) {
					$rewardCharac = array(98, 235, 236, 237, 282, 283);
					$addValId[] = $rewardCharac[rand(0, 5)];
				}
				break;

			case 3006: // 황금 상자
				if (rand(0,999) < 80)
					$addValId[] = 1505;
				break;
			case 3007: // 황금 상자 10 
				$addValId[] = 1505;
				break;

			case 3008: // 마법 황금 상자
				if (rand(0,999) < 80) {
					if (rand(0,999) < 80)
						$addValId[] = 10505;
					else
						$addValId[] = 10504;
				}
				break;
			case 3009: // 마법 황금 상자 10 
				if (rand(0,999) < 80)
					$addValId[] = 10505;
				else
					$addValId[] = 10504;
				break;

			case 3010: // 영혼석 상자
				if (rand(0,999) < 80) {
					$addValId[] = 99999;
				}
				break;
			case 3011: // 영혼석 상자 10
				$addValId[] = 99999;
				if (rand(0,999) < 30)
					$addValId[] = 99999;
				break;
			default:
				break;
		}
		return $addValId;
	}

	function setPremiumReward($db, $userId, $priceType, $chestCount,$goodsId, &$typeList) {
		$RandManager = new RandManager();
		switch($goodsId) {	
			case 3006: 
			case 3007: 
				$apc_key = 'ShopChest_GoldChest';
				break;
			case 3008: 
			case 3009: 
				$apc_key = 'ShopChest_GoldChest';
				break;
			case 3010: 
			case 3011: 
				$apc_key = 'ShopChest_GoldChest';
				break;
			default:
				$apc_key = 'ShopChest_GoldChest';
				break;
		}
		$temp = apc_fetch($apc_key);

		$resultData = $temp[1];	
		$dd1 = explode('/', $resultData);

		$resultArray = $temp[2];	
		$dd2 = explode('/', $resultArray);

		for ($i = 0; $i<$chestCount; $i++ ) {
			$itemId = $RandManager->Rate($dd1, $dd2);
			$typeList[] = $itemId;
		}
	}

	function setNormalReward($db, $userId, $priceType, $chestCount,$itemId) {
		$RandManager = new RandManager();
		$apc_key = 'ShopChest_NormalChest';
		$temp = apc_fetch($apc_key);

		$resultData = $temp[1];	
		$dd1 = explode('/', $resultData);

		$resultArray = $temp[2];	
		$dd2 = explode('/', $resultArray);

		for ($i = 0; $i<$chestCount; $i++ ) {
			$itemId = $RandManager->Rate($dd1, $dd2);
			$typeList[] = $itemId;
		}

		return $typeList;
	}

	function addItem ($db, $userId, $typeList, $amount) {
		$AddManager =  new AddManager();
		$chestCount  = count($typeList);
		for ( $idx = 0; $idx < $chestCount; $idx++ ) {
			$rewardType = (int)$typeList[$idx];
			if ( $rewardType < 1000 ) {        //고정 캐릭터 소울스톤 지급
				$this->logger->logError(__FUNCTION__.' :char :idx '.$rewardType);
				if ( $rewardType > 500 ) {
					$key_name = $rewardType-500;
					$apc_key = 'CharacIndex_'.$key_name;
					$charData = apc_fetch($apc_key);
					$charMin = $charData[0];
					$charMax = $charData[1];
					$charId = rand($charMin, $charMax);
				} else {
					$charId = $rewardType;
				}
				$CharResult = $AddManager->setAddHero($db, $userId, $charId, 10);
				if ($CharResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.' :Weapon FAIL userId : '.$userId);
					continue;
				}
				$charTemp = null;
				$charTemp['charTableId'] = $CharResult['charTableId'];
				$charTemp['charId'] = $CharResult['charId'];
				$charTemp['exp'] = $CharResult['curExp'];
//				$charTemp['addExp'] = $CharResult['addExp'];
				$this->logger->logError(__FUNCTION__.' : :idx '.$idx);

				$charData[] = $charTemp;
			} else if ( $rewardType < 2000 ) {    //보구 지급
				$this->logger->logError(__FUNCTION__.' :weapon :idx '.$rewardType);
				$rewardType = $rewardType - 1000;
				if ( $rewardType > 500 ) {
					$array_pos = $rewardType - 500;
					$apc_key = 'WeaponInform_WeaponMinIdx';
					$weaponTempMin = apc_fetch($apc_key);
					$minTemp = explode('/', $weaponTempMin[1]);
					$weaponMin = $minTemp[$array_pos];

					$apc_key = 'WeaponInform_WeaponMaxIdx';
					$weaponTempMax = apc_fetch($apc_key);
					$maxTemp = explode('/', $weaponTempMax[1]);
					$weaponMax = $maxTemp[$array_pos];

					$rewardType = rand($weaponMin, $weaponMax);
				}

				// 아이템 지급 	
				$WeaponResult =	$AddManager->addWeapon($db, $userId, $rewardType, null);
				if ($WeaponResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.' :Weapon FAIL userId : '.$userId);
					continue;
				}
				$weaponTemp = null;
				$weaponTemp['weaponTableId'] = $WeaponResult['weaponTableId'];
				$weaponTemp['weaponId'] = $WeaponResult['weaponId'];
				$weaponTemp['level'] = $WeaponResult['level'];
				$weaponTemp['statIds'] = $WeaponResult['statIds'];
				$weaponTemp['statVals'] = $WeaponResult['statVals'];
				$weaponTemp['exp'] = $WeaponResult['exp'];

				$this->logger->logError(__FUNCTION__.' : :idx '.$idx);
				$weaponData[] = $weaponTemp;
			} else if ( $rewardType  < 12000 ) {  //마법 지급
				$this->logger->logError(__FUNCTION__.' :magic :idx '.$rewardType);
				$resultId = $rewardType-10000;
				if ( $resultId > 500 ) {
					$key_name = $resultId - 500;
					$apc_key = 'MagicPowderValue_'.$key_name;
					$magicT = apc_fetch($apc_key);
					$magicMin = $magicT[2];
					$magicMax = $magicT[3];	

					$resultId = rand($magicMin, $magicMax);
				}

				$MagicResult = $AddManager-> addMagic($db, $userId, $resultId);
				if ($MagicResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.' :Magic FAIL userId : '.$userId);
					continue;
				}
				$magicTemp = null;
				$magicTemp['magicTableId'] = $MagicResult['magicTableId'];
				$magicTemp['magicId'] = $MagicResult['magicId'];
				$magicTemp['exp'] = 0;

				$this->logger->logError(__FUNCTION__.' : :idx '.$idx);
				$magicData[] = $magicTemp;
			} else {
				if ($rewardType == 99999) {
					$RandManager = new RandManager();
					$dd1 = array(6,7,8,9,10,11,12,13,14);
					$dd2 = array(295,295,200,100,50,30,15,10,5);
	
					$tempData = $RandManager->Rate_N_Idx($dd1, $dd2);
					$itemCnt = $tempData['val'];
				}
				else {
					$itemCnt = 1;
				}

				$itemResult = $AddManager->addItem($db, $userId, $rewardType,$itemCnt);
				if ($itemResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
					break;
				}
				$itemTemp = null;
				$itemTemp['itemTableId'] = $itemResult['itemTableId'];
				$itemTemp['itemId'] = $itemResult['itemId'];
				$itemTemp['cur'] = $itemResult['cnt'];
				$itemTemp['add'] = $itemCnt;
				$itemData[] = $itemTemp;
			}

		}
		$dataTotal = null;

        if (isset($weaponData)) {
            $dataTotal['weapons'] = $weaponData;
        }
        if (isset($magicData)) {
            $dataTotal['magics'] = $magicData;
        }
        if (isset($itemData)) {
            $dataTotal['items'] = $itemData;
        }
        if (isset($charData)) {
            $dataTotal['characs'] = $charData;
        }

		$result['data'] = $dataTotal;	
		return $result;
	}
		
	function BuyResource($param) {
		$resultFail['ResultCode'] = 300;
		$userId = $param["userId"];
		$serverId = @(int)$param["serverId"];
		$goodsId = $param["goodsId"];
		$isTest = $param["isTest"] == 1 ? true : false;

		$db = new DB();
		$sql = "SELECT priceType, price, itemCount, itemId 
				FROM frdShopData 
				WHERE shopItemId = :shopItemId";
		$db -> prepare($sql);
		$db -> bindValue(':shopItemId', $goodsId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			// 존재 하지 않는 상품 
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
			return $resultFail;
		}
		$priceType = $row['priceType'];
		$price = $row['price'];
		$count = $row['itemCount'];
		$itemId = $row['itemId'];

		// 차감 로직 시작 
		$AssetManager = new AssetManager();
		switch($priceType) {
			case 5000:
				$AssetResult = $AssetManager->useGold($db, $userId, $price);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $resultFail;
				}
				$gold['addGold'] = 0 - $price;
				$gold['curGold'] = $AssetResult['gold'];
				break;
			case 5004:
				$AssetResult = $AssetManager->useJewel($db, $userId, $price);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $resultFail;
				}
				$jewel['addJewel'] = 0 - $price;
				$jewel['curJewel'] = $AssetResult['jewel'];
				break;
			case 5003:
				$AssetResult = $AssetManager->useHeart($db, $userId, $price);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $resultFail;
				}
				$heart['addHeart'] = 0 - $price;
				$heart['curHeart'] = $AssetResult['curHeart'];
				$heart['maxHeart'] = $AssetResult['maxHeart'];
				$heart['passTime'] = $AssetResult['passTime'];
				break;
			default :
				$this->logger->logError(__FUNCTION__.' : FAIL userId : '.$userId.' type' );
				return $resultFail;
		}

		$addResult = $this->addAssetItem($db, $userId, $itemId, $count, $AssetManager);
		$data['rewards'] = $addResult;
        if (isset($gold)) {
            $data['use']['gold'] = $gold;
        }
        if (isset($jewel)) {
            $data['use']['jewel'] = $jewel;
        }
        if (isset($heart)) {
			$data['use']['heart'] = $heart;
        }

		$result['Procotol'] = 'ResBuyResource';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}

	function GetShopData($param) {
		$cVersion 	= $param["cVersion"];
		$serverId 	= @(int)$param["serverId"];
		$isTest 	= $param["isTest"] == 1 ? true : false;

		$version = Shop_Version;
		if ( ($version == $cVersion) && (!$isTest) ) {
			$result['Procotol'] = 'ResGetShopData';
			$result['ResultCode'] = 100;
			$result['compVers'] = $version.", ".$cVersion.", Shop_Vers = ".Shop_Version;
			$data['version'] = 0;
			$result['Data'] = $data;
			return $result;
		}

		$db = new DB();
		if ($isTest) {
			$sql = " SELECT * FROM frdShopData WHERE isTest = 1";
			// isTest 는 무조건 테스트 데이터를 판단하여 내려 준다.
		} else {
			/*  
			isDisplay 
			0 : 표시 하지 않음
			1 : 표시
			2 : 기간제 표시 
			*/
			// 후에 데이터를 관리 한다면 오픈 할 예정인 것은 isDisPlay 를 2로 셋팅
			// 그리고 시작 날짜는 모두 오픈 상태로 되어 있다.
			// 이벤트 종료일을 직접 조종 하여(endDate) 노출 할지를 정하면 된다.
			$sql = "
				SELECT * FROM frdShopData WHERE 
				( isDisPlay = 1 ) OR 
				( 	
					( TIMESTAMPDIFF(SECOND, startDate, NOW()) > 0  AND isDisPlay = 2 )
					AND
					( TIMESTAMPDIFF(SECOND, NOW(), endDate) > 0  AND isDisPlay = 2 ) 
				)";
		}

        $db -> prepare($sql);
        $db -> execute();
        $resultShopList   = null;
        while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            $resultShopList[] = $row;
        }
	
		$taps = null;
		if ( strpos($GLOBALS['COUNTRY'], 'China_QA') !== false ) {
			$taps[2] = $GLOBALS['GOLD'];
			$taps[3] = $GLOBALS['CHEST'];
			$taps[4] = $GLOBALS['HEART'];
			$taps[5] = $GLOBALS['TICKET'];
		}
		else {
			$taps[1] = $GLOBALS['JEWEL'];
			$taps[2] = $GLOBALS['GOLD'];
			$taps[3] = $GLOBALS['CHEST'];
			$taps[4] = $GLOBALS['HEART'];
			$taps[5] = $GLOBALS['TICKET'];
			$taps[6] = $GLOBALS['PACKAGE'];	
		}
		

		$eventCheck = false;
		foreach($resultShopList as $rs) {
			if ($rs['shopItemId'] > 7000 ) {
				$eventCheck = true;
			}
			$list = null;
			$list['Id'] 		= $rs['shopItemId'];
			$list['desc'] 		= $rs['desc'];
			$list['Name'] 		= $rs['name'];
			$list['imageAtlas'] = $rs['imageAtlas'];
			$list['ImageName'] 	= $rs['imageName'];
			$list['PriceType'] 	= (int)$rs['priceType'];
			$list['Price'] 		= $rs['price'];
			$list['iabId'] 		= $rs['productId'];
			$listArray[] = $list;
		}
		
		if ($eventCheck == true) {
			$taps[7] = $GLOBALS['EVENT'];	
		}

		if ($isTest) {
			$data['version'] = $cVersion;
		} else {
			$data['version'] = $version;
		}

		$data['tabs'] = $taps;
		$data['goods'] = $listArray;
		$result['Procotol'] = 'ResGetShopData';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;
	}	

	function addAssetItem($db, $userId, $itemId, $rewardCount, $AssetManager) {
		$total = null;
		switch ($itemId) {
			case 5000: // 골드 지급
				$gold_result = $AssetManager->addGold($db, $userId, $rewardCount);
				if ($gold_result['ResultCode'] != 100 ) {
					return $gold_result;
				}
				$data["curGold"] = $gold_result['gold'];
				$data["addGold"] = $rewardCount;
				$total['gold'] = $data;
				break;
			case 5002: //티켓 지급
				$AssetResult = $AssetManager->addTicket($db, $userId, $rewardCount);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $AssetResult;
				}
				$data["curTicket"] = $AssetResult['curTicket'];
				$data["addTicket"] = $rewardCount;
				$total['ticket'] = $data;
				break;
			case 5003: //행동력 지급
				$AssetResult = $AssetManager->addHeart($db, $userId, $rewardCount);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $AssetResult;
				}
				$data["curHeart"] = $AssetResult['curHeart'];
				$data["addHeart"] = $rewardCount;
				$data["maxHeart"] = $AssetResult['maxHeart'];
				$data["passTime"] = $AssetResult['passTime'];
				$total['heart'] = $data;
				break;
			case 5004: // 보석 지급
				$AssetResult = $AssetManager->addJewel($db, $userId, $rewardCount);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $AssetResult;
				}
				$data["curJewel"] = $AssetResult['jewel'];
				$data["addJewel"] = $rewardCount;
				$total['jewel'] = $data;
				break;
			case 5005: // 메달 지급
				$AssetResult = $AssetManager->addMedal($db, $userId, $rewardCount);
				if ($AssetResult['ResultCode'] != 100 ) {
					return $AssetResult;
				}
				$data["curSkillPoint"] = $AssetResult['skillPoint'];
				$data["addSkillPoint"] = $rewardCount;
				$total['skillPoint'] = $data;
				break;
			default : //일반 아이템 
				if ( $itemId >= 100000 && $itemId < 200000 ) {   //아이템 지급
					$AddManager = new AddManager();
					$itemResult = $AddManager->addItem($db, $userId, $itemId,$rewardCount);
					if ($itemResult['ResultCode'] != 100 ) {
						$this->logger->logError(__FUNCTION__.__LINE__.': FAIL userId : '.$userId);
						break;
					}
					$itemTemp = null;
					$itemTemp['itemTableId'] = $itemResult['itemTableId'];
					$itemTemp['itemId'] = $itemResult['itemId'];
					$itemTemp['cur'] = $itemResult['cnt'];
					$itemTemp['add'] = $rewardCount;

					$total['items'][] = $itemTemp;
				}
				break;

		}

		return $total;
	}
}
?>
