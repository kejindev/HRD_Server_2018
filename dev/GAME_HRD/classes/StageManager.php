<?php
require_once './common/DB.php';
require_once './common/define.php';
require_once './lib/Logger.php';
require_once "./lib/RandManager.php";
require_once "./classes/AssetManager.php";


require_once "./classes/DailyQuestManager.php";
require_once "./classes/GetAbillityManager.php";
require_once './classes/LogDBSendManager.php';


require_once './classes/WebviewManager.php';
require_once './classes/EventManager.php';
require_once './classes/LobbyEventManager.php';
require_once './send_data/SendDataManager.php';
require_once "./classes/GetRewardManager.php";

class StageManager {

	public function __construct() {
		$this -> logger = Logger::get();
	}
	function IsSuccess($conditionType, $lastWave, $std, $seconds, $difficulty, $isClear) {

		switch($conditionType) {
			case 0:
				$value = (int)$seconds;
				if ( $std < $value || !$isClear )
					return false;
				else
					return true;

			case 4:
				if ( $lastWave >= $std )
					return true;
				else
					return false;

			case 5:
				if ( $isClear > 0 )
					return true;
				else
					return false;

			case 6:
				return true;

			case 10:      // Life difficulty 0
				return true;

			case 11:      // Life difficulty 1
				if ( $difficulty >= 1 && $isClear > 0 )
					return true;
				else
					return false;

			case 12:      // Life difficulty 2
				if ( $difficulty >= 2 && $isClear > 0 )
					return true;
				else
					return false;

			case 13:      // Life difficulty 3
				if ( $difficulty >= 3 && $isClear > 0 )
					return true;
				else
					return false;

			default:
				return false;
		}
	}


	function getFirstClearBonus($stageId) {
		$apc_key = "FirstClearBonus_New_".$stageId;
		$apc_data = apc_fetch($apc_key);
		$firstRewardDatas = $apc_data;

		$temp =  null;
		if ( $firstRewardDatas ) {
			$temp['resultRewardType'] = (int)$firstRewardDatas[1];
			$temp['resultRewardCount'] =  (int)$firstRewardDatas[2];
		}

		return $temp;	
	}

	function EnterStage($param) {
		$EventManager = new EventManager();

		$resultFail['ResultCode'] = 300;

		$userId = $param["userId"];
		$stageId = $param["stageId"];
		$selectedDifficult = $param["selectedDifficult"];
		$curPId = $param["curPId"];
		//		$st = $param["startDate"];

		$cliVers = $param["cliVers"]; // FLOAT
		if ( $stageId >= 600 && $stageId < 605 ) {
			// $now = date('ymdH'); 
			// if ( $now >= 17111305 && $userId != '200075628' ) {
			// 	$resultFail['ResultCode'] = 10006;
			// 	return $resultFail;
			// }
			if (
				( $GLOBALS['Type'] == 'PS' && $cliVers < 1.433 )
				|| ( $GLOBALS['Type'] == 'OS' && $cliVers < 1.125 )
				|| ( $GLOBALS['Type'] == 'AS' && $cliVers < 1.125 )
			) {
				$resultFail['ResultCode'] = 10003;
				return $resultFail;
			}
		}

		if (isset($param["saveLength"]) ) {
			$saveLength = $param["saveLength"];
		} else {
			$saveLength = 0;
		}

		$db = new DB();
		$sql = "select curPId from frdUserData where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row ) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
			return $resultFail;
		}

		if ( ($curPId+1) < (int)$row["curPId"] ) {    //curPid = 1     db = 1
			$this->logger->logError(__FUNCTION__.' : error userId :'.$userId.' curPId :'.$curPId);
			return $resultFail;
		}

## 고탑을 제외한 모든 스테이지 입장 처리를 여기서처리 한다.
## 뷴류가 많으나. 현재는 할 수 없다.
		if ( $stageId >= 500 && $stageId < 20000 ) {
			if ( $stageId >= 10000) {     // 강림
				// 강림던전 스케쥴러 인데 디비로 옮기는게 나을듯 하다.
				$sql = "select now() as nowData";
				$db -> prepare($sql);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				$endTime = $row['nowData'];

				// $endTime = mktime($h,0,0,$m,$d,$y) + $datas[1]*3600 + 3600;
			} else if ( $stageId < 600 ){  // 요일던전
				if ( !$param['Test'] ) {
					$dayOfWeek = ($stageId-500)%7;
					if ( $dayOfWeek == 0 )
						$dayOfWeek = 7;
					if ( (int)$dayOfWeek !== (int)date("N") ) {
						$this->logger->logError(__FUNCTION__.' : error date userId :'.$userId);
						$resultFail['ResultCode'] = 10001;
						return $resultFail;
					}
				}
			} else if ( $stageId > 2000 && $stageId < 4000 ) {  //신전
				$templeId = (int)(($stageId-2000)/100);

				$sql = "SELECT dailyCount FROM frdTempleClearCount where userId = :userId";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				if ( $row ) {
					$dailyCount = (int)$row["dailyCount"];
					switch($dailyCount) {
						case 0:case 1:break;
						case 2: $needTicket = 3;break;
						case 3: $needTicket = 6;break;
						case 4: $needTicket = 9;break;
						default:$needTicket = 9999999;break;
					}
				}
			} 
			// error pass 
		}

		if ( $stageId>1000&&$stageId<2000 ) {
			$sql = "SELECT stageId FROM frdAbyss where userId = :userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			$heartAbyssUse = false;
			if ( $row ) {
				if ($stageId > $row['stageId']) {
					$heartAbyssUse = true;
				}
			}

		}

		if ( ($stageId>=600 && $stageId<700) ) {  //climbTop
			$time = time() + 9*3600 - 24*3600*4-5*3600;
			$totalWeeks = (int)($time / 604800);
			$weekVal = (int)600 + ((int)($totalWeeks%5));

			if ( (int)$weekVal !== (int)$stageId ) {
				$this->logger->logError(__FUNCTION__.' : error stageId 2 userId :'. $userId);
				$resultFail['ResultCode'] = 10001;
				return $resultFail;
			}

			//event
			$EventManager->getClimbEnterReward($db,$userId);
			//~event
		}

		$AssetManager = new AssetManager();
		$result_check_SP = $AssetManager->checkHeart($db, $userId);
        if ($result_check_SP['ResultCode'] != 100) {
            $this -> logger -> logError(__FUNCTION__.' : checkHeart FAIL userId :'.$userId);
            return $resultFail;
        }

		if ( @!is_null($needTicket) ) {
			$sql = "select exp, ticket,heart from frdUserData 
				where userId =:userId";
		} else if ( @!is_null($needJewel) ) {
			$sql = "select exp, jewel, heart 
				from frdUserData where userId = :userId";
		} else {
			$sql = "select exp, heart 
				from frdUserData where userId = :userId";
		}
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row ) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
			return $resultFail;
		}

		if (isset($row['ticket'])){ 
			$ticket = $row['ticket'];
		}
		if (isset($row['jewel'])){ 
			$jewel = $row['jewel'];
		}
		$myHeart = $row['heart'];

		$apc_key = 'stageDataTotal_'.$stageId;
		$stageData = apc_fetch($apc_key);

		$heartTemp = explode('/', $stageData[4]);
		$data["heart_bundleId"] = $heartTemp;

		$arrHeart= explode('/',$stageData[4]);
		$needHeart = $arrHeart[0];

		if ( $selectedDifficult >= 0 && $selectedDifficult < 100 ) {
			$addHeartCount=0;

			// apc 를 이용해서 가져 와야 한다.	
			/*
			   $apc_key = 'heart_'.$name;
			   $apc_data = apc_fetch($apc_key);
			 */

			if ( $stageId>1000&&$stageId<2000 ) {
				if ($heartAbyssUse == false ) {
					$needHeart = 0;
				}
			}

			if ( $EventManager->IsActiveEvent(EVENT_HALF_HEART) ) {
				if ( $stageId >= 500 && $stageId < 600 ) {
					$needHeart = round($needHeart/2);
				}
			}

			if ($needHeart > $myHeart) {
				$this->logger->logError(__FUNCTION__.' : need heart FAIL userId :'.$userId. 'need'.$needHeart.'my'.$myHeart);
				$resultFail['ResultCode'] = 10002;
				return $resultFail;
			}

			if ( $stageId>1000&&$stageId<2000 ) {
				// 이미 플레이한 심연 던전은 차감하지 않는다. 
				if ( $heartAbyssUse == true && false == $EventManager->IsActiveEvent(EVENT_INFINITY_HEART) ) {
					$data["heart_bundleId"] = array(0,0);

					$AssetResult = $AssetManager->useHeart($db, $userId, $needHeart);
					if($AssetResult['ResultCode'] != 100 ) {
						$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId);
						return $resultFail;
					}
					$data['use']['heart']["addHeart"] = 0 - $needHeart;
					$data['use']['heart']["curHeart"] = $AssetResult['curHeart'];
					$data['use']['heart']["maxHeart"] = $AssetResult['maxHeart'];
					if ($AssetResult['passTime'] == null) {
						$data['use']['heart']["passTime"] = -1;
					} else {
						$data['use']['heart']["passTime"] = $AssetResult['passTime'];
					}
				}
			} 
			else if ( false == $EventManager->IsActiveEvent(EVENT_INFINITY_HEART) ) {
				$AssetResult = $AssetManager->useHeart($db, $userId, $needHeart);
				if($AssetResult['ResultCode'] != 100 ) {
					$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId);
					return $resultFail;
				}
				$data['use']['heart']["addHeart"] = 0 - $needHeart;
				$data['use']['heart']["curHeart"] = $AssetResult['curHeart'];
				$data['use']['heart']["maxHeart"] = $AssetResult['maxHeart'];
				if ($AssetResult['passTime'] == null) {
					$data['use']['heart']["passTime"] = -1;
				} else {
					$data['use']['heart']["passTime"] = $AssetResult['passTime'];
				}
			}

			if (@!is_null($needTicket) ) {
				$resultTicket = $ticket - $needTicket;
				if ($resultTicket < 0 ) {
					$this->logger->logError(__FUNCTION__.' : need - tikcet FAIL userId :'.$userId);
					return $resultFail;
				}
				$AssetResult = $AssetManager->useTicket($db, $userId, $needTicket);
				if ($AssetResult['ResultCode'] != 100) {
					$this->logger->logError(__FUNCTION__.' : need ticket FAIL userId :'.$userId);
					return $resultFail;
				} else {
					// 일일 미션 
					$DailyQuestManager = new DailyQuestManager();
					$DailyQuestManager->updateDayMission($db, $userId, 4, 1);
				}
				$data['use']['ticket']["curTicket"] = $resultTicket;
				$data['use']['ticket']["addTicket"] = 0 - $needTicket;
			} else if (@!is_null($needJewel) ) {
				$resultJewel = $jewel - $needJewel;
				if ( $resultJewel < 0) {
					$this->logger->logError(__FUNCTION__.' : need  - jewel FAIL userId :'.$userId);
					return $resultFail;
				}
				$AssetResult = $AssetManager->useJewel($db, $userId,$needJewel);
				if ($AssetResult['ResultCode'] != 100) {
					$this->logger->logError(__FUNCTION__.' : need jewel FAIL userId :'.$userId);
					return $resultFail;
				}
				$data['use']['jewel']["curJewel"] = $resultJewel;
				$data['use']['jewel']["addJewel"] = 0 - $needJewel;
			} 
			// not need else 
		} else {    //load
			$sql = "select saveLength from frdSaveDatas where userId = :userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row ) {
				$sql = "insert into frdSaveDatas values (:userId, :saveLength)";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':saveLength', $saveLength, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.' :  FAIL userId :'.$userId.' sql :'.$sql);
					return $resultFail;
				}
			} else  {
				$sql = "UPDATE frdSaveDatas SET saveLength=:saveLength where userId=:userId";
				$db -> prepare($sql);
				$db -> bindValue(':saveLength', $saveLength, PDO::PARAM_INT);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.' :  FAIL userId :'.$userId.' sql :'.$sql);
					return $resultFail;
				}
			}
		}

#0 StageId
#1 TotalWave/MaxMon/monCount/intervalTimer
#2 StartMamber
#3 MakeChaPosition
#4 NeedHeart
#5 MonWay
#6 ClearType
#7 ClearTemp
#8 RewardId
#9 RewardAmount
#10 RewardPercent
#11 NeedClearIdx
#12 GimicType
#13 Gimic
		//"Wave","Timer","MonIdx","Atribute","Size","Hp","Def","Speed","Price","isBoss","reward"
		// 몬스터번호	속성	원본크기	체력	방어력	이동속도	가격
		// 스테이지 정보에 합쳐짐 
		$totalData1 = $stageData[1];
		$totalDataTemp = explode('/',$totalData1);

		$waveTimer = null;
		$isBoss = null;
		$rewardChar = null;
		for($tempI = 0; $tempI < 400; $tempI++){
			$apc_key = $stageId.'_'.$tempI;
			$monData = apc_fetch($apc_key);
			if ($monData) {
				if ($monData[8] == "undefined") {
					$monData[8] = 0;
				}
				$tempMon[$tempI] = $monData[2].','.$monData[3].','.$monData[4].',0,0,'.$monData[5].','.$monData[6].','.$monData[7].','.$monData[8];
				if ($monData[10] != "undefined") {
					$rTemp = explode('/',$monData[10]);
					$rewardChar[$tempI] = $rTemp;
				}

				if ($tempI < $totalDataTemp[0]) {		
					$waveTimer[] = $monData[1];
				}
				if ($monData[9] == 'True') {
					$isBoss[] = $tempI;
				}
			}
		}


		$data["monInform"] = $tempMon;

		$data["tw_ml_wmc_if"] = $totalDataTemp;
		//		$data["monGenCount"] = $totalDataTemp[2];
		$data["monGenIntervalFrame"] = $totalDataTemp[3];

		$data["waveRewardCharacs"] = $rewardChar;
		$data["waveTimer"] = $waveTimer;

		$stageData[5] = str_replace(":",",",$stageData[5]);
		$tempStage = explode('&',$stageData[5]);
		foreach ($tempStage as $ts) {
			$tempSS = explode('/',$ts);
			$data["monPath"][] = $tempSS;
		}


		$data["bossWave"] = $isBoss;

		$startTemp = explode('/',$stageData[2]);
		if ($stageData[2] == "" ) {
			$startTemp = null;
		}
		$data["startGenCharacs"] = $startTemp;

		$posTemp = explode('/',$stageData[3]);
		foreach($posTemp as $pt) {
			$pTemp = str_replace(":",",",$pt);
			$data["characPos"][] = $pTemp;
		}

		$data['bossSkills'] = null;
		$apc_key = 'Stage_boss_'.$stageId;
		$abyssData = apc_fetch($apc_key);
		if ($abyssData) {
			$data['bossSkills'] = $abyssData[1].'|'.$abyssData[2].'|'.$abyssData[3].'|'.$abyssData[4].'|'.$abyssData[5].'|'.$abyssData[6].'|'.$abyssData[7].'|'.$abyssData[8].'|'.$abyssData[9].'|'.$abyssData[10];
		}

		$gimicType = explode('/', $stageData[12]);
		$gimicValue = explode('&', $stageData[13]);
		$temp = null;
		$tempValue = null;
		$tTotal = null;
		$gimicCnt = count($gimicType);
		if ($gimicType[0] != null ) {
			for($i=0; $i < $gimicCnt; $i++) {
				$tList = null;
				$tempValue = explode('/', $gimicValue[$i]);
				foreach($tempValue as $tv) {
					$tv = str_replace(":",",",$tv);

					if ($tList == null) {
						$tList[] = $gimicType[$i];
						$tList[] = $tv;
					} else {
						$tList[] = $tv;
					}

				}

				$tTotal[] = $tList;
			}	
		}
		$data["specialDatas"] = $tTotal;

		// 일일 미션 체크 
		if ($stageId > 599 && $stageId < 605 ) { // 고난의 탑 플레이 
			$DailyQuestManager = new DailyQuestManager();
			$DailyQuestManager->updateDayMission($db, $userId, 2, 1);
		} else if  ($stageId >= 2000 && $stageId < 3000 ) { // 신전 스테이지 플레이 
			$DailyQuestManager = new DailyQuestManager();
			$DailyQuestManager->updateDayMission($db, $userId, 12, 1);
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendStage($userId, $stageId, 1);

		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResEnterStage';
		$result['Data'] = $data;

		return $result;
	}

	function GetChapterReward($param){
		$resultFail['Resultcode'] = 300;

		$chapter = $param["chapter"];
		$userId = $param["userId"];
		switch($chapter) {
			case 1:
				$rewardType = 1502;
				$rewardCount = 1;
				break;
			case 2:
				$rewardType = 10501;
				$rewardCount = 1;
				break;
			case 3:
				$rewardType = 100003;
				$rewardCount = 1;
				break;
			case 4:
				$rewardType = 10502;
				$rewardCount = 1;
				break;
			case 5:
				$rewardType = 1503;
				$rewardCount = 1;
				break;
			case 6:
				$rewardType = 100003;
				$rewardCount = 2;
				break;
			case 7:
				$rewardType = 10503;
				$rewardCount = 1;
				break;
			case 8:
				$rewardType = 100001;
				$rewardCount = 1;
				break;
			case 9:
				$rewardType = 100003;
				$rewardCount = 3;
				break;
			case 10:
				$rewardType = 110000;
				$rewardCount = 1;
				break;
			default:
				$rewardType = 1502;
				$rewardCount = 1;
				break;
		}

		$db = new DB();

		$sql = "select chapterReward, now() as curTime from frdUserData where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row ) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
			return $resultFail;
		}

		$val = $row["chapterReward"];
		$now = $row["curTime"];

		if ( (($val >> $chapter) & 1) == 1 ) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.'val :'.$val.' chap:'.$chapter);
			return $resultFail;
		}

		$bitVal = (1 << $chapter);
		$val |= $bitVal;

		$sql = "UPDATE frdUserData set chapterReward=:val where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':val', $val, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
			return $resultFail;
		}

		$sql = "INSERT INTO frdUserPost 
			(recvUserId, sendUserId, type, count, expire_time,  reg_date)
			values (:userId, 0, :rewardType, :rewardCount, DATE_ADD(now(), INTERVAL 6 DAY), now())";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':rewardType', $rewardType, PDO::PARAM_INT);
		$db -> bindValue(':rewardCount', $rewardCount, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
			return $resultFail;
		}

		$data["chapterReward"] = $val;

		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResGetChapterReward';
		$result['Data'] = $data;

		return $result;
	}

	function GetStage ($param) {
		$userId = $param['userId'];
		$stageId = $param['stageId'];

		$apc_key = 'stageDataTotal_'.$stageId;
		$stageData = apc_fetch($apc_key);

#0 StageId
#1 TotalWave/MaxMon/monCount/intervalTimer
#2 StartMamber
#3 MakeChaPosition
#4 NeedHeart
#5 MonWay
#6 ClearType
#7 ClearTemp
#8 RewardId
#9 RewardAmount
#10 RewardPercent
#11 NeedClearIdx
#12 GimicType
#13 Gimic

		$heartTemp = explode('/', $stageData[4]);
		$EventManager = new EventManager();
		if ( $EventManager->IsActiveEvent(EVENT_HALF_HEART) ) {
			if ( $stageId >= 500 && $stageId < 600 ) {
				$heartTemp[0] = round((int)$heartTemp[0]/2);
			}
		}
		$data["heart_bundleId"] = $heartTemp;

		$twTemp = explode('/', $stageData[1]);
		$data["tw_ml_wmc_if"] = $twTemp;

		$typeTemp = explode('/', $stageData[6]);
		$data["clearConditionType"] = $typeTemp;

		$valTemp = explode('/', $stageData[7]);
		$data["clearConditionVal"] = $valTemp;

		$idTemp = explode('/', $stageData[8]);
		$data["rewardId"] = $idTemp;

		$cntTemp = explode('/', $stageData[9]);
		$data["rewardCount"] = $cntTemp;

		$perTemp = explode('/', $stageData[10]);
		$data["rewardPercent"] = $perTemp;

		$condTemp = explode('/', $stageData[11]);
		$data["clearConditionIdx"] = $condTemp;

		$gimicType = explode('/', $stageData[12]);
		$gimicValue = explode('&', $stageData[13]);
		$temp = null;
		$tempValue = null;
		$tTotal = null;
		$gimicCnt = count($gimicType);
		if ($gimicType[0] != null ) {
			for($i=0; $i < $gimicCnt; $i++) {
				$tList = null;
				$tempValue = explode('/', $gimicValue[$i]);
				foreach($tempValue as $tv) {
					$tv = str_replace(":",",",$tv);

					if ($tList == null) {
						$tList[] = $gimicType[$i];
						$tList[] = $tv;
					} else {
						$tList[] = $tv;
					}

				}

				$tTotal[] = $tList;
			}	
		}
		$data["specialDatas"] = $tTotal;

		$data['bossSkills'] = null;
		$apc_key = 'Stage_boss_'.$stageId;
		$abyssData = apc_fetch($apc_key);
		if ($abyssData) {
			$data['bossSkills'] = $abyssData[6];
		}

		if ( $stageId > 1000 ) {
			// firstClearBonus get apc data 
			$apc_key = 'FirstClearBonus_New_'.$stageId;
			$apc_data = apc_fetch($apc_key);
			if ( $apc_data != null ) {
				if ($apc_data[1] != null) {
					$data["rewardId"][] = (int)$apc_data[1];
					$data["rewardCount"][] = (int)$apc_data[2];
					$data["rewardPercent"][] = 1000;
					$data["clearConditionIdx"][] = 0;
					$data["firstClearBonusCount"] = 1;
				}
				if ($apc_data[3] != null) {
					$data["rewardId"][] = (int)$apc_data[3];
					$data["rewardCount"][] = (int)$apc_data[4];
					$data["rewardPercent"][] = 1000;
					$data["clearConditionIdx"][] = 0;
					$data["firstClearBonusCount"] = 2;
				}
			}
		}

		if ( $stageId >= 500 && !($stageId>=600 && $stageId<700) ) {
			$db = new DB();
			$sql = "SELECT clearCount from frdClearCount 
				where userId = :userId and stageId=:stageId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row ) {
				$data["clearCount"] = 0;
			} else {
				$data["clearCount"] = $row["clearCount"];
			}
		}


		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResGetStage';
		$result['Data'] = $data;
		return $result;
	}

	function setLogRecord( $db, $userId,$stageId, $saveLength, $lastWave, $seconds ) {
		$resultFail['ResultCode'] = 300;

		// Log ~
		$sql = "select userId from frdLogPlay where userId = :userId and stageId=:stageId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row ) {
			$sql = "INSERT INTO frdLogPlay 
				(userId, stageId, totalWave, totalSecs, playCount, ticketCount, reg_date)
				VALUES (:userId, :stageId, :lastWave, :seconds, 1, 0, now())";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
			$db -> bindValue(':lastWave', $lastWave, PDO::PARAM_INT);
			$db -> bindValue(':seconds', $seconds, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
				return $resultFail;
			}

		} else {
			$sql = "UPDATE frdLogPlay set 
				totalWave= totalWave+:lastWave, totalSecs=totalSecs+:seconds, 
				playCount=playCount+1 
					WHERE userId= :userId AND stageId= :stageId"; 
					$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
			$db -> bindValue(':lastWave', $stageId, PDO::PARAM_INT);
			$db -> bindValue(':seconds', $stageId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
				return $resultFail;
			}
		}

		$result['ResultCode'] = 100;	
		return $result;
	}

	function setClearAdd($db, $userId, $stageId, $seconds) {
		$resultFail['ResultCode'] = 300;

		$sql = "SELECT clearCount, bestSecs from frdClearCount where userId=:userId and stageId=:stageId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row ) {
			$sql = "insert into frdClearCount 
				(userId, stageId, clearCount, bestSecs) values (:userId, :stageId, 1, :bestSecs)";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
			$db -> bindValue(':bestSecs', $seconds, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
				return $resultFail;
			}
			$result['bestSecs'] = $seconds;
		} else {
			$finalBestSecs = $seconds < $row['bestSecs'] ? $seconds : $row['bestSecs'];
			$sql = "UPDATE frdClearCount SET 
				clearCount=clearCount+1,
				bestSecs=:bestSecs
				WHERE userId=:userId and stageId=:stageId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
			$db -> bindValue(':bestSecs', $finalBestSecs, PDO::PARAM_INT);
			$returnBestSecs = $row['bestSecs'];
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
				return $resultFail;
			}
			$result['bestSecs'] = $returnBestSecs;
		}
		
		$result['ResultCode'] = 100;
		return $result;
	}

	function GetMyClearData($param) {
		$resultFail['ResultCode'] = 300;

		$userId = $param["userId"];

		$data = null;	

		$db = new DB();
		$sql = "select stageId, clearCount from frdClearCount where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$stageId = null;
		$count = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$stageId[] = $row['stageId'];
			$count[] = $row['clearCount'];
		}

		$data['stageId'] = $stageId;
		$data['count'] = $count;

		$sql = "select accuSkillPoint,exp,userLevel from frdUserData where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' : FAIL  userId :'.$userId.' sql :'.$sql);
			return $resultFail;
		}
		$data["accuSkillPoint"] = $row["accuSkillPoint"];
		$userLevel = $row['userLevel'];

		$myKey = $userLevel-1;
		if ($userLevel > 1) {
			$lv_apc_key = 'UserExp_MaxHeart_'.$myKey;
			$lv_data = apc_fetch($lv_apc_key);
			if (!$lv_data) {
				$this->logger->logError(__FUNCTION__.' : APC NOT userId :'.$userId.' level :'.$userLevel);
			}
			$curMaxExp = $lv_data[3]+ $row['exp'];
		} else {
			$curMaxExp = $row['exp'];
		}
	
		$data['UserExpAccu'] = $curMaxExp;

		$result['Protocol'] = 'ResGetMyClearData';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;
		return $result;

	}


	function GetStageReward ($param) {
		$EventManager = new EventManager();

		$resultFail['ResultCode'] = 300;

		$userId = $param["userId"];
		$isClear = $param["isClear"];
		$stageId = (int)$param["stageId"];
		$lastWave = (int)$param["lastWave"];
		$seconds = (int)$param["seconds"];
		$difficulty = (int)$param["difficulty"];

		if (isset($param['isHack'])) {
			$isHack = $param["isHack"];
		} else {
			$isHack = 0;
		}
		$curRewardIds = $param["curRewardIds"];

		$templeId = null;
		$diffLevel = null;
		$firstReward = null;

		// CHECK SAVE HACK 
		// 변경된 테이블 구조로 로직을 변경 해야 한다.
		// 체크 하는 조건은 아래의 클리어 난이도 스테이지 레벨을 준수 한다
		// 검증 내용은 stageStage 와 마지막 입장 시간후 경과 시간을 조건을 검사 한다.

		$db = new DB();
		$sql = "SELECT stageLevel,UNIX_TIMESTAMP(NOW()) as nowTime, tutoLevel, userLevel FROM frdUserData where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row ) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
			return $resultFail;
		}

		$curMaxStageLevel = (int)$row["stageLevel"];
		$curMaxWave = (int)$row["stageLevel"];
		$tutoLevel = (int)$row["tutoLevel"];
		$userLevel = (int)$row["userLevel"];

		$nowTime = $row['nowTime'];

		if ( $isClear > 0 && $difficulty >= 0 && $stageId>80 ) {   
			$sql = "SELECT * FROM frdLastStageTime where userId = :userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> execute(); $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row ) {
				// DATETIME 으로 시간을 변경 해야 한다.
				$sql = "INSERT INTO frdLastStageTime 
					(userId, status, stageId, enter_time)
					values (:userId, 0,:stageId, now())";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
					return $resultFail;
				}
			} else {
				$checkTimer = 240;
				if ( $stageId>1000 && $stageId<2000 ) {
					$checkTimer = 180;
				}
			}
		}

		// 소모되는 하트 양을 계산한다 APC 에서 가져 온다
		$apc_key = 'stageDataTotal_'.$stageId;
		$stageData = apc_fetch($apc_key);

		$heartTemp = explode('/', $stageData[4]);
		$data["heart_bundleId"] = $heartTemp;

		$arrHeart= explode('/',$stageData[4]);
		$needHeart = $arrHeart[0];
		if ( $EventManager->IsActiveEvent(EVENT_HALF_HEART) ) {
			if ( $stageId >= 500 && $stageId < 600 ) {
				$needHeart = round($needHeart/2);
			}
		}

		$AssetManager = new AssetManager();
		// 하트 차감 이 안에 check Heart 가 들어 있다.
		// 난이도가 0 보다 작을떄 
		// 티켓 사용 진입 - 빠른 방어 
		if ( $difficulty < 0) {
			if ( $stageId > 20000 ) {
				$needTicket = (int)floor($needHeart/4);
			} else {
				$needTicket = (int)floor($needHeart/6);
			}
			// 티켓 차감 
			$AssetResult = $AssetManager->useTicket($db, $userId, $needTicket);
			if ($AssetResult['ResultCode'] != 100) {
				$this->logger->logError(__FUNCTION__.' : need jewel FAIL userId :'.$userId);
				return $resultFail;
			} else {
				// 일일 미션 
				$DailyQuestManager = new DailyQuestManager();
				$DailyQuestManager->updateDayMission($db, $userId, 4, 1);
			}

			$ticket = null;
			$ticket['curTicket'] = $AssetResult["ticket"];
			$ticket['addTicket'] = $needTicket;
			$use['ticket'] = $ticket;

			// 빠른 방어 이기 떄문에 로그가 항상 존재 한다.
			$sql = "UPDATE frdLogPlay SET ticketCount=ticketCount+1 
				WHERE userId=:userId and stageId=:stageId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':stageId', $stageId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
				return $resultFail;
			}

			// 티켓 사용이 아닌 정상 플레이 
			// 로그 기록 여부를 체킹 하고 기록 
		} else {
			if ( $isHack > 0 ) {
				$this->logger->logError(__FUNCTION__.' : Hack Checked userId :'.$userId);
				return $resultFail;
			}
			if (!isset($saveLength)){
				$saveLength = 0;
			}

			$this->setLogRecord( $db, $userId, $stageId, $saveLength, $lastWave, $seconds);
			$data["needHeart"] = $needHeart;
		}

		$data["time"] = $nowTime;

		$idx = 0;

		// 클리어 누적 기록 처리 
		if ($isClear) {
			$ClearResult = $this->setClearAdd($db, $userId, $stageId, $seconds);
			if ($ClearResult['ResultCode'] != 100) {
				$this->logger->logError(__FUNCTION__.' : CLEAR PROCESS FAIL  userId :'.$userId);
				return $resultFail;
			}
			$data['bestSecs'] = $ClearResult['bestSecs'];
		}


		
		if ( $stageId < 210 ) { //normal
			if ( ($curMaxStageLevel <= $stageId-1) ) {
				if ($isClear == 1) {//&& ($lastWave >= $curMaxWave-6) ) 
					$resultStageLevel = $stageId;
					$sql = "UPDATE frdUserData set stageLevel=:resultStageLevel where userId=:userId"; 
					$db -> prepare($sql);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> bindValue(':resultStageLevel', $resultStageLevel, PDO::PARAM_INT);
					$row = $db -> execute();
					if (!isset($row) || is_null($row) || $row == 0) {
						$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
						$this->logger->logError(__FUNCTION__.' : FAIL resultStageLevel :'.$resultStageLevel);
						return $resultFail;
					}

					$data["stageLevel"] = $resultStageLevel;
					$data["isFinalStageClear"] = 1;
					$firstReward = $this->getFirstClearBonus($stageId);

					$addVal = (((int)$tutoLevel) & 0xf0000000);
					$tutoLevel = $stageId;
					switch($stageId) {
						case 1:
							$tutoLevel = 3;break;
						case 2:case 3:case 4:case 5:
							$tutoLevel = 5;break;
						default:
							$tutoLevel = $stageId;break;
					}

					$tempTuto = $addVal + $tutoLevel;

					$sql = "UPDATE frdUserData SET tutoLevel=:tutoLevel WHERE userId=:userId";
					$db -> prepare($sql);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> bindValue(':tutoLevel', $tempTuto, PDO::PARAM_INT);
					$row = $db -> execute();
					if (!isset($row) || is_null($row) || $row == 0) {
						$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
						return $resultFail;
					}

					$data["tutoLevel"] = $addVal+$tutoLevel;
	
					$this->logger->logError(__FUNCTION__.' : TT userId :'.$userId.' tutoLevel :'.$data['tutoLevel']);

				} else if ($lastWave >= $curMaxWave) {
					$resultStageLevel = $stageId-1;

					$sql = "UPDATE frdUserData set stageLevel=:resultStageLevel where userId=:userId"; 
					$db -> prepare($sql);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> bindValue(':resultStageLevel', $resultStageLevel, PDO::PARAM_INT);
					$row = $db -> execute();
					if (!isset($row) || is_null($row) || $row == 0) {
						$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
						$this->logger->logError(__FUNCTION__.' : FAIL resultStageLevel :'.$resultStageLevel);
						return $resultFail;
					}
					$data["stageLevel"] = $resultStageLevel;
				}
			}
		} else if ( $stageId > 2000 && $stageId < 4000 ) {    // 신전 스테이지
			$diffLevel = $stageId%100;
			$templeId = (int)round(($stageId-2000) / 100);

			if ($stageId == 2100 ) {
				$addVal = (((int)$tutoLevel) & 0xf0000000);
				$tempLevel = $row["tutoLevel"] - $addVal;
				if ($tempLevel < 76) {
					$resultTuto = $addVal+76;
					$sql = "UPDATE frdUserData SET tutoLevel=:tutoLevel WHERE userId=:userId";
					$db -> prepare($sql);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> bindValue(':tutoLevel', $resultTuto, PDO::PARAM_INT);
					$row = $db -> execute();
					if (!isset($row) || is_null($row) || $row == 0) {
						$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
						return $resultFail;
					}
					$data["tutoLevel"] = $resultTuto;
				}
			}

			if ( $isClear > 0 ) {

				$sql = "select dailyCount, totalCount from frdTempleClearCount where userId = :userId";
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				if (!$row ) {
					$sql = "insert into frdTempleClearCount (userId, dailyCount, totalCount, reg_date) 
						values (:userId, 1, 1, now())";
					$db -> prepare($sql);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$row = $db -> execute();
					if (!isset($row) || is_null($row) || $row == 0) {
						$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
						return $resultFail;
					}
					$data["templeDCC"] = 1;
				} else {
					$resultTempleDCC = (int)$row["dailyCount"]+1;
					if ( $resultTempleDCC >= 6) {
						$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' error templeDCC '.$resultTempleDCC);
						$resultFail['ResultCode'] = 10001;
						return $resultFail;
					}

					$totalCount = (int)$row["totalCount"]+1;
					$sql = "update frdTempleClearCount 
						set dailyCount= $resultTempleDCC, totalCount=$totalCount, update_date=now() where userId=:userId";
					$db -> prepare($sql);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$row = $db -> execute();
					if (!isset($row) || is_null($row) || $row == 0) {
						$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
						return $resultFail;
					}

					$data["templeDCC"] = $resultTempleDCC;
				}

				$sql = "select clearLevel from frdTemple 
					where userId=:userId and templeId=:templeId";//, $id, $templeId);
				$db -> prepare($sql);
				$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> bindValue(':templeId', $templeId, PDO::PARAM_INT);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				if (!$row ) {
					$sql = "INSERT INTO frdTemple 
						(userId, templeId, templeLevel, clearLevel, 
						 upState,lastRecvTime, startLvUpTime, update_date, reg_date)
						values (:userId, :templeId, 1, 1, 
								0, now(), now(), now(), now())";
					$db -> prepare($sql);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> bindValue(':templeId', $templeId, PDO::PARAM_INT);
					$row = $db -> execute();
					if (!isset($row) || is_null($row) || $row == 0) {
						$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
						return $resultFail;
					}

					$GetAbillityManager = new GetAbillityManager();
					$abil_result = $GetAbillityManager->getAbillity( $db, $userId, 0, $templeId, $data );

					$data['bundle0'] = $abil_result["bundle0"];
					$data['bundle1'] = $abil_result["bundle1"];


					$data["templeClearLevel"] = 1;
					$sql = "insert into frdGoddess 
						(userId, goddessId, level, exp, reg_date)	
						values (:userId, :templeId, 1, 0, now())"; 
						$db -> prepare($sql);
					$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
					$db -> bindValue(':templeId', $templeId, PDO::PARAM_INT);
					$row = $db -> execute();
					if (!isset($row) || is_null($row) || $row == 0) {
						$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
						return $resultFail;
					}
				} else {
					if ( $diffLevel >= $row["clearLevel"] ) {
						$sql = "UPDATE frdTemple SET
							 clearLevel= ($diffLevel+1) where userId= :userId and templeId= :templeId";
						$db -> prepare($sql);
						$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
						$db -> bindValue(':templeId', $templeId, PDO::PARAM_INT);
						$row = $db -> execute();
						if (!isset($row) || is_null($row) || $row == 0) {
							$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql :'.$sql);
							return $resultFail;
						}
						$data["templeClearLevel"] = $diffLevel+1;
					} else {
						$sql = null;
					}
				}
			}
		} else {    // 요일 강림 히든 심연 스테이지
			if ( $isClear > 0 ) {
				if ( $stageId > 1000 && $stageId < 2000 ) {   // abyss
				}
			}
		}

		if ( $difficulty < 0) {
			$AssetResult = $AssetManager->useHeart($db, $userId, $needHeart);
			if($AssetResult['ResultCode'] != 100 ){
				$resultFail['ResultCode'] = $AssetResult['ResultCode'];
				$this->logger->logError(__FUNCTION__.' : FAIL need heart userId :'.$userId);
				return $resultFail;
			}

			$heart = null;
			$heart['curHeart'] = $AssetResult["curHeart"];
			$heart['addHeart'] = 0 - $needHeart;
			$heart['passTime'] = $AssetResult["passTime"];
			$heart['maxHeart'] = $AssetResult["maxHeart"];
			$use['heart'] = $heart;
		}
		else {
			if(!$isClear) {
				
			}
		}

		// ----------------- reward set start ----------------------
		if ( empty($result) )
			$result = array();
		
		$GetRewardManager = new GetRewardManager();
		$data['rewards'] = $GetRewardManager->GetReward(
			$result, $db, $userId, $userLevel, $stageId, $lastWave, $isClear, $seconds, $curRewardIds, $difficulty, false, $firstReward, null, null
		);

		
		// 업적 체크 
		$this->achieveStageCheck($db, $userId, $stageId, $isClear);

		// 웹뷰 이벤트 체크 
		// if ($isClear > 0 ) {
		// 	$EventManager = new EventManager();
		// 	$EventManager->getEventWebviewStageReward($db, $userId);
		// }

		// 일일 퀘스트 체크 
		// db, userId , questIdx
		if ( $stageId < 500 ) { // 영광의 승리	0	스테이지 클리어	1	티켓	3
			$DailyQuestManager = new DailyQuestManager();
			$DailyQuestManager->updateDayMission($db, $userId, 0, 1);
		} else if ( ( $stageId >= 10000 ) && ( $stageId <= 15000 ) ) {   //  이계의 영웅	1	강림 던전 클리어	1	골드	1500
			$DailyQuestManager = new DailyQuestManager();
			$DailyQuestManager->updateDayMission($db, $userId, 1, 1);
		}

		if ( $EventManager->IsActiveEvent(EVENT_HOTTIME) ) {
			$LobbyEventManager = new LobbyEventManager();
			$hottimeEventResult = $LobbyEventManager->getHottimeBonus();
			if ($hottimeEventResult['eventCheck']) {
				$tData = array();
				$tData['str'] = $hottimeEventResult['str'];
				$tData['obj'] = $hottimeEventResult['obj'];
				$data['eventprefab'][] = $tData;
			}
		}

		
	
		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResGetStageReward';
		
		if (isset($use) ) {
			$data['use'] = $use;
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendStage($userId, $stageId, 2);

		$result['Data'] = $data;
		return $result;
	}
	

	function achieveStageCheck($db, $userId, $stageId, $isClear ) {
		$resultFail['ResultCode'] = 300;

		if ($isClear > 0 ) {
			$isPass = false;
			if ( $stageId < 210 ) { //normal and hidden
				$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo 
					where userId=:userId and achieveId > 2100 and achieveId < 2200";
			} else if ( $stageId > 2000 && $stageId < 4000 ) {    // 신전 스테이지
				$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo 
					where userId=:userId and achieveId > 2200 and achieveId < 2300";
			} else if ( $stageId >= 1000 && $stageId < 2000 ) {    // 심연  스테이지
				$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo 
					where userId=:userId and achieveId > 2300 and achieveId < 2400";
			} else if ( $stageId >= 10000 && $stageId < 20000 ) {    // 강림  스테이지
				$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo 
					where userId=:userId and achieveId > 2500 and achieveId < 2600";
			} else if ( $stageId >= 500 && $stageId < 600 ) {    // 요일 스테이지
				$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo 
					where userId=:userId and achieveId > 2600 and achieveId < 2700";
			} else if ( $stageId >= 600 && $stageId < 1000 ) {    // 고탑스테이지
				$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo 
					where userId=:userId and achieveId > 2400 and achieveId < 2500";
			} else {
				// error
				$isPass = true;
			}
			if (!$isPass){
				$db->prepare($sql);
				$db->bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				if (!$row) {
//					$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
					return $resultFail;
				}	

				$achieveId = $row['achieveId'];
				if ($row['cnt'] != -1 ) {
					require_once './classes/AchieveManager.php';
					$AchieveManager = new AchieveManager();
					$AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
				} 
			}

			if ( ( ($stageId == 70 ) || ($stageId == 100) ) ) {
				if ($stageId == 70) {
					$achieveId = 1301;
					$cnt = 70;
				} else {
					$achieveId = 1302;
					$cnt = 100;
				}

				$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo 
					where userId=:userId and achieveId = :achieveId";
				$db->prepare($sql);
				$db->bindValue(':userId', $userId, PDO::PARAM_INT);
				$db->bindValue(':achieveId', $achieveId, PDO::PARAM_INT);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				if ($row) {
					if ($row['cnt'] != -1 ) {
						require_once './classes/AchieveManager.php';
						$AchieveManager = new AchieveManager();
						$AchieveManager->addAchieveData($db, $userId, $achieveId, $cnt);
					} 
				}
			}
		} else { // 고난의 탑은 클리어와 상관 없음 
			if ( $stageId >= 600 && $stageId < 1000 ) {    // 고탑스테이지
				$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo 
					where userId=:userId and  achieveId > 2400 and achieveId < 2500";
				$db->prepare($sql);
				$db->bindValue(':userId', $userId, PDO::PARAM_INT);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				if ($row) {
					$achieveId = $row['achieveId'];
					if ($row['cnt'] != -1 ) {
						require_once './classes/AchieveManager.php';
						$AchieveManager = new AchieveManager();
						$AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
					} 
				}	
			}
		}

		$result['ResultCode'] = 100;

		return $result;
	}

	
    function GetGoddessAttribute($id) {
        switch($id) {
            case 1:return 4;
            case 2:return 2;
            case 3:return 3;
            case 4:return 1;
            case 5:return 0;
            case 6:return 2;
            case 7:return 1;
            case 8:return 2;
            case 9:return 3;
            case 10:return 4;
            default:return 0;
        }
    }

}
?>
