<?php
require_once './common/DB.php';
require_once './common/define.php';
require_once './lib/Logger.php';
require_once './send_data/SendDataManager.php';

class TitleEventManager {

	public function __construct() {
		$this -> logger = Logger::get();
	}


	//UserInfoManager - 접속 횟수에 따른 보상
	function getChkEnterReward(&$db, $userId) {
        $SendDataManager = new SendDataManager();

        $event_Key = EVENT_CHK_ENTERCOUNT;
        $apc_key =  "Event_".$GLOBALS['COUNTRY'].'_'.$event_Key;
        $eventType = apc_fetch($apc_key);
        if (!$eventType)
            return false;

        $event_now = date('ymdH');
        $eventResult = $SendDataManager->getEventData($event_Key+1, $eventType[1],$event_now);
        if (!$eventResult)
            return false;


        $sql = "SELECT totalChk FROM Event_ChkAttendance WHERE userId=:userId ";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {

			$sql = "INSERT INTO Event_ChkAttendance ( userId, startYMD, lastYMD, totalChk )
											VALUES (:userId, :startYMD, :lastYMD, 0)";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':startYMD', (int)date('ymd'), PDO::PARAM_INT);
			$db -> bindValue(':lastYMD', (int)date('ymd'), PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql: '.$sql);
				return;
			}

			$lastYMD = 0;
			$totalChk = 0;
		}
		else {
			$lastYMD = (int)$row['lastYMD'];
			$totalChk = (int)$row['totalChk'];
		}

		$lastYMD = (int)date('ymd');
		$totalChk++;
		$sql = "UPDATE Event_ChkAttendance SET lastYMD = :lastYMD, totalChk = :totalChk WHERE userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':lastYMD', $lastYMD, PDO::PARAM_INT);
		$db -> bindValue(':totalChk', $totalChk, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' :FAIL userId :'.$userId.' sql : '.$sql);
			return;
		}

		if ( $totalChk == 30 ) {
			$rewardType = array();
			$rewardCount = array();

			$rewardType[] = 135; $rewardCount[] = 5;
			$rewardType[] = 136; $rewardCount[] = 5;
			$rewardType[] = 137; $rewardCount[] = 5;

			$sql = "INSERT INTO frdUserPost (recvUserId, sendUserId, type, count, expire_time, reg_date) values ";
			for ( $ii=0; $ii<count($rewardType); $ii++ ) {
				$rt = $rewardType[$ii];
				$rc = $rewardCount[$ii];
				if ( $ii != 0 )
					$sql .= ", ";
				$sql .= "($userId, 5, $rt, $rc, DATE_ADD(now(), INTERVAL 7 DAY ), now()) ";
			}
			$db -> prepare($sql);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
				return $resultFail;
			}
		}
		
	}

}

/*
@PostTitle0,운영자님으로부터 받은 선물
@PostTitle1,일일퀘스트 보상
@PostTitle2,출석 보상
@PostTitle3,고난의 탑 보상
@PostTitle4,쿠폰 보상
@PostTitle5,이벤트 보상
@PostTitle6,업적 보상
@PostTitle10,복귀 수호자님을 위한 선물,
@PostTitle200,스타터 패키지,
@PostTitle201,허니잼 패키지,
@PostTitle202,히랜디 패키지,
@PostTitle203,어빌리티 패키지,
@PostTitle204,영혼 각인석 패키지1,
@PostTitle205,영혼 각인석 패키지2,
@PostTitle206,엑스칼리버 패키지2,
@PostTitle207,일로케이터 패키지2,
@PostTitle208,갓핸드 패키지2,
@PostTitle209,일곱장막전승자 패키지,
@PostTitle210,이벤트 어빌리티 패키지,
@PostTitle211,메달 패키지,
@PostTitle212,메달 묶음 패키지,
@PostTitle213,영혼각인석 패키지,
@PostTitle214,영혼각인석 묶음 패키지,
@PostTitle215,설날 패키지 1,
@PostTitle216,설날 패키지 2,
@PostTitle217,설날 패키지 3,
@PostTitle218,설날 패키지 4,
*/
?>
