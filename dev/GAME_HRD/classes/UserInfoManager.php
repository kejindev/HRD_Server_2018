<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
include_once './classes/AccountManager.php';
include_once './classes/ClimbManager.php';
include_once './classes/AssetManager.php';
include_once './classes/SessManager.php';
require_once './classes/TitleEventManager.php';
class UserInfoManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

    public function GetUserInfo($param) {

        $resultFail['Protocol'] = 'ResUserInfo';
        $resultFail['ResultCode'] = 300;
		
		if (isset($param['hashcodeStr'])) {
			$hashcodeStr	= $param['hashcodeStr'];
		} else {	
			$hashcodeStr	= null;
		}

        $userId 	= $param['userId'];
        $Type 		= $param['Type'];

		if ( ( $Type == "PS" ) || ($Type == "OS") ) {  //google, onestore
			if ( $hashcodeStr != "1C85B667" ) {
				$resultFail['ResultCode'] = 300;
				$this->logger->logError(__FUNCTION__.', FAIL HASHCODE  userId : '.$userId);
				$this->logger->logError(__FUNCTION__.', FAIL HASHCODE  : '.$hashcodeStr);
				return $resultFail;
			}
		}

        $userId 	= $param['userId'];
        $name 		= $param['userId'];

        $result = null;
        $result['Protocol'] = "ResUserInfo";
        $result['ResultCode'] = 100;

		/* 
		* ## User Info Check 
		* check userId isAvailable 
		* exisit Block List
		*/
		$Data = null;
		if ( $param['Test'] == "Test" ) {
			$accDBNo = substr($userId, 0,3);
			$bigServer = (int)floor($accDBNo/100);
			if ( $bigServer == 2 || $bigServer == 7 )
				$GLOBALS['DB_HOST'] = "172.27.100.3";
			else //5,6,8
				$GLOBALS['DB_HOST'] = "172.27.100.4";

			if ( $bigServer == 6 ) $bigServer = 5;
			$smallServer = (int)($accDBNo%100);
			$dbName = "hrd_db_game_".$bigServer.$smallServer;
			$GLOBALS['DB_NAME'] = $dbName;
		}
		$db = new DB();
		// CHECK AND GET USER INFO
		// 마지막 인자인 name 이 공란 일수 없다 
		// 유니크 키인 유저아이디를 넣고 후에 업데이트 한다. 
		$UserDataResult = $this -> check_get_user_info($db, $userId,$userId);
		if ($UserDataResult['ResultCode'] != 100) {
			$resultFail['ResultCode'] = $UserDataResult['ResultCode'];
            return $resultFail;
        }
		$Data = $UserDataResult['data'];
		// GET USER CHARACTER INFO
        $Charac_Data = $this -> getCharacEvolveInfo($db, $userId);
        if ($Charac_Data == Null) {
			$resultFail['ResultCode'] = 300;
			$this->logger->logError(__FUNCTION__.' FAIL charInfo  userId : ' . $userId );
            return $resultFail;
        }
		$Data['characs'] = $Charac_Data;
		// GET USER SKILL INFO
        $SkillData = $this -> getfrdSkillLevels($db, $userId);
		$Data['skillLevels'] = $SkillData;

		// GET USER Magics INFO
        $Arti_Data = $this -> getHavingMagics($db, $userId);
		$Data['magics'] = $Arti_Data;

        $equipMagic = $this -> getEquipMagics($db, $userId);
		$Data['magicsEquipData'] = $equipMagic;

		// GET USER frdHavingWeapons INFO
        $Weapon_Data = $this -> getHavingWeapons($db, $userId);
		$Data['weapons'] = $Weapon_Data;

		// GET USER frdHavingWeapons INFO
        $Weapon_EQData = $this -> getEquipWeapons($db, $userId);
		$Data['weaponsEquipData'] = $Weapon_EQData;


		// GET USER ability INFO
        $Abillity_Data = $this -> getAbillity($db, $userId);
		$Data['abillity'] = $Abillity_Data;

		// GET USER item INFO
        $item_Data = $this -> getHavingItems($db, $userId);
		$Data['items'] = $item_Data['items'];



		$climeTopManager =  new ClimbManager();
	 	$clime_temp = $climeTopManager -> GetClimeTopInfo($db, $userId, $Data['name']);

		$myRank = null;
		if ( $clime_temp['myRank'] > 0 )  {
			$myRank['rank'] = $clime_temp['myRank'];
			$myRank['totalUser'] = $clime_temp['userCount'];
			$Data['myRank'] = $myRank;
		}
		else {
			$Data['totalUser'] = $clime_temp['userCount'];
		}
		$clime_data = $clime_temp['climbAccuData'];
		$climbAccuData = null;	
		if(isset( $clime_data['bestRank']) ) {
			$climbAccuData['bestRank'] = $clime_data['bestRank'];
		} else {
			$climbAccuData['bestRank'] = 0;
		}

		if(isset( $clime_data['bestTotalUserCount']) ) {
			$climbAccuData['bestTotalUserCount'] = $clime_data['bestTotalUserCount'];
		} else {
			$climbAccuData['bestTotalUserCount'] = 0;
		}

		if(isset( $clime_data['playCount']) ) {
			$climbAccuData['playCount'] = $clime_data['playCount'];
		} else {
			$climbAccuData['playCount'] = 0;
		}

		if(isset( $clime_data['averageRank']) ) {
			$climbAccuData['averageRank'] = $clime_data['averageRank'];
		} else {
			$climbAccuData['averageRank'] = 0;
		}

		if(isset( $clime_data['avrgTotalUserCount']) ) {
			$climbAccuData['avrgTotalUserCount'] = $clime_data['avrgTotalUserCount'];
		} else {
			$climbAccuData['avrgTotalUserCount'] = 0;
		}

		if(isset( $clime_data['lastRank']) ) {
			$climbAccuData['lastRank'] = $clime_data['lastRank'];
		} else {
			$climbAccuData['lastRank'] = 0;
		}

		if(isset( $clime_data['lastTotalUserCount']) ) {
			$climbAccuData['lastTotalUserCount'] = $clime_data['lastTotalUserCount'];
		} else {
			$climbAccuData['lastTotalUserCount'] = 0;
		}

		$Data['climbAccuData'] = $climbAccuData;

		// TEMPLE DATA NO SET
		$sql = "select dailyCount
			 from frdTempleClearCount where userId=:userId";
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$Data["templeDCC"] = 0;
		} else {
			$Data["templeDCC"] = $row["dailyCount"];
		}

		$SessManager = new SessManager();
		$Sess = $SessManager->set_make_sessionkey($db, $userId);
		if ($Sess == null) {
			$this->logger->logError(__FUNCTION__.': UserInfo : Sess Make Fail userId : '.$userId);
		} else {
			$Data['Sess'] = $Sess;
		}

		// 이벤트 처리 매니저
		$TitleEventManager = new TitleEventManager();
		$TitleEventManager->getChkEnterReward($db, $userId);

		$result['Data'] = $Data; 


        return $result;
	
	}

	public function getUserBaseInfo($db, $userId, $name) {
		// CHECK AND GET USER INFO
		$UserDataResult = $this -> check_get_user_info($db, $userId, $name);
		if ($UserDataResult['ResultCode'] != 100) {
			if ($UserDataResult['ResultCode'] == 101) { 
				$result['ResultCode'] = 100; 
				return $UserDataResult;
			}

			$resultFail['ResultCode'] = $UserDataResult['ResultCode'];
            return $resultFail;
        }

		$result['ResultCode'] = 100; 
		$result['data'] = $UserDataResult['data'];
        return $result;
    }

	public function check_get_user_info($db, $userId, $name) {
		$resultFail['ResultCode'] = 300;

        $sql = <<<SQL
			SELECT   userId,
					 name,
					 blockState,
					 tutoLevel,
					 userLevel,
					 manaLevel,
					 exp ,
					 gold ,
					 jewel ,
					 powder ,
					 stageLevel ,
					 skillPoint ,
					 chapterReward ,
					 accuSkillPoint,
					 ticket,
					 heart ,
					 maxHeart ,
					 session ,
					 lastLoginDate ,
					 heartStartTime ,
					UNIX_TIMESTAMP(NOW()) as nowTime
		 FROM frdUserData WHERE userId =:userId		
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
			// CREATE NEW USER 
            $this -> logger -> logError(__FUNCTION__.',NEW USER COME IN userId : ' . $userId );
			$AccountManager = new AccountManager();
			$AccountResult = $AccountManager->create_account($db, $userId, $name);
			if ($AccountResult['ResultCode'] !=100) {
				$this -> logger -> logError(__FUNCTION__.' USER FAIL userId : ' . $userId);
				return $resultFail;
			} 
			
			$UserDataResult = $this->getUserBaseInfo($db, $userId, $name);
			return $UserDataResult;
        } 

		if ($row['blockState'] != 0) {
			switch ($row['blockState']) {
				case 1: 
					$result['ResultCode'] = 301;
					return $result;
				case 2: 
					$result['ResultCode'] = 302;
					return $result;
				default :
					break;
			}
		}
		$AssetManager = new AssetManager();
		$myHeart = $AssetManager->checkHeart($db, $userId);
		if ($myHeart['ResultCode'] != 100) {
			return $resultFail;
		}

        $curLevel = $row['userLevel'];
        $nextLevel = $curLevel;

        $myKey = $row['userLevel'];
        $lv_apc_key = 'UserExp_MaxHeart_'.$myKey;
        $lv_data = apc_fetch($lv_apc_key);
        $curMaxExp = $lv_data[1];
        $curMaxHeart = $lv_data[2];

	
		$data["userId"]	 			= $row["userId"];
		$data["name"] 				= $row["name"];
		$data["tutoLevel"] 			= $row["tutoLevel"];
		$data["manaLevel"] 			= $row["manaLevel"];
		$data["curLevel"] 			= $curLevel;
		$data["curExp"] 			= $row["exp"];
		$data["maxExp"] 			= $curMaxExp;
		$data["gold"] 				= $row["gold"];
		$data["jewel"] 				= $row["jewel"];
		$data["powder"] 			= $row["powder"];
		$data["stageLevel"] 		= $row["stageLevel"];
		$data["chapterReward"] 		= $row["chapterReward"];
		$data["skillPoint"] 		= $row["skillPoint"];
		$data["ticket"] 			= $row["ticket"];
		$data["time"]	 			= $row["nowTime"];

		$data["lastLoginDate"] 		= $row["lastLoginDate"];

		$heart["passTime"]			= $myHeart["passTime"];
		$heart["curHeart"]			= $myHeart['heart'];
		$heart["maxHeart"] 			= $myHeart['maxHeart'];
		$data["passTime"]			= $myHeart["passTime"];
		$data["curHeart"]			= $myHeart['heart'];
		$data["maxHeart"] 			= $myHeart['maxHeart'];

		$data['heart'] = $heart;
		$result['ResultCode'] = 100;
		$result['data'] = $data;


		$sql = "UPDATE frdUserData SET lastLoginDate = now() where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
            $this->logger->logError(__FUNCTION__.' : fail userId : ' . $userId . ', sql : ' . $sql);
			return $resultFail;
		}


		return $result;
	}

	public function getCharacEvolveInfo($db, $userId) {

		$data = null;
		$char = null;

        $sql = " 
			SELECT charTableId, charId, exp FROM frdCharInfo WHERE userId = :userId ";
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
		$temp 	= null;
        while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$temp[] = $row;
        }	
		return $temp;
	}

	public function getfrdSkillLevels($db, $userId) {

		$data = null;

        $sql = <<<SQL
    SELECT skillTableId, skillId, exp FROM frdSkillLevels WHERE userId = :userId 
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            $data[] = $row;
        }

		return $data;
	}

	public function getHavingMagics($db, $userId) {

		$data = null;
		$char = null;

        $sql = <<<SQL
    SELECT magicTableId,magicId, level,exp, equipIdx 
		FROM frdHavingMagics WHERE userId = :userId 
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            $char["magicTableId"] 	= $row["magicTableId"];
            $char["magicId"] 		= $row["magicId"];
            $char["level"] 		= $row["level"];
            $char["exp"] 		= $row["exp"];
            $data[] 	= $char;
        }

		return $data;
	}

	function getEquipMagics($db, $userId) {

		$data = null;

        $sql = <<<SQL
		SELECT * FROM frdEquipMagics WHERE userId = :userId 
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$data['groupIdx'] = (int)$row['slotNo'];				
			$data['equipIdx'][] = (int)$row['eq0'];
			$data['equipIdx'][] = (int)$row['eq1'];
			$data['equipIdx'][] = (int)$row['eq2'];
		}
		
		return $data;
	}


	public function getHavingWeapons($db, $userId) {

		$data = null;
		$char = null;

        $sql = <<<SQL
		SELECT * FROM frdHavingWeapons WHERE userId = :userId 
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$statIds = null;
			$statVals = null;

			$char["weaponTableId"]		= $row["weaponTableId"];
            $char["weaponId"] 	= $row["weaponId"];
            $char["level"] 		= $row["level"];
            $char["exp"] 		= $row["exp"];

			if (!is_null($row["statId0"])) {
				$statIds[] 	= $row["statId0"];
				$statVals[]	= $row["statVal0"];
			}
			if (!is_null($row["statId1"])) {
				$statIds[] 	= $row["statId1"];
				$statVals[]	= $row["statVal1"];
			}
			if (!is_null($row["statId2"])) {
				$statIds[] 	= $row["statId2"];
				$statVals[]	= $row["statVal2"];
			}
			if (!is_null($row["statId3"])) {
				$statIds[] 	= $row["statId3"];
				$statVals[]	= $row["statVal3"];
			}
			if (!is_null($row["statId4"])) {
				$statIds[] 	= $row["statId4"];
				$statVals[]	= $row["statVal4"];
			}

			if ( ((int)($row["transcend"])) !== 0  )
				$char['transcend'] = $row["transcend"];
			if ( ((int)($row["transcendExp"])) !== 0  )
				$char['transcendExp'] = $row["transcendExp"];

			$char['statIds'] 	= $statIds;
			$char['statVals'] 	= $statVals;
            $data[] 	= $char;
            
            $char = null;
        }
		return $data;
	}

	function getEquipWeapons($db, $userId) {

		$data = null;

        $sql = <<<SQL
		SELECT * FROM frdEquipWeapons WHERE userId = :userId 
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$data['groupIdx'] = (int)$row['slotNo'];				
			$data['equipIdx'][] = (int)$row['eq0'];
			$data['equipIdx'][] = (int)$row['eq1'];
			$data['equipIdx'][] = (int)$row['eq2'];
			$data['equipIdx'][] = (int)$row['eq3'];
			$data['equipIdx'][] = (int)$row['eq4'];
			$data['equipIdx'][] = (int)$row['eq5'];
			$data['equipIdx'][] = (int)$row['eq6'];
			$data['equipIdx'][] = (int)$row['eq7'];
			$data['equipIdx'][] = (int)$row['eq8'];
			$data['equipIdx'][] = (int)$row['eq9'];
			$data['equipIdx'][]  = (int)$row['eq10'];
			$data['equipIdx'][]  = (int)$row['eq11'];
			$data['equipIdx'][]  = (int)$row['eq12'];
			$data['equipIdx'][]  = (int)$row['eq13'];
			$data['equipIdx'][]  = (int)$row['eq14'];
			$data['equipIdx'][]  = (int)$row['eq15'];
			$data['equipIdx'][]  = (int)$row['eq16'];
			$data['equipIdx'][]  = (int)$row['eq17'];
			$data['equipIdx'][]  = (int)$row['eq18'];
			$data['equipIdx'][]  = (int)$row['eq19'];
			$data['equipIdx'][]  = (int)$row['eq20'];
			$data['equipIdx'][]  = (int)$row['eq21'];
			$data['equipIdx'][]  = (int)$row['eq22'];
			$data['equipIdx'][]  = (int)$row['eq23'];
		}
		

		return $data;
	}

	public function getHavingItems($db, $userId) {

		$data = null;
		$char = null;

        $sql = <<<SQL
    SELECT * FROM frdHavingItems WHERE userId = :userId 
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
            $char = array();
            $char["itemTableId"] 	= $row["itemTableId"];
            $char["itemId"] 	= $row["itemId"];
            $char["cur"] 	= $row["itemCount"];
            $data["items"][] 	= $char;
        }

		return $data;
	}

	public function getAbillity($db, $userId) {
		$resultFail['ResultCode'] = 300;

		$data = null;
		$char = null;

        $sql = <<<SQL
    SELECT * FROM frdAbillity WHERE userId = :userId 
SQL;
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
			$char["openedSlot"] 	= 0;
			$char["bundle0"] 		= 0;
			$char["bundle1"] 		= 0;
			$char["usingBundle0"] 	= 0;
			$char["usingBundle1"] 	= 0;
        } else {
			$char["openedSlot"] 	= $row["openedSlot"];
			$char["bundle0"] 		= $row["bundle0"];
			$char["bundle1"] 		= $row["bundle1"];
			$char["usingBundle0"] 	= $row["usingBundle0"];
			$char["usingBundle1"] 	= $row["usingBundle1"];
		}
		$data[] 	= $char;

		return $data;
	}

	public function getFixData($param) {
		$data = null;
		// not check user Info
		// 기획 데이터 변경 후 조정이 핊요.

		// DailyReward
		$dType = null;
		$dAmount = null;
        $dk1 = trim("DailyReward_1");
        $dk1_data = apc_fetch($dk1);
        $dType[] = $dk1[1];
        $dAmount[] = $dk1[2];

        $dk2 = trim("DailyReward_2");
        $dk2_data = apc_fetch($dk2);
        $dType[] = $dk2[1];
        $dAmount[] = $dk2[2];

        $dk3 = trim("DailyReward_3");
        $dk3_data = apc_fetch($dk3);
        $dType[] = $dk3[1];
        $dAmount[] = $dk3[2];

        $dk4 = trim("DailyReward_4");
        $dk4_data = apc_fetch($dk4);
        $dType[] = $dk4[1];
        $dAmount[] = $dk4[2];

        $dk5 = trim("DailyReward_5");
        $dk5_data = apc_fetch($dk5);
        $dType[] = $dk5[1];
        $dAmount[] = $dk5[2];

        $dk6 = trim("DailyReward_6");
        $dk6_data = apc_fetch($dk6);
        $dType[] = $dk6[1];
        $dAmount[] = $dk6[2];

        $dk7 = trim("DailyReward_7");
        $dk7_data = apc_fetch($dk7);
        $dType[] = $dk7[1];
        $dAmount[] = $dk7[2];

		$data["dailyRewardType"] 	= $dType;
		$data["dailyRewardAmount"] 	= $dAmount;

		$tHidden = null;
		$tMin = null;
		$tMax = null;
		$tCharg = null;

		for($i = 0; $i < 18; $i++ ) {
			$CI = trim("CharacIndex_".$i);
			$apc_data = apc_fetch($CI);
			$tMin[] = $CI[0];
			$tMax[] = $CI[1];
			$tHidden[] = $CI[3];
			$tCharg[] = $CI[4];
		}

		// check APC DATA
		/*
		$data["characHiddenStartIdx"] = $redis->lrange('characHiddenStartIdx', 0, -1);
		$data["characMinIdx"] = $redis->lrange('characMinIdx', 0, -1);
		$data["characMaxIdx"] = $redis->lrange('characMaxIdx', 0, -1);
		$data["chargingCharacs"] = $redis->lrange('chargingCharacs', 0, -1);
		*/
	
		$data["isSaleCharac"] = $redis->get('isSaleCharac');
		$data["characPrices"] = json_decode( $redis->get('characPrices') );
		$result = null;	
		$result['data'] = $data;		
		return $result['Data'];
	}

	public function getMyClearData($param) {
		$resultFail['ResultCode'] = 300;
		$userId = $param['userId'];

		$data["stageId"] = array();
		$data["count"] = array();

		$sql = "select stageId, clearCount from frdClearCount where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
		$resultList = null;
        while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$data["stageId"][] = $row["stageId"];
			$data["count"][] = $row["clearCount"];
        }

		$sql = "SELECT accuSkillPoint FROM frdUserData where userId = :userId";
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this->logger->logError('getMyClearData : fail userId : ' . $userId . ', sql : ' . $sql);
			return $resultFail;
		}
	
		$data["accuSkillPoint"] = $row["accuSkillPoint"];
		$result['ResultCode'] = 100;	
		$result['Data'] = $data;	

		return $data;
	}

	public function SuccessEffectPreCheck($param) {
		$resultFail['ResultCode'] = 300;
		$type = $param['type'];
		$userId = $param['userId'];
		$val = $param['val'];

		$db = new DB();

		$sql = "select isSuccess from frdEffectForPreCheckRewards where userId=:userId and type=:type";
        $db -> prepare($sql);
        $db -> bindValue(':type', $type, PDO::PARAM_INT);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this->logger->logError(__FUNCTION__.' : fail userId : ' . $userId . ', sql : ' . $sql);
            return $resultFail;
        }

		if ( is_null($val) ) {
			 $sql = "update frdEffectForPreCheckRewards set isSuccess=1 where userId=:userId and type=:type";
			 $db -> prepare($sql);
			 $db -> bindValue(':type', $type, PDO::PARAM_INT);
			 $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
  		} else{
			$sql = "update frdEffectForPreCheckRewards set val=val+:val, isSuccess=1 
				where userId=:userId and type=:type";
			$db -> prepare($sql);
			$db -> bindValue(':val', $val, PDO::PARAM_INT);
			$db -> bindValue(':type', $type, PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		}	
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
            $this->logger->logError(__FUNCTION__.' : fail userId : ' . $userId . ', sql : ' . $sql);
			return $resultFail;
		}

		$result['Protocol'] = 'ResSuccessEffectPreCheck';
		$result['ResultCode'] = 100;
		$result['Data'] = "";
		return $result;
	}


}
?>
