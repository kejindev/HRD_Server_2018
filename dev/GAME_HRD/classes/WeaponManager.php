<?php
include_once './common/DB.php';
include_once './common/define.php';
require_once './lib/Logger.php';
require_once './classes/UserManager.php';
require_once './classes/AssetManager.php';
require_once './classes/AddManager.php';
require_once './classes/AchieveManager.php';
require_once './classes/DailyQuestManager.php';
require_once './classes/LogDBSendManager.php';
require_once './classes/ConsumeManager.php';

class WeaponManager {
	public function __construct() {
		$this -> logger = Logger::get();
	}

	function WeaponExtraction ($param) {
		$resultFail['ResultCode'] = 300;
		$userId = $param["userId"];
		$objectIdx = $param["objectIdx"];

		$db = new DB();
		$sql = "SELECT * FROM frdHavingWeapons 
			where weaponTableId=:objectIdx and userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':objectIdx', $objectIdx, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.' sql :'.$sql);
			$resultFail['ResultCode'] = 300;
			return $resultFail;
		}

		$wt = $row;

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendWeapon($userId, $wt['weaponId'], 2, 1, 0, $wt['statId0'], $wt['statId1'], $wt['statId2'], $wt['statId3'], $wt['statVal0'], $wt['statVal1'], $wt['statVal2'], $wt['statVal3']);
		

		$weaponSellValue = 60;
		$weaponExpValue = 60;
		$weaponPowderValue = 50;

		$weaponId = $row['weaponId'];
		$grade = $this->GetWeaponGrade($weaponId);
		$apc_key = 'WeaponInform_Weaponpowder';
		$apc_data = apc_fetch($apc_key);
		$temp = explode('/', $apc_data[1]);
		$amountValue = $temp[$grade];

		$apc_key = 'WeaponInform_WeaponValue';
		$apc_data = apc_fetch($apc_key);
		$temp = explode('/', $apc_data[1]);
		$amountPercent = $temp[$grade];

		$amountPercent = $weaponPowderValue;

		$define_value = ABILL_Bonus_Extraction;
		$sql = "SELECT val from frdEffectForEtc where userId = :userId and type=:type";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':type', $define_value, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ( $row ) {
			$percent = $row["val"] / 100000;
			$amountValue += round($amountValue * $percent);
		}

		$min = $amountValue - ceil($amountValue*$amountPercent/100);
		$max = $amountValue + (int)($amountValue*$amountPercent/100);

		$resultAmount = rand($min, $max);    
		/*
		$this -> logger -> logError(__FUNCTION__.':  userId : '.$userId.' ap :'.$amountPercent);
		$this -> logger -> logError(__FUNCTION__.':  userId : '.$userId.' min : '.$min);
		$this -> logger -> logError(__FUNCTION__.':  userId : '.$userId.' max : '.$max);
		$this -> logger -> logError(__FUNCTION__.':  userId : '.$userId.' rm : '.$resultAmount);
		*/

		$AssetManager = new AssetManager();
		$AssetResult = $AssetManager->addPowder($db, $userId, $resultAmount);
		if($AssetResult['ResultCode'] != 100) {
			$this -> logger -> logError(__FUNCTION__.': FAIL userId : '.$userId);
			return $resultFail;
		}
		$powder["curPowder"] = $AssetResult['powder'];
		$powder["addPowder"] = $resultAmount;

		$sql = "DELETE FROM frdHavingWeapons 
			WHERE weaponTableId=:weaponTableId and userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> bindValue(':weaponTableId', $objectIdx, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
			return $resultFail;
		}

		$result['Protocol'] =  'ResWeaponExtraction';
		$result['ResultCode'] = 100;

		$data['use']['weapons']['delTableIds'][] = $objectIdx;
		$data['rewards']['powder'] = $powder;

		// 일일 퀘스트 	
		$DailyQuestManager = new DailyQuestManager();
		$DailyQuestManager->updateDayMission($db, $userId, 3, 1);

		$result['Data'] = $data;
		return $result;	
	}

	function GradeUPWeapon ($param){
		// 유물몬  진화!!!!!!!!!!!!!!!!!
		// 메탈 그레이몬!
		$resultFail['ResultCode'] = 300;
		$userId = $param["userId"];
		$resultId = $param["resultId"];

		$db = new DB();

		// 장착 상태에서는 강화를 못하게 막을 것이다!
		// 장착 정보는 weapon 테이블이 보유 한다.
		$sql = "SELECT * FROM frdHavingWeapons 
			where weaponTableId=:objectIdx and userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':objectIdx', $param['matPId0'], PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.' sql :'.$sql);
			$resultFail['ResultCode'] = 300;
			return $resultFail;
		}

		$weaponTableId = $row['weaponTableId'];
		$grade = $this->GetWeaponGrade((int)$row["weaponId"]);
		$apc_key = "WeaponInform_WeaponMexLevel";
		$apc_data = apc_fetch($apc_key);
		$needLevel = explode('/',$apc_data[1]);

		if ((int)$row["level"] < $needLevel[$grade]  ) {
			// error
			$this->logger->logError(__FUNCTION__.':error userId :'.$userId.", wid : ".$row['weaponTableId']);
			$resultFail['ResultCode'] = 308;
			return $resultFail;
		}

		$sql = "SELECT * FROM frdEquipWeapons WHERE userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$resultList = null;
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' :  userId : '.$userId.", sql : ".$sql);
			return $resultFail;
		}

		for($i = 0; $i < 23; $i++) {
			if ( @$row['eq'.$equipIdx] == $weaponTableId ) {
				$resultFail['ResultCode'] = 309;
				$this->logger->logError(__FUNCTION__.' :  userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}
		}

		$sellCount =0;
		$temp = null;
		for ( $idx = 0; $idx<4; $idx++ ) {
			$val = @$param["matPId".$idx];
			if ( $val > 0 ) {
				$idxArr[$sellCount++] = $val;
				$temp[] = $val;
			}
		}

		$sql = sprintf("select * from frdHavingWeapons where ");
		for ( $idx=0; $idx<$sellCount; $idx++ ) {
			if ( $idx < $sellCount-1 )
				$sql .= sprintf("(weaponTableId='%s' and userId='%s') or ", $idxArr[$idx], $userId);
			else
				$sql .= sprintf("(weaponTableId='%s' and userId='%s')", $idxArr[$idx], $userId);
		}
		//		$sql .= " ORDER BY privateId ASC";
		$db->prepare($sql);
		$db->execute();
		$ResultList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$ResultList[] = $row;
		}

		if (count($temp) != count($ResultList)) {
			$this->logger->logError(__FUNCTION__.' : NOT MATCH userId.'.$userId);
			$resultFail['ResultCode'] = 309;
			return $resultFail;
		}


		// 6성 유물 진화시 골드 소모
		$apc_key = 'WeaponInform_WeaponGradeupPrice';
		$apc_data = apc_fetch($apc_key);
		$apc_data = explode('/',$apc_data[1]);
		$price = $apc_data[$grade];
		if ( $price > 0 ) {
			$AssetManager = new AssetManager();
			$AssetResult = $AssetManager->useGold($db,$userId,(int)$price);
			if ($AssetResult['ResultCode'] != 100 ) {
				$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId);
				return $resultFail;
			}
			$gold = null;
			$gold['curGold'] = $AssetResult['gold'];
			$gold['addGold'] = 0 - $subGold;
			$data['use']['gold'] = $gold;
		}

		//6성 유물 진화시 무지개룬 5개 필요
		if ( $grade == 5 ) {	
			$ConsumeManager = new ConsumeManager();
			$cResult = $ConsumeManager->setConsumeResource($db, $userId, 0, 110000, 5);
			if ( $cResult["ResultCode"] != 100 ) {
				$this -> logger -> logerror(__FUNCTION__.' : FAIL use userId :' . $userId );
				return $resultFail;
			}
			$data['use']['items'][] = $cResult['items'];
		}


		$AddManager = new AddManager();
		$addResult = $AddManager->addWeapon($db, $userId, $resultId, 0);
		$weapons['weaponId'] = $resultId;
		$weapons['weaponTableId'] = $addResult['weaponTableId'];
		$weapons['statIds']    = $addResult['statIds'];
		$weapons['statVals']   = $addResult['statVals'];
		$weapons['exp'] = 0;
		$row = null;

		$logDBSendManager = new LogDBSendManager();
		foreach($ResultList as $RL) {
			$sql = "DELETE from frdHavingWeapons 
				where weaponTableId=:weaponTableId and userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':weaponTableId', $RL['weaponTableId'], PDO::PARAM_INT);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}


			$logDBSendManager->sendWeapon($userId, $RL['weaponId'], 2, 1, 0, $RL['statId0'], $RL['statId1'], $RL['statId2'], $RL['statId3'], $RL['statVal0'], $RL['statVal1'], $RL['statVal2'], $RL['statVal3']);
			
		}

		$sql = "select achieveTableId, achieveId, cnt FROM frdAchieveInfo
			where userId=:userId and achieveId > 4100 and achieveId < 4200";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$achieveId = $row['achieveId'];
			if ($row['cnt'] != -1 ) {
				$AchieveManager = new AchieveManager();
				$AchieveManager->addAchieveData($db, $userId, $achieveId, 1);
			}
		}

		// 일일 퀘스트 	
		$DailyQuestManager = new DailyQuestManager();
		$DailyQuestManager->updateDayMission($db, $userId, 5, 1);


		$data['rewards']['weapons'][] = $weapons;
		$data['use']['weapons']['delTableIds'] = $temp;

		$result['Protocol'] =  'ResGradeUPWeapon';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;

		return $result;	
	}

	function LVUpWeapon($param) {
		$resultFail['ResultCode'] = 300;

		$userId = $param["userId"];
		$deskPId = $param["deskPId"];

		$sellCount =0;
		$temp = null;
		for ( $idx = 0; $idx<8; $idx++ ) {
			$val = @(int)$param["matPId".$idx];
			if ( $val > 0 ) {
				$idxArr[$sellCount++] = (int)$val;
				$temp[] = (int)$val;

			}
		}

		$db = new DB();
		$sql = "SELECT * FROM frdHavingWeapons 
			WHERE weaponTableId=:objectIdx and userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->bindValue(':objectIdx', $deskPId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.' sql :'.$sql);
			$resultFail['ResultCode'] = 300;
			return $resultFail;
		}
		$deskLevel = $row['level'];

		$sql = sprintf("select weaponTableId, weaponId, exp,level from frdHavingWeapons where ");
		for ( $idx=0; $idx<$sellCount; $idx++ ) {
			if ( $idx < $sellCount-1 )
				$sql .= sprintf("(weaponTableId=%d and userId=%d) or ", $idxArr[$idx], $userId);
			else
				$sql .= sprintf("(weaponTableId=%d and userId=%d)", $idxArr[$idx], $userId);
		}
		//		$sql .= " ORDER BY privateId ASC";
		$db -> prepare($sql);
		$row = $db -> execute();
		$resultList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$resultList[] = $row;
		}

		if (count($temp) != count($resultList)) {
			$this->logger->logError(__FUNCTION__.' : NOT MATCH userId.'.$userId);
			$this->logger->logError(__FUNCTION__.' : NOT MATCH temp:'.count($temp).' result :'.count($resultList) );
			$resultFail['ResultCode'] = 309;
			return $resultFail;
		}

		$subGold 	= 0;
		$addExp 	= 0;
		$weaponExpValue = 60;

		//Level,WeaponPrice,WeaponNeedExp
		$apc_key = "WeaponPrice_Exp_".$deskLevel;
		$apc_data = apc_fetch($apc_key);
		$price = $apc_data[2];

		foreach($resultList as $rs) {		

			$grade = $this->GetWeaponGrade((int)$rs["weaponId"]);

			$apc_key = "WeaponInform_WeaponValue";
			$apc_data = apc_fetch($apc_key);
			$tempData = $apc_data[1];
			$tempData = explode('/',$apc_data[1]);
			$tempGold = $tempData[$grade];
			$subGold += $tempGold;

			$apc_key = "WeaponPrice_Exp_".$rs['level'];
			$apc_data = apc_fetch($apc_key);
			$price = $apc_data[1];
			$needExp = $apc_data[3];
			$sumAccuExp = $apc_data[4];
			$subGold += $price;

			$subGrade[] = $grade;
			$subLevel[] = $rs['level'];
			$subPrices[] = $price;

			$apc_key = 'WeaponInform_WeaponValue';
			$apc_data = apc_fetch($apc_key);
			$apc_data = explode('/',$apc_data[1]);
			$sum = $apc_data[$grade];
			$sum += $sumAccuExp;
			$sum = (int)round($sum * $weaponExpValue / 100);
			if ( $rs['weaponId'] >= 490 ) {
				$sum = (int)round($sum*1.5);	
			}

			$addExp += $sum;
		}


		$AssetManager = new AssetManager();
		$AssetResult = $AssetManager->useGold($db,$userId,(int)$subGold);
		if ($AssetResult['ResultCode'] != 100 ) {
			$this->logger->logError(__FUNCTION__.': FAIL userId : '.$userId);
			return $resultFail;
		}
		$gold = null;
		$gold['curGold'] = $AssetResult['gold'];
		$gold['addGold'] = 0 - $subGold;

		$data['use']['gold'] = $gold;

		$delTableIds[] = $temp;
		$data['use']['weapons']['delTableIds'] = $temp;
		$weaponIdxs =null;
		$weaponIdxs[] = $deskPId;
		$addExp = $addExp * 10;
		
		$AddManager = new AddManager();
		$expResult = $AddManager->addWeaponExp($db, $userId, $weaponIdxs, $addExp, false);

		$sql = sprintf("delete from  frdHavingWeapons where ");
		for ( $idx=0; $idx<$sellCount; $idx++ ) {
			if ( $idx < $sellCount-1 )
				$sql .= sprintf("(weaponTableId=%d and userId=%d) or ", $idxArr[$idx], $userId);
			else
				$sql .= sprintf("(weaponTableId=%d and userId=%d)", $idxArr[$idx], $userId);
		}
		$db -> prepare($sql);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
			return $resultFail;
		}

		// 일일 퀘스트 	
		$DailyQuestManager = new DailyQuestManager();
		$DailyQuestManager->updateDayMission($db, $userId, 6, 1);

		$result['Protocol'] =  'ResLVUpWeapon';
		$result['ResultCode'] = 100;
		$data['rewards']['weaponExp'] = $expResult['exp'];
		$result['Data'] = $data;

		return $result;	
	}

	function SellWeapon($param) {
		// 유물 판매 
		$resultFail['ResultCode'] = 300;
		$userId = $param["userId"];
		$isExtraction = $param["isExtraction"];
		$matPIds = $param["matPIds"];
		$matLevels = $param["matLevels"];


		$idxArr = explode(";", $matPIds);
		$levelArr = explode(";", $matLevels);
		$sellCount = count($idxArr);
		$weaponTableIds = null;
		
		for ( $idx=0; $idx<$sellCount; $idx++ ) {
			if ($weaponTableIds == null) {
				$weaponTableIds = $idxArr[$idx];
			} else {
				$weaponTableIds .= ','.$idxArr[$idx];
			}
		}

		$this->logger->logError(__FUNCTION__.': test weaponTableIds : '.$weaponTableIds);
		$db = new DB();
		$sql = "SELECT * FROM frdHavingWeapons 
			WHERE weaponTableId IN ( $weaponTableIds ) and userId = :userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$ResultList = null;
		while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			$ResultList[] = $row;
		}

		if (count($ResultList) > 100 ) {
			$this->logger->logError(__FUNCTION__.': userId : '.$userId. ' result : '. count($ResultList). ' request : '.$sellCount);
		}

		if (count($ResultList) !== $sellCount ) {
			$this->logger->logError(__FUNCTION__.': count not match userId : '.$userId. ' result : '. count($ResultList). ' request : '.$sellCount);
			return $resultFail;
		}

		$AssetManager = new AssetManager();

		$sum = 0;
		$SellValue = 60;
		if ( $isExtraction <= 0 ) {
			// 유물 일괄 팔기 
			$procResult = $this->procSellWeapon($db, $userId, $ResultList);
			if ($procResult['ResultCode'] != 100) {
				$this -> logger -> logError(__FUNCTION__.': FAIL error gold userId : '.$userId);
				return $resultFail;
			}
			$gold = $procResult['gold'];
		} else {    //extraction
			$WEAPONPOWDERVALUE = 60; // DEFINE 작업 해야함

			$addPowder = 0;
			$abil_val = ABILL_Bonus_Extraction;
			$sql = "SELECT val from frdEffectForEtc where userId = :userId and type=:type";
			$db->prepare($sql);
			$db->bindValue(':userId', $userId, PDO::PARAM_INT);
			$db->bindValue(':type', $abil_val, PDO::PARAM_INT);
			$db->execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				//				$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.' sql :'.$sql);
				//				$resultFail['ResultCode'] = 300;
				$add_val = 0;
			}  else {
				$add_val = $row['val'];
			}

			$weaponTableIds = null;
			$sendBulkInsert = null;

			foreach ($ResultList as $RL) {
				if ($weaponTableIds == null) {
					$weaponTableIds = $RL['weaponTableId'];
				} else {
					$weaponTableIds .= ','.$RL['weaponTableId'];
				}


				$apc_key = 'WeaponInform_Weaponpowder';
				$apc_data = apc_fetch($apc_key);
				$temp = explode('/', $apc_data[1]);
				$grade = (int)$this->GetWeaponGrade($RL['weaponId']);
				$amountValue = $temp[$grade];

				//add transcend price ~
				if ( $grade == 6 ) {
					$transcendCount = (int)$RL['transcend'];
					$amountValue = AddTranscendPrice($transcendCount);
				}
				//~ add transcend price

				if ( $add_val > 0 ) {
					$percent = $add_val / 100000;
					$amountValue += round($amountValue * $percent);
				}

				$amountPercent = 50;

				$min = $amountValue - ceil($amountValue*$amountPercent/100);
				$max = $amountValue + (int)($amountValue*$amountPercent/100);
				$resultAmount = rand($min, $max);
				$addPowder +=  $resultAmount;
				

				if ($sendBulkInsert == null) {
					$sendBulkInsert = '('.$userId.','.$RL['weaponId'].',2,1,0,'.$RL['statId0'].','.$RL['statId1'].','.$RL['statId2'].','.$RL['statId3'].','.$RL['statVal0'].','.$RL['statVal1'].','.$RL['statVal2'].','.$RL['statVal3'].',"'.$GLOBALS['protocol'].'", now())';
				} else {
					$sendBulkInsert .= ',('.$userId.','.$RL['weaponId'].',2,1,0,'.$RL['statId0'].','.$RL['statId1'].','.$RL['statId2'].','.$RL['statId3'].','.$RL['statVal0'].','.$RL['statVal1'].','.$RL['statVal2'].','.$RL['statVal3'].',"'.$GLOBALS['protocol'].'", now())';
				}
				
			}

			$sql = "DELETE FROM frdHavingWeapons 
				WHERE weaponTableId IN ($weaponTableIds) and userId=:userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
				return $resultFail;
			}


			$logDBSendManager = new LogDBSendManager();
			$logDBSendManager->sendWeapon_bulk($userId, $sendBulkInsert);

			$resultPowder = $row["powder"]+$addPowder;

			$AssetResult = $AssetManager->addPowder($db,$userId, (int)$addPowder);
			if ($AssetResult['ResultCode'] != 100 ) {
				$this -> logger -> logError(__FUNCTION__.': FAIL error powder userId : '.$userId);
				return $resultFail;
			}
			$powder["curPowder"] = $AssetResult['powder'];
			$powder["addPowder"] = (int)$addPowder;

			// 일일 퀘스트 	
			$DailyQuestManager = new DailyQuestManager();
			$DailyQuestManager->updateDayMission($db, $userId, 3, 1);
		}

		$result['Protocol'] =  'ResSellWeapon';
		$result['ResultCode'] = 100;

		if (isset($powder)) {
			$data['rewards']['powder'] = $powder;
		}
		if (isset($gold)) {
			$data['rewards']['gold'] = $gold;
		}

		$data['use']['weapons']['delTableIds'] = $idxArr;

		$result['Data'] = $data;

		return $result;	
	}

	function EquipWeapon($param) {
		$resultFail['ResultCode'] = 300;

		$userId = $param['userId'];
		$equipIdx = $param['equipIdx'];
		$weaponTableId = $param['weaponTableId'];

		//weaponTableId 가 -1 로 오면 해제 입니다.

		if ($equipIdx < 0 || $equipIdx > 23 ) {
			$this->logger->logError(__FUNCTION__.' : wrong  userId : '.$userId.", equipIdx : ".$equipIdx);
			$resultFail['ResultCode'] = 300;
			return $resultFail;
		}

		$db = new DB();
		/*
		$sql = "SELECT * FROM frdEquipWeapons WHERE userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$resultList = null;
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' :  userId : ' . $userId . ", sql : " . $sql);
			return $resultFail;
		}
		*/

		$data = null;	
		$data['equipIdx'] = $equipIdx;
		$data['weaponTableId'] = $weaponTableId;

		$sql = "UPDATE frdEquipWeapons SET eq".$equipIdx." = :weaponTableId
			WHERE userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':weaponTableId', $weaponTableId, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' :  userId : ' . $userId . ", sql : " . $sql);
			return $resultFail;
		}

		$result['Protocol'] = 'ResEquipWeapon';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;
		return $result;
	}
	
	function procSellWeapon($db, $userId, $ResultList) {
		$resultFail['ResultCode'] = 300;

		$weaponTableIds = null;
		$SellValue = 60;

		$sendBulkInsert = null;	
		$sum = 0;
		foreach ($ResultList as $RL) {
			if ($weaponTableIds == null) {
				$weaponTableIds = $RL['weaponTableId'];
			} else {
				$weaponTableIds .= ','.$RL['weaponTableId'];
			}

			$tempGrade = (int)$this->GetWeaponGrade($RL['weaponId']);
			$apc_key = 'WeaponInform_WeaponValue';
			$apc_data = apc_fetch($apc_key);

			$tempData = explode('/', $apc_data[1]);

			$gradePrice = $tempData[$tempGrade];
			$apc_key = 'WeaponPrice_Exp_'.$RL['level'];
			$apc_data = apc_fetch($apc_key);
			$tempPrice = $apc_data[2];
			
			(int)$sum += $gradePrice + $tempPrice*$SellValue/100;
			
			//add transcend price ~
			if ( $tempGrade == 6 ) {
				$transcendCount = (int)$RL['transcend'];
				$sum = AddTranscendPrice($transcendCount);
			}
			//~ add transcend price

			if ($sendBulkInsert == null) {
				$sendBulkInsert = '('.$userId.','.$RL['weaponId'].',2,1,0,'.$RL['statId0'].','.$RL['statId1'].','.$RL['statId2'].','.$RL['statId3'].','.$RL['statVal0'].','.$RL['statVal1'].','.$RL['statVal2'].','.$RL['statVal3'].',"'.$GLOBALS['protocol'].'", now())';
			} else {
				$sendBulkInsert .= ',('.$userId.','.$RL['weaponId'].',2,1,0,'.$RL['statId0'].','.$RL['statId1'].','.$RL['statId2'].','.$RL['statId3'].','.$RL['statVal0'].','.$RL['statVal1'].','.$RL['statVal2'].','.$RL['statVal3'].',"'.$GLOBALS['protocol'].'", now())';
			}
			
		}


		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendWeapon_bulk($userId, $sendBulkInsert);

		$sql = "DELETE FROM frdHavingWeapons 
				WHERE weaponTableId IN ($weaponTableIds) and userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this -> logger -> logError(__FUNCTION__.': FAIL userId : '.$userId.", sql : ".$sql);
			return $resultFail;
		}

		$AssetManager = new AssetManager();

		$AssetResult = $AssetManager->addGold($db,$userId, (int)$sum);
		if ($AssetResult['ResultCode'] != 100 ) {
			$this -> logger -> logError(__FUNCTION__.': FAIL error gold userId : '.$userId);
			return $resultFail;
		}

		$gold = null;
		$gold["curGold"] = $AssetResult['gold'];
		$gold["addGold"] = (int)$sum;

		$result['ResultCode'] = 100;
		$result['gold'] = $gold;
		return $result;
	}

	function ChangeWeaponGroup($param) {
		$resultFail['ResultCode'] = 300;

		$userId = $param['userId'];
		$slotNo = $param['groupIdx'];

		if ($slotNo < 0 || $slotNo > 2 ) {
			$this->logger->logError(__FUNCTION__.' : wrong  userId : '.$userId.", groupIdx : ".$slotNo);
			$resultFail['ResultCode'] = 300;
			return $resultFail;
		}
		$db = new DB();
		$sql = "UPDATE frdEquipWeapons SET slotNo = :slotNo
			WHERE userId=:userId";
		$db -> prepare($sql);
		$db -> bindValue(':slotNo', $slotNo, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' :  userId : ' . $userId . ", sql : " . $sql);
			return $resultFail;
		}
		$data['groupIdx'] = $slotNo;

		$result['Protocol'] = 'ResChangeWeaponGroup';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;
		return $result;
	}

	function TranscendWeapon($param) {
		$resultFail['ResultCode'] = 300;

		$userId = $param['userId'];
		$weaponTableId  = $param['weaponTableId'];
		$matId  = $param['matId'];

		$db = new DB();
		$sql = "SELECT * FROM frdHavingWeapons 
			where weaponTableId=:weaponTableId and userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':weaponTableId', $weaponTableId, PDO::PARAM_INT);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.' id :'.$weaponTableId);
			$resultFail['ResultCode'] = 300;
			return $resultFail;
		}
		$mainWt = $row;

		$sql = "SELECT * FROM frdHavingWeapons 
			where weaponTableId=:weaponTableId and userId=:userId ";
		$db->prepare($sql);
		$db->bindValue(':weaponTableId', $matId, PDO::PARAM_INT);
		$db->bindValue(':userId', $userId, PDO::PARAM_INT);
		$db->execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this->logger->logError(__FUNCTION__.' : mat FAIL userId.'.$userId.' id :'.$matId);
			$resultFail['ResultCode'] = 301;
			return $resultFail;
		}
		$wt = $row;

		if ($mainWt['weaponId'] != $wt['weaponId']) {
			$this->logger->logError(__FUNCTION__.' : mat FAIL userId.'.$userId.' id :'.$matId);
			$resultFail['ResultCode'] = $mainWt['weaponId']+", ".$wt['weaponId'];
			return $resultFail;
		} 

		$transcendLv = (int)$mainWt['transcend'];
		$transcendExp = (int)$mainWt['transcendExp'];

		$price = $this->GetTranscendPrice($transcendLv);
		$upChance = $this->GetSuccesspercentForTranscend($transcendLv, $transcendExp);
		
		$AssetManager = new AssetManager();
		$AssetResult = $AssetManager->useGold($db, $userId, $price);
		if ($AssetResult['ResultCode'] != 100) {
			$this -> logger -> logerror(__FUNCTION__.' : FAIL use userId :' . $userId );
			return $resultFail;
		}
		$gold['curGold'] = $AssetResult['gold'];
		$gold['addGold'] = 0 - $price;
		$data['use']['gold'] = $gold;
		$data['use']['weapons']['delTableIds'][] = $matId;
		if ( $upChance >= rand(0,100) ) {

			$data["success"] = true;
			$transcendLv++;
			$transcendExp = 0;

		}
		else {

			$data["success"] = false;
			$transcendExp += $this->GetBonusTranscendpointWhenFail();
	
		}


		$sql = "UPDATE frdHavingWeapons set transcend = :transcendLv, transcendExp = :transcendExp";

		switch ($transcendLv) {
		case 2:
			$preStatVal1 = (int)$mainWt['statVal1'];
			$newStatVal1 = rand($preStatVal1,250);
			$sql .= ", statVal1 = $newStatVal1";
			$data["statVal1"] = $newStatVal1;
			break;

		case 4:
			$preStatVal0 = (int)$mainWt['statVal0'];
			$newStatVal0 = rand($preStatVal0,250);
			$sql .= ", statVal0 = $newStatVal0";
			$data["statVal0"] = $newStatVal0;
			break;
		
		default:
			# code...
			break;
		}

		$sql .= " WHERE weaponTableId=:weaponTableId AND userId=:userId";
		$db -> prepare($sql);
		$db -> bindvalue(':transcendLv', $transcendLv, PDO::PARAM_INT);
		$db -> bindvalue(':transcendExp', $transcendExp, PDO::PARAM_INT);
		$db -> bindvalue(':weaponTableId', $weaponTableId, PDO::PARAM_INT);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			$resultFail['ResultCode'] = 300;
			return $resultFail;
		}
		$data["transcendLevel"] = $transcendLv;
		$data["transcendExp"] = $transcendExp;



		$sql = "DELETE FROM frdHavingWeapons WHERE weaponTableId=:matId AND userId=:userId";
		$db -> prepare($sql);
		$db -> bindvalue(':matId', $matId, PDO::PARAM_INT);
		$db -> bindvalue(':userId', $userId, PDO::PARAM_INT);
        $row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId :'.$userId.' sql : '.$sql);
			$this->logger->logError(__FUNCTION__.' : FAIL matId :'.$matId);
			return $resultFail;
		}

		$logDBSendManager = new LogDBSendManager();
		$logDBSendManager->sendWeapon($userId, $wt['weaponId'], 2, 1, 0, $wt['statId0'], $wt['statId1'], $wt['statId2'], $wt['statId3'], $wt['statVal0'], $wt['statVal1'], $wt['statVal2'], $wt['statVal3']);

		$result['Protocol'] = 'ResTranscendWeapon';
		$result['ResultCode'] = 100;
		$result['Data'] = $data;
		return $result;
	}




	function GetWeaponGrade($privateId) {
		switch($privateId) {
			case 498:
				return 1;
			case 497:
				return 2;
			case 496:
				return 3;
			case 495:
				return 4;
			default:
				if ( $privateId < 10 )
					return 1;
				else if ( $privateId < 50 )
					return 2;
				else if ( $privateId < 100 )
					return 3;
				else if ( $privateId < 200 )
					return 4;
				else if ( $privateId < 300 )
					return 5;
				else if ( $privateId < 390 )
					return 6;
				else
					return 7;
		}
	}
	function GetCanGetMaxPoint_Transcend($transcend) {
		return ($this->GetMaxPoint_Transcend($transcend) - $this->GetWeaponTranscendPoint());
	}
	function GetMaxPoint_Transcend($transcend) {
		switch($transcend) {
		case 0:
			return 1000;
		case 1:
			return 1111;
		case 2:
			return 1330;
		case 3:
			return 2000;
		case 4:
			return 2500;
		default:
			return 3000;
		}
	}
	function GetWeaponTranscendPoint() {
		return 1000;
	}
	function GetBonusTranscendpointWhenFail() {
		return $this->GetWeaponTranscendPoint()/5;
	}
	function GetSuccesspercentForTranscend($deskTranscendLv, $deskTranscendExp) {
		$basePercent = (float)($this->GetWeaponTranscendPoint() / $this->GetMaxPoint_Transcend($deskTranscendLv));
		$bonusPercent = (float)($deskTranscendExp / $this->GetMaxPoint_Transcend($deskTranscendLv));
		return round(($basePercent + $bonusPercent)*100);
	}
	function GetTranscendPrice($fromTranscend) {
		switch($fromTranscend) {
		case 0:
			return 50000;
		case 1:
			return 65000;
		case 2:
			return 84500;
		case 3:
			return 110000;
		case 4:
			return 143000;
		default:
			return 186250;
		}
	}
	function AddTranscendPrice($price, $transcend) {
		for ( $i=0; $i<$transcend; $i++ )
			$price = round($price * 1.2);
		return $price;
	}

}
?>

