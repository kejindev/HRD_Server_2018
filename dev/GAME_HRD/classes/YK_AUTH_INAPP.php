<?php
include_once '../common/config.php';
include_once '../common/DB.php';
include_once '../common/LOGIN_DB.php';
require_once '../lib/Logger.php';
require_once "../common/define.php";
require_once './AddManager.php';
require_once './LogDBSendManager.php';
require_once './InAppManager_YK.php';

$request = file_get_contents("php://input");

/*
■	amount ：amount는 이번 총 소비가격, 실제로 RMB(할인후) 받은 후 전까지 정확하게 계산
■	cpinfo ：개발사가 RoRoSDK에 기입한, RoRoServer OSPE
■	pforderid ：모바일게임 마켓 서드파티 오픈 플랫폼 계정
■	pfuid ：모바일게임 마켓 서드파티 오픈 플랫폼 계정
■	plappid ：RoRo마켓에서 발행한 appid
■	plchid ：각 모바일게임 마켓 서드파티 오픈 플랫폼이 영킹게임 플랫폼에서 사용하는 ID; 동시에 RoRoSDK API에서 해당 파라미터를 앱 클라이언트로 전달
■	plorderid ：주문번호 (RoRo마켓 주문번호)
■	roleid ：게임의 roleid，게임 클라이언트 결제 시 RoRoSDK에 잔달, 서버에서 그대로 돌려보냄
■	sendtime ：시간, 포맷：yyyy-mm-dd HH24:mm:ss
■	sign ：RoRo 마켓 서명 MD5암호화 적용
■	zoneid ：게임 서버지역 ID. 게임 클라이언트 결제시 RoRoSDK에 전달, 서버에서 그대로 돌려보냄
*/

$param = json_decode($request);

$amount 	= $param['amount'];
$cpinfo 	= $param['cpinfo'];
$pforderid 	= $param['pforderid'];
$pfuid 		= $param['pfuid'];
$plappid 	= $param['plappid'];
$plchid 	= $param['plchid'];
$plorderid 	= $param['plorderid'];
$roleid 	= $param['roleid'];
$sendtime 	= $param['sendtime'];
$sign 		= $param['sign'];
$zoneid 	= $param['zoneid'];

$ResultFail = null;
$ResultFail['code'] = 101;
$ResultFail['msg'] = 'Notify Success';


if (isset($amount) || $amount < 1 ) {
	$result['code'] = 101;
	$result['msg'] = 'Invalid Amount';

	$result = json_encode($response);
	echo $result;
}

// RORO SDK OSPE CHECK 
if ($cpinfo != 'cpinfo' ) {
	$result['code'] = 102;
	$result['msg'] = 'Invalid Argument';

	$result = json_encode($response);
	echo $result;
}

if ( 
	( $pforderid == null || $pforderid == "") ||
	( $pfuid == null || $pfuid == "") || 
	( $plappid != 10003) ||
	( $plchid == null || $plchid == "") ||
	( $plorderid == null || $plorderid == "") ||
	( $roleid == null || $roleid == "") ||
	( $sendtime == null || $sendtime == "") || 
	( $sign == null || $sign == "") ||
	( $zoneid == null || $zoneid == "") 
	)
	{

	$result['code'] = 102;
	$result['msg'] = 'Invalid Argument';

	$result = json_encode($response);
	echo $result;
}

$str = "amount=".$amount."&cpinfo=".$cpinfo."&pforderid=".$pforderid."&pfuid=".$pfuid."&plappid=".$plappid."&plchid=".$plchid;
$str .= "&plorderid=".$plorderid."&roleid=".$roleid."&sendtime=".$sendtime."&zoneid=".$zoneid."&plappsecret=nKcitRHim6ttGRAV";

if ( md5($str)!= $sign ) {
	$result['code'] = 103;
	$result['msg'] = 'Invalid Signature';

	$result = json_encode($response);
	echo $result;
}

// 상품 지급  ---> 
// 계정 체크 
$login_db = new LOGIN_DB();
$sql = "select userId from t_Account_User where connectId=:connectId";
$login_db -> prepare($sql);
$login_db -> bindValue(':connectId', $plchid, PDO::PARAM_STR);
$login_db -> execute();
$row = $login_db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
if (!$row) {
	$result['code'] = 104;
	$result['msg'] = 'Unknown Error';

	$result = json_encode($response);
	echo $result;
}
$userId = $row['userId'];

$db_no = substr($userId, 0, 3);
$GLOBALS['AccessNo'] = $db_no;

$db = new DB();
// 주문확인 
$sql = "SELECT * FROM frdLogBuy WHERE orderId = :orderId";
$db -> prepare($sql);
$db -> bindValue(':orderId', $plorderid, PDO::PARAM_STR);
$db -> execute();
$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
if (!$row) {
	$result['code'] = 104;
	$result['msg'] = 'Unknown Error';

	$result = json_encode($response);
	echo $result;
}
$productId = $row['producId'];

$sql = "SELECT shopItemId, itemId, itemCount,price FROM frdShopData 
WHERE productId = :productId";
$db -> prepare($sql);
$db -> bindValue(':productId', $productId, PDO::PARAM_STR);
$db -> execute();
$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
if (!$row) {
	$result['code'] = 104;
	$result['msg'] = 'Unknown Error';

	$result = json_encode($response);
	echo $result;
}

$shopItemId = $row['shopItemId'];
$itemId = $row['itemId'];
$itemCount = $row['itemCount'];
$price = $row['price'];

$InAppManager_YK = new InAppManager_YK();

// 패키지 
if ( $shopItemId > 6000 && $shopItemId != 6005 ) {
	$tempTypes = explode('/',$itemId);	
	$tempCnts = explode('/',$itemCount);	

	$addResult = $this->addItemForPackage($db, $userId, $tempTypes, $tempCnts);
	// 보석 
} else if ( $shopItemId > 1000 && $shopItemId < 2000) {

	$AssetManager = new AssetManager();
	$AssetResult = $AssetManager->addJewel($db, $userId, $itemCount);

} else if ( $shopItemId == 6005  ) {	

	$sql = "UPDATE frdMonthlyCard SET startTime = now(), duration = 30, recvCnt = 1
		WHERE UserId = :userId";
	$db -> prepare($sql);
	$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
	$row = $db -> execute();
	if (!isset($row) || is_null($row) || $row == 0) {
		$result['code'] = 104;
		$result['msg'] = 'Unknown Error';

		$result = json_encode($response);
		echo $result;
	}

	$sql = "INSERT INTO frdUserPost
		(recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
	$sql .= "($userId, 101, 5004, 20, DATE_ADD(now(), INTERVAL 30 DAY ), now()), ";  // jewel
	$sql .= "($userId, 101, 5003, 20, DATE_ADD(now(), INTERVAL 30 DAY ), now())  ";  // heart
	$db -> prepare($sql);
	$row = $db -> execute();
	if (!isset($row) || is_null($row) || $row == 0) {
		$result['code'] = 104;
		$result['msg'] = 'Unknown Error';

		$result = json_encode($response);
		echo $result;
	}
}


$result['code'] = 100;
$result['msg'] = 'Notify Success';

$result = json_encode($response);
echo $result;

?>
