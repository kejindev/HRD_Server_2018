<?php
class LOGIN_DB {
	public $dsn = null;
	public $dbUser = null;
	public $dbPasswd = null;

	public $pdo = null;
	public $stmt = null;

	/**
	 *
	 * Enter description here ...
	 */

	public function __construct() {
$this->dsn = "mysql:dbname=".$GLOBALS['LOGIN_DB_NAME'].";host=".$GLOBALS['LOGIN_DB_HOST'].";port=".$GLOBALS['LOGIN_DB_PORT'];
        $this->dbUser = $GLOBALS['LOGIN_DB_USER'];
        $this->dbPasswd = $GLOBALS['LOGIN_DB_PASS'];


		try {
			$this -> pdo = new PDO($this -> dsn, $this -> dbUser, $this -> dbPasswd, array(PDO::ATTR_PERSISTENT => true));
			$this -> pdo -> query("SET NAMES 'utf8'");
			//$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(PDOException $e) {

			$err = $e -> getMessage();

			$date = date('Y-m-d H:i:s');

			$resultData['Protocol'] = str_replace('Req', 'Res', $GLOBALS['requestData']['Protocol']);
			$resultData['ResultCdoe'] = '300';
	//		$resultData['PDOException'] = $err;


			require_once "./lib/JsonParserTest.php";
			$jsonP = new JsonParser();
			$resultJson = $jsonP->encode($resultData);
			error_log("{$date}| {$resultJson} | {$err}" . "\n", 3, "/home/log/db_error_" . date('Ymd') . '.log');
				
			// require_once "./lib/EncJsonParser.php";
			// $jsonTest = new JsonParserTest();
			// $resultJson = $jsonTest -> encode($resultData);
			// error_log("{$date}| {$resultJson}| {$err}" . "\n", 3, "/home/log/db_error_" . date('Ymd') . '.log');
			// $resultJson = $jsonTest -> encrypt($key, $iv, $resultJson);

			echo $resultJson;
			exit ;
		}
		//		return $this->$db;
	}

	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $column
	 * @param unknown_type $param
	 */
	public function bindColumn($column, $param) {
		$this -> stmt -> bindColumn($column, $param);
	}

	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $param
	 * @param unknown_type $variable
	 * @param unknown_type $type
	 * @param unknown_type $length
	 */
	public function bindParam($param, $variable, $type, $length = null) {
		if ($length != null)
			$this -> stmt -> bindParam($param, $variable, $type, $length);
		else
			$this -> stmt -> bindParam($param, $variable, $type);
	}

	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $param
	 * @param unknown_type $value
	 * @param unknown_type $type
	 */
	public function bindValue($param, $value, $type) {
		$this -> stmt -> bindValue($param, $value, $type);
	}

	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $fetch_style
	 * @param unknown_type $orientation
	 */
	public function fetch($fetch_style, $orientation = null) {
		if ($orientation != null)
			return $this -> stmt -> fetch($fetch_style, $orientation);
		else
			return $this -> stmt -> fetch($fetch_style);
	}

	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $fetch_style
	 * @param unknown_type $fetch_argument
	 */
	public function fetchAll($fetch_style, $fetch_argument = null) {
		if ($fetch_argument != null)
			return $this -> stmt -> fetch($fetch_style, $fetch_argument);
		else
			return $this -> stmt -> fetch($fetch_style);
	}

	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $column_number
	 */
	public function fetchColumn($column_number = null) {
		if ($column_number != null)
			return $this -> stmt -> fetchColumn($column_number);
		else
			return $this -> stmt -> fetchColumn();
	}

	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $class_name
	 */
	public function fetchObject($class_name) {
		return $this -> stmt -> fetchObject($column_number);
	}

	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $statement
	 * @param unknown_type $driver_options
	 */
	public function prepare($statement, $driver_options = null) {
		if ($driver_options != null)
			$this -> stmt = $this -> pdo -> prepare($statement, $driver_options);
		else
			$this -> stmt = $this -> pdo -> prepare($statement);
	}

	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $statement
	 * @param unknown_type $driver_options
	 */
	public function query($statement, $driver_options = null) {
		if ($driver_options != null)
			return $this -> pdo -> query($statement, $driver_options);
		else
			return $this -> pdo -> query($statement);
	}

	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $inParam
	 */
	public function execute($inParam = null) {
		if ($inParam != null)
			return $this -> stmt -> execute($inParam);
		else
			return $this -> stmt -> execute();
	}

	/**
	 *
	 * Enter description here ...
	 * @param unknown_type $attribute
	 * @param unknown_type $value
	 */
	public function setAttribute($attribute, $value) {
		return $this -> pdo -> setAttribute($attribute, $value);
	}

	/**
	 *
	 * Enter description here ...
	 */
	public function rowCount() {
		return $this -> stmt -> rowCount();
	}

	/**
	 *
	 * Enter description here ...
	 */
	public function columnCount() {
		return $this -> stmt -> columnCount();
	}

	/**
	 *
	 * Enter description here ...
	 */
	public function errorCode() {
		return $this -> stmt -> errorCode();
	}

	/**
	 *
	 * Enter description here ...
	 */
	public function errorInfo() {
		$ErrorInfo = $this -> stmt -> errorInfo();
		return $ErrorInfo[2];
	}

	/**
	 *
	 * Enter description here ...
	 */
	public function lastInsertId() {
		return $this -> pdo -> lastInsertId();
	}

}
