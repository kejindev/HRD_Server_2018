<?php

// 재화
define("Type_Gold", 5000);
define("Type_Exp", 5001);
define("Type_Ticket", 5002);
define("Type_Heart", 5003);
define("Type_Jewel", 5004);
define("Type_Medal", 5005);
define("Type_Powder", 5006);

define("ALL", 0);
define("DAILY_REWARD_START", 1);
define("DAILY_REWARD_END", 10);
define("ABILL_BonusScore_ClimbTop", 1008);
define("ABILL_TEMPLEOUTPUT", 1000);
define("ABILL_Bonus_MaxHeart", 1032);

define("ABILL_Bonus_Extraction", 1017);

$shopVerInform = apc_fetch('EventData'.$GLOBALS['COUNTRY'].'300'); 
//$shopVerInform = apc_fetch("Shop_Version".$GLOBALS['COUNTRY']);
if ( false !== $shopVerInform ) {
	$define_date = date('ymdHis');
	if (is_object($shopVerInform))	//stdClass -> array
		$shopVerInform = get_object_vars($shopVerInform);

	if (is_object($shopVerInform['shopVerInform']))	//stdClass -> array
		$shopVerInform['shopVerInform'] = get_object_vars($shopVerInform['shopVerInform']);
//	var_dump($shopVerInform);
//	$shopVerInform = $shopVerInform['shopVerInform'];
	if ( $shopVerInform['shopVerInform']['fr_ymdHis'] < $define_date && $define_date < $shopVerInform['shopVerInform']['to_ymdHis'] )
		define("Shop_Version", (int)$shopVerInform['shopVerInform']['ver']);
	else
		define("Shop_Version", 1);
}
else {
	define("Shop_Version", 1);
}

define("ABILL_Sale_BlackMarket", 1011);
define("ABILL_Bonus_MagicLvUpPercent", 1041);

define("ABILL_Up_Per_Rune", 1038);
define("ABILL_Up_Per_QuestItem", 1046);

define("EVENT_CHEST", 100);
define("EVENT_PAY_FIRST", 200);
define("EVENT_SELL_PACKAGE", 300);
define("EVENT_HOTTIME", 400);
define("EVENT_CAFE", 500);
define("EVENT_BLACKMARKET", 600);
define("EVENT_DOUBLE_WEAPONEXP", 700);
define("EVENT_RETURN_USER", 800);
define("EVENT_HALF_HEART", 900);
define("EVENT_PAY_ACCU", 1000);
define("EVENT_WEBVIEW", 1100);
define("EVENT_CHK_ATTENDANCE", 1200);
define("EVENT_ENTER_CLIMB_REWARD", 1300);
define("EVENT_CHK_ENTERCOUNT", 1400);
define("EVENT_INFINITY_HEART", 1500);
define("EVENT_369", 1600);
define("EVENT_DOUBLE_LEAPACKAGE", 1700);
define("EVENT_DAILY_CLEARCOUNT", 1800);
define("EVENT_DOUBLE_ABYSS", 1900);

// 범위 
define("Char_Min", 0);
define("Char_Max", 506);


//업적 맥스 
define("achieve_1100", 1113);
define("achieve_1200", 1207);
define("achieve_1300", 1302);
define("achieve_1400", 1409);
define("achieve_1500", 1505);
define("achieve_1600", 1605);
define("achieve_1700", 1706);
define("achieve_1800", 1806);
define("achieve_1900", 1906);

define("achieve_2100", 2106);
define("achieve_2200", 2206);
define("achieve_2300", 2306);
define("achieve_2400", 2406);
define("achieve_2500", 2506);
define("achieve_2600", 2606);

define("achieve_3100", 3105);
define("achieve_3200", 3205);
define("achieve_3300", 3305);
define("achieve_3400", 3405);
define("achieve_3500", 3505);
define("achieve_3600", 3607);

define("achieve_4100", 4105);
define("achieve_4200", 4205);
define("achieve_4300", 4305);
define("achieve_4400", 4405);
define("achieve_4500", 4505);
define("achieve_4600", 4605);



?>
