<?php
date_default_timezone_set('Asia/Seoul');

require_once __DIR__ . "/lib/EncJsonParser.php";
require_once __DIR__ . "/common/DB.php";
require_once __DIR__ . "/common/Config.php";
require_once __DIR__ . "/common/Controller.php";

$getPostData = file_get_contents("php://input");

$date = date('Y-m-d H:i:s');

# json parser
$jsonTest = new JsonParserTest();
//error_log("{$date}| {$getPostData}" . "\n", 3, "/home/log/enc_" . date('Ymd') . '.log');
$getPostData = $jsonTest -> decrypt($key, $iv, $getPostData);
$check = $jsonTest -> isJson($getPostData);
if (!$check) {
    error_log($_SERVER['REQUEST_URI'] . ' failed while parsing parameters');
    header("HTTP/1.0 400 Bad Request");
    echo "<h1>400 Bad Request</h1>\n";
    echo "Error occured while parsing parameters";
    return;
}

$date = date('Y-m-d H:i:s');

//        syslog(LOG_INFO | LOG_LOCAL1, "{$getPostData}");
error_log("{$date}| {$getPostData}" . "\n", 3, "/home/log/request_" . date('Ymd') . '.log');

$requestData = $jsonTest -> parser($getPostData);

# controller logic process
$controller = new Controller();
$resultData = $controller -> process($requestData);

# json encode
$resultJson = $jsonTest -> encode($resultData);

error_log("{$date}| {$resultJson}" . "\n", 3, "/home/log/response_" . date('Ymd') . '.log');
       
$resultJson = $jsonTest -> encrypt($key, $iv, $resultJson);

echo $resultJson;
