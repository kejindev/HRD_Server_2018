<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
$date = date('Y-m-d H:i:s');

// 유저 정보  
$userArray = array(
"200059507/60",
"500015851/60",
"500042423/60",
"700036505/55",
"700037593/55",
"200041286/40",
"200013133/35",
"800030343/25",
"200001471/145",
"700037854/145",
"800028217/135",
"800030674/135",
"200086177/130",
"600002504/130",
"700034590/125",
"800029062/120",
"200087857/90",
"800002117/85",
"800000494/75",
"700027474/245",
"200081089/235",
"600010441/190",
"200000283/175",
"800028383/405",
"200043697/370",
"500039978/340",
"600010585/330",
"600010583/325",
"600006499/320",
"500004393/310",
"700039327/310",
"600008107/290",
"700033484/275",
"500036968/1020",
"600009219/975",
"200086129/915",
"600010861/810",
"600010323/605",
"800010944/545",
"600005026/490",
"600005537/475",
"800011833/470",
"700036531/455",
"200051340/450",
"600010639/450",
"200016716/440",
"500016391/430"
);

// DB 12종 선언 
$GLOBALS['DB_HOST'] = '14.49.38.192';
$GLOBALS['DB_USER'] = "root";
$GLOBALS['DB_PASS'] = "eoqkrskwk12";
$GLOBALS['DB_NAME'] = "ddookdak";
$GLOBALS['DB_PASS'] = "eoqkrskwk12";
$GLOBALS['DB_PORT'] = 2111;

$GLOBALS['DB_PORT'] = 2111;
$db200 = new DB();
$GLOBALS['DB_PORT'] = 5111;
$db500 = new DB();
$GLOBALS['DB_PORT'] = 7111;
$db700 = new DB();
$GLOBALS['DB_PORT'] = 8111;
$db800 = new DB();


$GLOBALS['DB_NAME'] = "ddookdak2";
$GLOBALS['DB_PORT'] = 2111;
$db202 = new DB();

$GLOBALS['DB_NAME'] = "ddookdak2";
$GLOBALS['DB_PORT'] = 5111;
$db502 = new DB();


$tt = 0;
foreach ($userArray as $ua) {
	$userData = explode('/', $ua);
	$userId = $userData[0];
	$cnt = $userData[1];
	if ($userId < 0 || $cnt < 1) {
		$mes = ' : FAIL userId.'.$userId.' cnt :'.$cnt;
		error_log("{$date} | {$mes}" . "\n", 3, "/home/log/" . date('Ymd') . '.log');
	}
	$db_no = substr($userId, 0, 3);

	switch ($db_no) {
		case 200:
			$tempDB = $db200;
			break;
		case 500:
		case 600:
			$tempDB = $db500;
			break;
		case 700:
			$tempDB = $db700;
			break;
		case 800:
			$tempDB = $db800;
			break;
		case 202:
			$tempDB = $db202;
			break;
		case 502:
			$tempDB = $db502;
			break;
		default:
			$mes = ' : FAIL userId.'.$userId.' db_no :'.$db_no;
			error_log("{$date} | {$mes}" . "\n", 3, "/home/log/" . date('Ymd') . '.log');
			break;
	}

	$sql = "SELECT jewel FROM frdUserData WHERE privateId=:userId ";
	$tempDB->prepare($sql);
	$tempDB->bindValue(':userId', $userId, PDO::PARAM_INT);
	$tempDB->execute();
	$row = $tempDB -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
	if (!$row) {
		$mes = ' : FAIL userId.'.$userId.' sql :'.$sql;
		error_log("{$date} | {$mes}" . "\n", 3, "/home/log/" . date('Ymd') . '.log');

		break;
	}
	$myJewel = $row['jewel'];

	$sql = " SELECT sum(count) as Cnt FROM frdUserPost WHERE recvUserId = :userId and type = 5004";
	$tempDB->prepare($sql);
	$tempDB->bindValue(':userId', $userId, PDO::PARAM_INT);
	$tempDB->execute();
	$row = $tempDB -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
	if (!$row) {
		$mes = ' : FAIL userId.'.$userId.' sql :'.$sql;
		error_log("{$date} | {$mes}" . "\n", 3, "/home/log/" . date('Ymd') . '.log');
		break;
	}

	$postJewel = $row['Cnt'];
	$tempJewel = $myJewel + $postJewel;
	if ( $tempJewel >= $cnt ) {
		$tt++;
		$jewel = $tempJewel - $cnt;
		$mes = 'userId.'.$userId.' 회수: '.$cnt.' 보유 : '.$myJewel.' 우편: '.$postJewel.' 최종보유 :'.$jewel;
		error_log("{$date} | {$mes}" . "\n", 3, "/home/log/event_" . date('Ymd') . '.log');

		$sql = "UPDATE frdUserData
			SET jewel = :jewel WHERE privateId =:userId";
		$tempDB -> prepare($sql);
		$tempDB -> bindValue(':jewel', $jewel, PDO::PARAM_INT);
		$tempDB -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $tempDB -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$mes = ' : FAIL userId.'.$userId.' sql :'.$sql;
			error_log("{$date} | {$mes}" . "\n", 3, "/home/log/" . date('Ymd') . '.log');
			break;
		}
		
		if ($myJewel < $cnt ) {	
			$mes = 'userId.'.$userId.' 보석 우편 삭제 유저 .  최종보유 :'.$jewel;
			error_log("{$date} | {$mes}" . "\n", 3, "/home/log/event_" . date('Ymd') . '.log');

			$sql = "DELETE from frdUserPost WHERE recvUserId =:userId and type = 5004";
			$tempDB -> prepare($sql);
			$tempDB -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$row = $tempDB -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$mes = ' : FAIL userId.'.$userId.' sql :'.$sql;
				error_log("{$date} | {$mes}" . "\n", 3, "/home/log/" . date('Ymd') . '.log');
				break;
			}
			
		}
	} else {
		$sql = "UPDATE frdUserData
			SET jewel = 0 WHERE privateId =:userId";
		$tempDB -> prepare($sql);
		$tempDB -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $tempDB -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$mes = ' : FAIL userId.'.$userId.' sql :'.$sql;
			error_log("{$date} | {$mes}" . "\n", 3, "/home/log/" . date('Ymd') . '.log');
			break;
		}

		$sql = "DELETE from frdUserPost WHERE recvUserId =:userId and type = 5004";
		$tempDB -> prepare($sql);
		$tempDB -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$row = $tempDB -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$mes = ' : FAIL userId.'.$userId.' sql :'.$sql;
			error_log("{$date} | {$mes}" . "\n", 3, "/home/log/" . date('Ymd') . '.log');
			break;
		}
		$jewel = $tempJewel - $cnt;
		$mes = '#강제집행자 userId.'.$userId.' 회수: '.$cnt.' 보유 : '.$myJewel.' 우편: '.$postJewel.' 부족 :'.$jewel;
		error_log("{$date} | {$mes}" . "\n", 3, "/home/log/event_" . date('Ymd') . '.log');
	}
}
var_dump($tt);


?>
