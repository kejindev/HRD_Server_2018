<?php
include_once './common/DB.php';
require_once './lib/RandManager.php';
require_once './lib/Logger.php';

class GetSummonManager {

    public function __construct() {
        $this -> logger = Logger::get();
    }

    function get_chest_open($db, $UID, $CHEST_LIST, $event_chance_up_rate = null, $event_chance_up_id = null, $promise_grade = null, $special_summon_check = null, $summon_target_grade_up = null) {

        $RAND = new RandManager();

        $count = count($CHEST_LIST);

        $GET_OPEN_SUMMON_DTOS = array();
        $GET_OPEN_SUMMON_DTOL = array();
        //$GET_OPEN_SUMMON_DTOS['func'] = 0;

        //$GET_OPEN_SUMMON_DTOS['ResultCOde'] = array();
        $GET_OPEN_SUMMON_DTOS['ResultCode'] = 100;
        //set err
        $total_idx = 0;
        #######################################################
        // $event_chance_up == 확률증가
        // $event_chest_id == n성이상 뽑기 ID
        if ($promise_grade != null) {
            $CHEST_LIST[0] = $promise_grade;

            if ($special_summon_check != null) {
                $CHEST_LIST[0] = $special_summon_check;
                $CHEST_LIST[1] = $promise_grade;
            }
        }

        #######################################################

        //$CONN_DB_DATA_W = $DB->Conn_DB($SERVER_ID,"DATA","w");

        $angel_devil_summon_new_status = 0;
        $special_summon_status = 0;
        $angel_devil_summon_passble = false;
        $angel_devil_summon_update_check = false;

        for ($i = 0; $i < $count; $i++) {
            // SCRIPT::영혼석 상자 검색
            //$CLASS = "SUMMON";
            //$OPTION = "CHEST";
            $IDX = $CHEST_LIST[$i];
            // <- Client 에서 요청한 상자 IDX LIST
            $apc_key = 'SUMMON_CHEST_' . $IDX;
            $SCRIPT_SUMMON_CHEST_DTO = apc_fetch($apc_key);

            //print_r($CHEST_LIST);exit;
            //var_dump($SCRIPT_SUMMON_CHEST_DTO);EXIT;

            //$SCRIPT_SUMMON_CHEST_DTO = $DATA->get_game_script($CLASS,$OPTION,$IDX);
            $RESULT_DATA = explode("/", $SCRIPT_SUMMON_CHEST_DTO[1]);
            $RATE_DATA = explode("/", $SCRIPT_SUMMON_CHEST_DTO[2]);

            $summon_grade_str_array = explode("/", $SCRIPT_SUMMON_CHEST_DTO[9]);

            if (count($RESULT_DATA) != count($RATE_DATA) or count($RESULT_DATA) != count($summon_grade_str_array)) {
                $this -> logger -> logError($GLOBALS['AccessNo'] . ', GetSummunManager::get_chest_open count($RESULT_DATA) != count($RATE_DATA) UID : ' . $UID);
            }

            #########################
            //  이벤트 확률업

            if ($event_chance_up_rate != null and $event_chance_up_id != null) {
                for ($j = 0; $j < count($RESULT_DATA); $j++) {

                    if ($RESULT_DATA[$j] == $event_chance_up_id) {
                        $RATE_DATA[$j] = $RATE_DATA[$j] * ($event_chance_up_rate / 100);
                        $RATE_DATA[$j] = floor($RATE_DATA[$j]);
                    }
                }
            }
            #########################
            // n성 영웅 등장확률 업

            if ($summon_target_grade_up != null) {

                $reward_target = null;

                $reward_target = explode('/', $summon_target_grade_up['reward_target']);

                foreach ($summon_grade_str_array as $key => $value) {

                    foreach ($reward_target as $reward_target_key => $reward_target_value) {

                        if ($value == $reward_target_value) {
                            $RATE_DATA[$key] = $RATE_DATA[$key] * ($summon_target_grade_up['reward_value'] / 100);
                            $RATE_DATA[$key] = floor($RATE_DATA[$key]);
                        }

                    }

                }

            }

            #########################

            // RAND::상자에 등록 된 랜덤 아이템 검색
            $RAND_SUMMON_CHEST_IDX = $RAND -> Rate($RESULT_DATA, $RATE_DATA);

            //echo "RAND_IDX : {$RAND_CHEST_ITEM_IDX} <br>";

            //SCRIPT::영혼석 정보 검색
            //$CLASS = "SUMMON";
            //$OPTION = "BASE";

            //$IDX = $RAND_SUMMON_CHEST_IDX;
            $IDX = $RAND_SUMMON_CHEST_IDX;
            $apc_key = "SUMMON_BASE_" . $IDX;

            $SCRIPT_SUMMON_BASE_DTO = apc_fetch($apc_key);

            //$SCRIPT_SUMMON_BASE_DTO = $DATA->get_game_script($CLASS,$OPTION,$IDX);

            $GET_SUMMON_INFO_DTO['Summon_Idx'] = $SCRIPT_SUMMON_BASE_DTO[0];
            $GET_SUMMON_INFO_DTO['summon_type'] = $SCRIPT_SUMMON_BASE_DTO[1];
            $GET_SUMMON_INFO_DTO['summon_grade'] = $SCRIPT_SUMMON_BASE_DTO[3];
            $GET_SUMMON_INFO_DTO['summon_level'] = 1;
            $GET_SUMMON_INFO_DTO['summon_exp'] = 0;
            $GET_SUMMON_INFO_DTO['summon_status'] = 0;
            $GET_SUMMON_INFO_DTO['summon_equips_amulet'] = -1;
            $GET_SUMMON_INFO_DTO['summon_equips_ring'] = -1;
            $GET_SUMMON_INFO_DTO['summon_equips_belt'] = -1;

            $get_stone_uno = $this -> add_new_summon($db, $UID, $GET_SUMMON_INFO_DTO);

            if ($get_stone_uno == 0) {//insert fail
                $GET_OPEN_SUMMON_DTOS = array();
                $GET_OPEN_SUMMON_DTOS['ResultCode'] = 300;
                //set err
                return $GET_OPEN_SUMMON_DTOS;
            }

            $GET_SUMMON_BASE_DTO['Summon_Idx'] = $SCRIPT_SUMMON_BASE_DTO[0];
            $GET_SUMMON_BASE_DTO['GET_STACK'] = 1;
            $GET_SUMMON_BASE_DTO['GET_UNO'] = $get_stone_uno;
            $GET_SUMMON_BASE_DTO['summon_grade'] = $GET_SUMMON_INFO_DTO['summon_grade'];
            //var_dump($GET_SUMMON_BASE_DTO);exit;

            // 획득 아이템  DTOL 추가
            array_push($GET_OPEN_SUMMON_DTOL, $GET_SUMMON_BASE_DTO);
            $total_idx++;
        }

        //var_dump($GET_OPEN_SUMMON_DTOL);exit;
        $GET_OPEN_SUMMON_DTOS['ResultCode'] = 100;
        $GET_OPEN_SUMMON_DTOS['GET_SUMMONS'] = $GET_OPEN_SUMMON_DTOL;
        $GET_OPEN_SUMMON_DTOS['CHEST_COUNT'] = $total_idx;
        //print_r($GET_OPEN_SUMMON_DTOS);exit;
        return $GET_OPEN_SUMMON_DTOS;
    }

    // ITEM_BASE::고정 아이템 습득
    function get_summon_open($db, $UID, $Summon_Idx) {

        // function get_summon_open($db, $UID, $Summon_Idx, $summon_lv) {
        $resultFail['ResultCode'] = 300;

        $apc_key = "SUMMON_BASE_" . $Summon_Idx;
        $summon_base_apc_data = apc_fetch($apc_key);

        if ($summon_base_apc_data == null) {
            $this -> logger -> logError('DB : ' . $GLOBALS['AccessNo'] . ', GetSummonManager::get_summon_open apc_fetch FAIL UID : ' . $UID . ', $Summon_Idx : ' . $Summon_Idx);
            return $resultFail;
        }

        $new_summon_info['Summon_Idx'] = $summon_base_apc_data[0];
        $new_summon_info['summon_type'] = $summon_base_apc_data[1];
        $new_summon_info['summon_grade'] = $summon_base_apc_data[3];
        $new_summon_info['summon_level'] = 1;
        // $new_summon_info['summon_level'] = $summon_lv;
        $new_summon_info['summon_exp'] = 0;
        $new_summon_info['summon_status'] = 0;
        $new_summon_info['summon_equips_amulet'] = -1;
        $new_summon_info['summon_equips_ring'] = -1;
        $new_summon_info['summon_equips_belt'] = -1;
        // @NOTE : 2016.01.17 PHB MODIFY
        $new_summon_info['combine_id'] = $summon_base_apc_data[16];

        $get_summon_id = $this -> add_new_summon($db, $UID, $new_summon_info);

        if ($get_summon_id == null or $get_summon_id == 0) {
            $this -> logger -> logError('DB : ' . $GLOBALS['AccessNo'] . ', GetSummonManager::get_summon_open add_new_summon FAIL UID : ' . $UID . ', $Summon_Idx : ' . $Summon_Idx);
            return $resultFail;
        }

        $result['ResultCode'] = 100;
        $result['Summon_Idx'] = $Summon_Idx;
        $result['GET_STACK'] = 1;
        $result['GET_UNO'] = $get_summon_id;
        $result['summon_grade'] = $new_summon_info['summon_grade'];
        $result['combine_id'] = $new_summon_info['combine_id'];

        return $result;
    }

    function add_new_summon($db, $UID, $GET_SUMMON_INFO_DTO) {

        $inUID = $UID;
        $inSummon_Idx = $GET_SUMMON_INFO_DTO['Summon_Idx'];
        $inSummon_type = $GET_SUMMON_INFO_DTO['summon_type'];
        $inSummon_grade = $GET_SUMMON_INFO_DTO['summon_grade'];
        $inSummon_level = $GET_SUMMON_INFO_DTO['summon_level'];
        $inSummon_exp = $GET_SUMMON_INFO_DTO['summon_exp'];
        $inSummon_status = $GET_SUMMON_INFO_DTO['summon_status'];
        $inSummon_equips_amulet = $GET_SUMMON_INFO_DTO['summon_equips_amulet'];
        $inSummon_equips_ring = $GET_SUMMON_INFO_DTO['summon_equips_ring'];
        $inSummon_equips_belt = $GET_SUMMON_INFO_DTO['summon_equips_belt'];
        // $inSummon_stones_slot1 = $GET_SUMMON_INFO_DTO['summon_stones_slot1'];
        // $inSummon_stones_slot2 = $GET_SUMMON_INFO_DTO['summon_stones_slot2'];
        // $inSummon_stones_slot3 = $GET_SUMMON_INFO_DTO['summon_stones_slot3'];

        if (isset($GLOBALS['requestData']['Protocol'])) {
            $protocol = $GLOBALS['requestData']['Protocol'];
        } else {
            $protocol = 0;
        }

        $sql = <<<SQL
        INSERT INTO Inven_Summon(UID,Summon_Idx,summon_type,summon_grade,summon_level,summon_exp,summon_status,summon_equips_amulet,summon_equips_ring,summon_equips_belt,protocol,reg_date,update_date)
        VALUES (?,?,?,?,?,?,?,?,?,?,?,now(),now())
SQL;

        $db -> prepare($sql);
        $db -> bindValue(1, $inUID, PDO::PARAM_INT);
        $db -> bindValue(2, $inSummon_Idx, PDO::PARAM_INT);
        $db -> bindValue(3, $inSummon_type, PDO::PARAM_INT);
        $db -> bindValue(4, $inSummon_grade, PDO::PARAM_INT);
        $db -> bindValue(5, $inSummon_level, PDO::PARAM_INT);
        $db -> bindValue(6, $inSummon_exp, PDO::PARAM_INT);
        $db -> bindValue(7, $inSummon_status, PDO::PARAM_INT);
        $db -> bindValue(8, $inSummon_equips_amulet, PDO::PARAM_INT);
        $db -> bindValue(9, $inSummon_equips_ring, PDO::PARAM_INT);
        $db -> bindValue(10, $inSummon_equips_belt, PDO::PARAM_INT);
        $db -> bindValue(11, $protocol, PDO::PARAM_INT);
        $db -> execute();

        $get_stone_uno = $db -> lastInsertId();

        // 소환수 도감
        require_once './classes/SummonBookManager.php';
        $SummonBook = new SummonBookManager();
        $param['UID'] = $UID;
        $param['Summon_Idx'] = $inSummon_Idx;
        $param['summon_level'] = $inSummon_level;
        $SummonBook -> check_summon_book($db, $param);

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // HQuest :: SUMMON_GET
        require_once './classes/HQuestManager.php';
        $HQuestManager = new HQuestManager();
        $get_type = $HQuestManager -> check_hquest_type('HERO_GET');
        $value = 1;
        $HLOG['UID'] = $UID;
        $HLOG['type'] = $get_type;
        $HLOG['value'] = $value;
        $HLOG_LIST = array();
        array_push($HLOG_LIST, $HLOG);
        $HQuestManager -> set_archivemet($db, $HLOG_LIST, $UID);

        //////////////////////////////////////////////////////////////////////////////////////////
        // DQuest :: SUMMON_GET
        require_once './classes/DailyQuestManager.php';
        $DailyQuestManager = new DailyQuestManager();
        $get_type = $DailyQuestManager -> check_dquest_type("HERO_GET");
        $value = 1;
        $RS_LOG['type'] = $get_type;
        $RS_LOG['value'] = $value;
        $RS_LOG_LIST = array();
        array_push($RS_LOG_LIST, $RS_LOG);
        $DailyQuestManager -> set_daily_achivement($db, $UID, $RS_LOG_LIST);

        return $get_stone_uno;
    }

    function get_special_summon_status($db, $UID) {
        $angel_devil_status = null;

        $sql = <<<SQL
        SELECT angel_devil_status FROM Special_Item_Info WHERE UID = :UID
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        $angel_devil_status = $row['angel_devil_status'];

        return $angel_devil_status;
    }

    function set_special_summon_status($db, $UID, $rate) {
        if ($this -> get_special_summon_status($db, $UID)) {
            $sql = <<<SQL
            UPDATE Special_Item_Info SET angel_devil_status = :rate
            WHERE UID = :UID
SQL;
        } else {
            $sql = <<<SQL
            INSERT INTO Special_Item_Info (UID, angel_devil_status, reg_date)
            VALUES (:UID, :rate, now())
SQL;
        }

        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> bindValue(':rate', $rate, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!$row) {
            $this -> logger -> logError("DB : {$GLOBALS['AccessNo']}, set_special_summon_status : Qeury SELECT  FAIL UID : {$UID}, sql = {$sql}");
        }
    }

    function set_special_summon_complete($db, $UID, $summon_idx) {

        if ($this -> get_special_summon_status($db, $UID)) {
            $sql = <<<SQL
            UPDATE Special_Item_Info SET angel_devil_status = :summon_idx
            WHERE UID = :UID
SQL;
        } else {
            $sql = <<<SQL
            INSERT INTO Special_Item_Info (UID, angel_devil_status, reg_date)
            VALUES (:UID, :summon_idx, now())
SQL;
        }
        $db -> prepare($sql);
        $db -> bindValue(':summon_idx', $summon_idx, PDO::PARAM_INT);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $row = $db -> execute();

    }

    function get_new_summon_chest_data($UID, $RAND, $IDX) {
        $SCRIPT_SUMMON_BASE_DTO = null;

        for ($i = 0; $i < 10; $i++) {
            $apc_key = 'SUMMON_CHEST_' . $IDX;
            $SCRIPT_SUMMON_CHEST_DTO = apc_fetch($apc_key);

            $RESULT_DATA = explode("/", $SCRIPT_SUMMON_CHEST_DTO[1]);
            $RATE_DATA = explode("/", $SCRIPT_SUMMON_CHEST_DTO[2]);

            if (count($RESULT_DATA) != count($RATE_DATA)) {
                $this -> logger -> logError($GLOBALS['AccessNo'] . ', GetSummunManager::get_summon_chest_data count($RESULT_DATA) != count($RATE_DATA) UID : ' . $UID);
            }

            // RAND::상자에 등록 된 랜덤 아이템 검색
            $RAND_SUMMON_CHEST_IDX = $RAND -> Rate($RESULT_DATA, $RATE_DATA);

            $IDX = $RAND_SUMMON_CHEST_IDX;
            $apc_key = "SUMMON_BASE_" . $IDX;

            $SCRIPT_SUMMON_BASE_DTO = apc_fetch($apc_key);

            if ($SCRIPT_SUMMON_BASE_DTO[16] != 10000 && $SCRIPT_SUMMON_BASE_DTO[16] != 10001) {
                break;
            }
        }

        return $SCRIPT_SUMMON_BASE_DTO;
    }

    function get_angel_devil_summon_chest_data($UID, $RAND) {
        $IDX = 7901;
        $SCRIPT_SUMMON_BASE_DTO = null;

        $apc_key = 'SUMMON_CHEST_' . $IDX;
        $SCRIPT_SUMMON_CHEST_DTO = apc_fetch($apc_key);

        $RESULT_DATA = explode("/", $SCRIPT_SUMMON_CHEST_DTO[1]);
        $RATE_DATA = explode("/", $SCRIPT_SUMMON_CHEST_DTO[2]);

        if (count($RESULT_DATA) != count($RATE_DATA)) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', GetSummunManager::get_summon_chest_data count($RESULT_DATA) != count($RATE_DATA) UID : ' . $UID);
        }

        // RAND::상자에 등록 된 랜덤 아이템 검색
        $RAND_SUMMON_CHEST_IDX = $RAND -> Rate($RESULT_DATA, $RATE_DATA);
        //echo $RAND_SUMMON_CHEST_IDX;

        $IDX = $RAND_SUMMON_CHEST_IDX;
        $apc_key = "SUMMON_BASE_" . $IDX;

        $SCRIPT_SUMMON_BASE_DTO = apc_fetch($apc_key);

        return $SCRIPT_SUMMON_BASE_DTO;
    }

	/*=======================================================
	  hansw
	  function get_first_summon_chest
	  최초 영웅뽑기 함수
	  기획 데이터 SOW_SUMMON_CHEST에서 키값 7207값을 이용한다.
	=======================================================*/
	function get_first_summon_chest($db, $UID, $CHEST_LIST)
	{
		$RAND = new RandManager();

		$IDX = $CHEST_LIST;
		$apc_key = 'SUMMON_CHEST_'. $IDX;
		$Summon_Chest_Data = apc_fetch($apc_key);

		$Result_Data = explode("/", $Summon_Chest_Data[1]);
		$Rate_Data = explode("/", $Summon_Chest_Data[2]);
		$Summon_Grade_Data = explode("/", $Summon_Chest_Data[9]);

		if(count($Result_Data) != count($Rate_Data) || count($Result_Data) != count($Summon_Grade_Data))
		{
			$this -> logger -> logError($GLOBALS['AccessNo']. ', GetSummonManager::get_first_summon_chest Result Count discord UID : '. $UID);
		}

		$Rand_Summon_Chest_IDX = $RAND -> Rate($Result_Data, $Rate_Data);

		$IDX = $Rand_Summon_Chest_IDX;
		$apc_key = 'SUMMON_BASE_'. $IDX;
		$Result_Summon_Base = apc_fetch($apc_key);

		$Get_Summon_Info_Data['Summon_Idx'] = $Result_Summon_Base[0];
		$Get_Summon_Info_Data['summon_type'] = $Result_Summon_Base[1];
		$Get_Summon_Info_Data['summon_grade'] = $Result_Summon_Base[2];
		$Get_Summon_Info_Data['summon_level'] = 1;
		$Get_Summon_Info_Data['summon_exp'] = 0;
		$Get_Summon_Info_Data['summon_status'] = 0;
		$Get_Summon_Info_Data['summon_equips_amulet'] = -1;
		$Get_Summon_Info_Data['summon_equips_ring'] = -1;
		$Get_Summon_Info_Data['summon_equips_belt'] = -1;

		$get_stone_uno = $this -> add_new_summon($db, $UID, $Get_Summon_Info_Data);
		if($get_stone_uno == 0)
		{
			$this -> logger -> logError($GLOBALS['AccessNo']. ", GetSummonManager::get_first_summon_chest add_new_summon Fail UID : ". $UID);
			$ResultFail['ResultCode'] = 300;
			return $ResultFail;
		}

		//최초 영웅뽑기 수행을 하고 DB값을 수정한다.
		$sql =<<<SQL
			UPDATE Special_Item_Info SET first_summon_chest = 0 WHERE UID = :UID
SQL;
		$db -> prepare($sql);
		$db -> bindValue(':UID', $UID, PDO::PARAM_INT);
		$row = $db -> execute();
		if(!$row)
		{
			$this -> logger -> logError($GLOBALS['AccessNo']. ", GetSummonManager::get_first_summon_chest Qeury Update Fail UID : ". $UID. ", sql : ". $sql);
		}

		$Result['ResultCode'] = 100;
		$Result['GET_SUMMONS'][0] = $Get_Summon_Info_Data;

		return $Result;
	}
	//function get_first_summon_chest End

}
?>
