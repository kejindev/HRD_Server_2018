<?php
include_once './common/define.php';
include_once './lib/DataCalculate.php';

class SendDataManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

	function getLobbyData($param) {
		$userId = $param['userId'];	
		$Type = $param['Type'];	

		$data = null;
		$userNeedExp = null;
		$userMaxHeart = null;
		for ($i = 0; $i < 65; $i++ ) {
			$apc_key = "UserExp_MaxHeart_" . $i;
			$Exp_Apc = apc_fetch(trim($apc_key));
			$userNeedExp[] = $Exp_Apc[1];
			$userMaxHeart[] = $Exp_Apc[2];
		}
		// 시간 관련 내용은 디비로 옮겨서 시간의 통일성을 유지 하는게 좋을듯 하다.
		$curTime = time();
		$yesterday = date('ymd', $curTime-86399); // -24h
		$today = date('ymd', $curTime); // -24h
		$tommorow = date('ymd', $curTime+86400); // +24h
		$data["curTime"] = $curTime;

		/* 
		key List 
		google Descend_g
		apple Descend_a
		one  Descend_o

		example
		17052600,24,11960
		17052700,24,11920
		17052800,24,12890
		17052900,24,12850
		17053000,24,12910
		17053100,24,12930
		17060100,24,12840
		*/
		$descend = null;	
		$apc_key = 	"Descend_".$yesterday;
		$yes = apc_fetch($apc_key);
		$descend[] = $yes[0].'00,'.$yes[1].','.$yes[2];

		$apc_key = 	"Descend_".$today;
		$to = apc_fetch($apc_key);
		$descend[] = $to[0].'00,'.$to[1].','.$to[2];

		$apc_key = 	"Descend_".$tommorow;
		$tom = apc_fetch($apc_key);
		$descend[] = $tom[0].'00,'.$tom[1].','.$tom[2];

		$data["descendTT"] = $descend;

		$data['event'] = array();
		$event_now = date('ymdH');


		$event_Keys = array(
			EVENT_CHEST,
			EVENT_PAY_FIRST,
			EVENT_SELL_PACKAGE,
			EVENT_HOTTIME,
			EVENT_CAFE,
			EVENT_BLACKMARKET,
			EVENT_DOUBLE_WEAPONEXP,
			EVENT_RETURN_USER,
			EVENT_HALF_HEART,
			EVENT_PAY_ACCU,
			EVENT_WEBVIEW,
			EVENT_CHK_ATTENDANCE,
			EVENT_ENTER_CLIMB_REWARD,
			EVENT_CHK_ENTERCOUNT,
			EVENT_INFINITY_HEART,
			EVENT_369,
			EVENT_DOUBLE_LEAPACKAGE,
			EVENT_DAILY_CLEARCOUNT,
			EVENT_DOUBLE_ABYSS
		);

		$len = count($event_Keys);
		for ($i=0; $i<$len; $i++) {
			$event_Key = $event_Keys[$i];

			$apc_key = 	"Event_".$GLOBALS['COUNTRY'].'_'.$event_Key;

			$eventType = apc_fetch($apc_key);
		//	$data['testo'][] = $apc_key;
			if ($eventType) {
		//		$data['test'][] = $apc_key;
				$eventResult = $this->getEventData_forUser($event_Key+1, $eventType[1],$event_now);
				if ($eventResult) {
					$data['event'] = array_merge($data['event'], $eventResult);	
				}
			}
		//	$data['test'][] = $apc_key;
		}
		$data['testServer'] = $_SERVER['SERVER_ADDR'];
		
		
		switch ($GLOBALS['COUNTRY']) {
			case 'Korea':
				$data["bannerUrl"] = trim('http://14.63.196.82:3081/banners/');
				break;
			
			case 'Korea_QA':
				$data["bannerUrl"] = trim('http://14.63.196.82:1081/banners/');
				break;

			case 'China_QA':
				$data["bannerUrl"] = trim('http://106.15.0.164/banners/');
				break;
				
			default:
				break;
		}
		
		$data['loadingCount']= 9;
		$data["halloween"] = "10,25,11,06";

		$result['Data'] = $data;
		$result['ResultCode'] = 100;
		return $result;
	}

	function getEventData_forUser($eventNo,$eventCnt,$event_now) {
		$eventArray = null;
		for($eventI = $eventNo; $eventI < $eventNo+$eventCnt; $eventI++) {
			$apc_key = 	"Event_".$GLOBALS['COUNTRY'].'_'.$eventI;
			$ev = apc_fetch($apc_key);
			if ($ev) {
				if ($event_now >= $ev[1] && $event_now < $ev[4])  {
					$eventArray[] = $ev[1].','.$ev[3].','.$ev[2];
				}
			}
		}
		return $eventArray;
	}
	
	function getEventData($eventNo,$eventCnt,$event_now) {
		$eventArray = null;
		for($eventI = $eventNo; $eventI < $eventNo+$eventCnt; $eventI++) {
			$apc_key = 	"Event_".$GLOBALS['COUNTRY'].'_'.$eventI;
			$ev = apc_fetch($apc_key);
			if ($ev) {
				if ($event_now >= $ev[1] && $event_now < $ev[4])  {
					$eventArray[] = $ev[1].','.$ev[3].','.$ev[2];
				}
			}
		}
		return $eventArray;
	}

	function GetCharacInfo() {
		$data = null;

		$starti = DAILY_REWARD_START; // define 
		$endi = DAILY_REWARD_END; // define 

		$dailyRewardType = null;
		$dailyRewardAmount = null;
		for ($i = $starti; $i < $endi; $i++ ) {
			$apc_key = "DailyReward_" . $i;
			$apc_data = apc_fetch($apc_key);
			if (!$apc_data) {
				break;
			}

			if ($apc_data[1]) {
				$day_data = explode('/', $apc_data[1]);
				$dailyRewardType[] =  trim($day_data[0]);
				$dailyRewardAmount[] = $day_data[1];
			}

			if ($apc_data[2]) {
				$day_data = explode('/', $apc_data[2]);
				$dailyRewardType[] =  trim($day_data[0]);
				$dailyRewardAmount[] = $day_data[1];
			}
			
		}

		###################################################
		$starti = 0;
		$endi = 20;

		$characHiddenStartIdx = null;
		$characMinIdx = null;
		$characMaxIdx = null;
		$characPrices = null;
		$chargingCharacs = null;
	
		for ($i = $starti; $i < $endi; $i++ ) {
			$apc_key = "CharacIndex_" . $i;
			$apc_data = apc_fetch($apc_key);
			if (!$apc_data) {
				break;
			}
			if ($apc_data[0] != "" ) {
				$characMinIdx[] = $apc_data[0];
			}
			if ($apc_data[1] != "") {
				$characMaxIdx[] = $apc_data[1];
			}
			if ($apc_data[2] != "") {
				$characHiddenStartIdx[] = $apc_data[2];
			} 
			if ($apc_data[3] != "") {
				$chargingCharacs[] = $apc_data[3];
			}
		}

		$chrPrice = null;
		$resultPrice = null;
		$DataCalculate = new DataCalculate();
		$prePrice = 0;
		$p = 0;
		for ( $i=0; $i<400; $i++ ) {
			$price = $DataCalculate->GetCharacBuyPrice($i);
			if ( $price !== $prePrice ) {
				$prePrice = $price;
				$chrPrice[0] = $i;
				$chrPrice[1] = $price;
				$resultPrice[$p++] = $chrPrice;
			}
		}

		$data["characHiddenStartIdx"] = $characHiddenStartIdx;
		$data["characMinIdx"] = $characMinIdx;
		$data["characMaxIdx"] = $characMaxIdx;
		$data["characPrices"] = $resultPrice;
		$data["chargingCharacs"] = $chargingCharacs;
	
		$result['Data'] = $data;
		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResGetCharacInfo';

		return $result;
	}

	function  getDailyReward() {
		$data = null;

		$starti = DAILY_REWARD_START; // define 
		$endi = DAILY_REWARD_END; // define 

		$dailyRewardType = null;
		$dailyRewardAmount = null;
		for ($i = $starti; $i < $endi; $i++ ) {
			$apc_key = "DailyReward_" . $i;
			$apc_data = apc_fetch($apc_key);
			if (!$apc_data) {
				break;
			}

			if ($apc_data[1]) {
				$day_data = explode('/', $apc_data[1]);
				$dailyRewardType[] =  trim($day_data[0]);
				$dailyRewardAmount[] = $day_data[1];
			}

			if ($apc_data[2]) {
				$day_data = explode('/', $apc_data[2]);
				$dailyRewardType[] =  trim($day_data[0]);
				$dailyRewardAmount[] = $day_data[1];
			}
			
		}

		$data['dailyRewardType'] = null;
		$data['dailyRewardAmount'] = null;

		###################################################
		$starti = 0;
		$endi = 20;

		$characHiddenStartIdx = null;
		$characMinIdx = null;
		$characMaxIdx = null;
		$characPrices = null;
		$chargingCharacs = null;
	
		for ($i = $starti; $i < $endi; $i++ ) {
			$apc_key = "CharacIndex_" . $i;
			$apc_data = apc_fetch($apc_key);
			if (!$apc_data) {
				break;
			}
			if ($apc_data[0] != "" ) {
				$characMinIdx[] = $apc_data[0];
			}
			if ($apc_data[1] != "") {
				$characMaxIdx[] = $apc_data[1];
			}
			if ($apc_data[2] != "") {
				$characHiddenStartIdx[] = $apc_data[2];
			} 
			if ($apc_data[3] != "") {
				$chargingCharacs[] = $apc_data[3];
			}
		}

		$chrPrice = null;
		$resultPrice = null;
		$DataCalculate = new DataCalculate();
		$prePrice = 0;
		$p = 0;
		for ( $i=0; $i<400; $i++ ) {
			$price = $DataCalculate->GetCharacBuyPrice($i);
			if ( $price !== $prePrice ) {
				$prePrice = $price;
				$chrPrice[0] = $i;
				$chrPrice[1] = $price;
				$resultPrice[$p++] = $chrPrice;
			}
		}

		$data["characHiddenStartIdx"] = $characHiddenStartIdx;
		$data["characMinIdx"] = $characMinIdx;
		$data["characMaxIdx"] = $characMaxIdx;
		$data["characPrices"] = $resultPrice;
		$data["chargingCharacs"] = $chargingCharacs;
	
		$result['Data'] = $data;
		$result['ResultCode'] = 100;
		return $result;
	}
	function getWeaponInfo() {
		$data = null;

		$characHiddenStartIdx = null;
		$characMinIdx = null;
		$characMaxIdx = null;
		$characPrices = null;
		$chargingCharacs = null;

		$weaponMinIdx = null;
		$weaponMaxIdx = null;
		$weaponValue  = null;
		$weaponPowder = null; 

		$apc_key = "WeaponInform_WeaponMinIdx";
		$apc_data = apc_fetch($apc_key);
		if ($apc_data) {
//			$weaponMinIdx = str_replace("/", ",", $apc_data[1]);
			$weaponMinIdx = explode("/", $apc_data[1]);
		}

		$apc_key = "WeaponInform_WeaponMaxIdx";
		$apc_data = apc_fetch($apc_key);
		if ($apc_data) {
//			$weaponMaxIdx = str_replace("/", ",", $apc_data[1]);
			$weaponMaxIdx = explode("/", $apc_data[1]);
		}

		$apc_key = "WeaponInform_WeaponValue";
		$apc_data = apc_fetch($apc_key);
		if ($apc_data) {
			$weaponValue = explode("/", $apc_data[1]);
		}

		$apc_key = "WeaponInform_Weaponpowder";
		$apc_data = apc_fetch($apc_key);
		$weaponPowder = array();
		if ($apc_data) {
//			$weaponPowder = str_replace("/", ",", $apc_data[1]);
			$weaponPowder = explode("/", $apc_data[1]);
		}

		$data['weaponExpValue'] = 60;
		$data['weaponPowderValue'] = 50;
		$data['weaponSellValue'] = 60;

		for($i = 1; $i < 51; $i++) {
			$apc_key = "WeaponPrice_Exp_".$i;
			$apc_data = apc_fetch($apc_key);
			if (!$apc_data) {
				break;
			}
	
			$weaponPrice[] 	= $apc_data[1];
			$weaponNeedExp[] 	= $apc_data[3];
		}

		$data["weaponPowder"] 	= $weaponPowder;
		$data["weaponMinIdx"] 	= $weaponMinIdx;
		$data["weaponMaxIdx"] 	= $weaponMaxIdx;
		$data["weaponValue"] 	= $weaponValue;
		$data["weaponPrice"] 	= $weaponPrice;
		$data["weaponNeedExp"] 	= $weaponNeedExp;

		$weaponStat = null;
		for($i = 0; $i < 501; $i++) {
			$apc_key = "WeaponStat_".$i;
			$apc_data = apc_fetch($apc_key);
			if (!$apc_data) {
				continue;
			}
			$tempI = null;
			$tempI = $apc_data[0];
			$tempI .= ','.$apc_data[1];
			$tempI .= ','.$apc_data[2];
			$weaponStat[] 	= $tempI;
		}


		$data["weaponStat"] 	= $weaponStat; // ??? 없당.

		$data["weaponGradeUpPrices"]		= array(0,0,0,0,100000);	//6성진화시 300000골드

		$result['Data'] = $data;
		$result['ResultCode'] = 100;
		return $result;
	}

	function GetMagicInfo() {
		$data = null;
		for ($i = 0; $i < 7; $i++ ) {
			$apc_key = "MagicPowderValue_" . $i;
			$apc_key = "MagicPowderValue_" . $i;
			$apc_data = apc_fetch($apc_key);
			if (!$apc_data) {
				break;
			}
			$data["magicPowderValue"] = 80;
			$data["magicPowder"][] = $apc_data[1];
			$data["magicMinIdx"][] = $apc_data[2];
			$data["magicMaxIdx"][] = $apc_data[3];
		}

		$data['magicPowderValue'] = 80;

		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResGetMagicInfo';
		$result['Data'] = $data;
		return $result;
	}

	function GetCharacData() {
		$data = null;

		$apc_key = "characData25_";
		$apc_data = apc_fetch($apc_key);
		$data["characData"][] = $apc_data;

		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResGetCharacData';
		$result['Data'] = $data;
		return $result;
	}

	function GetCharacEvolveStatus() {
		$data = null;
		$apc_key = "characEvolveStatus19_";
		$apc_data = apc_fetch($apc_key);

		$data["characEvolveStatus"][] = $apc_data;
		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResGetCharacData';
		$result['Data'] = $data;
		return $result;
	}

	function GetMonData() {
		$data = null;
		$apc_key = "monData11_23_";
		$apc_data = apc_fetch($apc_key);

		$data["monData"][] = $apc_data;
		$result['ResultCode'] = 100;
		$result['Protocol'] = 'ResGetMonData';
		$result['Data'] = $data;
		return $result;
	}


}



?>

