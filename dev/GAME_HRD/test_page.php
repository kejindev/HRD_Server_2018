<?php
if ( !isset($_COOKIE['TEST_API_URL']) )
{
	$_COOKIE['TEST_API_URL'] = '';
}
if ( !isset($_COOKIE["TEST_API_JSON"]) )
{
	$_COOKIE['TEST_API_JSON'] = '';
}

?>

<html>
<head>
<title> API Test </title>
<script lang="javascript">
    function dataSubmit() {
    	
        var xmlhttp;
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp=new XMLHttpRequest();
        }
        else {// code for IE6, IE5
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        var api_url = document.getElementById('nform')['url'].value;
        xmlhttp.open("post", api_url, false);
        xmlhttp.send(document.getElementById('nform')['json'].value);
        document.getElementById("result").innerHTML= xmlhttp.responseText.split("&").join("&amp;").split( "<").join("&lt;").split(">").join("&gt;").split("\n").join("<br/>");
        return false;
    }
</script>
</head>
<body>
<h1>  Heroes Random Defence Test Page : TEST SERVER 10.7.14.39 <h1>
<h2> FOR GAME SERVER </h2>
<form id="nform" action="#" method="post" onsubmit="return dataSubmit();">
    <label> api url </label><br/>
    <input id="url" name="url" type="text" size="62" value="<?php if($_COOKIE['TEST_API_URL']){echo $_COOKIE['TEST_API_URL'];} else {echo "gateway.php";} ?>"/><br/><br />
    <label> json type parameter </label><br/>
    <textarea id="json" rows="5" cols="60"><?php echo $_COOKIE["TEST_API_JSON"] ?></textarea><br/><br />
    <input type="submit" value="Submit"/><br/><br />
</form>
<h2> Results </h2>
<pre id="result">
</pre>
</body>
</html>
