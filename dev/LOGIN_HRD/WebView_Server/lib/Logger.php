<?php
require_once './common/Singleton.php';

class Logger extends Singleton {
    private $flag;

    public static function get() {
        return Singleton::getInstance(__CLASS__);
    }

    public function __construct() {
        $this -> flag = true;
    }

    public function enable($onoff) {
        $this -> flag = $onoff;
    }

    function logError($mes) {
		$date = date('Y-m-d H:i:s');
		error_log("{$date} | {$mes}" . "\n", 3, "/home/log/WV_" . date('Ymd') . '.log');

	}

}
?>
