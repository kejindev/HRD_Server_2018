<?php
require_once "./lib/Logger.php";
require_once "./common/Config.php";
require_once "./classes/AccountManager.php";
require_once "./classes/InitManager.php";
require_once "./classes/BuyOneStoreKeyMaker.php";
require_once './lib/ParamChecker.php';

class AccountApi {

	public function __construct() {
		$this -> logger = Logger::get();

	}

	function ReqAccountLogin($param) {
		$ParamChecker = new ParamChecker();
		$base_param[] = 'idToken';
		$base_param[] = 'email';
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['Protocol'] = "ResAccountLogin";
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}
		$resultFail = array('Protocol' => 'ResAccountLogin', 'ResultCode' => 200);

		$AccountManager = new AccountManager();
		$result = $AccountManager ->AccountLogin($param);
		if ($result['ResultCode'] == 100) {
			return $result;
		} else {
			$resultFail['ResultCode'] = $result['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	function ReqAuthYKLogin($param) {
		$ParamChecker = new ParamChecker();
		$base_param = null;
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['Protocol'] = "ResAccountLogin";
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}
		$resultFail = array('Protocol' => 'ResAccountLogin', 'ResultCode' => 200);

		$AccountManager = new AccountManager();
		$result = $AccountManager ->AuthYKLogin($param);
		if ($result['ResultCode'] == 100) {
			return $result;
		} else {
			$resultFail['ResultCode'] = $result['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	function ReqInitAccount($param) {
		$resultFail = array('Protocol' => 'ResInitAccount', 'ResultCode' => 200);

		$InitManager = new InitManager();
		$result = $InitManager ->InitAccount($param);
		if ($result['ResultCode'] == 100) {
			return $result;
		} else {
			$resultFail['ResultCode'] = $result['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

	function ReqBuyOneStoreKeyMake($param) {
		$resultFail = array('Protocol' => 'ResBuyOneStoreKeyMake', 'ResultCode' => 200);

		$Manager = new BuyOneStoreKeyMaker();
		$result = $Manager ->KeyMaker($param);
		if ($result['ResultCode'] == 100) {
			return $result;
		} else {
			$resultFail['ResultCode'] = $result['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}
}
?>

