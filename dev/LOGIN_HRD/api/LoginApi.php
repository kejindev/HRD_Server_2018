<?php
require_once "./common/Config.php";
require_once "./lib/Logger.php";
require_once "./classes/AccountManager.php";
require_once './lib/ParamChecker.php';

class LoginApi {

	public function __construct() {
		$this -> logger = Logger::get();

	}

	public function ReqLogin($param) {
		$ParamChecker = new ParamChecker();
		$base_param[] = 'idToken';
		$base_param[] = 'email';
		$check_result = $ParamChecker -> param_check($param, $base_param);
		if ($check_result == 200) {
			$resultFail['Protocol'] = "ResLogin";
			$resultFail['ResultCode'] = $check_result;
			return $resultFail;
		}
		$resultFail = array('Protocol' => 'ResLogin', 'ResultCode' => 200);

		$LoginManager = new AccountManager();
		$resultLogin = $LoginManager ->Login($param);
		if ($resultLogin['ResultCode'] == 100) {
			return $resultLogin;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultLogin['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

}
?>

