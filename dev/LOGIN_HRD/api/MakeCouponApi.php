<?php
require_once "./lib/Logger.php";
require_once "./common/Config.php";
require_once "./classes/MakeCouponManager.php";

class MakeCouponApi {

	public function __construct() {
		$this -> logger = Logger::get();

	}

	public function ReqMakeCoupon($param) {
		$resultFail = array('Protocol' => 'ResMakeCoupon', 'ResultCode' => 200);

		$resultProc = makeCoupon($param);
		if ($resultProc['ResultCode'] == 100) {
			return $resultProc;
		} else//FAIL RETURN
		{
			$resultFail['ResultCode'] = $resultProc['ResultCode'];
			return $resultFail;
		}

		return $resultFail;
	}

}
?>

