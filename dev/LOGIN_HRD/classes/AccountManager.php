<?php
require_once './lib/Logger.php';
require_once './lib/GoogleAuth.php';
include_once './common/DB.php';
require_once './classes/LogDBSendManager.php';

class AccountManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

	function checkAccountGoogle($db, $param, $connectId) {
		// 구글은 항상 유니크한 connectId 가 존재 한다.
		// 단 One Store 와 Google Play Store 의 구분이 필요하다.
		// 해당 구분은 market 값을 이용하여 구분한다.
		$deviceModel	= $param['deviceModel'];
		$deviceId		= $param['deviceId'];
		$osVersion 		= $param['osVersion'];
		$market 		= $param['market'];

		if (isset($param['email'])) {
			$email 		= $param['email'];
		} else {
			$email 		= null;
		}


		$result = null;
		$result['ResultCode'] = 100;
	
		// 체크 포인트
		// connectId 와 market 값이 둘다 일치 하는 값이 있다면 이미 가입된 계정이다.
		$sql = "
			SELECT connectId, deviceId, userId FROM t_Account_User 
			WHERE connectId = :connectId AND market = :market";
		$db -> prepare($sql);
		$db -> bindValue(':connectId', $connectId, PDO::PARAM_STR);
		$db -> bindValue(':market', $market, PDO::PARAM_STR);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		

		if ($row) {
			// connectId 와 market 은 업데이트 하지 않는다.
			// 최신 정보로 해당 유저의 deviceId 정보를 업데이트 한다.

			$userId = $row['userId'];
			$sql = "
				UPDATE t_Account_User SET 
				deviceId = :deviceId,  deviceModel = :deviceModel, osVersion = :osVersion,	
				email = :email, reg_date = now() 
				WHERE userId = :userId
			";
			$db -> prepare($sql);
			$db -> bindValue(':deviceId', $deviceId, PDO::PARAM_STR);
			$db -> bindValue(':deviceModel', $deviceModel, PDO::PARAM_STR);
			$db -> bindValue(':osVersion', $osVersion, PDO::PARAM_STR);
			$db -> bindValue(':email', $email, PDO::PARAM_STR);
			$db -> bindValue(':userId', $userId, PDO::PARAM_STR);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId : '.$userId.' SQL : ' . $sql);
				$this->logger->logError(__FUNCTION__.' : FAIL ' . $deviceId);
				$this->logger->logError(__FUNCTION__.' : FAIL ' . $deviceModel);
				$this->logger->logError(__FUNCTION__.' : FAIL ' . $osVersion);
				$this->logger->logError(__FUNCTION__.' : FAIL ' . $email);
				return $resultFail;
			}
			$result['userId'] = $userId;
		} else {
			// 없는 계정이므로 신규 유저 처리를 해야 한다.
			$result['userId'] = null;
			$result['ResultCode'] = 101;
		}
		return $result;
	}

	function procLegacy_GuestId($db, $param, $legacyGuestId, $connectId) {
		$deviceModel	= $param['deviceModel'];
		$deviceId		= $param['deviceId'];
		$osVersion 		= $param['osVersion'];
		$market 		= $param['market'];

		$result = null;
		$result['ResultCode'] = 100;

		$sql = "
			SELECT deviceId, connectId, userId FROM t_Account_User WHERE connectId = :legacyGuestId AND market = :market";
		$db -> prepare($sql);
		$db -> bindValue(':legacyGuestId', $legacyGuestId, PDO::PARAM_STR);
		$db -> bindValue(':market', $market, PDO::PARAM_STR);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$userId = $row['userId'];
			$row_connectId  = $row['connectId'];

			$sql = "SELECT userId FROM t_Account_User 
			WHERE ( connectId = :connectId  or deviceId = :deviceId ) AND market = :market";
			$db -> prepare($sql);
			$db -> bindValue(':connectId', $connectId, PDO::PARAM_STR);
			$db -> bindValue(':deviceId', $deviceId, PDO::PARAM_STR);
			$db -> bindValue(':market', $market, PDO::PARAM_STR);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if (!$row) {
				// 전달된 connectId가 존재 하고, 게스트 아이디와 다른 경우 업데이트 한다. 
				$sql = "
					UPDATE t_Account_User SET connectId = :connectId,
					deviceId = :deviceId, deviceModel = :deviceModel, osVersion = :osVersion,reg_date = now() 
					WHERE userId = :userId
							   ";
				$db -> prepare($sql);
				$db -> bindValue(':connectId', $connectId, PDO::PARAM_STR);
				$db -> bindValue(':deviceId', $deviceId, PDO::PARAM_STR);
				$db -> bindValue(':deviceModel', $deviceModel, PDO::PARAM_STR);
				$db -> bindValue(':osVersion', $osVersion, PDO::PARAM_STR);
				$db -> bindValue(':userId', $userId, PDO::PARAM_STR);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.' : FAIL userId : '.$userId.' SQL : ' . $sql);
					return $resultFail;
				}
			}
		}

		return true;
	}

	function checkAccountAppStore($db, $param, $connectId) {
		$deviceModel	= $param['deviceModel'];
		$deviceId		= $param['deviceId'];
		$osVersion 		= $param['osVersion'];
		$market 		= $param['market'];
		if (isset($param['email'])) {
			$email 		= $param['email'];
		} else {
			$email 		= null;
		}


		$result = null;
		$result['ResultCode'] = 100;

		if ( is_null($connectId) || $connectId == "" ) {
			// IOS는 이경우 게스트 상태다. 
			$sql = "
				SELECT deviceId, connectId, userId FROM t_Account_User WHERE deviceId = :deviceId";
			$db -> prepare($sql);
			$db -> bindValue(':deviceId', $deviceId, PDO::PARAM_STR);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if ($row) {
				$userId = $row['userId'];
				$connectId = $row['deviceId'];
				$result['userId'] = $userId;
			} else {
				// 찾을 수 없는 데이터 라면 신규 회원이다.
				$result['userId'] = null;
				$result['ResultCode'] = 101;
			}
		} else {
			$sql = "
				SELECT connectId, deviceId, userId FROM t_Account_User 
				WHERE connectId = :connectId AND market = :market";
			$db -> prepare($sql);
			$db -> bindValue(':connectId', $connectId, PDO::PARAM_STR);
			$db -> bindValue(':market', $market, PDO::PARAM_STR);
			$db -> execute();
			$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
			if ($row) {
				$userId = $row['userId'];
				$connectId = $row['connectId'];
				// IOS 게스트가 전환 되었는지 확인하고 업데이트 
				$sql = "
					UPDATE t_Account_User SET 
					deviceId = :deviceId, deviceModel = :deviceModel, osVersion = :osVersion, email = :email, reg_date = now() 
					WHERE userId = :userId
					";
				$db -> prepare($sql);
				$db -> bindValue(':deviceId', $deviceId, PDO::PARAM_STR);
				$db -> bindValue(':deviceModel', $deviceModel, PDO::PARAM_STR);
				$db -> bindValue(':osVersion', $osVersion, PDO::PARAM_STR);
				$db -> bindValue(':email', $email, PDO::PARAM_STR);
				$db -> bindValue(':userId', $userId, PDO::PARAM_STR);
				$row = $db -> execute();
				if (!isset($row) || is_null($row) || $row == 0) {
					$this->logger->logError(__FUNCTION__.' : FAIL userId : '.$userId.' SQL : ' . $sql);
					return $resultFail;
				}
				$result['userId'] = $userId;
			} else {
				// 게스트 상태에서 첫 플랫폼 연동 로그인 을 시도했다면 여기를 타게 된다.
				// 여기에서도 찾을 수 없는 데이터 라면 신규 회원이다.
				$sql = "
					SELECT deviceId, userId FROM t_Account_User WHERE deviceId = :deviceId and market = :market";
				$db -> prepare($sql);
				$db -> bindValue(':deviceId', $deviceId, PDO::PARAM_STR);
				$db -> bindValue(':market', $market, PDO::PARAM_STR);
				$db -> execute();
				$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
				if ($row) {
					$userId = $row['userId'];
					if ( $connectId && $connectId != "") {
						$sql = "
							UPDATE t_Account_User SET 
							connectId = :connectId,	email = :email, reg_date = now() 
							WHERE userId = :userId
							";
						$db -> prepare($sql);
						$db -> bindValue(':connectId', $connectId, PDO::PARAM_STR);
						$db -> bindValue(':email', $email, PDO::PARAM_STR);
						$db -> bindValue(':userId', $userId, PDO::PARAM_STR);
						$row = $db -> execute();
						if (!isset($row) || is_null($row) || $row == 0) {
							$this->logger->logError(__FUNCTION__.' : FAIL userId : '.$userId.' SQL : ' . $sql);
							return $resultFail;
						}
					}
					$result['userId'] = $userId;
				} else {
					// 여기에서도 찾을 수 없는 데이터 라면 신규 회원이다.
					$result['userId'] = null;
					$result['ResultCode'] = 101;
				}
			}  
		}

		return $result;
	}

	function AccountLogin($param) {
		$resultFail['ResultCode'] = 300;

		$platformId 	= $param['platformId'];
        $deviceModel	= $param['deviceModel'];
        $deviceId		= $param['deviceId'];
        $osVersion 		= $param['osVersion'];
        $market 		= $param['market'];
		if (isset( $param['strGPU'] ) ){
			$strGPU 		= $param['strGPU'];
		} else {
			$strGPU 		= null;
		}

		if (isset($param['email'])) {
			$email 		= $param['email'];
		} else {
			$email 		= null;
		}

		if (isset($param['guestId'])) {
			$guestId = $param['guestId'];
		} else {
			$guestId = null;
		}

		$legacyGuestId 	= null;
		if (isset($param['legacyGuestId'])) {
			$legacyGuestId 	= $param['legacyGuestId'];
		}

		// 대문자 변환 
		$market = strtoupper($market);
        /* 		
		* server_group_no 국가간 또는 샤딩된 다른 서버군을 관리하기 위해 넣은 값
		* ex) korea google : 1, japan total : 2
		* 미사용 중임
		* $server_group_no = $param['server_group_no'];
		*/
        $server_group_no = 1;

		/*
		* Google 인증을 위해 google api 통신을 진행 하여 platformId 값을 검증 한다.
		* /lib/GoogleAuth.php 를 이용한다. 
		* 결국 최후에는 google 이외의 것들까지. 서비스 한다고 염두해 둔다.
		* platformId 마켓 별 유니크 값 
		*/

		$userId = null;
		$connectId = null;

        $db = new DB();
		switch ($market) {
			case 'PS':
			case 'OS':
				if ( strpos($GLOBALS['COUNTRY'], "China") == false ) {
					$googleAuthLib = new GoogleAuthLib();
					$connectId = $googleAuthLib->fGoogleAuth($platformId);	
				} else {
					$connectId = $deviceId;
				}

				if ($connectId == null){
					$this->logger->logError('AccountLogin : googleAuth FAIL platformId : '.$platformId);
					return $resultFail;
				}

				$checkLogin = $this->checkAccountGoogle($db, $param, $connectId);
				break;
			case 'AS':
				$connectId = $platformId;	
				if ( $legacyGuestId != null && $legacyGuestId != "") {
					$checkLogin = $this->procLegacy_GuestId($db, $param, $legacyGuestId, $connectId);
				}
				$checkLogin = $this->checkAccountAppStore($db, $param, $platformId);
				break;
			case 'YK':
				$connectId = $platformId;	
				$checkLogin = $this->checkAccountGoogle($db, $param, $connectId);
				break;
			default :
				$connectId = null;	
				break;
		}


		if ( $deviceId == "" && $connectId == "" ) {
			$this->logger->logError('AccountLogin : googleAuth FAIL market'.$market);
			$this->logger->logError('AccountLogin : googleAuth FAIL connectId : '.$connectId);
			return $resultFail;
		}

		$textureCompressType = "";

		if ($strGPU) {
			$gpuResult = $this->getGPUType($db,$strGPU);
			if ($gpuResult) {
				$textureCompressType = $gpuResult;
			}
		}

		$data['textureCompressType'] = $textureCompressType;

		$userId = $checkLogin['userId'];
		if ($userId) {

			$logDBSendManager = new LogDBSendManager();	
			$logDBSendManager->sendLogin($userId);

			// NORMAL RESPONSE RETURN ---
			$result = null;
			$result['ResultCode'] = 100;
			$result['Protocol'] = 'ResAccountLogin';

			$data['locate'] = $GLOBALS['REAL'];
			$data['userId'] = $userId;	
			$result['Data'] = $data;
			return $result;
		} 

		// 신규 유입을 경우 아래 루틴을 타게 된다.
		// NEW ACCOUNT INSERT START---
		$sql = <<<SQL
			SELECT db_11, db_12, db_13, db_14, db_15, db_16, db_17, db_18, db_19, db_20, db_21, db_22, play_last, one_last, app_last
			FROM t_Server_User_Count WHERE server_group_no = :server_group_no
SQL;
		$db -> prepare($sql);
		$db -> bindValue(':server_group_no', $server_group_no, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$this -> logger -> logError(__FUNCTION__.': FAIL connectId : ' . $connectId . ' SQL : ' . $sql);
			return $resultFail;
		}

        /*FUNCTION BASE ROUND ROBIN
         ROUNDROBIN :1
         WEIGHTED ROUND ROBIN :2
         */
		$GLOBALS['DISTRIBUTION'] = 1;

        if ($GLOBALS['DISTRIBUTION'] == 1) {
            switch ($market) {
                case 'PS' : // 구글플레이 스토어 
					$start_db 	= 11;
                    $max_db 	= 14;
					$last_no 	= $row['play_last'];
					$sql_add = "play_last";
                    break;
                case 'OS' : // 앱스토어 
					$start_db 	= 15;
                    $max_db 	= 18;
					$last_no 	= $row['one_last'];
					$sql_add = "one_last";
                    break;
                case 'AS' : // 원스토어  
					$start_db 	= 19;
                    $max_db 	= 22;
					$last_no 	= $row['app_last'];
					$sql_add = "app_last";
                    break;
                case 'YK' : // 원스토어  
					$start_db 	= 11;
                    $max_db 	= 14;
					$last_no 	= $row['play_last'];
					$sql_add = "play_last";
                    break;
                default :
                    return $resultFail;
                    break;
            }
			/* 최대 DB 정의 수 보다 클수 없으며 무조건 10 이상 두자리수 이다 */
            if ($last_no >= $max_db || $last_no <= 10) {
                $db_no = $start_db;
                $last_no = $db_no;
            } else {
                $db_no = $last_no + 1;
                $last_no = $db_no;
            }
        } else {
            // 특정 논리 DB 가중치 로직이다. 
			// 신규 서버가 생성 되었을경우 를 대비 하여 작성 하였다. 
            // weight rate to target db (  * 0.8 ? 0.5  )
			// 자동으로 작성하지 않았다. 
			switch ($market) {
				case 'PS' : 
					$temp = null;
					$temp[] = $row['db_11'] + ($row['db_11'] * 1);
					$temp[] = $row['db_12'] + ($row['db_12'] * 1);
					$temp[] = $row['db_13'] + ($row['db_13'] * 1);
					$temp[] = $row['db_14'] + ($row['db_14'] * 1);

					$low['cnt'] = $temp[0];
					$low['db_no'] = 0;

					$cnt = count($temp);
					for ($i = 0; $i < $cnt; $i++) {
						for ($j = $i + 1; $j < $cnt; $j++) {
							if ($temp[$j] < $low['cnt']) {
								$low['cnt'] = $temp[$j];
								$low['db_no'] = $j;
							}
						}
					}
					$db_no = $low['db_no'] + 11;
					$last_no = $db_no;
					$sql_add = "play_last";
					break;
				case 'OS' : 
					$temp = null;
					$temp[] = $row['db_15'] + ($row['db_15'] * 1);
					$temp[] = $row['db_16'] + ($row['db_16'] * 1);
					$temp[] = $row['db_17'] + ($row['db_17'] * 1);
					$temp[] = $row['db_18'] + ($row['db_18'] * 1);

					$low['cnt'] = $temp[0];
					$low['db_no'] = 0;

					$cnt = count($temp);
					for ($i = 0; $i < $cnt; $i++) {
						for ($j = $i + 1; $j < $cnt; $j++) {
							if ($temp[$j] < $low['cnt']) {
								$low['cnt'] = $temp[$j];
								$low['db_no'] = $j;
							}
						}
					}
					$db_no = $low['db_no'] + 15;
					$last_no = $db_no;
					$sql_add = "one_last";
					break;
				case 'AS' : 
					$temp = null;
					$temp[] = $row['db_19'] + ($row['db_19'] * 1);
					$temp[] = $row['db_20'] + ($row['db_20'] * 1);
					$temp[] = $row['db_21'] + ($row['db_21'] * 1);
					$temp[] = $row['db_22'] + ($row['db_22'] * 1);

					$low['cnt'] = $temp[0];
					$low['db_no'] = 0;

					$cnt = count($temp);
					for ($i = 0; $i < $cnt; $i++) {
						for ($j = $i + 1; $j < $cnt; $j++) {
							if ($temp[$j] < $low['cnt']) {
								$low['cnt'] = $temp[$j];
								$low['db_no'] = $j;
							}
						}
					}
					$db_no = $low['db_no'] + 19;
					$last_no = $db_no;
					$sql_add = "app_last";
					break;
				case 'YK' : 
					$temp = null;
					$temp[] = $row['db_11'] + ($row['db_11'] * 1);
					$temp[] = $row['db_12'] + ($row['db_12'] * 1);
					$temp[] = $row['db_13'] + ($row['db_13'] * 1);
					$temp[] = $row['db_14'] + ($row['db_14'] * 1);

					$low['cnt'] = $temp[0];
					$low['db_no'] = 0;

					$cnt = count($temp);
					for ($i = 0; $i < $cnt; $i++) {
						for ($j = $i + 1; $j < $cnt; $j++) {
							if ($temp[$j] < $low['cnt']) {
								$low['cnt'] = $temp[$j];
								$low['db_no'] = $j;
							}
						}
					}
					$db_no = $low['db_no'] + 11;
					$last_no = $db_no;
					$sql_add = "play_last";
					break;

				default :
					break;
			}

        }

		$sql = "INSERT INTO frdID_".$last_no." ( reg_date) VALUES ( now()) ";
        $db -> prepare($sql);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this->logger->logError(__FUNCTION__.': FAIL lastNo : ' . $last_no );
            return $resultFail;
        }
		$insertId = $db -> lastInsertId();

        $sql = "
			INSERT INTO t_Account_User (
				connectId, deviceId, db_no, server_group_no,deviceModel, osVersion, market, userId, reg_date
				) VALUES ( :connectId, :deviceId, :db_no, :server_group_no, :deviceModel, :osVersion, :market, :userId, now() ) ";
        $db -> prepare($sql);
        $db -> bindValue(':connectId', $connectId, PDO::PARAM_STR);
        $db -> bindValue(':deviceId', $deviceId, PDO::PARAM_STR);
        $db -> bindValue(':db_no', $db_no, PDO::PARAM_INT);
        $db -> bindValue(':server_group_no', $server_group_no, PDO::PARAM_INT);
        $db -> bindValue(':deviceModel', $deviceModel, PDO::PARAM_STR);
        $db -> bindValue(':osVersion', $osVersion, PDO::PARAM_STR);
        $db -> bindValue(':market', $market, PDO::PARAM_STR);
        $db -> bindValue(':userId', $insertId, PDO::PARAM_STR);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__.': FAIL  connectId : ' . $connectId . ' SQL : ' . $sql);
            return $resultFail;
        }

        $sql = "
			UPDATE t_Server_User_Count 
			SET db_".$db_no." = db_".$db_no." + 1, ".$sql_add." = :last_no 
			WHERE server_group_no = :server_group_no	";
        $db -> prepare($sql);
        $db -> bindValue(':last_no', $last_no, PDO::PARAM_INT);
        $db -> bindValue(':server_group_no', $server_group_no, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__.': FAIL connectId : ' . $connectId . ' SQL : ' . $sql);
            return $resultFail;
        }


        $result['ResultCode'] = 100;
        $result['Protocol'] = 'ResAccountLogin';
		
		$data['userId'] = $insertId;
		$data['AccountId'] = $connectId;
		$result['Data'] = $data;
        return $result;
    }
	
	function getGPUType ($db, $strGPU) {
		$sql = " SELECT * FROM frdGPULog WHERE strGPU = :strGPU ";
		$db -> prepare($sql);
		$db -> bindValue(':strGPU', $strGPU, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$sql =  " UPDATE frdGPULog SET cnt = cnt+1  WHERE strGPU = :strGPU ";
			$db -> prepare($sql);
			$db -> bindValue(':strGPU', $strGPU, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError(__FUNCTION__.' :  FAIL userId : '.$userId . ", sql : ".$sql);
				return false;
			}
		} else {
			$sql =  " INSERT INTO frdGPULog (strGPU, cnt) VALUES (:strGPU, 1) ";
			$db -> prepare($sql);
			$db -> bindValue(':strGPU', $strGPU, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError(__FUNCTION__.' :  FAIL userId : '.$userId . ", sql : ".$sql);
				return false;
			}

		}

		
		$gpuList["etc1_0_0"] = array("Mali");
		$gpuList["atc1_0_0"] = array("Adreno");
		$gpuList["pvr1_0_0"] = array("Apple", "PowerVR", "Imagination");
		$gpuList["dxt1_0_0"] = array("OpenGL", "Tegra", "Bluestacks", "NVIDIA");

		$isFind = false;
		$textureCompressType = "etc1_0_0";
		foreach ($gpuList as $key => $value) {
			foreach($value as $gpu) if(stripos($strGPU, $gpu) !== false) {
				$isFind = true;
	    		$textureCompressType = $key;
	    		break;
	    	}
	    	if ( $isFind )
	    		break;
		}

		return $textureCompressType;
	}

	function AuthYKLogin($param) {
    	$resultFail['ResultCode'] = 300;

        $plchid 	= $param['plchid'];
        $plappid	= $param['plappid'];
        $pfuid 		= $param['pfuid'];
        $token 		= $param['token'];

		$plappsecret="nKcitRHim6ttGRAV";	
		$sign_total = "plchid=".$plchid."&plappid=".$plappid."&pfuid=".$pfuid."&token=".$token."&plappsecret=".$plappsecret;
		
		$platformId = "plchid=".$plchid."&plappid=".$plappid."&pfuid=".$pfuid."&token=".$token."&sign=".md5($sign_total)."&plappsecret=".$plappsecret;

		// 구글 인증 라이브러리의 curl 함수를 이용한다. 
		$googleAuthLib = new GoogleAuthLib();
		$rResult = $googleAuthLib->fGetCurl("http://unionlogin.rorogame.com/v30/auth2/checktoken","get","$platformId");

		$sResult = json_decode($rResult,true);

		$data = null;
		$data['yk_code'] = $sResult['code'];
		$data['yk_msg'] = $sResult['msg'];

        $result['ResultCode'] = 100;
        $result['Protocol'] = 'ResAuthYKLogin';
		
		$result['Data'] = $data;

        return $result;
	}
}
?>


