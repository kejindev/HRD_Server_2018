<?php
require_once './lib/Logger.php';
include_once './common/DB.php';

class BuyOneStoreKeyMaker {
    public function __construct() {
        $this -> logger = Logger::get();
    }

	function KeyMaker($param) {
		$userId	= $param['userId'];

		$resultFail['ResultCode'] = 303;
		$result = null;
		$result['ResultCode'] = 100;

		$db = new DB();

		$sql = "INSERT INTO frdPayLoad (userId, RegDate ) VALUES (:userId, now())";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_STR);
		$row = $db -> execute();
		if (!isset($row) || is_null($row) || $row == 0) {
			$this->logger->logError(__FUNCTION__.' : FAIL userId : '.$userId.' SQL : ' . $sql);
			return $resultFail;
		}
		$PayID = $db->lastinsertID();

		$sql = "SELECT PayLoadID, userId, DATE_FORMAT(RegDate,'%m%d%k%i%s') AS regdate 
			FROM frdPayLoad WHERE PayLoadID = :PayID AND userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':PayID', $PayID, PDO::PARAM_INT);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$PID = $row["PayLoadID"];
			$userId = $row["userId"];
			$regdate = $row["regdate"];

			$PayLoadID = $PID.$userId.$regdate;
		} else {
			$this->logger->logError(__FUNCTION__.' : FAIL userId : '.$userId.' SQL : ' . $sql);
			return $resultFail;
		}

		$data = null;
		$data['PayLoadID'] = $PayLoadID;

		$result['Data'] = $data;
		$result['ResultCode'] = 100;
		return $result;
	}
}
?>

