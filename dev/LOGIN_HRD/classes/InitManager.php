<?php
require_once './lib/Logger.php';
include_once './common/DB.php';
require_once './classes/LogDBSendManager.php';

class InitManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

	function InitAccount($param) {
		$userId	= $param['userId'];

		$result = null;
		$result['ResultCode'] = 100;

		$db = new DB();

		$deviceId = null;
		$connectId = null;
		$sql = "
			SELECT connectId, deviceId, userId FROM t_Account_User 
			WHERE userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		$result["sql"] = $sql;
		if ($row) {
			$userId 	= $row['userId'];
			$connectId 	= $row['connectId'];
			$deviceId 	= $row['deviceId'];

			$curTime = time();
			$today = date('ymdHms', $curTime); 


			if ( (!is_null($connectId)) && $connectId != "") {
				$connectId = $connectId.'_b_'.$today;
			}

			if ( (!is_null($deviceId)) && $deviceId != "") {
				$deviceId = $deviceId.'_b_'.$today;
			}

			$sql = "
				UPDATE t_Account_User SET 
				connectId = :connectId, 
				deviceId = :deviceId, reg_date = now() 
				WHERE userId = :userId
			";
			$db -> prepare($sql);
			$db -> bindValue(':connectId', $connectId, PDO::PARAM_STR);
			$db -> bindValue(':deviceId', $deviceId, PDO::PARAM_STR);
			$db -> bindValue(':userId', $userId, PDO::PARAM_STR);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId : '.$userId.' SQL : ' . $sql);
				$this->logger->logError(__FUNCTION__.' : FAIL ' . $connectId);
				$this->logger->logError(__FUNCTION__.' : FAIL ' . $deviceId);
				return $resultFail;
			}
			$result['Data']['userId'] = $userId;
		} else {
			$result['Data']['userId'] = null;
			$result['ResultCode'] = 300;
		}
		return $result;
	}
}
?>

