<?php
include_once './common/log_DB.php';
require_once './lib/Logger.php';

class LogDBSendManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

    function sendLogin($userId ) {
        $db = new log_DB();
        $sql = <<<SQL
INSERT INTO login_data (userId, reg_date)  VALUES ( :userId,now())
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError(__FUNCTION__.': FAIL userId.' . $userId);
            $result['ResultCode'] = 300;
            return false;
        }

		return true;
    }

}
?>

