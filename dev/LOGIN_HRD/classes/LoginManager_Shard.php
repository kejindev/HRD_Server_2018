<?php
include_once './common/DB.php';
require_once './lib/Logger.php';
require_once './classes/AccountManager.php';
require_once './classes/SessManager.php';

class LoginManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

    public function my_login($param) {
        $resultFail['Protocol'] = 'ResLogin';
        $resultFail['ResultCode'] = 300;

        /* Common Device Info Create Ready */
        $PlayerID = $param['PlayerID'];
        $ostype = $param['ostype'];
        $marketname = $param['marketname'];
        $DeviceID = $param['DeviceID'];

        $db = new DB();
        /* JOIN CHECK PROCESS */
        $AccountManager = new AccountManager();

        $checkResult = $AccountManager -> check_join($db, $PlayerID);
        if ($checkResult['ResultCode'] == 300) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . 'LoginManager::my_login : check_join fail PlayerID :' . $PlayerID);
            return $resultFail;
        }

        if ($checkResult['ResultCode'] == 301 or $checkResult['ResultCode'] == 302) {
            // 탈퇴
            $resultFail['ResultCode'] = $checkResult['ResultCode'];
            return $resultFail;

        }

        /* CODE 1 IS JOIN START  */
        if ($checkResult['ResultCode'] == 1) {
            //  Create Account
            //echo '300';exit;

            $AccountResult = $AccountManager -> create_account($db, $param);
            if (($AccountResult['ResultCode'] != 100) or ($AccountResult['UID'] == null)) {
                return $resultFail;
            }

            $UID = $AccountResult['UID'];

            $SessManager = new SessManager();
            $Sess = $SessManager -> set_make_sessionkey($db, $UID);
            if ($Sess == null) {
                $this -> logger -> logError($GLOBALS['AccessNo'] . 'LoginManager::my_login : set_make_sessionkey : Sess Make Fail UID : ' . $UID);
                $this -> logger -> logError($GLOBALS['AccessNo'] . 'LoginManager::my_login : set_make_sessionkey : Sess Make Fail Sess : ' . $Sess);
            }

            $result = null;
            $result['Protocol'] = 'ResLogin';
            $result['ResultCode'] = 100;
            $result['Sess'] = $Sess;
            $Data = array();

            $Data['UID'] = $UID;
            $Data['IsChar'] = "0";
            $Data['status'] = 1;
            $result['Data'] = $Data;

            return $result;
        }

        // Fail UID NOT EXISIT
        $UID = $checkResult['UID'];
        if (!$UID) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . 'LoginManager::my_login : ERROR UID NULL : ' . $UID);
            $resultFail['ResultCode'] = 300;
            return $resultFail;
        }

        //DeviceID, MarketName Update
        $UpdateResult = $this -> set_user_update($db, $UID, $DeviceID, $marketname, $ostype);
        if ($UpdateResult['ResultCode'] != 100) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . 'LoginManager::my_login : set_user_update : ERROR UID NULL : ' . $UID);
            $resultFail['ResultCode'] = $UpdateResult['ResultCode'];
            return $resultFail;
        }

        $SessManager = new SessManager();
        $Sess = $SessManager -> set_make_sessionkey($db, $UID);
        if ($Sess == null) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . 'LoginManager::my_login : set_make_sessionkey : Sess Make Fail UID : ' . $UID);
            $this -> logger -> logError($GLOBALS['AccessNo'] . 'LoginManager::my_login : set_make_sessionkey : Sess Make Fail Sess : ' . $Sess);
        }

        //Make Data For Response
        $data_response = $this -> get_login_response($db, $UID);
        if ($data_response['ResultCode'] != 100) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . 'LoginManager::my_login : get_login_response : ERROR UID NULL : ' . $UID);
            $resultFail['ResultCode'] = $data_response['ResultCode'];
            return $resultFail;
        }

        /* SUCCES LOGIN RETURN */
        $result['Protocol'] = 'ResLogin';
        $result['ResultCode'] = 100;
        $Data = null;

        $Data['UID'] = $UID;
        $Data['IsChar'] = $data_response['IsChar'];
        $Data['status'] = $data_response['status'];
        $Data['unblockdate'] = $checkResult['unblockdate'];
        $result['Sess'] = $Sess;
        $result['Data'] = $Data;

        return $result;
    }

    public function set_user_update($db, $UID, $DeviceID, $marketname, $ostype) {
        $result['ResultCode'] = 300;
        //FAIL CODE

        if (!$UID) {
            $this -> logger -> logError('setUserUpdate : UID:' . $UID);
            return $result;
        }

        // Update Device Info
        $result['ResultCode'] = 100;
        //FAIL CODE
        return $result;
    }

    public function get_login_response($db, $UID) {
        $result = null;
        $result['ResultCode'] = 300;
        if (!$UID) {
            return $result;
        }
        /* INSERT T_Device_Info  TABLE INFO */
        $result['IsChar'] = $row['IsChar'];
        $result['status'] = $row['status'];
        $result['ResultCode'] = 100;
        return $result;
    }

    // @NOTE : 2015.11.19 PHB ADD : nick, space, special char
    const NG_PATTERN = "/[ค็็็็็็็็็็็็็็็©¶★∆]+/u";
    const NG_SPECIAL_CHARACTER = "/[#\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[{\}]/";
    const NG_SPACING = "/[\x20\s]/";

    private function check_ng_pattern($nick_name) {

        mb_regex_encoding("UTF-8");
        $result = FALSE;

        preg_match(self::NG_SPECIAL_CHARACTER, $nick_name, $sp_m_1);

        $special_character = isset($sp_m_1[0]);

        if ($special_character) {
            $result = TRUE;
        }

        preg_match(self::NG_SPACING, $nick_name, $sp_m_2);

        $spacing_check = isset($sp_m_2[0]);

        if ($spacing_check) {
            $result = TRUE;
        }

        preg_match(self::NG_PATTERN, $nick_name, $sp_m_3);

        $ng_pattern = isset($sp_m_3[0]);

        if ($ng_pattern) {
            $result = TRUE;
        }

        return $result;
    }

    public function nick_check($param) {

        $UID = $param['UID'];
        $nick = $param['nick'];

        $result = null;

        $result['ResultCode'] = 300;
        $result['Protocol'] = 'ResNickCheck';
        $Data = array();

        // NAME_BAN CHECK : APC DATA
        $name_ban_key = 'SOW_NAME_BAN';
        $ban_arr = apc_fetch($name_ban_key);
        $ban_cnt = count($ban_arr);

        $ban_FLAG = FALSE;

        for ($i = 0; $i < $ban_cnt; $i++) {

            // stripos($nick, $ban_arr[$i]) 값이 0(정수) 일 때 $ban_FLAG = TRUE 반환
            if (stripos($nick, $ban_arr[$i]) !== false) {
                $ban_FLAG = TRUE;
                break;
            }

        }

        // @NOTE : 2015.11.19 PHB ADD : nick, space, special char
        $ng_pattern = $this -> check_ng_pattern($nick);

        if ($ng_pattern) {
            $ban_FLAG = TRUE;
        }

        if ($ban_FLAG) {
            $result['ResultCode'] = 100;
            $Data['nick'] = $nick;
            $Data['type'] = 3;
            $result['Data'] = $Data;
            return $result;
        }

        if (!$UID) {
            return $result;
        }

        /* INSERT T_Device_Info TABLE INFO */
        $db = new DB();
        $SuperDB = new MultiDB();
        $mdb = $SuperDB -> set_multi_db();
        $mcnt = count($mdb);
        $icnt = 0;

        foreach ($mdb as $m) {

            if ($icnt == $mcnt) {
                break;
            }

            $icnt++;
            $sql = <<<SQL
            SELECT UID FROM Info_Base WHERE char_name = :char_name 
SQL;

            $m -> prepare($sql);
            $m -> bindValue(':char_name', $nick, PDO::PARAM_STR);
            $m -> execute();

            $row = $m -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);

            if (!$row) {
                $result['ResultCode'] = 100;
                $Data['nick'] = $nick;
                $Data['type'] = 0;
            } else {
                // @NOTE : 2015.12.07 PHB EDIT : type 1 -> 2
                $result['ResultCode'] = 100;
                $Data['nick'] = $nick;
                $Data['type'] = 2;
                $result['Data'] = $Data;

                return $result;
            }

        }

        $result['Data'] = $Data;

        return $result;
    }

    // DAYLY ACCESS REWARD
    public function set_daily_reward($UID) {

        $db = new DB();

        $sql = <<<SQL
SELECT DATEDIFF(LastAccess,now()) as dcnt  FROM User_Device where UID = :UID
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        if (!$row) {
            $this -> logger -> logError('set_daily_reward : SELECT FAIL User_Device UID.' . $UID);
            $this -> logger -> logError('setUserUpdate : sql ' . $sql);
            $result['ResultCode'] = 300;
            return $result;
        }

        if ($row['dcnt']) {
            $type = 1;
            $stack = 1;
        }

        $sql = <<<SQL
INSERT INTO Mail_Info (set_UID, get_UID, mail_type, mail_status, mail_set_time,
mail_post_type, mail_post_code, mail_post_stack,reg_date,update_date) 
) VALUES ( 0, :UID, 1, 0, now(),:mail_post_code, :mail_post_stack,now(),now())
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':UID', $UID, PDO::PARAM_INT);
        $db -> bindValue(':mail_post_code', $type, PDO::PARAM_INT);
        $db -> bindValue(':mail_post_stack', $stack, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError('set_daily_reward : INSERT FAIL User_Regist UID.' . $UID);
            $this -> logger -> logError('set_daily_reward : SQL.' . $sql);
            $result['ResultCode'] = 300;
            return $result;
        }

        $result['ResultCode'] = 100;
        return $result;
    }

}
?>

