<?php
/*error_reporting(E_ALL);
 ini_set("display_errors", 1);*/
require_once './lib/Performance.php';

class Controller {
    public function __construct() {
    }

    public function write_log_to_file($cat, $mes) {

        $date = date('Y-m-d H:i:s');
        error_log("{$date}| {$cat} | {$mes}" . "\n", 3, "/home/log/perfor_" . date('Ymd') . '.log');

    }

    public function process($param) {

        // If Critical Problem in Server then use  this source

		/*
		$ApiName = $param['Protocol'];
		if ($param['Ver'] == null) {
			$ReturnName = $ApiName;
			$result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
			$result['ResultCode'] = 0; //FAIL CODE
			return $result;
		}
		*/

        $Perfor = new Performance();
        $Perfor -> timer_start('process');

        $ApiName = $param['Protocol'];

        $ApiResult = null;

        /* Ready for API Send */
        if (isset($param['Data'])) {
            // Data For Using
            $Data = $param['Data'];
            $RequestData = $param['Data'];
        } else {
            $result = null;
            $ReturnName = $ApiName;
            $result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
            $result['ResultCode'] = 1500;
            //FAIL CODE
            return $result;
        }

		if (!isset($param['Type'])) {// Not Combine Type  Error
			$result = null;
			$ReturnName = $ApiName;
			$result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
			$result['ResultCode'] = 1902;
			//FAIL CODE
			return $result;
		} else {
			$market = $param['Type'];
			switch ($market) {
				case 'AS' :
					// Apple Store
					$SerVer = $GLOBALS['AS_Ver'];
					break;
				case 'PS' :
					// Play Store 
					$SerVer = $GLOBALS['PS_Ver'];
					break;
				case 'OS' :
					// One Store
					$SerVer = $GLOBALS['OS_Ver'];
					break;
				case 'YK' :
					// One Store
					$SerVer = $GLOBALS['OS_Ver'];
					break;
				default :
					// FAIL CODE
					$result = null;
					$ReturnName = $ApiName;
					$result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
					$result['ResultCode'] = 1902;
					return $result;
					break;
			}

            // Version Check Logic
            $Ver = $param['Ver'];
            // Receive Client Version
			/*
            if ((version_compare($Ver, $SerVer)) < 0) {
                $result = null;
                $ReturnName = $ApiName;
                $result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
                $result['ResultCode'] = 1901;
                //FAIL CODE
                return $result;
            }
			*/

        }
		/*
        if (!isset($RequestData['idToken'])) {
            $result = null;
            $ReturnName = $ApiName;
            $result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
            $result['ResultCode'] = 1501;
            //FAIL CODE
            return $result;
        }
		*/

        // API CALL
        switch ($ApiName) {

            case 'ReqCheckPassUser' :
                include_once "./api/LoginApi.php";
                $newLogin = new LoginApi();
                $ApiResult = $newLogin -> CheckPassUser($RequestData);
                break;

            case 'ReqAccountLogin' :
                include_once "./api/AccountApi.php";
                $newLogin = new AccountApi();
				$RequestData['market'] = $param['Type'];
                $ApiResult = $newLogin -> ReqAccountLogin($RequestData);
                break;
            case 'ReqAuthYKLogin' :
                include_once "./api/AccountApi.php";
                $newLogin = new AccountApi();
				$RequestData['market'] = $param['Type'];
                $ApiResult = $newLogin -> ReqAuthYKLogin($RequestData);
                break;

            case 'ReqResetUserData' :
                include_once "./api/AccountApi.php";
                $newLogin = new AccountApi();
				$RequestData['market'] = $param['Type'];
                $ApiResult = $newLogin -> ReqInitAccount($RequestData);
                break;

            case 'ReqBuyOneStoreKeyMake' :
                include_once "./api/AccountApi.php";
                $newLogin = new AccountApi();
				$RequestData['market'] = $param['Type'];
                $ApiResult = $newLogin -> ReqBuyOneStoreKeyMake($RequestData);
                break;

            default :
                $result['ResultCode'] = 1900;
                //FAIL CODE
                $ReturnName = $ApiName;
                $result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
                return $result;
        }

        $per_log = $Perfor -> timer_stop('process');
        Controller::write_log_to_file($ApiName, $per_log['process']);
        return $ApiResult;
    }

}
