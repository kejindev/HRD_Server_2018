<?php

/**
 *
 * Enter description here ...
 * @author Administrator
 *
 */
class JsonParserTest {
	public function __construct() {
	}

	public function isJson($data) {
		return true;
	}

	public function encode($data) {
		$resultData = json_encode($data);
//		$resultData = $this -> json_encode2($data);
		$resultError = json_last_error();

		if ($resultError != JSON_ERROR_NONE) {
			return false;
		} else {
			return $resultData;
		}

	}

	public function getallheader() {

		$headers = '';

		foreach ($_SERVER as $name => $value) {

			if (substr($name, 0, 5) == 'HTTP_') {
				$headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}

		}
		return $headers;
	}

	public function parser($data) {

		$resultData = json_decode($data);
		$resultError = json_last_error();

		if ($resultError != JSON_ERROR_NONE) {
			return false;
		} else {
			$returnArray = $this -> convertionArray($resultData);
			return $returnArray;
		}
	}

	public function convertionArray($obj) {

		$resultArray = array();

		if (!is_object($obj) && !is_array($obj)) {
			return $obj;
		}

		if (is_object($obj)) {

			foreach ($obj as $key => $value) {

				if (count($value) > 0) {
					$value = $this -> convertionArray($value);
				} else {

					if (isset($value))
						settype($value, 'array');

				}

				if (isset($value))
					$resultArray[$key] = $value;

			}

			return $resultArray;

		} else {

			foreach ($obj as $key => $value) {

				if (count($value) > 0) {
					$value = $this -> convertionArray($value);
				} else {

					if (isset($value))
						settype($value, 'array');

				}

				if (isset($value))
					$resultArray[$key] = $value;

			}

		}
		return $resultArray;
	}

	public function json_encode2($data) {

		// if (gettype($data) == 'integer' or gettype($data) == 'double') {
		//    $data = (string)$data;
		// }

		switch (gettype($data)) {
			case 'boolean' :
				return $data ? 'true' : 'false';
			case 'integer' :
				settype($data, "string");
				return $data;
			case 'double' :
				settype($data, "string");
				return $data;
			case 'string' :
				return '"' . strtr($data, array('\\' => '\\\\', '"' => '\\"')) . '"';
			case 'array' :
				$rel = false;
				// relative array?
				$key = array_keys($data);
				foreach ($key as $v) {
					if (!is_int($v)) {
						$rel = true;
						break;
					}
				}

				$arr = array();

				foreach ($data as $k => $v) {
					$arr[] = ($rel ? '"' . strtr($k, array('\\' => '\\\\', '"' => '\\"')) . '":' : '') . $this -> json_encode2($v);
				}

				return $rel ? '{' . join(',', $arr) . '}' : '[' . join(',', $arr) . ']';

			default :
				return 'null';
		}
	}

	public function encrypt($key, $iv, $value) {

		$padSize = 16 - (strlen($value) % 16);
		$value = $value . str_repeat(chr($padSize), $padSize);
		$output = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $value, MCRYPT_MODE_CBC, $iv);

		/*
        $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $padding = $block - (strlen($text) % $block);
        $text .= str_repeat(chr($padding), $padding);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $text, MCRYPT_MODE_CBC, $iv);
		*/

		return base64_encode($output);
	}

	public function decrypt($key, $iv, $value) {

		/*
		$aDigitNum = intval( log10($value));

		$returnStr ;

		$zeroCount = 16 - $aDigitNum - 1;

		for ( $idx = 0; $idx<$zeroCount; $idx++ ) {
			$returnStr .= '0';
		}
		$returnStr .= $value;

		$output = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($value), MCRYPT_MODE_CBC, $iv);

		return $output;
		*/
		// $key = JsonParserTest::hex2bin($key);
		// $iv = JsonParserTest::hex2bin($iv);

		$value = base64_decode($value);

		// $output = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $value, MCRYPT_MODE_CBC, str_repeat(chr(0),16));
		$output = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $value, MCRYPT_MODE_CBC, $iv);
		$valueLen = strlen($output);

		if ($valueLen % 16 > 0) {
			$output = "";
		}

		$padSize = ord($output[$valueLen - 1]);

		if (($padSize < 1) || ($padSize > 16)) {
			$output = "";
		}

		for ($i = 0; $i < $padSize; $i++) {

			if (ord($output[$valueLen - $i - 1]) != $padSize) {
				$output = "";
			}

		}

		$output = substr($output, 0, $valueLen - $padSize);

		return $output;
	}

	public function hex2bin($hexdata) {

		$bindata = "";

		for ($i = 0; $i < strlen($hexdata); $i += 2) {
			$bindata .= chr(hexdec(substr($hexdata, $i, 2)));
		}

		return $bindata;
	}

	public function ordchk($str) {

		$cnt = strlen($str);

		for ($i = 0; $i < $cnt; $i++) {

			if (ord($str[$i]) > 127) {
				return true;
			}

		}

		return false;
	}

}
