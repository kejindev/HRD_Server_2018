<?php

require_once './common/JsonParserTest.php';
require_once './common/FunctionManager.php';
require_once './common/DB.php';

$JsonParser = new JsonParser();

$db = new DB();

$FunctionManager = new FunctionManager();

//print_r($argv);

$json_data = $JsonParser -> parser($argv[1]);

//var_dump($json_data);exit;

switch ($json_data['case_name']) {

    case 'del_item' :
        $del_result = $FunctionManager -> del_item($json_data, $db);
        if ($del_result['ResultCode'] != 100) {
            echo "del_item fail";
            echo "\n";
        }

        break;

    default :
        break;
}

echo "backup end";
echo "\n";
exit ;
?>