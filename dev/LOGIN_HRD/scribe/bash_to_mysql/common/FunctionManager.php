<?php

class FunctionManager {

    function del_item($json_data, $db) {
        $resultFail['ResultCode'] = 200;
        
        $sql = <<<SQL
        INSERT INTO del_item
        (UID, ItemID, Item_Idx, item_exp, item_option_values, item_stone_slot, reg_date, backup_date)
        VALUES (:UID, :ItemID, :Item_Idx, :item_exp, :item_option_values, :item_stone_slot, :reg_date, now())
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':UID', $json_data['UID'], PDO::PARAM_INT);
        $db -> bindValue(':ItemID', $json_data['ItemID'], PDO::PARAM_INT);
        $db -> bindValue(':Item_Idx', $json_data['Item_Idx'], PDO::PARAM_INT);
        $db -> bindValue(':item_exp', $json_data['item_exp'], PDO::PARAM_INT);
        $db -> bindValue(':item_option_values', $json_data['item_option_values'], PDO::PARAM_INT);
        $db -> bindValue(':item_stone_slot', $json_data['item_stone_slot'], PDO::PARAM_INT);
        $db -> bindValue(':reg_date', $json_data['reg_date'], PDO::PARAM_INT);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            return $resultFail;
        }
        
        $result['ResultCode'] = 100;
        
        return $result;
    }

}
?>