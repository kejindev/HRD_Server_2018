<?php

$GLOBALS['THRIFT_ROOT'] = dirname(__FILE__);

include_once $GLOBALS['THRIFT_ROOT'] . '/packages/scribe/scribe.php';
include_once $GLOBALS['THRIFT_ROOT'] . '/transport/TSocket.php';
include_once $GLOBALS['THRIFT_ROOT'] . '/transport/TFramedTransport.php';
include_once $GLOBALS['THRIFT_ROOT'] . '/protocol/TBinaryProtocol.php';

class Log {

    public static function write($category, $message) {
        try {
            $entry = new LogEntry(array('category' => $category, 'message' => $message));
            $messages = array($entry);

            $socket = new TSocket('172.31.25.235', 1463, true);
            $transport = new TFramedTransport($socket);
            $protocol = new TBinaryProtocol($transport, false, false);
            $scribe_client = new scribeClient($protocol, $protocol);

            $transport->open();
            $scribe_client->Log($messages);
            $transport->close();
            return true;
        } catch (Exception $e) {
        }
        return false;
    }

}
