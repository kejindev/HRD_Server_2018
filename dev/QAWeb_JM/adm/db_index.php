<?php
$sub_menu = "999100";
include_once('./_common.php');

$g5['title'] = '빌더기능 메인';
include_once ('./admin.head.php');
?>
<div class="well well-sm">
        <strong>DB백업기능</strong><br /><br />
		현재 연결된 계정에 DB를 전체적으로 백업을 시작합니다.
</div>
<div class="text-center"><a href="<?php echo G5_ADMIN_URL;?>/db_backup.php" class="btn btn-primary">DB 백업</a></div>
<?php
include_once ('./admin.tail.php');
?>