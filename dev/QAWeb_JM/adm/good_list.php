<?php
$sub_menu = "200210";
include_once('./_common.php');
$listall = '<a href="'.$_SERVER['SCRIPT_NAME'].'" class="btn btn-default"><i class="fa fa-caret-square-o-down"></i> 전체목록</a>';

$g5['title'] = '추천인관리';
include_once ('./admin.head.php');
$colspan = 6;

?>

<div class="well">
<?php echo $listall ?> 추천인에게 포인트가 지급된 회원은 버튼이 작동하지 않습니다.
</div>

<form name="fpointlist" id="fpointlist" method="post" action="./good_list_update.php" onsubmit="return fpointlist_submit(this);">
<input type="hidden" name="sst" value="<?php echo $sst ?>">
<input type="hidden" name="sod" value="<?php echo $sod ?>">
<input type="hidden" name="sfl" value="<?php echo $sfl ?>">
<input type="hidden" name="stx" value="<?php echo $stx ?>">
<input type="hidden" name="page" value="<?php echo $page ?>">
<input type="hidden" name="token" value="">

<div class="table-responsive">
    <table class="table table-bordered table-striped">
    <caption><?php echo $g5['title']; ?> 목록</caption>
    <thead>
    <tr>
        <th scope="col">가입순서</th>
        <th scope="col">날짜</th>
        <th scope="col">추천받은 아이디</th>
        <th scope="col">추천적립금 지급</th>
        <th scope="col">추천한 회원</th>
        <th scope="col">추천한회원(가입자) 지급</th>
        <th scope="col">추천인SNS목록</th>
    </tr>
    </thead>
    <tbody>
    <?php
	$sql_common = " from {$g5['member_table']} ";
	$sql_search = " where (1) and mb_recommend <> '' ";

	$sql = " select count(*) as cnt {$sql_common} {$sql_search} ";
	$row = sql_fetch($sql);
	$total_count = $row['cnt'];

	$rows = $config['cf_page_rows'];
	$total_page  = ceil($total_count / $rows);  // 전체 페이지 계산
	if ($page < 1) $page = 1; // 페이지가 없으면 첫 페이지 (1 페이지)
	$from_record = ($page - 1) * $rows; // 시작 열을 구함
	$sql = "SELECT mb_no, mb_datetime, mb_name, mb_recommend, mb_id, mb_1, mb_2, mb_3 {$sql_common} {$sql_search} ORDER BY mb_datetime desc LIMIT {$from_record}, {$rows} ";
	$result = sql_query($sql);
        
	for ($i=0; $row=sql_fetch_array($result); $i++) {
    ?>
    <tr>
	    <td><?php echo $row['mb_no']?></td>
        <td><?php echo $row['mb_datetime']?></td>
        <td><?php echo $row['mb_recommend']?></td>
		<!--버튼 클릭시 추천받은회원에게 해당 포인트 지급-->
        <td>
			<?php
			$row2 = sql_fetch("select * from {$g5['point_table']} where mb_id = '{$row['mb_recommend']}' and po_rel_table = '@member' and po_rel_id = '{$row['mb_recommend']}' and po_rel_action = '{$row['mb_id']} 추천' ");
			if (!$row2['po_id']) {
			?>
			<div class="btn-group">
			<button type="button" class="btn btn-default btn-sm">추천인 포인트 지급</button>
			<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				<span class="caret"></span>
				<span class="sr-only">Toggle Dropdown</span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<?php 
				for ($i=10000; $i<=100000; $i=$i+10000) {
					echo "<li><a href='good_list_update.php?mb_id={$row['mb_id']}&point={$i}'>".number_format($i)."P 지급</a></li>";
				}
				?>
			</ul>
			<?php } else { ?>
				<a href="" class="btn btn-default btn-sm" disabled>추천인 포인트 <?php echo number_format($row2['po_point']); ?> 지급됨</a>
			<?php } ?>
			</div>

		</td>
        <td><?php echo $row['mb_id']?></td>
		<!--버튼 클릭시 추천인에게 해당 포인트 지급-->
        <td>
			<?php
			$row2 = sql_fetch("select * from {$g5['point_table']} where mb_id = '{$row['mb_id']}' and po_rel_table = '@member' and po_rel_id = '{$row['mb_id']}' and po_rel_action = '{$row['mb_id']} 스타셀럽' ");
			if (!$row2['po_id']) {
			?>
			<div class="btn-group">
			<button type="button" class="btn btn-default btn-sm">스타셀러 등업 포인트 지급</button>
			<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				<span class="caret"></span>
				<span class="sr-only">Toggle Dropdown</span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<?php 
				for ($i=10000; $i<=100000; $i=$i+10000) {
					echo "<li><a href='good_list_update2.php?mb_id={$row['mb_id']}&point={$i}'>".number_format($i)."P 지급</a></li>";
				}
				?>
			</ul>
			<?php } else { ?>
				<a href="" class="btn btn-default btn-sm" disabled>스타셀럽 등업 <?php echo number_format($row2['po_point']); ?> 지급됨</a>
			<?php } ?>
			</div>

			<?php
			$row2 = sql_fetch("select * from {$g5['point_table']} where mb_id = '{$row['mb_id']}' and po_rel_table = '@member' and po_rel_id = '{$row['mb_id']}' and po_rel_action = '{$row['mb_id']} 일반셀럽' ");
			if (!$row2['po_id']) {
			?>
			<div class="btn-group">
			<button type="button" class="btn btn-default btn-sm">일반셀럽 등업 포인트 지급</button>
			<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				<span class="caret"></span>
				<span class="sr-only">Toggle Dropdown</span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<?php 
				for ($i=10000; $i<=100000; $i=$i+10000) {
					echo "<li><a href='good_list_update3.php?mb_id={$row['mb_id']}&point={$i}'>".number_format($i)."P 지급</a></li>";
				}
				?>
			</ul>
			<?php } else { ?>
				<a href="" class="btn btn-default btn-sm" disabled>일반셀럽 등업 <?php echo number_format($row2['po_point']); ?> 지급됨</a>
			<?php } ?>
			</div>
		</td>
        <td>
		<div class="btn-group">
		   <button type="button" class="btn btn-default btn-sm"><?php echo $row['mb_id']?> SNS 목록</button>
		   <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
		    <span class="caret"></span>
			<span class="sr-only">Toggle Dropdown</span>
		   </button>
		   
		   <ul class="dropdown-menu" role="menu">
		      <!--추천인의 회원여분필드 1,2,3,4 값 클릭시 이동-->
			  <?php if ($row['mb_2']) { ?>
		      <li><a href="<?php echo $row['mb_2'] ?>" target="_blank">인스타그램</a></li>
			  <?php } ?>
			  <?php if ($row['mb_1']) { ?>
			  <li><a href="<?php echo $row['mb_1'] ?>" target="_blank">블로그</a></li>
			  <?php } ?>
			  <?php if ($row['mb_3']) { ?>
			  <li><a href="<?php echo $row['mb_3'] ?>" target="_blank">페이스북</a></li>
			  <?php } ?>
			  <?php if ($row['mb_4']) { ?>
			  <li><a href="<?php echo $row['mb_4'] ?>" target="_blank">그외SNS</a></li>
			  <?php } ?>
		   </ul>
		</div>
		</td>
		</td>
    </tr>

    <?php
    }

    if ($i == 0)
        echo '<tr><td colspan="'.$colspan.'" class="text-center">자료가 없습니다.</td></tr>';
    ?>
    </tbody>
    </table>
</div>

</form>

<?php echo get_paging(G5_IS_MOBILE ? $config['cf_mobile_pages'] : $config['cf_write_pages'], $page, $total_page, "{$_SERVER['SCRIPT_NAME']}?$qstr&amp;page="); ?>

<?php
include_once ('./admin.tail.php');
?>
