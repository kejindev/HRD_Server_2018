<?php
$sub_menu = "200210";
include_once('./_common.php');

auth_check($auth[$sub_menu], 'r');

//check_admin_token();

$mb = get_member($_GET['mb_id']);
if (!$mb) alert("회원아이디 {$_GET['mb_id']} 는 존재하지 않습니다.");
$point = $_GET['point'];

$mb2 = get_member($mb['mb_recommend']);
if (!$mb2) alert("추천인 {$mb['mb_recommend']} 는 존재하지 않습니다.");

$d = insert_point($mb['mb_recommend'], $point, $mb['mb_id'].'의 추천인', '@member', $mb['mb_recommend'], $mb['mb_id'].' 추천');

alert("적립금 ".number_format($point)." 가 지급되었습니다.", $_SERVER['HTTP_REFERER']);