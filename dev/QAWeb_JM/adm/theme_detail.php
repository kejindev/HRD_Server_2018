<?php
$sub_menu = "100280";
include_once('./_common.php');

if ($is_admin != 'super')
    die('최고관리자만 접근 가능합니다.');

$theme = trim($_POST['theme']);
$theme_dir = get_theme_dir();

if(!in_array($theme, $theme_dir))
    die('<div class="alert alert-danger" role="alert">선택하신 테마가 존재하지 않습니다.</div>');

$info = get_theme_info($theme);

if($info['screenshot'])
    $screenshot = '<img src="'.$info['screenshot'].'" alt="'.$name.'">';
else
    $screenshot = '<img src="'.G5_ADMIN_URL.'/img/theme_img.jpg" alt="">';

$name = get_text($info['theme_name']);
if($info['theme_uri']) {
    $name = '<a href="'.set_http($info['theme_uri']).'" target="_blank" class="thdt_home">'.$name.'</a>';
}

$maker = get_text($info['maker']);
if($info['maker_uri']) {
    $maker = '<a href="'.set_http($info['maker_uri']).'" target="_blank" class="thdt_home">'.$maker.'</a>';
}

$license = get_text($info['license']);
if($info['license_uri']) {
    $license = '<a href="'.set_http($info['license_uri']).'" target="_blank" class="thdt_home">'.$license.'</a>';
}
?>
<style>
.modal-body img {width:100%; height:auto}
</style>

<div class="modal fade in" id="Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" style="display: block;">
<div class="modal-dialog">
	<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="window.location.reload();">×</button>
        <span class="modal-title"><strong><?php echo $name; ?></strong></span>
      </div>
    <div class="modal-body">
    <?php echo $screenshot; ?>
	<table class="table table-striped table-bordered" style="margin-top:15px;">
            <tr>
                <th scope="row">Version</th>
                <td><?php echo get_text($info['version']); ?></td>
            </tr>
            <tr>
                <th scope="row">Maker</th>
                <td><?php echo $maker; ?></td>
            </tr>
            <tr>
                <th scope="row">License</th>
                <td><?php echo $license; ?></td>
            </tr>
        </table>

	<div class="modal-footer">
        <div class="well well-sm text-left"><?php echo get_text($info['detail']); ?></div>
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="window.location.reload();"><i class="fa fa-times-circle"></i> Close</button>
      </div>
    </div>
</div>