<?php
$sub_menu = "100280";
define('_THEME_PREVIEW_', true);
include_once('./_common.php');

$theme_dir = get_theme_dir();

if(!$theme || !in_array($theme, $theme_dir))
    alert_close('테마가 존재하지 않거나 올바르지 않습니다.');

$info = get_theme_info($theme);

$arr_mode = array('index', 'list', 'view');
$mode = substr(strip_tags($_GET['mode']), 0, 20);
if(!in_array($mode, $arr_mode))
    $mode = 'index';

$qstr_index  = '&amp;mode=index';
$qstr_list   = '&amp;mode=list';
$qstr_view   = '&amp;mode=view';
$qstr_device = '&amp;mode='.$mode.'&amp;device='.(G5_IS_MOBILE ? 'pc' : 'mobile');

$sql = " select bo_table, wr_parent from {$g5['board_new_table']} order by bn_id desc limit 1 ";
$row = sql_fetch($sql);
$bo_table = $row['bo_table'];
$board = sql_fetch(" select * from {$g5['board_table']} where bo_table = '$bo_table' ");
$write_table = $g5['write_prefix'] . $bo_table;

// theme.config.php 미리보기 게시판 스킨이 설정돼 있다면
$tconfig = get_theme_config_value($theme, 'set_default_skin, preview_board_skin, preview_mobile_board_skin');
if($mode == 'list' || $mode == 'view') {
    if($tconfig['preview_board_skin'])
        $board['bo_skin'] = preg_match('#^theme/.+$#', $tconfig['preview_board_skin']) ? $tconfig['preview_board_skin'] : 'theme/'.$tconfig['preview_board_skin'];

    if($tconfig['preview_mobile_board_skin'])
        $board['bo_mobile_skin'] = preg_match('#^theme/.+$#', $tconfig['preview_mobile_board_skin']) ? $tconfig['preview_mobile_board_skin'] : 'theme/'.$tconfig['preview_mobile_board_skin'];
}

// 스킨경로
if (G5_IS_MOBILE) {
    $board_skin_path    = get_skin_path('board', $board['bo_mobile_skin']);
    $board_skin_url     = get_skin_url('board', $board['bo_mobile_skin']);
    $member_skin_path   = get_skin_path('member', $config['cf_mobile_member_skin']);
    $member_skin_url    = get_skin_url('member', $config['cf_mobile_member_skin']);
    $new_skin_path      = get_skin_path('new', $config['cf_mobile_new_skin']);
    $new_skin_url       = get_skin_url('new', $config['cf_mobile_new_skin']);
    $search_skin_path   = get_skin_path('search', $config['cf_mobile_search_skin']);
    $search_skin_url    = get_skin_url('search', $config['cf_mobile_search_skin']);
    $connect_skin_path  = get_skin_path('connect', $config['cf_mobile_connect_skin']);
    $connect_skin_url   = get_skin_url('connect', $config['cf_mobile_connect_skin']);
    $faq_skin_path      = get_skin_path('faq', $config['cf_mobile_faq_skin']);
    $faq_skin_url       = get_skin_url('faq', $config['cf_mobile_faq_skin']);
} else {
    $board_skin_path    = get_skin_path('board', $board['bo_skin']);
    $board_skin_url     = get_skin_url('board', $board['bo_skin']);
    $member_skin_path   = get_skin_path('member', $config['cf_member_skin']);
    $member_skin_url    = get_skin_url('member', $config['cf_member_skin']);
    $new_skin_path      = get_skin_path('new', $config['cf_new_skin']);
    $new_skin_url       = get_skin_url('new', $config['cf_new_skin']);
    $search_skin_path   = get_skin_path('search', $config['cf_search_skin']);
    $search_skin_url    = get_skin_url('search', $config['cf_search_skin']);
    $connect_skin_path  = get_skin_path('connect', $config['cf_connect_skin']);
    $connect_skin_url   = get_skin_url('connect', $config['cf_connect_skin']);
    $faq_skin_path      = get_skin_path('faq', $config['cf_faq_skin']);
    $faq_skin_url       = get_skin_url('faq', $config['cf_faq_skin']);
}

$conf = sql_fetch(" select cf_theme from {$g5['config_table']} ");
$name = get_text($info['theme_name']);
if($conf['cf_theme'] != $theme) {
    if($tconfig['set_default_skin'])
        $set_default_skin = 'true';
    else
        $set_default_skin = 'false';

    $btn_active = '<button type="button" class="theme_sl theme_active" data-theme="'.$theme.'" '.'data-name="'.$name.'" data-set_default_skin="'.$set_default_skin.'" style="border:0; background:none;">테마적용</button>';
} else {
    $btn_active = '적용중';
}

$g5['title'] = get_text($info['theme_name']).' 테마 미리보기';
require_once(G5_PATH.'/head.sub.php');
?>

<script src="<?php echo G5_ADMIN_URL; ?>/theme.js"></script>
<style>
.title {
    padding:  0px 0 0 30px;
    float: left;
}

.titleimg {
    text-align:center;
    margin: 0 0 12px 0;
}

.title h2 {
    padding:  0;
    margin:  0 0 0 8px;
    font-size: 15px;
    color: #ddd;
}

.title h3 a {
    color: #fff;
    font-weight: bold;
}

.title h3 a:hover {
    color: #ddd;
    font-weight: bold;
	text-decoration: none;
}


.top-right {
    z-index: 1;
    margin: 40px 15px 0 0;
    float: right;
}


span.download {
    margin-right: 10px;
    font-size: 13px;
    font-weight: bold;
    padding: 8px 24px;
    border: 0;
    font-family: Arial, sans-serif;
    color: #fff;
    background: #2e8ece;
    text-decoration: none;
    text-shadow: none;
}

 a.close-button-right {
    font-size: 13px;
    font-weight: bold;
    padding: 8px 15px;
    border: 0;
    font-family: Arial, sans-serif;
    color: #fff;
    background: #3a718a;
    text-decoration: none;
}

#wrapper {
    -webkit-transition: height 240ms ease, width 240ms ease, margin 240ms ease;
    -moz-transition: height 240ms ease, width 240ms ease, margin 240ms ease;
    -ms-transition: height 240ms ease, width 240ms ease, margin 240ms ease;
    -o-transition: height 240ms ease, width 240ms ease, margin 240ms ease;
    transition: height 240ms ease, width 240ms ease, margin 240ms ease;
    -webkit-box-shadow: 0 0 12px 1px #000;
    -moz-box-shadow: 0 0 12px 1px #000;
    box-shadow: 0 0 12px 1px #000;
    -webkit-transform: translate3d(0,  0,  0);
    position: relative;
    background-color: #FFF;
    width: 100%;
    height: 100%;
    display: block;
    margin: 0 auto;
}

#wrapper iframe {
    width: 100%;
    height: 100%
}
.header {
    background: #295f77;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    height: 100px;
    z-index: 100;
    overflow: hidden;
}
body>section {
    position: fixed;
    top: 100px;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 3;
}
#devices {
    z-index: 1;
    float:right;
    margin: 28px 20px 0 0;
}
#devices a {
    background: #1b5069;
    text-decoration: none;
    border: 1px solid #12445d;
    border-right-width: 0;
    float: left;
    display: block;
    padding: 7px 15px;
    color: #fff;
    outline: none;
}
#devices a:hover {
    background: #12445d;
}
#devices a.active, #devices a:active {
    background: #12445d;
    outline: none;
}
#devices a.active span, #devices a:active span {
    opacity: 1;
}
#devices span {
    text-align: center;
    display: block;
    width: 40px;
    height: 30px;
    text-indent: -9999px;
    opacity: .75;
}
@media only screen and (max-width: 768px) {
    
    .top-right {
        margin-top: 20px;
    }
    
    #devices {
        margin-top: 0px;
        margin-right: 15px;
    }
}
</style>
<script>
// Responsive Bookmarklet namespace
window.resbook = {};

(function(rb) {
    // Private var & methods
    var d = document,
        w = window,
        url = d.URL,
        title = d.title,
        wrapper = null,
        devices = null,
        close = null,
        keyboard = null,
        refreshBtn = null,
        body = null,
        size = null,
        auto = true,
        isResized = false,
        isAnimated = false,
        sizes = {
            smartphonePortrait: [320, 480],
            smartphoneLandscape: [480, 320],
            tabletLandscape: [1024, 768],
            tabletPortrait: [768, 1024],
            auto: 'auto'
        },
        refreshCss = function(disable){
            var ifrm = d.querySelector('#wrapper iframe');
            ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;
            var b = ifrm.document.querySelector('body');
            if(disable){
                var el = ifrm.document.getElementById('cssrefresh');
                if(el){
                    el.parentNode.removeChild(el);
                    b.classList.remove('cssrefresh');
                }
            } else {
                
                var t = ifrm.document.createTextNode("(function(){var script=document.createElement('script');script.setAttribute('src','http://responsive.victorcoulon.fr/assets/js/cssrefresh.js');script.setAttribute('id','cssrefresh');var head=document.getElementsByTagName('head');head[0].appendChild(script)})()"),
                    s = ifrm.document.createElement("script");
                b.classList.add('cssrefresh');
                s.appendChild(t);
                ifrm.document.body.appendChild(s);
            }
        },
        resize = function(w,h,f) {
            w = w || wrapper.clientWidth;
            h = h || wrapper.clientHeight;
            size.innerHTML = w + 'x' + h;
        },
        setPosition = function(wh,t,cl){
            var width = (wh == 'auto') ? w.innerWidth : wh[0],
                height = (wh == 'auto') ? w.innerHeight : wh[1],
                style = 'width:'+width+'px;height:'+height+'px;margin-top:20px;';

            if (typeof(width) == 'undefined' || typeof(height) == 'undefined') return false;

            style += (wh === 'auto') ? 'margin-top:0;' : '';
            wrapper.setAttribute('style',style);
            wrapper.setAttribute('data-device',cl);
            body.setAttribute('style','min-height:'+height+'px;min-width:'+width+'px;');
            resize(width,height);
            if(wh === 'auto' && !t){
                isResized = false;
                setTimeout(function(){
                    wrapper.setAttribute('style','');
                    body.setAttribute('style','');
                    isAnimated = false;
                }, 260);
            } else {
                isAnimated = false;
            }
        },
        readyElement = function(id,callback){
          var interval = setInterval(function(){
            if(d.getElementById(id)){
              callback(d.getElementById(id));
              clearInterval(interval);
            }
          },60);
        };

    // === Public methods ====
    /**
     * Change url and the document title with pushState method
     * @param  {string} u   url of the new page
     * @param  {title} t title of the new page
     */
    rb.changeUrl = function (u,t){
       
        if(history.pushState) {
            try {
                history.pushState({},  "New Page", u);
            }
            catch (e) {}
        }
        if(refreshBtn.classList.contains('active')){
            refreshCss();
        } else {
            refreshCss(true);
        }
    };

    // "document ready"
    readyElement('wrapper', function(){

        // Set var cache
        wrapper = d.getElementById('wrapper');
        devices = d.getElementById('devices');
        size = d.getElementById('size');
        close = d.querySelector('.close a');
        keyboard = d.querySelector('.keyboard a');
       // refreshBtn = d.querySelector('.cssrefresh a');
        body = d.querySelector('body');

        // Detect webkit browser
        if(window.chrome || (window.getComputedStyle && !window.globalStorage && !window.opera)){
            //wrapper.setAttribute('scrolling','no');
        }

        // Disabled css refresh if we are not on server environment
        if(w.location.protocol !== 'http:'){
          //  refreshBtn.setAttribute('style','display:none');
        }

        // Events
        [].forEach.call(document.querySelectorAll('#devices a'), function(el) {
          el.addEventListener('click', function(e) {

            [].forEach.call(document.querySelectorAll('#devices a'), function(el) {
                el.classList.remove('active');
            });

            e.preventDefault();
            e.stopPropagation();
            var self = this;

            if((self.classList.contains('auto') && isResized === false) || isAnimated === true)
                return false;

            isAnimated = true;
            if(isResized === false){
                isResized = true;
                setPosition(sizes.auto,true);
            }
     
            setTimeout(function(){
                self.classList.add('active');
                if(self.classList.contains('smartphone-portrait')){
                    setPosition(sizes.smartphonePortrait, false,'smartphonePortrait');
                } else if(self.classList.contains('smartphone-landscape')){
                    setPosition(sizes.smartphoneLandscape, false, 'smartphoneLandscape');
                } else if(self.classList.contains('tablet-portrait')){
                    setPosition(sizes.tabletPortrait, false, 'tabletPortrait');
                } else if(self.classList.contains('tablet-landscape')){
                    setPosition(sizes.tabletLandscape, false, 'tabletLandscape');
                } else if(self.classList.contains('auto')){
                    setPosition(sizes.auto, false, 'auto');
                }
            }, 10);
            
          });
        });


        w.addEventListener('resize', function(){
            resize();
        }, false);


        resize();
        size.style.minWidth = 0;

    });

})(resbook);
</script>
<header class="header">
        <div class="title">
            <div class="titleimg">
			<h3><a href="./theme_preview.php?theme=<?php echo $theme.$qstr_index; ?>"><?php echo get_text($info['theme_name']); ?></a></h3>
            </div>
            
            <h2 class="hidden-xs">테마 미리보기 화면</h2>
        </div>

        <div class="top-right"> 
            <span class="download" title="Download the Day Theme"><?php echo $btn_active; ?></span> 
            <a href="<?php echo G5_ADMIN_URL?>/theme.php" target="_top" title="Hide the demo bar" class="close-button-right">X</a>
        </div>
            
        <div id="devices">
			<a href="#" class="auto active tooltip-top" title="Desktop"><i class="fa fa-television fa-2x"></i></a>
            <a href="#" class="tablet-portrait tooltip-top" title="Tablet Portrait"><i class="fa fa-tablet fa-2x "></i></a>
            <a href="#" class="tablet-landscape tooltip-top" title="Tablet Landscape"><i class="fa fa-tablet fa-2x fa-rotate-90"></i></a>
            <a href="#" class="smartphone-portrait tooltip-top" title="Mobile Phone Portrait"><i class="fa fa-mobile fa-2x"></i></a>
            <a href="#" class="smartphone-landscape tooltip-top" title="Mobile Phone Landscape" style="border-right-width:1px;"><i class="fa fa-mobile fa-2x fa-rotate-90"></i></a>
        </div>
                
        <span id="size" style="display: none; min-width: 0px;">1920x479</span> 
    
</header>
    
<section>
<div id="wrapper" style="" data-device="auto">
 <iframe src="<?php echo G5_ADMIN_URL?>/theme_preview2.php?theme=<?php echo get_text($info['theme_name']); ?>"></iframe>
</div>
</section>

<?php
require_once(G5_PATH.'/tail.sub.php');
?>