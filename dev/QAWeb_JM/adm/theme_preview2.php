<?php
define('_THEME_PREVIEW_', true);
include_once('./_common.php');
?>

<section id="preview_content">
    <?php
    switch($mode) {
        case 'list':
            include(G5_BBS_PATH.'/board.php');
            break;
        case 'view':
            $wr_id = $row['wr_parent'];
            $write = sql_fetch(" select * from $write_table where wr_id = '$wr_id' ");
            include(G5_BBS_PATH.'/board.php');
            break;
        default:
            include(G5_PATH.'/index.php');
            break;
    }
    ?>
</section>