<?php
	
	include_once('../common.php');
	include_once(G5_PATH.'/head.php');
	include_once('../define.php');

	$title_category = "보유 재화 현황";
	include_once('../title_category.php');

	if ( empty($exceptOver) )
 	   $exceptOver = "0";
?>


<!-- $('#셀렉트박스').change(function(){ 
    alert(this.name)
});
	$('#country').ready(function(){
	     $("select option[value='#country']").attr("selected", true);
 }); -->

<script>
	$("#country").val("Korea_QA").prop("selected", true);
</script>



<div class="well well-sm">
    <form id="fsearch" name="fsearch" class="form-inline" method="get">

        <div class="form-group">
        	<div class="col-md-11.5 col-md-offset-0.5">
        		<li> DB 조건  -> </li>
	            <select name="country" id="country" class="form-control">
	            	<?php 
	        			for ( $i=0; $i<count($arr_country); $i++ ) {
	        				$id = $arr_country[$i]['id'];
	        				echo "<option value=".$id.(($id==$country)?" selected>":">").$arr_country[$i]['name']."</option>";
	        			}
	        		?>
	            </select>

	            <select name="platform" id="platform" class="form-control">
	            	<?php 
	            		for ( $i=0; $i<count($arr_platform); $i++ )
	        				echo "<option value=".$i.(($platform==$i)?" selected>":">").$arr_platform[$i]."</option>";
	        		?>
	            </select>

	            <select name="search_db_count" id="search_db_count" class="form-control">
	                <?php 
	            		for ( $i=0; $i<count($arr_searchRange); $i++ )
	        				echo "<option value=".$i.(($search_db_count==$i)?" selected>":">").$arr_searchRange[$i]."</option>";
	        		?>
	            </select>
	        </div>
	        <p></p>
	        <div class="col-md-11.5 col-md-offset-0.5">
	        	<li> 유저 범위 -> </li>
	        	<select name="minUserRank" id="minUserRank" class="form-control">
	        		<?php 
	        			for ( $i=1; $i<count($arr_userRankName); $i++ )
	        				echo "<option value=".$i.(($minUserRank==$i)?" selected>":">").$arr_userRankName[$i]."</option>";
	        		?>
	                
	            </select>
	            <select name="minUserTier" id="minUserTier" class="form-control">
	                <?php 
	        			for ( $i=1; $i<count($arr_userTierName); $i++ )
	        				echo "<option value=".$i.(($minUserTier==$i)?" selected>":">").$arr_userTierName[$i]."</option>";
	        		?>
	            </select>

	            <label> ~ </label>

	            <select name="maxUserRank" id="maxUserRank" class="form-control">
	        		<?php 
	        			for ( $i=1; $i<count($arr_userRankName); $i++ )
	        				echo "<option value=".$i.(($maxUserRank==$i)?" selected>":">").$arr_userRankName[$i]."</option>";
	        		?>
	                
	            </select>
	            <select name="maxUserTier" id="maxUserTier" class="form-control">
	                <?php 
	        			for ( $i=1; $i<count($arr_userTierName); $i++ )
	        				echo "<option value=".$i.(($maxUserTier==$i)?" selected>":">").$arr_userTierName[$i]."</option>";
	        		?>
	            </select>
		    </div>
		    <p></p>
		   	<div class="row">
			    <div class="col-md-6 col-md-offset-0.5">
		        	<li> 재화 </li>
		        </div>
		    </div>

	        	<select name="type_resource" id="type_resource" class="form-control">
	        		<?php 
	        			for ( $i=0; $i<count($arr_resource); $i++ )
	        				echo "<option value=".$i.(($type_resource==$i)?" selected>":">").$arr_resource[$i]['name']."</option>";
	        		?>
	                
	            </select>
	            &nbsp&nbsp&nbsp&nbsp최대 검색 범위 :
	           	<input type='text' size='6' maxlength='10' name="exceptOver" value=<?php echo $exceptOver?> id="exceptOver" onkeypress="if (event.keyCode<48|| event.keyCode>57) event.returnValue=false;" style='IME-MODE:disabled;' required="" class="required form-control" placeholder="0 = 모든 범위 적용"> 

	        <p></p>
	        <div class="input-group">
	            
	       		<!-- <div class="row"> -->
			    <div class="col-md-11.5 col-md-offset-1">
		            <span class="input-group-btn">
		                <button type="submit" class="btn btn-primary">적용</button>
		            </span>
		 <!--        </div> -->
		    </div>
		    </div>

        </div>

        
    </form>

</div>
<br>


<br>


<div class="alert alert-info">
	<strong><?php 
		if ( empty($arr_resource[$type_resource]['name']) )
			echo "조건을 선택하여 '적용'버튼을 눌러주세요.";
		else
			echo $arr_resource[$type_resource]['name']." 보유 현황";
		?></strong>
</div>

<?php
    $dbIdxs = array();
    $storeIds = array();
    if ( $platform >= count($arr_platform)-1 ) {
    	$storeIds[] = 0;
    	$storeIds[] = 1;
    	$storeIds[] = 2;
    }
    else {
    	$storeIds[] = $platform;
    }
   
    foreach ( $storeIds as $storeId )
    	$dbIdxs[] = 800 + (int)$storeId;
    
    if ( $search_db_count >= 1 ) {
    	foreach ( $storeIds as $storeId )
    		$dbIdxs[] = 500 + (int)$storeId;
    }
    if ( $search_db_count >= 2 ) {
    	foreach ( $storeIds as $storeId ) {
    		$dbIdxs[] = 200 + (int)$storeId;
    		$dbIdxs[] = 700 + (int)$storeId;
    	}
    }

    $multidb = new MultiDB();
    $dbs = array();
    foreach ( $dbIdxs as $dbIdx )
    	$dbs[] = $multidb->get_game_db($country, $dbIdx);

    
    $minUserLevel = 1+((int)$minUserRank-1)*5 + (int)$minUserTier-1;
    $maxUserLevel = 1+((int)$maxUserRank-1)*5 + (int)$maxUserTier-1;
    $resourceColName = $arr_resource[$type_resource]['colName'];
    $resourceTableName = $arr_resource[$type_resource]['table'];

    $arr_vals = array();
    $arr_userCount = array();
    $arr_avrgVal = array();
    $arr_minVal = array();
    $arr_maxVal = array();
    $arr_userLevels = array();
    $max_userCount=0;
    $max_resource=0;
    $max_avrgResource=0;
    for ($i=0; $i<80; $i++)
    	$arr_minVal[$i] = 999999999999;

    foreach ( $dbs as $db ) {
    	$exceptOver = (int)$exceptOver;
    	if ( $resourceTableName == 'frdUserData' ) {
    		$sql = " SELECT userLevel, $resourceColName  FROM frdUserData as ud
    				 WHERE userLevel >= $minUserLevel AND userLevel <= $maxUserLevel AND update_date != '0000-00-00 00:00:00'";
    		if ( $exceptOver > 0 )
	    		$sql .= " AND $resourceColName <= $exceptOver";
    	}
    	else {
    		$resouceColCondName = $arr_resource[$type_resource]['condColName'];
    		$itemIdx = $arr_resource[$type_resource]['itemIdx'];
    		$sql = " SELECT ud.userLevel, at.$resourceColName  FROM frdUserData as ud
    			INNER JOIN $resourceTableName as at
    			ON ud.userId = at.userId
    			WHERE ud.userLevel >= $minUserLevel AND ud.userLevel <= $maxUserLevel AND ud.update_date != '0000-00-00 00:00:00'
    			AND at.$resouceColCondName = $itemIdx";
    		if ( $exceptOver > 0 )
	    		$sql .= " AND at.$resourceColName <= $exceptOver";
    	}
    	
    	$sql .= " ORDER BY ud.userLevel ASC ";

        $db -> prepare($sql);
        $db -> execute();
        while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
        	$userLevel = $row['userLevel'];
        	$val = (int)$row[$resourceColName];
        	$arr_vals[$userLevel][] = $val;
        	if ( $val < $arr_minVal[$userLevel] )
        		$arr_minVal[$userLevel] = $val;
        	if ( $val > $arr_maxVal[$userLevel] ) {
        		$arr_maxVal[$userLevel] = $val;
        		if ( $val > $max_resource )
        			$max_resource = $val;
        	}
        	$arr_avrgVal[$userLevel] += $val;
            $arr_userCount[$userLevel] = (int)$arr_userCount[$userLevel] + 1;
        }
    }

    for ( $key=0; $key<80; $key++ ) {
    	if ( empty($arr_avrgVal[$key]) )
    		continue;

    	$arr_userLevels[] = $key;
    //	echo "key=".$key.", ".$arr_avrgVal[$key].", ".$arr_userCount[$key]."<br>";
    	$val = $arr_userCount[$key];
    	$avrgVal = round( $arr_avrgVal[$key] / $val );
    	$arr_avrgVal[$key] = $avrgVal;
    	if ( $avrgVal > $max_avrgResource )
    		$max_avrgResource = $avrgVal;
    	if ( $val > $max_userCount )
    		$max_userCount = $val;
    }
?>


<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>


<strong>

<label id="curLevel">현재 선택된 유저계급: 없음</label><br>
<font id="avrg_resources" color="#1460C0"></font><br>
<font id="max_resources" color="#7B92A2"></font><br>
<font id="userCount" color="#51A651"></font><br>
<br>
</strong><br>

<div id="myfirstchart" style="height: 250px;"></div>



<script>
	var arr_vals = <?php echo json_encode($arr_vals)?>;
	var arr_userCount = <?php echo json_encode($arr_userCount)?>;
	var arr_avrgVal = <?php echo json_encode($arr_avrgVal)?>;
	var arr_maxVal = <?php echo json_encode($arr_maxVal)?>;
	var arr_minVal = <?php echo json_encode($arr_minVal)?>;
	var arr_userLevels = <?php echo json_encode($arr_userLevels)?>;
	var max_avrgResource = <?php echo $max_avrgResource?>;

	var mainGraph = new Morris.Line({
		hoverCallback: function(index, options, content) {
			var userLevel = arr_userLevels[index];
			var rank = Math.floor((userLevel-1)/5)+1;
			var tier = (userLevel-1)%5+1;
			document.getElementById('curLevel').innerText = 		"현재 마우스 오버된 유저계급: "+arr_userRankName[rank]+tier;
			document.getElementById('max_resources').innerText =  	"최대 개인 보유량 :"+arr_maxVal[userLevel];
			document.getElementById('avrg_resources').innerText = 	"평균 자원량 	: "+arr_avrgVal[userLevel];
			document.getElementById('userCount').innerText = 		"유저 수 	    : "+arr_userCount[userLevel];

			return(content);
	        // var data = options.data[index];
	        // $(".morris-hover").html('<div>Custom label: ' + data.label + '</div>');
	    },
		 // hoverCallback: function(index, options, content) {
		 // 	var data = options.data[index];
   //      	//data.label
   //      	$(".morris-hover").html('<div>Custom label: ' + 'TT' + '</div>');
	  //   	return(content);
	  //   },
		element: 'myfirstchart',
		<?php 
			$str = 'data: [ ';
			foreach ( $arr_avrgVal as $key=>$value ) {
				$str .= '{ level: '.$key.', value: '.($value/$max_avrgResource).', maxVal: '.($arr_maxVal[$key]/$max_resource).', userCount: '.($arr_userCount[$key]/$max_userCount).' },';
			}
			$str = trim($str, ",") . "], ";
			echo $str;
		?>
	  
		xkey: 'level',
		ykeys: ['value', 'maxVal', 'userCount'],
		labels: ['평균 자원량/'+<?php echo round($max_avrgResource)?>, '최대 개인 보유량/'+<?php echo $max_resource?>, '유저 수/'+<?php echo $max_userCount?>],
		parseTime: false

	}).on('click', function(idx, row) {

		var INTERVAL_COUNT = 50;
		var userLevel = arr_userLevels[idx];

		var minVal = Math.sqrt(arr_minVal[userLevel]);
		var maxVal = Math.sqrt(arr_maxVal[userLevel]);

		var unitVal = (maxVal-minVal)/INTERVAL_COUNT;
	//	alert("Min-"+minVal+", Max-"+maxVal+", Unit-"+unitVal+", maxVal="+arr_maxVal[userLevel]);

		var interval_valCounts = new Array();
		var interval_sqrtVals = new Array();
		for ( var i=0; i<INTERVAL_COUNT; i++ ) {
			interval_valCounts[i] = 0;
			interval_sqrtVals[i] = (minVal + unitVal*i);
		}
		
		var len = arr_vals[userLevel].length;
	    for ( var i=0; i<len; i++ ) {
	    	var val = arr_vals[userLevel][i];
	    	var intervalVal = Math.sqrt(val);
	    	
	    	var st = 0;
	    	var end = INTERVAL_COUNT-1;

	    	var mid = Math.ceil(INTERVAL_COUNT/2);
	    	while(true) {
		    	if ( intervalVal > interval_sqrtVals[mid] ) {
		    		st = mid;
		    		mid = Math.ceil((end+mid)/2);
		    		if ( st == mid )
		    			break;
		    		continue;
		    	}
		    	else {
		    		end = mid;
		    		mid = Math.ceil((mid+st)/2);
		    		if ( mid == end ) {
		    			mid--;
		    			break;
		    		}
		    		continue;
		    	}
		    }
		    interval_valCounts[mid]++;
	    }

		var logStr = "";
		var graph_data = [];
		for ( var i=0; i<INTERVAL_COUNT; i++ ) {
			var x = interval_sqrtVals[i];
			graph_data.push({'stddvt': Math.round(x*x)+'↑', 'value' : interval_valCounts[i]});

		//	var nxtX = interval_sqrtVals[i+1];
		//	graph_data.push({'stddvt': Math.round(x*x)+'~'+Math.round(nxtX*nxtX), 'value' : interval_valCounts[i]});
			logStr += "("+Math.round(x*x)+", "+interval_valCounts[i]+")";
		}


	//	alert(logStr);
		normalDistributionGraph.setData(graph_data);


		


		var rank = Math.floor((userLevel-1)/5)+1;
		var tier = (userLevel-1)%5+1;
	
		var text = $('#chartTitleText').text();
		$('#chartTitleText').text(arr_userRankName[rank]+tier+" 자원 현황");

	});



	// for(i = 0; i < mainGraph.data.length; i++) {
	// 	mainGraph.data[i].handlers['hover'].push(function(i){
	// 		$('#morrisdetails-item .morris-hover-row-label').text("FF"+mainGraph.data[i].label);
	// 		$('#morrisdetails-item .morris-hover-point').text("FFF"+mainGraph.data[i].value);
	// 	});
	// }
	
</script>

<div class="alert alert-info">
	<strong id="chartTitleText" >
		 자원 분포를 확인할 레벨대를 위 그래프에서 클릭해주세요.
	</strong>
</div>
<div id="mysecondchart" style="height: 250px;"></div>

<script>
	var isShowingGraph = <?php 
		if ( empty($arr_resource[$type_resource]['name']) )
			echo 0;
		else
			echo 1;
	?>;

	if ( isShowingGraph == 1 ) {

		var data = [];
		for ( var i=0; i<1; i++ ) {
			
			data[i] = { "stddvt": i, "value": 0 };
		}
		var normalDistributionGraph = new Morris.Line({
			// hoverCallback: function(index, options, content) {
		 //        var data = options.data[index];
		 //        $(".morris-hover").html('<div>Custom label: '+data.label+'</div>');
		 //    },
		    // xLabelFormat: function (x) {
		    //     return x;
		    // },
			element: 'mysecondchart',
			data: data,
		  	
			xkey: 'stddvt',
			ykeys: ['value'],
			labels: ['유저 수'],

			parseTime: false,
		});
	}

</script>


<?php
	include_once(G5_PATH.'/tail.php');
?>

