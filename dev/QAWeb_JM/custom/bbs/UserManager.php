<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
include_once ('../common.php');
include_once ('../define.php');

$preDir = getcwd();
chdir(PATH_GAMESERVER);
require_once './classes/LobbyEventManager.php';
chdir($preDir);

function debug_to_console($data) {
    if (is_array($data) || is_object($data)) {
        echo("<script>console.log('PHP: " . json_encode($data) . "');</script>");
    } else {
        echo("<script>console.log('PHP: " . $data . "');</script>");
    }
}

class UserManager {
    public function __construct() {

    }

    public function ProcessUpdateResource(&$db, &$row, $userId, $sql, $memo) {

        if (!isset($row) || is_null($row) || $row == 0 || $row == false) {
            echo $db -> errorInfo();
            error_log('Fail!!! Fail!!!!  userId : ' . $userId . ",\nsql : " . $sql."\nMemo : ".$memo);
            return false;
        }

        $date = date('Y-m-d H:i:s');
        $dbHost = $GLOBALS['DB_HOST'];
        error_log("{$date}| {$dbHost} {$userId} | {$sql}" . "\nmemo = ".$memo."\n", 3, "/home/log/" . date('Ymd') . '.log');
        return true;

    }

    function send_mail_insert($country, $userId, $item_idx, $item_count) {

        $multidb = new MultiDB();
        $db = $multidb -> get_game_db($country, $userId);

        $sql = "INSERT INTO frdUserPost
            (recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
        $sql .= "($userId, 0, $item_idx, $item_count, DATE_ADD(now(), INTERVAL 7 DAY ), now()) ";  
        $db -> prepare($sql);
        $row = $db -> execute();
        $success = $this->ProcessUpdateResource($db, $row, $userId, $sql, $memo);
        if ( false == $success )
            echo "<script>alert(\" 우편 발송이 실패 하였습니다.\\n전송 DB = $dbIdxStr \");</script>";
        else
            echo "<script>alert(\" 우편 발송이 완료 되었습니다. \");</script>";

    }

    public function change_gold_update($country, $userId, $change_gold, $memo) {
        $multidb = new MultiDB();
        $db = $multidb -> get_game_db($country, $userId);
        $dbHost = $GLOBALS['DB_HOST'];
        error_log("{$country} {$userId} {$dbHost}\n", 3, "/home/log/" . date('Ymd') . '.log');
        $sql = " UPDATE frdUserData SET gold = $change_gold WHERE userId = $userId ";
        $db -> prepare($sql);
        $row = $db -> execute();
        if ( false == $this->ProcessUpdateResource($db, $row, $userId, $sql, $memo) )
            echo "<script>alert(\" 업데이트가 실패 하였습니다. \");</script>";
        else
            echo "<script>alert(\" 업데이트가 완료 되었습니다. \");</script>";
    }

    public function change_jewel_update($country, $userId, $change_jewel, $memo) {
        $multidb = new MultiDB();
        $db = $multidb -> get_game_db($country, $userId);
        $sql = "UPDATE frdUserData SET jewel = $change_jewel WHERE userId = $userId";
        $db -> prepare($sql);
        $row = $db -> execute();
        return $this->ProcessUpdateResource($db, $row, $userId, $sql, $memo);
    }

    public function user_kick($country, $userId) {

        $date = date('Y-m-d H:i:s');

        $multidb = new MultiDB();
        $db = $multidb -> get_game_db($country, $userId);
        $sql = " UPDATE frdUserData SET lastLoginDate = now() WHERE userId = :userId";
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $row = $db -> execute();
        if ( false == $this->ProcessUpdateResource($db, $row, $userId, $sql, $memo)) {
            echo "<script>alert(\" 처리가 실패 하였습니다. \");</script>";
            return false;
        }
        echo "<script>alert(\" 처리되었습니다. \");</script>";
        return true;
    }

    function change_accuSkillPoint($country, $userId, $change_accuSkillPoint, $memo) {
        $multidb = new MultiDB();
        $db = $multidb -> get_game_db($country, $userId);
        $sql = " UPDATE frdUserData  SET accuSkillPoint = $change_accuSkillPoint WHERE userId = $userId ";
        $db -> prepare($sql);
        $row = $db -> execute();
        return $this->ProcessUpdateResource($db, $row, $userId, $sql, $memo);
    }

    function change_skillPoint($country, $userId, $change_skillPoint, $memo) {
        $multidb = new MultiDB();
        $db = $multidb -> get_game_db($country, $userId);
        $sql = " UPDATE frdUserData  SET skillPoint = $change_skillPoint WHERE userId = $userId ";
        $db -> prepare($sql);
        $row = $db -> execute();
        return $this->ProcessUpdateResource($db, $row, $userId, $sql, $memo);
    }
    function change_powder($country, $userId, $change_powder, $memo) {
        $multidb = new MultiDB();
        $db = $multidb -> get_game_db($country, $userId);
        $sql = " UPDATE frdUserData  SET powder = $change_powder WHERE userId = $userId ";
        $db -> prepare($sql);
        $row = $db -> execute();
        return $this->ProcessUpdateResource($db, $row, $userId, $sql, $memo);
    }

    function change_maxHeart($country, $userId, $change_maxHeart, $memo) {
        $multidb = new MultiDB();
        $db = $multidb -> get_game_db($country, $userId);
        $sql = " UPDATE frdUserData  SET maxHeart = $change_maxHeart WHERE userId = $userId ";
        $db -> prepare($sql);
        $row = $db -> execute();
        return $this->ProcessUpdateResource($db, $row, $userId, $sql, $memo);
    }

    function all_send($country, $platform, $item_idx, $item_count, $memo, $arr_platform) {
        $preDir = getcwd();
        $dbIdxs = array();
        $storeIds = array();
        
        if ( $platform >= count($arr_platform)-1 ) {
            $storeIds[] = 0;
            $storeIds[] = 1;
            $storeIds[] = 2;
        }
        else {
            $storeIds[] = $platform;
        }
        $dbIdxStr = "";
        foreach ( $storeIds as $storeId ) {
            $dbIdxs[] = 800 + (int)$storeId;
            $dbIdxs[] = 500 + (int)$storeId;
            $dbIdxs[] = 200 + (int)$storeId;
            $dbIdxs[] = 700 + (int)$storeId;
        }
        $multidb = new MultiDB();
        $dbs = array();
        foreach ( $dbIdxs as $dbIdx ) {
            $dbs[] = $multidb->get_game_db($country, $dbIdx);
            $dbIdxStr .= $dbIdx.", ";
        }

        $querySuccess = true;
        foreach ( $dbs as $db ) {
            $sql = "INSERT INTO frdUserPost
                (recvUserId, sendUserId, type, count, expire_time, reg_date)
                SELECT userId, 0, $item_idx, $item_count, DATE_ADD(now(), INTERVAL 7 DAY ), now() 
                FROM frdUserData";
            $db -> prepare($sql);
            $row = $db -> execute();
            $success = $this->ProcessUpdateResource($db, $row, 0, $sql, $memo);
            if ( false == $success )
                $querySuccess = false;

            sleep(1);
        }

        if ( false == $querySuccess )
            echo "<script>alert(\" 우편 발송이 실패 하였습니다. \");</script>";
        else
            echo "<script>alert(\" 우편 발송이 완료 되었습니다.\\n전송 DB = $dbIdxStr \");</script>";

        return;
    }

    function package_send($country, $userId, $item_group, $memo) {
        $multidb = new MultiDB();
        $db = $multidb -> get_game_db($country, $userId);
        $data = explode(',',$item_group);

        $item_group = $data[0];
        $itemIds = count($data) > 1 ? $data[1] : "";
        $itemCnts = count($data) > 2 ? $data[2] : "";

        if ($itemIds == "") {
            if  ($item_group == "buy_monthly_card0") {
                $sql = "UPDATE frdMonthlyCard SET startTime = now(), duration = 30, recvCnt = 0 WHERE userId = $userId";
                $db -> prepare($sql);
                $row = $db -> execute();
                $this->ProcessUpdateResource($db, $row, $userId, $sql, $memo);
                
                $LobbyEventManager = new LobbyEventManager();
                if ( false == $LobbyEventManager->sendReaPackReward($db, $userId, 101) ) {
                    echo "<script>alert(\" 우편 발송이 실패 하였습니다. 서버개발자에게 문의해 주세요\");</script>";
                }
                else {
                    echo "<script>alert(\" 우편 발송이 완료 되었습니다. \");</script>";
                }
            } else {
                echo "<script>alert(\" 우편 발송이 실패 하였습니다. 서버개발자에게 문의해 주세요\");</script>";
            }
        } 
        else {

            $tempTypes = explode('/',$itemIds);
            $tempCnts = explode('/',$itemCnts);

            $sendI = 0;
            $checkI = 0;

            $sql = "INSERT INTO frdUserPost (recvUserId, sendUserId, type, count, expire_time, reg_date) VALUES ";
            for($sendI = 0; $sendI < count($tempTypes); $sendI++) {
                $item_idx = $tempTypes[$sendI];
                $item_count = $tempCnts[$sendI];
                
                $sql .= "($userId, 0, $item_idx, $item_count, DATE_ADD(now(), INTERVAL 7 DAY ), now()),";  
                
            }
            $sql = trim($sql, ",");
            $db -> prepare($sql);
            $row = $db -> execute();
            if ( false == $this->ProcessUpdateResource($db, $row, $userId, $sql, $memo) )
                echo "<script>alert(\" 우편 발송이 실패 하였습니다. \");</script>";
            else
                echo "<script>alert(\" 우편 발송이 완료 되었습니다. \");</script>";

        }
    }









    public function delete_all_mail_info($db_number, $userId) {

        $multidb = new MultiDB();

        $dbs = $multidb -> set_multi_db();

        $db = $dbs[$db_number];

        $sql = <<<SQL
		DELETE FROM Mail_Info
		WHERE receiver_userId = :userId		
		
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $r = $db -> execute();
        if (!$r) {
            echo $db -> errorInfo();
            $this -> logger -> logError('DB :' . $db_number . 'delete_all_mail_info : Qeury UPDATE  FAIL userId : ' . $userId . ", sql : " . $sql);

        }

        return 0;

    }

    public function delete_mail_info($db_number, $userId, $mailid) {

        // First Log DB
        $log_db = new log_DB();

        // Second Del DB
        $multidb = new MultiDB();
        $dbs = $multidb -> set_multi_db();
        $db = $dbs[$db_number];

        // get mail db
        try {

            $sql = <<<SQL
            SELECT *
            FROM Mail_Info
            WHERE receiver_userId = {$userId} AND MailID = {$mailid}
SQL;

            $db -> prepare($sql);
            $db -> execute();
            $get_row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);

        } catch (Exception $e) {
            $this -> logger -> logError(', BackUpManager::BackUp : Qeury SELECT TABLE FAIL : ' . $e);
        }

        // set backup table name
        $table_name = "mail_" . date('Ymd');

        // check backup table
        $sql = <<<SQL
        SHOW TABLES LIKE :table_name
SQL;

        $log_db -> prepare($sql);
        $log_db -> bindValue(':table_name', $table_name, PDO::PARAM_INT);
        $log_db -> execute();
        $log_row = $log_db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);

        // create log table
        if (!$log_row) {
            try {

                $sql = <<<SQL
                CREATE TABLE `$table_name`
                (
                    `userId` int(11) NOT NULL,
                    `protocol` varchar(30) DEFAULT '0',
                    `MailID` int(11) NOT NULL,
                    `sender_userId` int(11) NOT NULL DEFAULT '-1' COMMENT '보내는 계정 userId',
                    `receiver_userId` int(11) NOT NULL DEFAULT '-1' COMMENT '받는 계정 userId',
                    `sender_name` varchar(16) NOT NULL DEFAULT '-1',
                    `sender_type` tinyint(4) NOT NULL DEFAULT '-1',
                    `expire_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '만료시간',
                    `item_type` int(11) NOT NULL DEFAULT '-1' COMMENT 'item_base type',
                    `value` int(11) NOT NULL DEFAULT '-1' COMMENT '첨부 아이템 대상 개수',
                    `reg_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '등록일',
                    `backup_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '메일 아이템 수령 시간',
                    `ID` int(11) NOT NULL AUTO_INCREMENT,
                    PRIMARY KEY (`ID`),
                    KEY `userId` (`userId`)
                )
                ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8
SQL;

                $log_db -> prepare($sql);
                $log_row = $log_db -> execute();

            } catch (Exception $e) {
                $this -> logger -> logError(', BackUpManager::BackUp : Qeury CREATE TABLE FAIL : ' . $e);
            }

            if (!isset($log_row) || is_null($log_row) || $log_row == 0) {
                $this -> logger -> logError($GLOBALS['AccessNo'] . ', BackUpManager::BackUp : Qeury CREATE TABLE FAIL $userId : ' . $userId, ', $sql : ' . $sql);
                return false;
            }

        }

        // setting
        // $dbnum_userId = $db_number.$userId;

        // insert log db
        try {

            $sql = <<<SQL
            INSERT INTO `$table_name`
            (
                `userId`, `protocol`, `MailID`, `sender_userId`,
                `receiver_userId`, `sender_name`, `sender_type`, `expire_time`,
                `item_type`, `value`, `reg_date`, `backup_date`
            )
            VALUES
            (
                :userId, 'ReqMailDel', :MailID, :sender_userId,
                :receiver_userId, :sender_name, :sender_type, :expire_time,
                :item_type, :value, :reg_date, now()
            );
SQL;

            $log_db -> prepare($sql);
            $log_db -> bindValue(':userId', $userId, PDO::PARAM_INT);
            $log_db -> bindValue(':MailID', $mailid, PDO::PARAM_INT);
            $log_db -> bindValue(':sender_userId', $get_row['sender_userId'], PDO::PARAM_INT);
            $log_db -> bindValue(':receiver_userId', $get_row['receiver_userId'], PDO::PARAM_INT);
            $log_db -> bindValue(':sender_name', $get_row['sender_name'], PDO::PARAM_INT);
            $log_db -> bindValue(':sender_type', $get_row['sender_type'], PDO::PARAM_INT);
            $log_db -> bindValue(':expire_time', $get_row['expire_time'], PDO::PARAM_INT);
            $log_db -> bindValue(':item_type', $get_row['item_type'], PDO::PARAM_INT);
            $log_db -> bindValue(':value', $get_row['value'], PDO::PARAM_INT);
            $log_db -> bindValue(':reg_date', $get_row['reg_date'], PDO::PARAM_STR);
            $log_row = $log_db -> execute();

        } catch (Exception $e) {
            $this -> logger -> logError(', BackUpManager::BackUp : Qeury INSERT TABLE FAIL : ' . $e);
        }

        if (!isset($log_row) || is_null($log_row) || $log_row == 0) {
            $this -> logger -> logError($GLOBALS['AccessNo'] . ', BackUpManager::BackUp : Qeury INSERT BACKUP ITEM $userId : ' . $userId, ', $sql : ' . $sql);
            return false;
        }

        // del db data
        try {

            $sql = <<<SQL
            DELETE FROM Mail_Info
            WHERE MailID = :MAILID AND receiver_userId = :userId
SQL;

            $db -> prepare($sql);
            $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
            $db -> bindValue(':MAILID', $mailid, PDO::PARAM_INT);
            $del_row = $db -> execute();

        } catch (Exception $e) {
            $this -> logger -> logError(', BackUpManager::BackUp : Qeury DEL TABLE FAIL : ' . $e);
        }

        if (!isset($del_row) || is_null($del_row) || $del_row == 0) {
            echo $db -> errorInfo();
            $this -> logger -> logError('DB :' . $GLOBALS['AccessNo'] . 'char_name_update : Qeury UPDATE  FAIL userId : ' . $userId . ", sql : " . $sql);
            return false;
        }

    }

    

    

    

    



    

    

	function event_buy($db_number, $userId, $price) {
        $multidb = new MultiDB();

        $dbs = $multidb -> set_multi_db();
        $db = $dbs[$db_number];

		$sql = "select * from frdAccuBuyEvent where userId = :userId";
		$db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
		$db -> execute();
		$row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if (!$row) {
			$reward = $this->eventInAppReward($price, 0);
			$progress = 0;
			if ($reward) {
				$rewardTypes = $reward['rewardTypes'];
				$rewardCnts = $reward['rewardCnts'];
				$progress = $reward['progress'];	
			}

			$sql = "INSERT INTO frdAccuBuyEvent 
				(userId, AmountOfPayment, progress, update_date, reg_date ) 
				values 
				(:userId, :AmountOfPayment, :progress, now(), now())";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':AmountOfPayment', $price, PDO::PARAM_INT);
			$db -> bindValue(':progress', $progress, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
				return $resultFail;
			}
		} else {
			$progress = $row['progress'];	
			$CheckPrice = $price + $row['AmountOfPayment'];
			if ( $progress < 6 ) {
				$reward = $this->eventInAppReward( $CheckPrice, $progress);
				if ($reward) {
					$rewardTypes = $reward['rewardTypes'];
					$rewardCnts = $reward['rewardCnts'];
					$progress = $reward['progress'];
				}
			}
			
			$sql = "UPDATE frdAccuBuyEvent SET
				AmountOfPayment = AmountOfPayment + :price, progress =  :progress, update_date = now() 
				where userId = :userId";
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':price', $price, PDO::PARAM_INT);
			$db -> bindValue(':progress', $progress, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
				return $resultFail;
			}

		}

		if ($rewardTypes) {
			$sql = "INSERT INTO frdUserPost (recvUserId, sendUserId, type, count, expire_time, reg_date) values ";
			for ($i = 0; $i < count($rewardTypes); $i++) {
				$rType = $rewardTypes[$i];
				$rCnt = $rewardCnts[$i];
				if ($i == 0) {
					$sql .= "($userId, 5, $rType, $rCnt, DATE_ADD(now(), INTERVAL 30 DAY ), now()) ";
				} else {
				$sql .= ",($userId, 5, $rType, $rCnt, DATE_ADD(now(), INTERVAL 30 DAY ), now()) ";
				}
			}
			$db -> prepare($sql);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this->logger->logError(__FUNCTION__.' : FAIL userId.'.$userId.', sql : '.$sql);
				return $resultFail;
			}

		}

        return true;
    }

    function eventInAppReward($accuPay, $state) {
        $eventPayArray = array(5000,10000,30000,50000,100000,300000);
        // 5000
        $accType[] = "5002,5005,5003";
        $accCnt[] = "5,30,50";
        // 10000
        $accType[] = "5002,5005,5006,5003,5003";
        $accCnt[]= "10,50,100,50,50";
        // 30000
        $accType[] = "1505,5002,5005,5006";
        $accCnt[] = "1,20,100,200";
        // 50000
        $accType[] = "10504,5005,5006,5003,5003,5003,5003";
        $accCnt[] = "1,200,500,50,50,50,50";
        // 100000
        $accType[] = "10505,100000,100004,100003";
        $accCnt[] = "1,2,3,10";
        // 300000
        $accType[] = "10505,1505,100004,5002,5005,100000";
        $accCnt[] = "1,1,10,150,1000,5";
        for($payI = 0; $payI < count($eventPayArray); $payI++) {
            if ($accuPay < $eventPayArray[$payI]) {
                break;
            }
        }

        $rewardTypes = array();
        $rewardCnts = array();
        if ($payI > $state) {
            for($rewardI = $state; $rewardI < $payI; $rewardI++) {
                $tempType = $accType[$rewardI];
                $realType = explode(',',$tempType);

                $tempCnt = $accCnt[$rewardI];
                $realCnt = explode(',',$tempCnt);

                $rewardTypes = array_merge($rewardTypes, $realType);
                $rewardCnts = array_merge($rewardCnts, $realCnt);
            }
        }

        $reward['rewardTypes'] = $rewardTypes;
        $reward['rewardCnts'] = $rewardCnts;
        $reward['progress'] = $payI;

        return $reward;
    }

    public function change_soul_fragment_update($db_number, $userId, $change_soul_fragment) {
        $multidb = new MultiDB();

        $dbs = $multidb -> set_multi_db();

        $db = $dbs[$db_number];
        $sql = "UPDATE frdUserData SET powder = :SOUL_FRAGMENT  WHERE userId = :userId ";
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':SOUL_FRAGMENT', $change_soul_fragment, PDO::PARAM_INT);
        $row = $db -> execute();

        if (!isset($row) || is_null($row) || $row == 0) {

            echo $db -> errorInfo();
            $this -> logger -> logError('DB :' . $GLOBALS['AccessNo'] . 'char_name_update : Qeury UPDATE  FAIL userId : ' . $userId . ", sql : " . $sql);
            return false;
        }

    }

    

    public function char_name_update($db_number, $userId, $char_name) {

        $multidb = new MultiDB();
        $dbs = $multidb -> set_multi_db();
        $db = $dbs[$db_number];

        $sql = <<<SQL
        UPDATE Info_Base A, Info_Char_Data B
        SET A.char_name = :NAME, B.char_name = :NAME
        WHERE A.userId = B.userId AND A.userId = :userId 
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':NAME', $char_name, PDO::PARAM_STR, 50);
        $row = $db -> execute();

        if (!isset($row) || is_null($row) || $row == 0) {
            echo $db -> errorInfo();
            $this -> logger -> logError('DB :' . $GLOBALS['AccessNo'] . 'char_name_update : Qeury UPDATE  FAIL userId : ' . $userId . ", sql : " . $sql);
            return false;
        }

    }

    

    public function user_reset($db_number, $userId) {
        //        $item_type = 0;

        //echo "<script>alert(\" $db_number : $userId : $item_group : $item_idx : $item_count \");</script>";
        //return;
        //echo "<script>history(-1);</script>";

        include_once './game_service_sender.php';

        $game_service_request = new game_service_request();

        $send_array['Protocol'] = 'ReqAccountReset';
        $send_array['Sess'] = 'server';
        $send_array['Ver'] = '1.1.1';
        $send_array['Type'] = 'A';
        $send_array['Data']['userId'] = $db_number . $userId;

        $result_send_mail = $game_service_request -> request($send_array);
        if ($result_send_mail['ResultCode'] != 100) {
            echo "<script>alert(\" 계정초기화가 실패 하였습니다. \");</script>";
            return;
        }

        echo "<script>alert(\" 계정초기화가 완료 되었습니다. \");</script>";

        return;
    }

    public function notice_delete($id_idx) {
        $admin_db = new Admin_DB();

        $sql = <<<SQL
        DELETE FROM game_notice WHERE id_idx = {$id_idx}       
SQL;
        $admin_db -> prepare($sql);
        $row = $admin_db -> execute();

        if (!isset($row) || is_null($row) || $row == 0) {

            //echo $db -> errorInfo();
            $this -> logger -> logError('DB :' . $GLOBALS['AccessNo'] . 'notice_delete : Qeury DELETE  FAIL id_idx : ' . $id_idx . ", sql : " . $sql);
            return false;
        }

    }

    public function send_mail_batchall_insert($GROUP_NAMES, $item_group, $item_count, $mail_title, $item_idx, $summon_idx, $summon_pull) {

        switch($item_group) {

            case "item" :
                $item_type = 1;
                $item_idx = $item_idx;
                break;

            case "summon" :
                $item_type = 2;
                $item_idx = $summon_idx;
                break;

            case "summon_pull" :
                $item_type = 5;
                $item_idx = $summon_pull;
                break;

            case "gold" :
                $item_type = 7;
                $item_idx = $item_count;
                break;

            case "cash" :
                $item_type = 8;
                $item_idx = $item_count;
                break;

            case "stemina" :
                $item_type = 9;
                $item_idx = $item_count;
                break;

            case "summon_evolution_stones" :
                $item_type = 11;
                $item_idx = $item_count;
                break;

            case "armor_evolution_stones" :
                $item_type = 12;
                $item_idx = $item_count;
                break;

            case "weapon_evolution_stones" :
                $item_type = 13;
                $item_idx = $item_count;
                break;

            case "chowol_ticket" :
                $item_type = 14;
                $item_idx = $item_count;
                break;

            case "random_evolution_stones" :
                $item_type = 17;
                $item_idx = $item_count;
                break;

            case "soul_fragment" :
                $item_type = 18;
                $item_idx = $item_count;
                break;

            case "evo_wing" :
                $item_type = 19;
                $item_idx = $item_count;
                break;

            case "gold_ticket" :
                $item_type = 21;
                $item_idx = $item_count;
                break;

            case "fish_1" :
                $item_type = 22;
                $item_idx = $item_count;
                break;

            case "fish_2" :
                $item_type = 23;
                $item_idx = $item_count;
                break;

            case "fish_3" :
                $item_type = 24;
                $item_idx = $item_count;
                break;

            default :
                echo "<script>alert(\" 우편 발송이 실패 하였습니다. \");</script>";
                return;
                break;
        }

        $GROUP_NAMES = trim($GROUP_NAMES);

        $GROUP_NAMES = explode("\n", $GROUP_NAMES);

        // count($GROUP_NAMES) 명이 지급예정입니다. 확인 팝업 뛰우기.

        $nick_name_str = null;

        foreach ($GROUP_NAMES as $key => $value) {

            $value = trim($value);

            if ($nick_name_str == null) {
                $nick_name_str = '"' . $value . '"';
            } else {
                $nick_name_str = $nick_name_str . ',"' . $value . '"';
            }

        }

        $multidb = new MultiDB();

        $dbs = $multidb -> set_multi_db();

        ###################################################
        //INSERT INTO Mail_Info(sender_userId, receiver_userId, sender_name, sender_type, text_string, use_text, reg_date, expire_time, item_type, value, update_date)
        //VALUES (:sender_userId, :receiver_userId_sub, :sender_name, :sender_type, :text_string, :use_text, now(), DATE_ADD(now(), INTERVAL :expire_day DAY), :item_type, :stack, now())

        foreach ($dbs as $key => $db) {

            $userId_array = array();

            $sql = <<<SQL
            SELECT userId
            FROM Info_Char_Data
            WHERE char_name IN ({$nick_name_str})
SQL;

            $db -> prepare($sql);
            $db -> execute();

            while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {

                $temp = null;

                $temp['sender_userId'] = -19;
                $temp['receiver_userId'] = $row['userId'];
                $temp['sender_name'] = 'SeedOfThrone';
                $temp['sender_type'] = -19;
                $temp['text_string'] = $mail_title;
                $temp['use_text'] = 1;
                $temp['reg_date'] = 'now()';
                $temp['expire_time'] = 'DATE_ADD(now(), INTERVAL 3 DAY)';
                $temp['item_type'] = $item_type;
                $temp['value'] = $item_idx;
                $temp['update_date'] = 'now()';

                array_push($userId_array, $temp);

            }

            $columns = 'sender_userId' . ', receiver_userId' . ', sender_name' . ', sender_type' . ', text_string' . ', use_text' . ', reg_date' . ', expire_time' . ', item_type' . ', value' . ', update_date';

            #############3

            $valuesArr = array();

            foreach ($userId_array as $userId_array_key => $userId_array_value) {

                $temp = null;

                $sender_userId = $userId_array_value['sender_userId'];
                $receiver_userId = $userId_array_value['receiver_userId'];
                $sender_name = $userId_array_value['sender_name'];
                $sender_type = $userId_array_value['sender_type'];
                $text_string = $userId_array_value['text_string'];
                $use_text = $userId_array_value['use_text'];
                $reg_date = $userId_array_value['reg_date'];
                $expire_time = $userId_array_value['expire_time'];
                $item_type = $userId_array_value['item_type'];
                $value = $userId_array_value['value'];
                $update_date = $userId_array_value['update_date'];

                $valuesArr[] = "('$sender_userId', '$receiver_userId', '$sender_name', '$sender_type', '$text_string', '$use_text', $reg_date, $expire_time, '$item_type', '$value', $update_date)";

            }
            #############3

            $sql = "INSERT INTO `Mail_Info` ({$columns}) VALUES ";

            $sql .= implode(',', $valuesArr);

            $db -> prepare($sql);
            $db -> execute();

        }

        echo "<script>alert(\" 우편 발송이 완료 되었습니다. \");</script>";

        return;

    }

    public function send_mail_batchall_insert_pid($GROUP_NAMES, $item_group, $item_count, $mail_title, $item_idx, $summon_idx, $summon_pull) {

        switch($item_group) {

            case "item" :
                $item_type = 1;
                $item_idx = $item_idx;
                break;

            case "summon" :
                $item_type = 2;
                $item_idx = $summon_idx;
                break;

            case "summon_pull" :
                $item_type = 5;
                $item_idx = $summon_pull;
                break;

            case "gold" :
                $item_type = 7;
                $item_idx = $item_count;
                break;

            case "cash" :
                $item_type = 8;
                $item_idx = $item_count;
                break;

            case "stemina" :
                $item_type = 9;
                $item_idx = $item_count;
                break;

            case "summon_evolution_stones" :
                $item_type = 11;
                $item_idx = $item_count;
                break;

            case "armor_evolution_stones" :
                $item_type = 12;
                $item_idx = $item_count;
                break;

            case "weapon_evolution_stones" :
                $item_type = 13;
                $item_idx = $item_count;
                break;

            case "chowol_ticket" :
                $item_type = 14;
                $item_idx = $item_count;
                break;

            case "random_evolution_stones" :
                $item_type = 17;
                $item_idx = $item_count;
                break;

            case "soul_fragment" :
                $item_type = 18;
                $item_idx = $item_count;
                break;

            case "evo_wing" :
                $item_type = 19;
                $item_idx = $item_count;
                break;

            case "gold_ticket" :
                $item_type = 21;
                $item_idx = $item_count;
                break;

            case "fish_1" :
                $item_type = 22;
                $item_idx = $item_count;
                break;

            case "fish_2" :
                $item_type = 23;
                $item_idx = $item_count;
                break;

            case "fish_3" :
                $item_type = 24;
                $item_idx = $item_count;
                break;

            default :
                echo "<script>alert(\" 우편 발송이 실패 하였습니다. \");</script>";
                return;
                break;
        }

        $GROUP_NAMES = trim($GROUP_NAMES);

        $GROUP_NAMES = explode("\n", $GROUP_NAMES);

        // count($GROUP_NAMES) 명이 지급예정입니다. 확인 팝업 뛰우기.

        $nick_name_str = null;

        foreach ($GROUP_NAMES as $key => $value) {

            $value = trim($value);

            if ($nick_name_str == null) {
                $nick_name_str = '"' . $value . '"';
            } else {
                $nick_name_str = $nick_name_str . ',"' . $value . '"';
            }

        }

        $multidb = new MultiDB();

        $dbs = $multidb -> set_multi_db();

        ###################################################
        //INSERT INTO Mail_Info(sender_userId, receiver_userId, sender_name, sender_type, text_string, use_text, reg_date, expire_time, item_type, value, update_date)
        //VALUES (:sender_userId, :receiver_userId_sub, :sender_name, :sender_type, :text_string, :use_text, now(), DATE_ADD(now(), INTERVAL :expire_day DAY), :item_type, :stack, now())

        foreach ($dbs as $key => $db) {

            $userId_array = array();

            $sql = <<<SQL
            SELECT userId
            FROM User_Regist
            WHERE PlayerID IN ({$nick_name_str})
SQL;

            $db -> prepare($sql);
            $db -> execute();

            while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {

                $temp = null;

                $temp['sender_userId'] = -19;
                $temp['receiver_userId'] = $row['userId'];
                $temp['sender_name'] = 'SeedOfThrone';
                $temp['sender_type'] = -19;
                $temp['text_string'] = $mail_title;
                $temp['use_text'] = 1;
                $temp['reg_date'] = 'now()';
                $temp['expire_time'] = 'DATE_ADD(now(), INTERVAL 3 DAY)';
                $temp['item_type'] = $item_type;
                $temp['value'] = $item_idx;
                $temp['update_date'] = 'now()';

                array_push($userId_array, $temp);

            }

            $columns = 'sender_userId' . ', receiver_userId' . ', sender_name' . ', sender_type' . ', text_string' . ', use_text' . ', reg_date' . ', expire_time' . ', item_type' . ', value' . ', update_date';

            #############3

            $valuesArr = array();

            foreach ($userId_array as $userId_array_key => $userId_array_value) {

                $temp = null;

                $sender_userId = $userId_array_value['sender_userId'];
                $receiver_userId = $userId_array_value['receiver_userId'];
                $sender_name = $userId_array_value['sender_name'];
                $sender_type = $userId_array_value['sender_type'];
                $text_string = $userId_array_value['text_string'];
                $use_text = $userId_array_value['use_text'];
                $reg_date = $userId_array_value['reg_date'];
                $expire_time = $userId_array_value['expire_time'];
                $item_type = $userId_array_value['item_type'];
                $value = $userId_array_value['value'];
                $update_date = $userId_array_value['update_date'];

                $valuesArr[] = "('$sender_userId', '$receiver_userId', '$sender_name', '$sender_type', '$text_string', '$use_text', $reg_date, $expire_time, '$item_type', '$value', $update_date)";

            }
            #############3

            $sql = "INSERT INTO `Mail_Info` ({$columns}) VALUES ";

            $sql .= implode(',', $valuesArr);

            $db -> prepare($sql);
            $db -> execute();

        }

        echo "<script>alert(\" 우편 발송이 완료 되었습니다. \");</script>";

        return;

    }

    // 소환수(영웅) 등급 및 레벨 변경
    public function summon_level_change($db_number, $userId, $summon_id, $summon_level, $summon_grade, $summon_enchant, $summon_key) {

        $multidb = new MultiDB();

        $dbs = $multidb -> set_multi_db();
        $db = $dbs[$db_number];

        $sql = <<<SQL
        UPDATE Inven_Summon
        SET summon_level = :SUMMON_LEVEL, enchant_level = :SUMMON_ENCHANT, update_date=NOW()
        WHERE userId = :userId AND SummonID = :SummonID
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':SUMMON_LEVEL', $summon_level, PDO::PARAM_INT);
        $db -> bindValue(':SUMMON_ENCHANT', $summon_enchant, PDO::PARAM_INT);
        $db -> bindValue(':SummonID', $summon_id, PDO::PARAM_INT);
        $row = $db -> execute();

        if (!isset($row) || is_null($row) || $row == 0) {

            echo $db -> errorInfo();
            $this -> logger -> logError('DB :' . $GLOBALS['AccessNo'] . 'summon_lv_update : Qeury UPDATE  FAIL userId : ' . $userId . ", sql : " . $sql);
            return false;

        }

    }

    // 업적(Quest_Archive) 진행도 변경
    public function mission_change_cnt($db_number, $userId, $mission_idx, $mission_cnt) {
        // error_log('소환수 넘버 : ' . $summon_idx . '/ lv : ' . $summon_level . '/ grade' . $summon_grade);
        $multidb = new MultiDB();

        $dbs = $multidb -> set_multi_db();
        $db = $dbs[$db_number];

        $sql = <<<SQL
        UPDATE Quest_Archive
        SET value = :VALUE, update_date=NOW()
        WHERE userId = :userId AND Quest_Idx = :QUEST_IDX
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':QUEST_IDX', $mission_idx, PDO::PARAM_INT);
        $db -> bindValue(':VALUE', $mission_cnt, PDO::PARAM_INT);
        $row = $db -> execute();

        if (!isset($row) || is_null($row) || $row == 0) {

            echo $db -> errorInfo();
            $this -> logger -> logError('DB :' . $GLOBALS['AccessNo'] . 'mission_change_cnt : Qeury UPDATE  FAIL userId : ' . $userId . ", sql : " . $sql);
            return false;

        }

    }

    // $user -> bingo_change_progress($db_number, $userId, $bingo_select, $bingo_value);
    public function bingo_change_progress($db_number, $userId, $bingo_select, $bingo_value) {
        // error_log('빙고 : ' . $db_number . ' / : ' . $userId . ' / : ' . $bingo_line_num);
        error_log('빙고 : ');
        $multidb = new MultiDB();

        $dbs = $multidb -> set_multi_db();
        $db = $dbs[$db_number];

        $sql = <<<SQL
        UPDATE Quest_Daily
        SET type_value = :TYPE_VALUE, update_date=NOW()
        WHERE userId = :userId AND bingo_position = :BINGO_POSITION
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':TYPE_VALUE', $bingo_value, PDO::PARAM_INT);
        $db -> bindValue(':BINGO_POSITION', $bingo_select, PDO::PARAM_INT);
        $row = $db -> execute();

        if (!isset($row) || is_null($row) || $row == 0) {

            echo $db -> errorInfo();
            $this -> logger -> logError('DB :' . $GLOBALS['AccessNo'] . 'bingo change count : Qeury UPDATE  FAIL userId : ' . $userId . ", sql : " . $sql);
            return false;

        }

    }

    /* hansw */
    public function change_level_update($db_number, $userId, $change_level) {

        //DB에 값 적용하기
        $multidb = new MultiDB();
        $dbs = $multidb -> set_multi_db();
        $db = $dbs[$db_number];

        $sql = <<<SQL
			UPDATE Info_Char_Status
			SET char_level = :LEVEL
			WHERE userId = :userId
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':LEVEL', $change_level, PDO::PARAM_INT);
        $row = $db -> execute();

        if (!isset($row) || is_null($row) || $row == 0) {
            echo $db -> errorInfo();
            $this -> logger -> logError('DB :' . $GLOBALS['AccessNo'] . 'char_level_update : Qeury UPDATE FAIL userId :' . $userId . ", sql : " . $sql);
            return false;
        }

        //memcashed와 값 가져오기
        include_once './lib/MemManager.php';

        $MemManager = new MemManager();
        $mem_key = trim("KEY_CHAT_COSTUME_" . $GLOBALS['AccessNo'] . $userId);
        $costume_info = $MemManager -> get($mem_key);

        if ($costume_info != NULL) {
            //memcashed에 값 적용하기
            $costume_info['info_idx']['char_level'] = $change_level;
            $v = $MemManager -> set_expire($mem_key, $costume_info, 86400);
            if (!$v) {
                $r = $MemManager -> delete($mem_key);
                if (!$r) {
                    $this -> logger -> logError($GLOBALS['AccessNo'] . ', UserManager::char_level_change : MEM DELETE FAIL userId : ' . $userId);
                }
            }
        }
    }

    /* hansw */

    public function no_admission_period($db_number, $userId) {

        $multidb = new MultiDB();

        $dbs = $multidb -> set_multi_db();
        $db = $dbs[$db_number];

        $sql = <<<SQL
        UPDATE User_Regist
        SET STATUS = 1, update_date=NOW()
        WHERE userId = :userId
SQL;

        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $row = $db -> execute();

        if (!isset($row) || is_null($row) || $row == 0) {

            echo $db -> errorInfo();
            $this -> logger -> logError('DB :' . $GLOBALS['AccessNo'] . 'no_admission_period : Qeury UPDATE  FAIL userId : ' . $userId . ", sql : " . $sql);
            return false;

        }

    }

    public function change_stage_star_update($db_number, $userId, $stage_idx, $star) {

        $multidb = new MultiDB();

        $dbs = $multidb -> set_multi_db();
        $db = $dbs[$db_number];

        if (0 < $star AND $star < 4) {
            $sql = <<<SQL
            UPDATE User_DG_Info
            SET star = :STAR , update_date=NOW()
            WHERE userId = :userId AND Stage_Idx = :STAGE_IDX 
SQL;
            $db -> prepare($sql);
            $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
            $db -> bindValue(':STAGE_IDX', $stage_idx, PDO::PARAM_INT);
            $db -> bindValue(':STAR', $star, PDO::PARAM_INT);
            $row = $db -> execute();
            if (!isset($row) || is_null($row) || $row == 0) {
                echo $db -> errorInfo();
                $this -> logger -> logError('DB :' . $GLOBALS['AccessNo'] . 'no_admission_period : Qeury UPDATE  FAIL userId : ' . $userId . ", sql : " . $sql);
                return false;
            }
        } else if ($star == 4) {
            $sql = <<<SQL
            DELETE 
            FROM User_DG_Info
            WHERE userId = :userId AND Stage_Idx = :STAGE_IDX 
SQL;
            $db -> prepare($sql);
            $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
            $db -> bindValue(':STAGE_IDX', $stage_idx, PDO::PARAM_INT);
            $row = $db -> execute();
            if (!isset($row) || is_null($row) || $row == 0) {
                echo $db -> errorInfo();
                $this -> logger -> logError('DB :' . $GLOBALS['AccessNo'] . 'no_admission_period : Qeury UPDATE  FAIL userId : ' . $userId . ", sql : " . $sql);
                return false;
            }
        }
    }

};




//Code Start
$user = new UserManager();
switch($CMD) {
    case "sendmail" :
        $user -> send_mail_insert($country, $userId, $item_idx, $item_count);
        break;
    case "change_gold" :
        $user -> change_gold_update($country, $userId, $change_gold, $memo);
        break;
    case "change_jewel" :
        $user -> change_jewel_update($country, $userId, $change_jewel, $memo);
        break;
    case "user_kick" :
        $user -> user_kick($country, $userId);
        break;
    case "change_accuSkillPoint" :
        $user -> change_accuSkillPoint($country, $userId, $change_accuSkillPoint, $memo);
        break;
    case "change_skillPoint" :
        $user -> change_skillPoint($country, $userId, $change_skillPoint, $memo);
        break;
    case "change_powder" :
        $user -> change_powder($country, $userId, $change_powder, $memo);
        break;
    case "change_maxHeart" :
        $user -> change_maxHeart($country, $userId, $change_maxHeart, $memo);
        break;

    case "all_send" :
        $user -> all_send($country, $platform, $item_idx, $item_count, $memo, $arr_platform);
        break;

    case "package_send" :
        $user -> package_send($country, $userId, $item_group, $memo);
        break;










    case "user_deny" :
        $MultiDB = new MultiDB();
        $get_MultiDB = $MultiDB -> set_multi_db();
        $get_UserDB = $get_MultiDB[$db_number];

        $sql = <<<SQL
			
		SELECT PlayerID FROM User_Regist WHERE userId =:userId
SQL;
        $get_UserDB -> prepare($sql);
        $get_UserDB -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $get_UserDB -> execute();
        $row = $get_UserDB -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
        $PlayerID = $row['PlayerID'];

        require_once ('./game_service_sender.php');
        $game_sender = new game_service_request();

        $date_option = $_POST['inlineRadioOptions'];
        switch($date_option) {
            //1일 정지
            case 'option1' :
                $date = 1;
                break;
            //3일 정지
            case 'option2' :
                $date = 3;
                break;
            //7일 정지
            case 'option3' :
                $date = 7;
                break;
            //30일 정지
            case 'option4' :
                $date = 30;
                break;
            //영구일 정지
            case 'option5' :
                $date = -1;
                break;
            //채팅금지
            case 'option6' :
                //$date = 3;
                break;

            default :
                break;
        }

        ###################################3
        // 운영자 로그 insert

        $memo = $_POST['inputMemo'];

        $sql = "insert into g5_admin_log_ban 
(login_id, target_userId, ban_type, date_ban_start, date_ban_end, memo, reg_date) 
values ('{$mb_id}', {$db_number}{$userId}, {$date}, now(), date_add(now(), interval {$date} day), '{$memo}', now())";

        sql_query($sql);

        //echo $sql;exit;

        ###################################3

        $Data['PlayerID'] = $PlayerID;
        $Data['date'] = $date;

        $send_array['Protocol'] = 'ReqBlockAccount';
        $send_array['Sess'] = 'server';
        $send_array['Ver'] = '1.1.1';
        $send_array['Type'] = 'A';

        $send_array['Data'] = $Data;

        $Res = $game_sender -> request($send_array);

        if ($Res['ResultCode'] != 100) {
            echo 'FUCK';
        } else {
            //	var_dump($Res);
        }

        break;
    case "change_stage_star" :
        $user -> change_stage_star_update($db_number, $userId, $stage_idx, $_POST['stage_clear_status']);
        break;
    
   
    
    

    case "change_soul_fragment" :
        $user -> change_soul_fragment_update($db_number, $userId, $change_soul_fragment);
        break;
    case "chanage_name" :
        $user -> char_name_update($db_number, $userId, $change_hero_name);
        break;

    case "change_invite" :
        $user -> change_invite_and_reward_update($db_number, $userId, $invite_count, $reward_group, $reward_count);
        break;

    case "summon_level_change" :
        $user -> summon_level_change($db_number, $userId, $summon_id, $summon_level, $summon_grade, $summon_enchant, $summon_key);
        break;

    case "mission_change_cnt" :
        $user -> mission_change_cnt($db_number, $userId, $mission_idx, $mission_cnt);
        break;

    case "bingo_change_progress" :
        $user -> bingo_change_progress($db_number, $userId, $bingo_select, $bingo_value);
        break;
    /* hansw */
    case "change_char_level" :
        $user -> change_level_update($db_number, $userId, $change_level);
        break;
    /* hansw */

    case "no_admission_period" :
        $user -> no_admission_period($db_number, $userId);
        break;

    case "sendmail_batchall" :
        $user -> send_mail_batchall_insert($group_ids, $item_group, $item_count, $mail_title, $item_idx, $summon_idx, $summon_pull);
        break;

    case "sendmail_batchall_pid" :
        $user -> send_mail_batchall_insert_pid($group_ids, $item_group, $item_count, $mail_title, $item_idx, $summon_idx, $summon_pull);
        break;
    
    
    

    
    case "event_buy" :
        $user -> event_buy($db_number, $userId, $price);
        break;
    
    case "delete_mail" :
        $user -> delete_mail_info($db_number, $userId, $MailID);
        break;
   
    case "user_reset" :
        $user -> user_reset($db_number, $userId);
        break;
    case 'user_mail' :
        $userId = $_GET['userId'];
        $db_num = $_GET['db_number'];

        $server_userId = $db_num . $userId;

        $btSel = $_POST['btSel'];

        $check_arr = array();

        foreach ($_POST as $idx => $value) {
            if (substr($idx, 0, 8) == 'checkbox') {
                //array_push($check_arr,substr($idx,9));
                array_push($check_arr, $value);
            }
        }
        //var_dump($check_arr);

        switch($btSel) {
            case 'btSelDel' :
                $cnt = count($check_arr);
                for ($i = 0; $i < $cnt; $i++) {
                    $user -> delete_mail_info($db_num, $server_userId, $check_arr[$i]);
                }
                break;

            case 'btAllDel' :
                $user -> delete_all_mail_info($db_num, $server_userId);
                break;
            default :
                break;
        }

        //var_dump($_POST);
        break;
    case 'delete_notice' :
        $user -> notice_delete($id_idx);
        break;

    default :
        alert("잘못 입력 되었습니다.!!".$CMD."!!");
        break;
}

echo "<script >location.href = document.referrer;</script>";
?>

