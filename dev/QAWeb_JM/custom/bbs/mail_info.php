<?php
    include_once ('../common.php');
    include_once ('../../_head.php');

    $title_category = "우편 관리";
    include_once('./manage_users_head.php');
    function get_content_type($type) {

        switch($type) {
            case 0 : $content = '운영자님으로부터 받은 선물'; break;
            case 1 : $content = '일일퀘스트 보상'; break;
            case 2 : $content = '출석 보상'; break;
            case 3 : $content = '고난의 탑 보상'; break;
            case 4 : $content = '쿠폰 보상'; break;
            case 5 : $content = '이벤트 보상'; break;
            case 6 : $content = '업적 보상'; break;
            case 10 : $content = '복귀 수호자님을 위한 선물'; break;
            case 200 : $content = '스타터 패키지'; break;
            case 201 : $content = '허니잼 패키지'; break;
            case 202 : $content = '히랜디 패키지'; break;
            case 203 : $content = '어빌리티 패키지'; break;
            case 204 : $content = '영혼 각인석 패키지1'; break;
            case 205 : $content = '영혼 각인석 패키지2'; break;
            case 206: $content = '엑스칼리버 패키지2'; break;
            case 207 : $content = '일로케이터 패키지2'; break;
            case 208 : $content = '갓핸드 패키지2'; break;
            case 209 : $content = '일곱장막전승자 패키지'; break;
            case 210 : $content = '이벤트 어빌리티 패키지'; break;
            case 211 : $content = '메달 패키지'; break;
            case 212 : $content = '메달 묶음 패키지'; break;
            case 213 : $content = '영혼각인석 패키지'; break;
            case 214 : $content = '영혼각인석 묶음 패키지'; break;
            default :
                $content = $type;
                break;
        }
        return $content;
    }

    function get_item_type($type) {

        switch($type) {
            case 5000 :
                $content = '골드';
                break;
            case 5001 :
                $content = '경험치';
                break;
            case 5002 :
                $content = '티켓';
                break;
            case 5003 :
                $content = '번개';
                break;
            case 5004 :
                $content = '보석';
                break;
            case 5005 :
                $content = '메달';
                break;
            case 5006 :
                $content = '가루';
                break;
            default :
                $content = $type;
                break;
        }

        return $content;
    }

    // $multidb = new MultiDB();
    // $db = $multidb -> get_game_db($country, $userId);

?>
<div class="well well-sm">
    <!-- <a class="btn btn-default">메일 목록</a> -->
    <!-- <a class="btn btn-primary" disabled><h5><strong>메일 목록</strong></h5></a> -->
    <h5><strong>지급 날짜 범위 검색</strong></h5>
    <form name="pet_info_select" id="pet_info_select" class="form-inline" method="post">
        <table>
            <tbody>
                <td>
                    <form class="form-inline">
                        <div class="form-group">
                            

                            <link href='//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/build/css/bootstrap-datetimepicker.css' rel='stylesheet'>
                            <script src='//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js'></script>
                            <script src='//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/a549aa8780dbda16f6cff545aeabc3d71073911e/src/js/bootstrap-datetimepicker.js'></script>
                            
                            
                            <div class='container'>
                                <div class='row'>
                                    <div class='col-sm-6'>
                                        <div class='form-group'>
                                            <div class='input-group date' id='fr_date' >
                                                <input type='text' value= <?php echo $fr_date?> class='form-control' name='fr_date' placeholder='여기부터'>
                                                <span class='input-group-addon'>
                                                    <span class='glyphicon glyphicon-calendar'></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <script type='text/javascript'>
                                        $(function () {
                                            $('#fr_date').datetimepicker({
                                                format:'YYYY-MM-DD 00:00:00'
                                            });
                                        });
                                    </script>
                                </div>
                            </div>

                            <div class='container'>
                                <div class='row'>
                                    <div class='col-sm-6'>
                                        <div class='form-group'>
                                            <div class='input-group date' id='to_date' >
                                                <input type='text' value= <?php echo $to_date?> class='form-control' name='to_date' placeholder='여기까지'>
                                                <span class='input-group-addon'>
                                                    <span class='glyphicon glyphicon-calendar'></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <script type='text/javascript'>
                                        $(function () {
                                            $('#to_date').datetimepicker({
                                                format:'YYYY-MM-DD 00:00:00'
                                            });
                                        });
                                    </script>
                                </div>
                            </div>


                            <?php
                                // include_once ('./calendar.php');

                                // $from_calendar = new calendar();
                                // $from_calendar -> show('fr_date', 'fr_date', '시작일');
    
                                // $to_calendar = new calendar();
                                // $to_calendar -> show('to_date', 'to_date', '종료일');
                            ?>
                            <p></p>
                            <span class="input-group-btn">
                                    <button type="submit" class="btn btn-primary">범위 갱신</button>
                                </span>


                        </div>
                    </form>
                </td>

            </tbody>
        </table>
    </form>
    <br>
</div>
<br>
<hr>
<br>
1. 수령 및 보관기간 초과 메일은 유저가 수령 할 수 없는 메일 입니다. 
<br>
2. 보낸일자 = 등록일
<br>
3. 만료일자 = 보유가능 날짜
<br>
4. 삭제일자 = 메일 수령/기간 초과 시 백업 날짜
<table class="table table-striped table-bordered">
    <thead>
        <tr role="row">
            <th class="sorting_asc" style="width: 50px;">Check</th>
            <th class="sorting_asc" style="width: 100px;">userId</th>
            <th class="sorting_asc" style="width: 250px;">보낸일자</th>
            <th class="sorting_asc" style="width: 150px;">만료일자</th>
            <th class="sorting_asc" style="width: 150px;">보낸이</th>
            <th class="sorting_asc" style="width: 150px;">아이템</th>
            <th class="sorting_asc" style="width: 150px;">개수</th>
        </tr>
    </thead>
    <tbody role="alert" aria-live="polite" aria-relevant="all">
        <?php
            $row_cnt = 0;
            
            $sql = "SELECT recvUserId, sendUserId, reg_date ,expire_time, type, count
                FROM frdUserPost
                WHERE recvUserId = $userId and reg_date >= '$fr_date' and reg_date <= '$to_date'";

            $multidb = new MultiDB();
            $db = $multidb -> get_game_db($country, $userId);
            $db -> prepare($sql);
            $db -> execute();

            $char_name = null;
            while($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)){
                $row_cnt++;

                echo "<tr>";

                $char_name = $row['char_name'];

                echo "<td class='center'><input type='checkbox'></td>";
                echo "<td class='center'>{$row['recvUserId']}</td>";
                // echo "<td class='center'>{$userId}</td>";
                echo "<td class='center'>{$row['reg_date']}</td>";
                echo "<td class='center'>{$row['expire_time']}</td>";
                $content = get_content_type($row['sender_userId']);
                echo "<td class='center'>$content</td>";
                $content = get_item_type($row['type']);
                echo "<td class='center'>$content</td>";
                echo "<td class='center'>{$row['count']}</td>";

    //            if ($row['recv_checked'] == 'N') {

    //                $cmd = 'delete_mail';
    //                $href = "'./UserManager.php?db_number={$db_number}&CMD={$cmd}&userId={$row['receiver_userId']}&MailID={$row['MailID']}'";

    //                echo "<td class='center'><a type='submit' class='btn btn-default' href={$href}>삭제</a></td>";
    //            }

                // echo "</tr>";

            }

        ?>
    </tbody>
</table>
