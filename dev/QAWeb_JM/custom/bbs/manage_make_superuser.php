
<?php
	$title_category = "슈퍼 계정 생성";
	include_once('./manage_users_head.php');
?>

<h4>
<strong><mark>
	닉네임 : <?php echo $loginRow['name']?><br><p></p>
	e-mail : <?php echo $loginRow['email']?><br><p></p>
	유저ID : <?php echo $loginRow['userId']?><br><p></p>
	플랫폼ID : <?php echo $loginRow['connectId']?><br>

</mark></strong>
</h4>
<br>

<button class="btn btn-primary" onclick="showConfirm()">슈퍼 계정으로 만들기</button>

<script>

	function showConfirm(){

		if ( !confirm("진행 하시겠습니까?") )
		 	return;

		<?php
			$GLOBALS['COUNTRY'] = $country;
			switch ($country) {
				case 'China_QA':
					$GLOBALS['REAL'] = 4;
					break;
				
				default:
					$GLOBALS['REAL'] = 5;
					break;
			}

			$sql = array();

			$query = "INSERT INTO frdCharInfo (userId, charId, exp) VALUES ";
			for ( $i=100; $i<390; $i++ ) {
				if ( $i !== 100 )
					$query .= ", ";
				$query .= "($userId, $i, 100)";
			}
			$query .= "ON DUPLICATE KEY UPDATE exp = exp";

			$sql[] = $query;
			$sql[] = "UPDATE frdUserData SET tutoLevel=90, userLevel=50, manaLevel=60, gold=7777777, jewel=777777, powder=777777, stageLevel=90, skillPoint=777777, accuSkillPoint=777777, ticket=777777 WHERE  userId =$userId";
			$sql[] = "delete from frdTemple where userId=$userId";
			$sql[] =  "insert into frdTemple values 
					(0, $userId, 1, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 2, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 3, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 4, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 5, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 6, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 7, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 8, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')
					,(0, $userId, 9, 1, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00')";

			$sql[] = "delete from frdAbillity where userId=$userId";
		    $sql[] = "insert into frdAbillity values ($userId, 63, 377614, ((1048576<<25)+(1048576<<20)+(1048576<<15)+(1048576<<10)+(1048576<<5)+1048576+32768+1024+32+1), 0, 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00')";
		    $sql[] = "delete from frdGoddess where userId=$userId";
		    $sql[] = "insert into frdGoddess (userId, goddessId, level, exp) values ($userId, 1, 1, 0),($userId, 2, 1, 0),($userId, 3, 1, 0),
		   			 ($userId, 4, 1, 0),($userId, 5, 1, 0),($userId, 6, 1, 0),($userId, 7, 1, 0),($userId, 8, 1, 0),
		    		 ($userId, 9, 1, 0),($userId, 10, 1, 0)";
		    $sql[] = "delete from frdHavingItems where userId=$userId";

		    $sql[] = "insert into frdUserPost (postTableId, recvUserId, sendUserId, type, count, expire_time) values   (0, $userId, 0, 5000, 220000, DATE_ADD(now(), INTERVAL 7 DAY )),(0, $userId, 0, 5006, 5000, DATE_ADD(now(), INTERVAL 7 DAY )),(0, $userId, 0, 1118, 1, DATE_ADD(now(), INTERVAL 7 DAY ))
		    ,(0, $userId, 0, 1129, 1, DATE_ADD(now(), INTERVAL 7 DAY )),(0, $userId, 0, 1117, 1, DATE_ADD(now(), INTERVAL 7 DAY )),     (0, $userId, 0, 1200, 1, DATE_ADD(now(), INTERVAL 7 DAY )),   (0, $userId, 0, 1225, 1, DATE_ADD(now(), INTERVAL 7 DAY ))
			,(0, $userId, 0, 1202, 1, DATE_ADD(now(), INTERVAL 7 DAY )),(0, $userId, 0, 1201, 1, DATE_ADD(now(), INTERVAL 7 DAY )),     (0, $userId, 0, 1208, 1, DATE_ADD(now(), INTERVAL 7 DAY )),   (0, $userId, 0, 1204, 1, DATE_ADD(now(), INTERVAL 7 DAY ))
		    ,(0, $userId, 0, 10107, 1, DATE_ADD(now(), INTERVAL 7 DAY )),(0, $userId, 0, 10209, 1, DATE_ADD(now(), INTERVAL 7 DAY )), (0, $userId, 0, 10400, 1, DATE_ADD(now(), INTERVAL 7 DAY )),   (0, $userId, 0, 10407, 1, DATE_ADD(now(), INTERVAL 7 DAY ))
		    ,(0, $userId, 0, 1215, 1, DATE_ADD(now(), INTERVAL 7 DAY )),(0, $userId, 0, 1204, 1, DATE_ADD(now(), INTERVAL 7 DAY )),(0, $userId, 0, 1211, 1, DATE_ADD(now(), INTERVAL 7 DAY )),(0, $userId, 0, 1217, 1, DATE_ADD(now(), INTERVAL 7 DAY )),(0, $userId, 0, 1218, 1, DATE_ADD(now(), INTERVAL 7 DAY ))";
		    
		    $query = "insert into frdHavingItems (userId, itemId, itemCount) values ";
		    for ( $i=101000; $i<=101009; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=102000; $i<=102009; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=103000; $i<=103009; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=104000; $i<=104009; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=105000; $i<=105009; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=106000; $i<=106009; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=111000; $i<=111004; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=112000; $i<=112004; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=113000; $i<=113004; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=114000; $i<=114004; $i++ )
		      $query .= "($userId, $i, 1000),";
		    for ( $i=115000; $i<=115004; $i++ )
		      $query .= "($userId, $i, 1000),";
		    $query .= "($userId, 110000, 1000)";
		    $sql[] = $query;

		    $query = "insert into frdHavingWeapons (userId, weaponId, level) values";
		    for ( $i=200; $i<=226; $i++ )
		      $query .= "($userId, $i, 40),";
		  	$query .= "($userId, 200, 40)";
		  	$sql[] = $query;

		  	$query = "insert into frdHavingMagics (userId, magicId, level) values";
		    for ( $i=400; $i<=407; $i++ )
		      $query .= "($userId, $i, 5),";
		  	$query .= "($userId, 400, 5)";
		  	$sql[] = $query;

		  	$queryResult = true;
		    for ( $i=0; $i<count($sql); $i++ ) {
		    	$db -> prepare($sql[$i]);
                $row = $db -> execute();
                if ( $row == false ) {
                	$queryResult = false;
                	error_log('Fail!!! Fail!!!!  userId : ' . $userId . ",\nsql : " . $sql[$i]);
                }
		    }
		   	
		   	if ( $queryResult ) {
                echo "alert(\" 슈퍼 계정으로 전환이 완료되었습니다. \");";
            }
            else {
                echo "alert(\" 슈퍼 계정으로 전환에 실패했습니다. \");";
            }
		?>

		
	}
</script>

<?php
include_once(G5_PATH.'/tail.php');
?>

