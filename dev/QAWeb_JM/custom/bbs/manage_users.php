


<?php
	$title_category = "유저 관리";
	include_once('./manage_users_head.php');

	$sql = "
	    SELECT U.userId, name, jewel, gold, heart, maxHeart, powder, skillPoint, accuSkillPoint,
		 L.productId, L.reg_date, lastLoginDate, blockState, userLevel
	    FROM frdUserData AS U
		INNER JOIN frdMonthlyCard AS M ON U.userId = M.userId
	    LEFT OUTER JOIN frdLogBuy AS L ON U.userId = L.userId
	    WHERE U.userId = $userId";
	$db->prepare($sql);
	$db->execute();
	$row = $db->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
	if ( $row == false ) {
	    echo    '검색된 사용자가 없습니다 4<br>'.$sql;
	    exit;
	}

    $userId = $row['userId'];
    $name = $row['name'];
    $lastLoginDate = $row['lastLoginDate'];
    $blockState = $row['blockState'];
    $productId = $row['productId'];
    $maxHeart = $row['maxHeart'];
    $heart = $row['heart'];
    $powder = $row['powder'];
    $jewel = $row['jewel'];
    $gold = $row['gold'];
    $maxHeart = $row['maxHeart'];
    $skillPoint = $row['skillPoint'];
    $accuSkillPoint = $row['accuSkillPoint'];
	$reg_date = $row['reg_date'];
	$lastLoginDate = $row['lastLoginDate'];
	$userLevel = $row['userLevel'];
?>

// <script>
// $(function(){
// //  $('.title_category').text('konnichiha');
// //  $("#title_category").text('konnichiha');
//   document.getElementById('#title_category').text('wefwef');
// });
// </script>

<div class="table-responsive">
<table class="table table-bordered">
<tbody>


<!-- <div class="row">
  <div class="col-xs-12 col-md-8">.col-xs-12 .col-md-8</div>
  <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
</div>


<div class="row">
  <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
  <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
  <div class="col-xs-6 col-md-4">.col-xs-6 .col-md-4</div>
</div>

<div class="row">
  <div class="col-xs-6">.col-xs-6</div>
  <div class="col-xs-6">.col-xs-6</div>
</div> -->

    <tr>
		<th BGCOLOR="#393939"><font color="#FFFFFF">유저ID</font></th>
	    <td>
	        <label class="bg-success"><?php echo $userId ?></label>
	    </td>

		
		<th BGCOLOR="#393939"><font color="white">닉네임</font></th>
		<td>
		    <label class="bg-success" ><?php echo $name ?></label>
		</td>

		<th BGCOLOR="#393939"><font color="white">마지막로그인</font></th>
		<td>
		    <label class="bg-success" ><?php echo $lastLoginDate ?></label>
		</td>
    </tr>

    <tr>
	    <th BGCOLOR="#393939"><font color="white">우편발송</font></th>
	    <td>
		    <a href="./modal_sendmail.php?country=<?php echo $country?>&userId=<?php echo $userId?>"
		    data-toggle="modal" data-target="#Modal" class="btn btn-default">우편 전송</a>
	    </td>

	    <th BGCOLOR="#393939"><font color="white">보유골드</font></th>
	    <td>
	    	<form class="form-inline">
  				<div class="form-group">
    				<label class="bg-success"><?php echo $gold ?></label>
    				<a href='./modal_update_gold.php?country=<?php echo $country?>&userId=<?php echo $userId?>&gold=<?php echo $gold?>'
		    		data-toggle="modal" data-target="#Modal_UpdateGold" class="btn btn-default">변경</a>
    			</div>
    		</form>
	    </td>

	    <th BGCOLOR="#393939"><font color="white">보유보석</font></th>
	    <td>
	    	<form class="form-inline">
  				<div class="form-group">
		            <label class="bg-success" ><?php echo $jewel ?></label>
		            <a href='./modal_update_jewel.php?country=<?php echo $country?>&userId=<?php echo $userId?>&jewel=<?php echo $jewel?>'
		    		data-toggle="modal" data-target="#Modal_UpdateJewel" class="btn btn-default">변경</a>
		        </div>
		    </form>
	    </td>
    </tr>

    <tr>
    	<th BGCOLOR="#393939"><font color="white">강제 킥</font></th>
	    <td>
		    <a href="./modal_kickuser.php?country=<?php echo $country?>&userId=<?php echo $userId?>"
		    data-toggle="modal" data-target="#Modal_KickUser" class="btn btn-default">세션 종료</a>
	    </td>

	    <th BGCOLOR="#393939"><font color="white">마법가루</font></th>
	    <td>
	    	<form class="form-inline">
  				<div class="form-group">
		            <label class="bg-success" ><?php echo $powder ?></label>
		            <a href='./modal_update_powder.php?country=<?php echo $country?>&userId=<?php echo $userId?>&powder=<?php echo $powder?>'
		    		data-toggle="modal" data-target="#Modal_UpdatePowder" class="btn btn-default">변경</a>
		        </div>
		    </form>
	    </td>

	    <th BGCOLOR="#393939"><font color="white">유저레벨</font></th>
	    <td>
			<div class="row">
				<div class="col-xs-11 col-sm-6">
					<p class="bg-success" ><?php echo $userLevel ?></p>
				</div>
			</div>
		</td>
    </tr>

    <tr>
    	<th BGCOLOR="#393939"><font color="white">메달</font></th>
	    <td>
	    	<form class="form-inline">
  				<div class="form-group">
		            <label class="bg-success" ><?php echo $skillPoint ?></label>
		            <a href='./modal_update_skillpoint.php?country=<?php echo $country?>&userId=<?php echo $userId?>&skillPoint=<?php echo $skillPoint?>'
		    		data-toggle="modal" data-target="#Modal_UpdateSkillPoint" class="btn btn-default">변경</a>
		        </div>
		    </form>
	    </td>

	    <th BGCOLOR="#393939"><font color="white">누적메달</font></th>
	    <td>
	    	<form class="form-inline">
  				<div class="form-group">
		            <label class="bg-success" ><?php echo $accuSkillPoint ?></label>
		            <a href='./modal_update_accuskillpoint.php?country=<?php echo $country?>&userId=<?php echo $userId?>&accuSkillPoint=<?php echo $accuSkillPoint?>'
		    		data-toggle="modal" data-target="#Modal_UpdateAccuSkillPoint" class="btn btn-default">변경</a>
		        </div>
		    </form>
	    </td>

	    <th BGCOLOR="#393939"><font color="white">번개</font></th>
	    <td>
	    	<form class="form-inline">
  				<div class="form-group">
		            <label class="bg-success" ><?php echo $heart.'/'.$maxHeart ?></label>
		            <a href='./modal_update_maxheart.php?country=<?php echo $country?>&userId=<?php echo $userId?>&maxHeart=<?php echo $maxHeart?>'
		    		data-toggle="modal" data-target="#Modal_UpdateMaxHeart" class="btn btn-default">변경</a>
		        </div>
		    </form>
	    </td>
    </tr>

    <tr>
	    <th BGCOLOR="#393939"><font color="white">차단상태</font></th>
	    <td>
		    <div class="row">
		        <div class="col-xs-11 col-sm-6">
		            <p class="bg-success">
					<?php
					if($blockState == 5)
						echo "영구정지";
					else if ( $blockState > 0)
						echo "정지";
					else
						echo "정상";
					?>
		            </p>
		        </div>
		    </div>
	    </td>

	    <th BGCOLOR="#393939"><font color="white">우편함</font></th>
	    <td>
	    	<a type='submit' class="btn btn-default" 
	    	href='./mail_info.php?
	    	inputVal=<?php echo $inputVal?>&type=<?php echo $type?>&country=<?php echo $country?>&userId=<?php echo $userId?>&fr_date=<?php echo date("Y-m-d H:i:s", strtotime ("-1 months"))?>&to_date=<?php echo date('Y-m-d H:i:s', strtotime ("+1 days"))?>'
		    >목록확인</a>
	    </td>

    </tr>
    </tbody>  
</table>
</div>
<br><hr>

<strong>결제 내역</strong><br>
#. reg_date 가 서버에서 지급을 실행한 시간입니다.(17년 8월 15일 이전은 해당 기록이 없을 수 있습니다.)
<br>
#. purchaseTime 은 현재 정상적인 데이터가 아닙니다.
<br>
#. 서버내에 아래 로우가 없다면 지급 실행이 되지 않은 상태 입니다.
<br> 
<br>

<div class="table-responsive">
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th scope="col">userId</th>
				<th scope="col">packageName</th>
				<th scope="col">orderId</th>
				<th scope="col">productId</th>
				<th scope="col">purchaseTime</th>
				<th scope="col">isComplete</th>
				<th scope="col">reg_date(게임서버구매기록시간)</th>
			</tr>
		</thead>
		<tbody>
			<?php
                $sql = " SELECT *  FROM frdLogBuy  WHERE userId = $userId";
                $db -> prepare($sql);
                $db -> execute();
                while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                    echo "<tr>";
                    echo "<td>{$row['userId']}</td>";
                    echo "<td>{$row['packageName']}</td>";
                    echo "<td>{$row['orderId']}</td>";
                    echo "<td>{$row['productId']}</td>";
                    echo "<td>{$row['purchaseTime']}</td>";
                    echo "<td>{$row['isComplete']}</td>";
                    echo "<td>{$row['reg_date']}</td>";
                    echo "</tr>";
                }
			?>
		</tbody>
	</table>
</div>


<br>
<br>
<strong>구매 오류 관련 내역</strong><br>
#. 로그인 디비의 내역 입니다.
<br>
#. 지급 실패 예시
<br> 
cpFe: package_hrd: Error consuming sku package_hrd (response: 8:Item not owned)
<br>
위 메시지의 경우 재접하여도 지급되지 않습니다. 수동으로 지급 하여야 합니다.
<br>

<div class="table-responsive">
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th scope="col">userId</th>
				<th scope="col">message</th>
				<th scope="col">update_date(마지막수정일)</th>
			</tr>
		</thead>
		<tbody>
			<?php
	
                $sql = " SELECT *  FROM frdLocalLog  WHERE userId = $userId";
                $loginDB -> prepare($sql);
                $loginDB -> execute();
                while ($row = $loginDB -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
                    echo "<tr>";
                    echo "<td>{$row['userId']}</td>";
                    echo "<td>{$row['log']}</td>";
                    echo "<td>{$row['update_date']}</td>";
                    echo "</tr>";
                }
			?>
		</tbody>
	</table>
</div>

<a href='./modal_send_package.php?country=<?php echo $country?>&userId=<?php echo $userId?>'
data-toggle="modal" data-target="#Modal_SendPackage" class="btn btn-primary">패키지 지급하기</a>


<?php
	include_once(G5_PATH.'/tail.php');
?>

