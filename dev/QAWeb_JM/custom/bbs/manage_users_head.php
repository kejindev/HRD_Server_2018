
<?php
include_once('../common.php');
include_once(G5_PATH.'/head.php');
include_once('../define.php');
include_once('../title_category.php');
if ( empty($inputVal) )
 
    if ( empty($inputVal) ) {
        $type= "type_nick";
        $country= "Korea";
    }
?>

<div class="well well-sm">
    <form id="fsearch" name="fsearch" class="form-inline" method="get">
        <!-- <a class="btn btn-default">계정검색</a>-->
        <div class="form-group">
            <select name="country" id="country" class="form-control">
                <?php 
                    for ( $i=0; $i<count($arr_country); $i++ ) {
                        $id = $arr_country[$i]['id'];
                        echo "<option value=".$id.(($id==$country)?" selected>":">").$arr_country[$i]['name']."</option>";
                    }
                ?>
            </select>

            <select name="type" id="type" class="form-control">
                <?php
                    echo "<option value=type_nick".(($type=="type_nick")?" selected>":">")."닉네임</option>";
                    echo "<option value=type_userId".(($type=="type_userId")?" selected>":">")."유저 ID</option>";
                    echo "<option value=type_platformId".(($type=="type_platformId")?" selected>":">")."플랫폼</option>";
                    echo "<option value=type_email".(($type=="type_email")?" selected>":">")."이메일</option>";
                ?>
            </select>
        </div>
        <div class="input-group">
            <input type="text" name="inputVal" value=<?php echo empty($inputVal)?"''":$inputVal?> id="inputVal" required="" class="required form-control" placeholder="여기에 입력">
            <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">검색</button>
            </span>
        </div>
<!--     </form> -->
</div>
<br>

#. 검색된 계정과 유저의 정보가 동일 한지 확인 후 진행 해주세요. 
<br>
#. 특수문자 구분에 유의해 주세요 ' ' 안에 스페이스나 특수문자가 있을 수 있습니다. 
<br>
<br>
<br>
<br>
<br><strong>
# 선택된 조건 :  <?php echo  $type.", ".$inputVal;?>
</strong>

<br><hr>
<?php
    if ( !$inputVal ){
        echo    '검색된 사용자가 없습니다(1)';
        exit;
    }

    $multidb = new MultiDB();
    $loginDB = $multidb->get_login_db($country);
    switch ($type) {
        case 'type_nick':
            $sql="SELECT * FROM t_Account_User WHERE name='$inputVal'";
            break;
        case 'type_userId':
            $sql="SELECT * FROM t_Account_User WHERE userId=$inputVal";
            break;
        case 'type_platformId':
            $sql="SELECT * FROM t_Account_User WHERE connectId=$inputVal";
            break;
        case 'type_email':
            $sql="SELECT * FROM t_Account_User WHERE email='$inputVal'";
            break;
            
        default:
            break;
    }
    $loginDB->prepare($sql);
    $stmt = $loginDB->execute();
    $loginRowCount = $loginDB->rowCount();

    $fetchCount = (int)$_REQUEST['fetchCount'];
    if ( $fetchCount >= $loginRowCount )
        $fetchCount = 0;
    for ( $i=0; $i<=$fetchCount; $i++ )
        $loginRow = $loginDB->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
    if ( $loginRow == false ) {
        echo    '검색된 사용자가 없습니다(2)<br>'.$sql;
        exit;
    }

    $userId = $loginRow['userId'];
    $db = $multidb->get_game_db($country, $userId);
    if (!$db){
        echo    '검색된 사용자가 없습니다(3)<br>'.$sql;
        exit;
    }
    
    $loginRowCount = $loginDB->rowCount();
    if ( $loginRowCount > 1 ) {
?>
        <input type ='hidden' name = 'fetchCount' value = <?php echo ((int)$fetchCount +1);?>>
        <button type="submit" class="btn btn-primary" >검색된 사용자가 <?php echo $loginRowCount?>명 입니다.<br> 검색된 다른 유저 보기</button>
        <p></p>
<?php
    }
?>


</form>