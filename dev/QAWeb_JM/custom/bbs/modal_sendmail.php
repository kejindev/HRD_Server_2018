<?php
include_once ('../common.php');
?>

<script>

	function all_option_none() {
		document.forms['form_option'].item.style.display = 'none';
		document.forms['form_option'].summon.style.display = 'none';
		document.forms['form_option'].stone.style.display = 'none';
		document.forms['form_option'].probability_item.style.display = 'none';
		document.forms['form_option'].probability_summon.style.display = 'none';
		document.forms['form_option'].probability_stone.style.display = 'none';
		document.forms['form_option'].item_idx.style.display = 'none';
		document.forms['form_option'].item_count.style.display = 'none';
		document.forms['form_option'].shard_7a7d.style.display = 'none';
		document.getElementById('reward_select_box_any').style.display = 'none';
		document.getElementById('reward_select_box_value').style.display = 'none';
		document.getElementById('reward_select_box_value_add').style.display = 'none';

		document.getElementById('item_count').setAttribute('type', 'number');
		//document.getElementById('reward_select_box_value').value = 0;
	}

	function set_reward_select_box_value_add() {
		var reward_type = document.getElementById('item_group').value;
		var get_reward_item_value = document.getElementById('reward_item');
		var get_reward_value = document.getElementById('item_count');
		var get_reward_summon_value = document.getElementById('reward_summon');
		var get_reward_select_box_value = document.getElementById('reward_select_box_value');
		var get_reward_select_box_target = document.getElementById('reward_select_box_target');
		var get_reward_select_box_any = document.getElementById('reward_select_box_any');

		if (reward_type == 'select_box_summon' || reward_type == 'select_box_item' || reward_type == 'select_box_any') {
			if (get_reward_value.value.length <= 1) {
				if (reward_type == 'select_box_item') {
					get_reward_value.value = get_reward_item_value.value;
					get_reward_select_box_target.value = '1';
				} else if (reward_type == 'select_box_summon') {
					get_reward_value.value = get_reward_summon_value.value;
					get_reward_select_box_target.value = '2';
				} else if (reward_type == 'select_box_any') {
					get_reward_value.value = get_reward_select_box_value.value;
					get_reward_select_box_target.value = get_reward_select_box_any.value;
				}
			} else {

				var reward_value_arr = get_reward_value.value.split('/');
				if (reward_value_arr.length > 4) {
					alert('5개 이상 입력불가.');
					return;
				}

				if (reward_type == 'select_box_item') {
					get_reward_value.value = get_reward_value.value + '/' + get_reward_item_value.value;
					get_reward_select_box_target.value = get_reward_select_box_target.value + '/1';
				} else if (reward_type == 'select_box_summon') {
					get_reward_value.value = get_reward_value.value + '/' + get_reward_summon_value.value;
					get_reward_select_box_target.value = get_reward_select_box_target.value + '/2';
				} else if (reward_type == 'select_box_any') {
					get_reward_value.value = get_reward_value.value + '/' + get_reward_select_box_value.value;
					get_reward_select_box_target.value = get_reward_select_box_target.value + '/' + get_reward_select_box_any.value;
				}
			}
		}
	}
</script>

 <div class="modal" id="itemIdTableModal" aria-hidden="true" style="display: none; z-index: 1060;">
	<div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">아이템 ID 테이블</h4>
        </div><div class="container"></div>
        <div class="modal-body">

			골드   : 5000<br>
			경험치 : 5001<br>
			티겟   : 5002<br>
			번개   : 5003<br>
			메달   : 5005<br>
			가루   : 5006<br>
			영웅ID : 0~500, 501~506 (1~6성 랜덤)<br>
			유물ID : 1000~1500 (0~500), 1501~1506 (1~6성 랜덤)<br>
			마법ID : 10000~10500 (0~500), 10501~10506 (1~6성 랜덤)<br>

		</div>
        
      </div>
    </div>
</div>


<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        	<h4 
        		class="modal-title">유저 <?php echo $userId?> 우편지급
        	</h4>

        </div>
        <div class="container"></div>

		<div class="modal-body">
			<div class="table-responsive">
				<form action="./UserManager.php?country=<?php echo $country?>&platform=<?php echo $platform?>&CMD=<?php echo "sendmail"?>&userId=<?php echo $userId?>" name="form_option" method="post">

					<h4>
						<strong>아이템 ID </strong>
						<a data-toggle="modal" href="#itemIdTableModal" class="btn btn-primary">아이템 IDs</a>
					</h4>
					
					<input type="number" name="item_idx" value="" id="item_idx" class="required form-control" max="9999999" required>

					<input type="number" step="0" style="margin-top:5px; display:none;" name="reward_select_box_value" value="0" id="reward_select_box_value" class="form-control default" placeholder="0" required>
					<input type="text" step="0" style="display:none;" name="reward_select_box_target" value="" id="reward_select_box_target" class="form-control default" >
					<input type="button" name="reward_select_box_value_add" value="추가" id="reward_select_box_value_add" class="form-control hasDatepicker" onclick="set_reward_select_box_value_add()" size="15" maxlength="10" style="margin-top:5px; display:none;">

					<h4><strong>아이템 갯수</strong></h4>
					<input type="number" name="item_count" value="" id="item_count" class="required form-control" max="999999" required>
					<h4><strong>지급 메세지</strong></h4>
					<select name="mail_msg" class="form-control">
						<option value="11-1">운영자지급</option>
					</select>
					
					<h4><strong>MEMO</strong></h4>
					<input type="text" name="memo" value="" id="memo"  class="required form-control" placeholder="<?php echo $user_cash?>" required>
					<div class="modal-footer">
						<button type="submit" class="btn btn-default">
							<i class="fa fa-rotate-left"></i>확인
						</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" onclick="window.location.reload();">
							<i class="fa fa-times-circle"></i>Close
						</button>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>

<script>
	function form_option_submit() {
		document.form_option.submit();
	}
</script>
<script>
	function hide() {
		var elem = $('#items');
		elem.style.visibility = 'hidden';
	}
</script>
<script>
	function show() {
		var elem = $('#items');
		elem.style.visibility = 'visible';
	}
</script>

