<?php
include_once ('../common.php');
?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 
                class="modal-title">최대행동력 변경
                <small>Update MaxHeart</small>
            </h4>
        </div>

        <div class="container"></div>
        <div class="modal-body">
            <div class="table-responsive">

                <form action="./UserManager.php?country=<?php echo $country?>&CMD=change_maxHeart&userId=<?php echo $userId?>"name="form_option" method="post">

                    <div class="row">
                        <div class="col-xs-4 col-md-4">
                            <strong>현재 최대행동력 :</strong>
                        </div>
                        <div class="col-xs-8 col-md-8">
                            <strong><?php echo $maxHeart?></strong>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4 col-md-4">
                            <strong>최대행동력 변경 :</strong>
                        </div>
                        <div class="col-xs-8 col-md-8">
                            <input type="number" name="change_maxHeart" value="" id="change_maxHeart" class="required form-control" max="999999" placeholder=<?php echo $maxHeart?> required>
                        </div>
                    </div>
                    <br>
 
                    <h4><strong>MEMO</strong></h4>
                     <input type="text" name="memo" value="" id="memo" class="required form-control" placeholder="메모입력">
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default"><i class="fa fa-rotate-left"></i>확인</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="window.location.reload();"><i class="fa fa-times-circle"></i>Close</button>                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
function form_option_submit(){
    document.form_option.submit();
}                
</script>
