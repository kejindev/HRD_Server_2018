<?php
	include_once('../common.php');

	$resultData   = $_POST['resultData'];
	$headerNames  = $_POST['headerNames'];
	$filePath = PATH_FILE_DESCEND;
	$file = fopen($filePath, "w");
	if ( $file == false || empty($resultData) )
		die(false);

	$len = count($resultData);
	$headerNames = trim($headerNames, ",")."\r\n";
	fputs($file, $headerNames);
	for ( $i=0; $i<$len; $i++ ) {
		$str="";
		for ( $j=0; $j< count($resultData[$i]); $j++ ) {
			$str .= $resultData[$i][$j].",";
		}

		apc_store('Descend_'.$resultData[$i][0], $resultData[$i]);
		$str = trim($str, ",")."\r\n";
		fputs($file, $str);
	}
	fclose($file);

	$resultDataJson = json_encode($resultData);
	
	foreach ( $productions as $production ) {
		$ssh = 'sudo /usr/bin/rsync -avz --delete '.PATH_FILE_DESCEND." root@".$production.":".PATH_LIVE_DATA;
		$result = exec($ssh);
		if ( empty($result) )
			die("Fail rsync");
	}

	foreach ( $URLs_LIVE_ROOT_QAWEB as $url_live_root_qaWeb ) {
		$post_data = array();
		$post_data['resultDataJson'] = $resultDataJson;
		$URL = $url_live_root_qaWeb."custom/descend/gameserver_updateDescend.php";

		$CH = curl_init();
		curl_setopt($CH, CURLOPT_URL, $URL);
		curl_setopt($CH, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($CH, CURLOPT_POST, 1);
		curl_setopt($CH, CURLOPT_POSTFIELDS, $post_data);
		$result = curl_exec($CH);
		if ( $result != "Success" )
			die ( $result );
	}

	die($result);
?>
