
function getHTML(arr_resourceTypes, isLoad, callback) {
	var curDate = document.getElementById('fr_date').value;
    var returnHTML = "<br><br><strong>복귀 기준 날짜</strong><br>";
    returnHTML += "<input style='width:80px;' type='text' value="+curDate+" class='form-control' name='return_date' id='return_date' placeholder='이날 이전접속자'>";
    returnHTML += "<br>";             

	loadScript('../lib/selectRewards.js', function(){
		returnHTML += "<br><br><strong>복귀 보상 선택</strong><br>";
		var selectRewardHTML = GetSelectRewardsHTML(arr_resourceTypes);
		returnHTML += selectRewardHTML;
		callback(returnHTML);
		if ( isLoad ) {
			LoadReturnDate();
			LoadSelectRewards();
		}
	});
}

function LoadReturnDate() {
    var apcNames = [
        'EventData'+GetCountry()+'800'
    ];

    $.ajax({
        type: "POST",
        url: "../lib/apc_fetch.php",
        data: {
            "apcName": JSON.stringify(apcNames)
        },
        cache: false,
        async: false,
        success: function(data) {
            result = data;
        }
    });

    var apcContents = JSON.parse(result);
    $("#return_date").attr("value", apcContents[0]['returnDate']);
 }
