<?php
	include_once('../../../config.php');
	include_once('../../lib/MultiDBConnect.php');
	
	$apcName   = $_POST['apcName'];
	$country   = $_POST['country'];
	$fr_date   = $_POST['fr_date'];
	$to_date   = $_POST['to_date'];

	$shopVerInform = apc_fetch($apcName);
	if ( $shopVerInform == false )
		$newVer = 2;
	else
		$newVer = ((int)$shopVerInform['ver'])+1;

	$shopVerInform = array();
	$shopVerInform['fr_ymdHis'] = $fr_date."0005";
	$shopVerInform['to_ymdHis'] = $to_date."0005";
	$shopVerInform['ver'] = $newVer;

	$result = apc_store($apcName, $shopVerInform);
	if ( $result < 1) {
		die("Fail apc_store EventData100");
		return;
	}

	$apcName = 'EventData'.$country.'100';
	$result = apc_store($apcName, $shopVerInform);
	if ( $result < 1) {
		die("Fail apc_store EventData100");
		return;
	}


	if ( false == strpos($country, 'QA') ) {
		foreach ( $URLs_LIVE_ROOT_QAWEB as $url_live_root_qaWeb ) {
			$post_data = array();
			$post_data['apcName'] = json_encode(array($apcName));
			$post_data['apcContents'] = json_encode(array($shopVerInform));
			$URL = $url_live_root_qaWeb."custom/lib/apc_store.php";

			$CH = curl_init();
			curl_setopt($CH, CURLOPT_URL, $URL);
			curl_setopt($CH, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($CH, CURLOPT_POST, 1);
			curl_setopt($CH, CURLOPT_POSTFIELDS, $post_data);
			$result = curl_exec($CH);
			if ( $result != "Success" )
				die ( $result );
		}
	}

	
	switch ($country) {
	case 'Korea_QA':
		$dbIdxs = array(0);
		break;
	
	default:
		$dbIdxs = array();
		$storeIds = array(0,1,2);
		foreach ( $storeIds as $storeId ) {
    		$dbIdxs[] = 200 + (int)$storeId;
    		$dbIdxs[] = 500 + (int)$storeId;
    		$dbIdxs[] = 700 + (int)$storeId;
    		$dbIdxs[] = 800 + (int)$storeId;
    	}
		break;
	}
    $multidb = new MultiDB();
    $dbs = array();
    foreach ( $dbIdxs as $dbIdx )
    	$dbs[] = $multidb->get_game_db($country, $dbIdx);

    
    $fr_str = date('Y-m-d H:i:s', strtotime('20'.$fr_date."0000"));
    $to_str = date('Y-m-d H:i:s', strtotime('20'.$to_date."0000"));

    $sqls[] = "UPDATE `frdShopData` SET startDate = '$fr_str', endDate = '$to_str' 
    			where (shopItemId >= 3006 and shopItemId <= 3011)";
	
	foreach ( $dbs as $db ) {
		foreach ( $sqls as $sql ) {
			$db -> prepare($sql);
			$row = $db -> execute();
            if ( $row == false ) {
            	die("DB Query Fail - sql:".$sql);
			    return;
            }
		}
	}
	die("Success");
?>
