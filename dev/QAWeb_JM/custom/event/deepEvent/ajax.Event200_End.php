<?php
	include_once('../../../config.php');
	include_once('../../lib/MultiDBConnect.php');
	function customError($errno, $errstr) {
		echo "Err Number = ".$errno.", Err Str = ".$errstr;
		die ( "Err Number = ".$errno.", Err Str = ".$errstr );
	}
	set_error_handler("customError");
	$resetDB			= (int)$_POST['resetDB'];
	$resultArrJson   	= $_POST['resultArr'];
	$country   			= $_POST['country'];

	$apcName = 'EventData'.$country.'200';
	apc_delete($apcName);
	$result = apc_store($apcName, $resultArrJson);
	if ( $result < 1) {
		die("Fail apc_store EventData200");
		return;
	}

	if ( false == strpos($country, 'QA') ) {
		foreach ( $URLs_LIVE_ROOT_QAWEB as $url_live_root_qaWeb ) {
			$post_data = array();
			$post_data['apcName'] = json_encode(array($apcName));
			$post_data['apcContents'] = json_encode(array($resultArrJson));
			$URL = $url_live_root_qaWeb."custom/lib/apc_store.php";

			$CH = curl_init();
			curl_setopt($CH, CURLOPT_URL, $URL);
			curl_setopt($CH, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($CH, CURLOPT_POST, 1);
			curl_setopt($CH, CURLOPT_POSTFIELDS, $post_data);
			$result = curl_exec($CH);
			if ( $result != "Success" )
				die ( $result );
		}
	}

	
	switch ($country) {
	case 'Korea_QA':
		$dbIdxs = array(0);
		break;
	
	default:
		$dbIdxs = array();
		$storeIds = array(0,1,2);
		foreach ( $storeIds as $storeId ) {
    		$dbIdxs[] = 200 + (int)$storeId;
    		$dbIdxs[] = 500 + (int)$storeId;
    		$dbIdxs[] = 700 + (int)$storeId;
    		$dbIdxs[] = 800 + (int)$storeId;
    	}
		break;
	}
    $multidb = new MultiDB();
    $dbs = array();
    foreach ( $dbIdxs as $dbIdx )
    	$dbs[] = $multidb->get_game_db($country, $dbIdx);

    if ( $resetDB > 0 ) {
	    $sqls[] = "DROP TABLE IF EXISTS `Event_Buy_First`";
	    $sqls[] = "CREATE TABLE `Event_Buy_First` (
					 `userId` int(11) NOT NULL,
					 `price` int(11) NOT NULL,
					 `state` tinyint(4) NOT NULL DEFAULT '0',
					 `reg_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					 PRIMARY KEY (`userId`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";

		
		foreach ( $dbs as $db ) {
			foreach ( $sqls as $sql ) {
				$db -> prepare($sql);
				$row = $db -> execute();
	            if ( $row == false ) {
	            	die("DB Query Fail - sql:".$sql);
				    return;
	            }
			}
		}
		die("Success and Reset db");
	}
	else
		die("Success and No Reset db");
?>
