<?php
	include_once('../../../config.php');
	include_once('../../lib/MultiDBConnect.php');
	
	$resetDB			= (int)$_POST['resetDB'];
	$shopVerName   = $_POST['shopVerName'];
	$packageNamesJson  = $_POST['packageNames'];
	$country   = $_POST['country'];
	$fr_date   = $_POST['fr_date'];
	$to_date   = $_POST['to_date'];

	$packageNames = json_decode($packageNamesJson);

	$shopVerInform = apc_fetch($shopVerName);
	if ( $shopVerInform == false )
		$newVer = 2;
	else
		$newVer = ((int)$shopVerInform['ver'])+1;

	$shopVerInform = array();
	$shopVerInform['fr_ymdHis'] = $fr_date."0005";
	$shopVerInform['to_ymdHis'] = $to_date."0005";
	$shopVerInform['ver'] = $newVer;

	$result = apc_store($shopVerName, $shopVerInform);
	if ( $result < 1) {
		die("Fail apc_store EventData300");
		return;
	}

	$apcContents = array();
	$apcContents['shopVerInform'] = $shopVerInform;
	$apcContents['packageNames'] = $packageNames;

	$apcName = 'EventData'.$country.'300';
	apc_delete($apcName);
	$result = apc_store($apcName, $apcContents);
	if ( $result < 1) {
		die("Fail apc_store EventData300");
		return;
	}


	if ( false == strpos($country, 'QA') ) {
		foreach ( $URLs_LIVE_ROOT_QAWEB as $url_live_root_qaWeb ) {
			$post_data = array();
			$post_data['apcName'] = json_encode(array($apcName));
			$post_data['apcContents'] = json_encode(array($apcContents));
			$URL = $url_live_root_qaWeb."custom/lib/apc_store.php";

			$CH = curl_init();
			curl_setopt($CH, CURLOPT_URL, $URL);
			curl_setopt($CH, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($CH, CURLOPT_POST, 1);
			curl_setopt($CH, CURLOPT_POSTFIELDS, $post_data);
			$result = curl_exec($CH);
			if ( $result != "Success" )
				die ( $result );
		}
	}

	
	switch ($country) {
	case 'Korea_QA':
		$dbIdxs = array(0);
		break;
	
	default:
		$dbIdxs = array();
		$storeIds = array(0,1,2);
		foreach ( $storeIds as $storeId ) {
    		$dbIdxs[] = 200 + (int)$storeId;
    		$dbIdxs[] = 500 + (int)$storeId;
    		$dbIdxs[] = 700 + (int)$storeId;
    		$dbIdxs[] = 800 + (int)$storeId;
    	}
		break;
	}
    $multidb = new MultiDB();
    $dbs = array();
    foreach ( $dbIdxs as $dbIdx )
    	$dbs[] = $multidb->get_game_db($country, $dbIdx);

    
    $fr_str = date('Y-m-d H:i:s', strtotime('20'.$fr_date."0000"));
    $to_str = date('Y-m-d H:i:s', strtotime('20'.$to_date."0000"));

    $sql = "UPDATE `frdShopData` SET startDate = '$fr_str', endDate = '$to_str' where ";
    $sql .= "(shopItemId >= 3006 and shopItemId <= 3011) or ";
    foreach ( $packageNames as $packageName ) {
    	$sql .= "name like '".$packageName."' or ";
    }
    $sql = trim($sql, " or ");

    $sqls = array();
    $sqls[] = $sql;

    $ymd =  '_bb'.date('ymd');
    $sqls[] = "update frdLogBuy set productId = concat( productId, '$ymd' ) WHERE productId LIKE 'package%' AND productId NOT LIKE '%bb%' AND productId NOT LIKE '%ab%'";	//abillity

    if ( $resetDB > 0 ) {
	    $sqls[] = "DROP TABLE IF EXISTS `Event_Buy_Accu`";
	    $sqls[] = "CREATE TABLE `Event_Buy_Accu` (
					 `userId` int(11) NOT NULL,
					 `AmountOfPayment` int(11) NOT NULL,
					 `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					 `reg_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					 PRIMARY KEY (`userId`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8";

		$sqls[] = "DROP TABLE IF EXISTS `Event_Buy_First`";
	    $sqls[] = "CREATE TABLE `Event_Buy_First` (
					 `userId` int(11) NOT NULL,
					 `price` int(11) NOT NULL,
					 `state` tinyint(4) NOT NULL DEFAULT '0',
					 `reg_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					 PRIMARY KEY (`userId`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
	}

	foreach ( $dbs as $db ) {

		foreach ( $sqls as $sql ) {
			$db -> prepare($sql);
			$row = $db -> execute();
	        if ( $row == false ) {
	        	die("DB Query Fail - sql:".$sql);
			    return;
	        }
		}

	}
	die("Success");

?>
