<?php
	$arr_Event = array();

	$arr_Event['100'] = array('name'=>'100_AD_황금상자 오픈');			
	$arr_Event['200'] = array('name'=>'200_AD_첫구매 추가 보상');
	$arr_Event['300'] = array('name'=>'300_AD_패키지 판매');
	$arr_Event['400'] = array('name'=>'400_핫타임(재화 일정 배수 지급)');	
	$arr_Event['500'] = array('name'=>'500_카페 이벤트');
	$arr_Event['600'] = array('name'=>'600_AD_암시장 오픈');
	$arr_Event['700'] = array('name'=>'700_유물경험치 두배 획득');
	$arr_Event['800'] = array('name'=>'800_AD_복귀 유저 보상 지급');
	$arr_Event['900'] = array('name'=>'900_번개 절반(스테이지 입장시 번개소모 절반, 빠방 포함)');
	$arr_Event['1000'] = array('name'=>'1000_누적 구매시 추가 보상');
	$arr_Event['1100'] = array('name'=>'1100_웹뷰 이벤트(작업 안됨)');
	$arr_Event['1200'] = array('name'=>'1200_출첵 시 추가 보상');
	$arr_Event['1300'] = array('name'=>'1300_고탑 입장시 보상 이벤트');
	$arr_Event['1400'] = array('name'=>'1400_게임 접속 이벤트(작업 안됨)');
	$arr_Event['1500'] = array('name'=>'1500_번개 무한(스테이지 입장시 번개소모 x, 빠방 미포함)');
	$arr_Event['1600'] = array('name'=>'1600_플레이 369 이벤트(작업 안됨)');
	$arr_Event['1700'] = array('name'=>'1700_매일 레아패키지 보상 두배');
	$arr_Event['1800'] = array('name'=>'1800_AD_매일 스테이지 클리어 일정 횟수 이상시 보상 지급');
	$arr_Event['1900'] = array('name'=>'1900_심연의 던전 5의 배수 클리어시 보상 두배');

	$keys = array();
	foreach ( $arr_Event as $key=>$value ) {
		$keys[] = $key;
	}
	foreach ( $keys as $key ) {
		$arr_Event[$key]['bannerIds'] = apc_fetch('eventBannerIds'.$key);
	}

?>

<script>
	var arr_Event = <?php echo json_encode($arr_Event)?>;
</script>