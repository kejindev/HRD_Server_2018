<?php
	$apcName		  = $_REQUEST['apcName'];
	$eventFileContentsJson   = $_REQUEST['eventFileContentsJson'];//$argv[1];
	if ( empty($eventFileContentsJson) ) {
		echo "Fail Json";
		return;
	}
	$eventFileContents = json_decode($eventFileContentsJson);

	$toDeleteAPC = new APCIterator('user', '/^'.$apcName.'/', APC_ITER_VALUE);
	if ( false == apc_delete($toDeleteAPC) ) {
		echo ("Fail Del APC");
		return;
	}


	$len = count($eventFileContents);

	for ( $i=0; $i<$len; $i++ ) {
		// echo "(".$apcName.$eventFileContents[$i][0].", ".$eventFileContents[$i].")";
		// continue;

		$result = apc_store($apcName.'_'.$eventFileContents[$i][0], $eventFileContents[$i]);
		if ( $result == false ) {
			echo "Fail Store";
			return;
		}

		$tempArr = apc_fetch($apcName.'_'.$eventFileContents[$i][0]);
		if ( $tempArr == false ) {
			echo "Fail Fetch";
			return;
		}
	}
	echo "Success";
?>
