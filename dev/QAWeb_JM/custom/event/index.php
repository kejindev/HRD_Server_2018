
<?php
	include_once('../common.php');
	include_once(G5_PATH.'/head.php');
	include_once('../define.php');
//	include_once('../define_idtable.php');
	include_once('eventName.php');
	

	$title_category = "이벤트 관리";
	include_once('../title_category.php');

	set_error_handler("customError");
	if ( empty($country) )
		$country = "Korea_QA";

	switch ($country) {
		case 'Korea':
			$eventFilePath = PATH_FILE_EVENT_PARENT."Event_Korea.csv";
			break;
		
		case 'Korea_QA':
			$eventFilePath = PATH_FILE_EVENT_PARENT."Event_Korea_QA.csv";
			break;

		default:
			echo $country;
			return;
	}
	
	$file = fopen($eventFilePath, "r");

	$eventFileTitles = array();
	$eventFileContents = array();

	$buffer = fgets($file);
	$buffer = str_replace('"','',$buffer);
	$buffer = explode(",", trim($buffer, ","));
	$eventFileTitles[] = $buffer;

	$colCount = count($buffer);
	while (!feof($file)) {
		
		$buffer = fgets($file);
		$buffer = str_replace('"','',$buffer);
		$buffer = explode(",", trim($buffer, ","));
		if ( count($buffer) !== $colCount )
			continue;

		$eventFileContents[] = $buffer;
		
	}
	fclose($file);

	$curDate = (int)date('ymdH');
	$eventName = "";
	$dynamicContent = array();


?>


<div class="well well-sm">
    <form id="fsearch" name="fsearch" class="form-inline" method="get">
        <!-- <a class="btn btn-default">계정검색</a>-->
        서버 선택: 
        <div class="form-group">
            <select name="country" id="country" class="form-control">
                <?php 
                    for ( $i=0; $i<count($arr_country); $i++ ) {
                        $id = $arr_country[$i]['id'];
                        echo "<option value=".$id.(($id==$country)?" selected>":">").$arr_country[$i]['name']."</option>";
                    }
                ?>
            </select>
        </div>
        <div class="input-group">
            <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">서버 변경</button>
            </span>
        </div>

    </form>
</div>

<div style="vertical-align:middle">
	<div class="alert" style="width:30%; height:40px; text-align:center; line-height: 10px; vertical-align:middle; background:#99FFFF;">
			<strong>적용되어 있는 이벤트</strong>
	</div>
</div>

    <!--vertical-align:bottom;
		border: 2px dashed #f69c55;
		line-height: 80px; 
		height: 80px; 
-->
	


<?php

//	$eventImagePath = PATH_LOGINSERVER."banners/";
//	$eventImagePath = "../../../LOGIN_HRD/banners/";
	$eventImagePath = "http://14.63.196.82:3081/banners/";
	$tempEventCount=0;
	$eventModalNumPointer=0;
	foreach ( $eventFileContents as $row ) {
		if ( $tempEventCount > 0 ) {
			$bannerId = $row[2];
			$imgName = $bannerId.".png";
			$imgPath = $eventImagePath.$imgName;

			$eventId = 			(int)$row[0];
			$startDT = 			(int)$row[1];
			$continueHours = 	(int)$row[3];
			$endDT =			(int)substr($row[4], 0, 8);
			$addHourQuery = "+".$continueHours." hours";
			$newEndDT = date("ymdH", strtotime($addHourQuery, strtotime('20'.$startDT.'0000')));
			if ( $endDT != $newEndDT ) {
				echo "<script>alert(\"".$imgName."의 이벤트 등록 기간이 잘못되었습니다.\\n시간을 확인해주세요.\");</script>";
			}
			if ( $endDT < $curDate )
				$status = "<font color=\"red\"><종료됨></font>";
			else if ( $curDate < $startDT )
				$status = "<font color=\"blue\"><진행 예정></font>";
			else
				$status = "<font color=\"green\"><진행중></font>";

			$eventFileTitlesJson = json_encode($eventFileTitles);
			$eventFileContentsJson = json_encode($eventFileContents);
			echo $imgPath;
			echo "
				<div class=\"row\">
					<a href=\"./modal_addEvent.php?eventFilePath=".$eventFilePath."&eventId=".$eventId."&bannerId=".$bannerId."&fr_date=".$startDT."&to_date=".$endDT."&country=".$country."\" data-toggle=\"modal\" data-target=\"#Modal_Event".(0)."\">
						<img src=\"$imgPath\" style=\"width:30%; margin-left:15px;\" hspace=15px title=\"마우스 오버시 텍스트\" alt=\"$imgPath\" align=\"left\">
					  
						
						<strong><font size=5>".$eventName."</font></strong>".$status."

				  		<strong><br>".substr($startDT, 0, 2)."년 ".substr($startDT, 2, 2)."월 ".substr($startDT, 4, 2)."일 ".substr($startDT, 6, 2)."시부터<br>"
						  .substr($endDT, 0, 2)."년 ".substr($endDT, 2, 2)."월 ".substr($endDT, 4, 2)."일 ".substr($endDT, 6, 2)."시까지<br>".$continueHours."시간동안 진행<br><br>".
						"</strong>
					</a>
					<br>


					

					<button class=\"btn btn-default\" onclick=\"onDelete(".$eventId.")\" >
						<img src=\"".G5_URL."/img/btn_delete.png\" width=\"15\" height=\"15\" >
							삭제
					</button>
					</div>
				<br><br>
			";
			$tempEventCount--;
			if ( false == empty($dynamicContent[$eventId]) )
				echo $dynamicContent[$eventId];
		}
		else {

			if ( false == empty($row[1]) ) {
				$tempEventCount = (int)$row[1];
				$eventName = $arr_Event[ $row[0] ]['name'];
				continue;
			}

		}
	}

	
?>



<br><br>
<?php 
echo "<a href=\"./modal_addEvent.php?eventFilePath=".$eventFilePath."&country=".$country."\" data-toggle=\"modal\" data-target=\"#Modal_Event".(0)."\" class=\"btn btn-default\">
		<img src=\"".G5_URL."/img/btn_modify.png\" width=\"15\" height=\"15\" >
			이벤트 추가
	</a>";
?>
<!-- eventModalNumPointer -->
  	

<script>
	

	function onDelete(eventId) {
		var eventFilePath = "<?php echo $eventFilePath?>";
		$.ajax({
            type: "POST",
            url: "./ajax.saveEvent.php",
            data: {
                "Action": "Del",
                "eventId": eventId,
                "filePath": eventFilePath
            },
            cache: false,
            async: false,
            success: function(data) {
                result = data;
            }
        });
        result = result.substring(1,result.length);
        if ( false == result.includes("Success") ) {
            alert("!!!이벤트 삭제가 실패하였습니다!!!<br>"+result);    
        }
        else {
            alert("이벤트가 삭제되었습니다.");
        }

        // if ( -1 == window.location.href.search("#refresh") )
        // 	window.location = window.location.href + "#refresh";
		window.location.reload();
	}


</script>

<style type="text/css">
    .buttonWrap {
        /*position:relative;*/
        float:left;
        cursor:pointer;
        
        overflow:hidden;
        background-image:url('/banners/100145.png');
        width:150px;
        height:40px;
    }
    .buttonWrap input {
        /*position：absolute;*/
        margin-left:10px;



        filter:alpha(opacity=0);
        opacity:0;
        -moz-opacity:0;
        cursor:pointer;
        width:74px;
        height:20px;
    }
</style>

<?php
	include_once(G5_PATH.'/tail.php');
?>
