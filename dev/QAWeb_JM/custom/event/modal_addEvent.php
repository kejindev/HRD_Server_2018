<?php
    include_once ('../common.php');
    include_once('eventName.php');
    include_once('../lib/loadOuterJS.php');
    include_once('../define_idtable.php');
?>


<div class="modal-dialog" id="modal">
	<div class="modal-content">
		<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        	<center><h4 class="modal-title">
                <strong>
                    <?php echo empty($eventId) ? '이벤트 추가' : '이벤트 수정' ?>
                </strong>
        	</h4></center>

        </div>

		<div class="modal-body">


            <form id="form_AddEvent">
                

                <center><select id="sel_event" onChange="showSub(this.options[this.selectedIndex].value)">
                    <option value=0>이벤트 종류를 선택해주세요</option>
                    <?php
                        foreach ( $arr_Event as $key=>$value ) {
                            echo "<option value=".$key.">".$value['name']."</option>";
                        }
                    ?>
                </select></center>
                <br>
                <div name="list_selEvent" id="list_selEvent" style="display: ;"></div>
                <div name="addEvent" id="addEvent" style="display: ;"></div>
                <br>
                <center><div class="form-inline">
                    <strong style=\"margin-left:10px;\">이벤트 기간 선택</strong>
                    </div>
                    <div class="form-inline">
                        <input style="width:80px;" type='text' value= <?php echo empty($fr_date) ? date('ymd').'00' : $fr_date?> class='form-control' name='fr_date' id='fr_date' placeholder='여기부터'> 
                        ~
                        <input style="width:80px;" type='text' value= <?php echo empty($to_date) ? date('ymd').'00' : $to_date?> class='form-control' name='to_date' id='to_date' placeholder='여기까지'>    
                    </div>
                    <div id="deepEventInform"></div>
                    <br><br>

                    <button style="margin-left:15px;" type="button" class="btn btn-primary" onclick="onAddEvent()">위 내용으로 이벤트 적용</button>
                </center>
               
            </form> 


            
		</div>
	</div>
</div>




<script>
    var loadedEventId = 0;
    var loadedBannerId = 0;
    var isLoad = false;
    var arr_resourceTypes = <?php echo json_encode($arr_resourceTypes)?>;
    var arr_resourceName = <?php echo json_encode($arr_resourceName)?>;
    var uploadedImg = null;

    $(document).ready(function() {
        $('.modal').on('hidden.bs.modal', function(e) { 
            $(this).removeData();
            $(this).empty();
        });

        loadedBannerId = <?php echo empty($bannerId) ? 0 : $bannerId?>;
        loadedEventId = <?php echo empty($eventId) ? 0 : $eventId?>;
        if ( loadedEventId > 0 ) {
            isLoad = true;
            var bigEventId = Math.floor(loadedEventId/100)*100;
            var sel_event = document.getElementById('sel_event');

            $("#sel_event").val(bigEventId).prop("selected", true);
            

            showSub(bigEventId);
            if ( loadedBannerId > 0 ) {
                document.getElementById('fr_date').value = <?php echo empty($fr_date) ? 0 : $fr_date?>;
                document.getElementById('to_date').value  = <?php echo empty($to_date) ? 0 : $to_date?>;

                var list = arr_Event[bigEventId]['bannerIds'];
                for ( var i=0; i<list.length; i++ ) {
                    if ( list[i] == loadedBannerId ) {
                        $("input[name=chk_eventImage][value="+i+"]").prop("checked", true);// "checked");
                    //    $("input[name=chk_eventImage]").val(i).prop("checked", "checked");
                        break;
                    }
                }
            }
        }
        
    });

    




    function GetCountry() {
        return "<?php echo $country?>";
    }
    

    function showSub(eventId) {
        clearOuterScript();

        var url = 'deepEvent/Event'+eventId+'_Start.js';
        loadScript(url, function(){
            getHTML(arr_resourceTypes, isLoad, function(val){
                deepEventInform.innerHTML = val;
            });

            alert(arr_Event[eventId]['bannerIds'].length);
            var str="<br><center><strong style=\"margin-left:10px;\">배너 이미지 선택</strong></center>";
            if ( arr_Event[eventId] == null )
                var list = new Array();
            else {
                var list = arr_Event[eventId]['bannerIds'];
            }
            for ( var i=0; i<list.length; i++ ) {
                var id = list[i];
                if ( i%2 == 0 )
                    str += "<div style=\"margin-left:8px; margin-top:20px;\" class=\"row\">";
                var filePath = "<?php echo G5_URL?>"+"/banners/"+id+".png";
                    
                str += "<img src=\""+filePath+"\" style=\"width:40%;\" hspace=16px title=\"마우스 오버시 텍스트\" align=\"top\">";
                str += "<input type=\"radio\" name=\"chk_eventImage\" value="+i+ ">";
                if ( i%2 == (2-1) )
                    str += "</div>";
            }

            var i = list.length;
            var id = 99999;
            if ( i%2 == 0 )
                str += "<div style=\"margin-left:8px; margin-top:20px;\" class=\"row\">";
            var filePath = "<?php echo G5_URL?>"+"/banners/"+id+".png";
                
            str += "<img src=\""+filePath+"\" style=\"width:40%;\" hspace=16px title=\"마우스 오버시 텍스트\" align=\"top\">";
            str += "<input type=\"radio\" name=\"chk_eventImage\" value="+i+">";
            if ( i%2 == (2-1) )
                str += "</div>";

            if ( (list.length-1)%2 !== (2-1) )
                str += "</div>";

            var list_selEvent = document.getElementById("list_selEvent");
            list_selEvent.innerHTML = str;

        //    selectedBannerId = list[0];

            $("input[name=chk_eventImage]").click( function(){
                var selectedIdx = $(this).val();
                if ( selectedIdx >= list.length ) {

                    var addStr = "";
                    addStr += "<br><div class=\"row\" style=\"margin-left:12px;\">";
                    addStr += "새로운 배너 이미지 추가<input id=\"bannerImg\" type=file >";
                    addStr += "</div>";
                    addStr += "<div id=\"holder\" class=\"row\" style=\"margin-left:14px; margin-top:10px;\"></div>";

                    var addEvent = document.getElementById("addEvent");
                    addEvent.innerHTML = addStr;

                    var upload = document.getElementById('bannerImg'),
                    holder = document.getElementById('holder');
                 
                    upload.onchange = function (e) {
                        e.preventDefault();

                        var file = upload.files[0],
                        reader = new FileReader();
                        reader.onload = function (event) {
                            var img = new Image();
                            img.src = event.target.result;

                            var imgCanvas = document.createElement("canvas");
                            var imgContext = imgCanvas.getContext("2d");
                            imgCanvas.width = img.width;
                            imgCanvas.height = img.height;
                            imgContext.drawImage(img, 0, 0, img.width, img.height);
                            
                            uploadedImg = imgCanvas.toDataURL("image/png");
                            if (uploadedImg.length < 100) {
                                reader.readAsDataURL(file);
                                return;
                            }

                            holder.innerHTML = '';
                            if ( img.width > 300 ) {
                                var percent = 300 / img.width;
                                img.width *= percent;
                                img.height *= percent;
                            }
                            holder.appendChild(img);
                        };
                        reader.readAsDataURL(file);
                    };

                }
                else {

                    loadedBannerId = list[selectedIdx];
                    uploadedImg = null;
                    var addEvent = document.getElementById("addEvent");
                    addEvent.innerHTML = "";

                }
            });

        });

    }
</script>

<script>

    function onAddEvent() {
        var form_AddEvent = document.getElementById('form_AddEvent');
        if ( !(form_AddEvent.fr_date.value && form_AddEvent.to_date.value) ) {
            alert("이벤트 날짜를 입력하세요.");
            return;
        }


        var bigEventId = form_AddEvent.sel_event.value;

        if ( uploadedImg ) {
            loadedBannerId = <?php echo apc_fetch("newBannerId") ?>;

            <?php apc_store("newBannerId", ((int)apc_fetch("newBannerId"))+1) ?>

            var filePath = "<?php echo PATH_LOGINSERVER?>"+"banners/"+loadedBannerId+".png";
            alert("filePath="+filePath+", BannerId="+loadedBannerId);
            $.ajax({
                type: "POST",
                url: "./ajax.saveImg.php",
                data: {
                    "bannerId": loadedBannerId,
                    "eventId": bigEventId,
                    "filePath": filePath,
                    "img": uploadedImg
                },
                cache: false,
                async: false,
                success: function(data) {
                    result = data;
                }
            });

            result = result.substring(1,result.length);
            if ( result != "Success") {
                alert("!!!이미지 추가에 실패하였습니다!!!<br>"+result);    
            }
            else {
                alert("서버에 이미지가 추가 되었습니다.");
            }
        }

        var url = 'deepEvent/Event'+bigEventId+'_End.js';
        loadScript(url, function(){
            var eventFilePath = "<?php echo $eventFilePath?>";
            $.ajax({
                type: "POST",
                url: "./ajax.saveEvent.php",
                data: {
                    "Action": isLoad ? "Mod" : "Add",
                    "bannerId": loadedBannerId,
                    "eventId": isLoad ? loadedEventId : bigEventId,
                    "filePath": eventFilePath,
                    "fr_date": form_AddEvent.fr_date.value,
                    "to_date": form_AddEvent.to_date.value
                },
                cache: false,
                async: false,
                success: function(data) {
                    result = data;
                }
            });

            result = result.substring(1,result.length);
            if ( false == result.includes("Success") ) {
                alert("!!!이벤트 등록이 실패하였습니다!!!\nmodal_addEvent\nStr="+result);
                return;
            }


            var returnEventId = Number(result.replace("Success", ""));

            play(loadedBannerId);
            alert("이벤트가 적용되었습니다.\n"+result);
            $("#Modal_Event0").modal('hide');
            window.location.reload();
        });
        
        
    }

</script>

<script>
	function form_option_submit() {
		document.form_option.submit();
	}
</script>
<script>
	function hide() {
		var elem = $('#items');
		elem.style.visibility = 'hidden';
	}
</script>
<script>
	function show() {
		var elem = $('#items');
		elem.style.visibility = 'visible';
	}
</script>
