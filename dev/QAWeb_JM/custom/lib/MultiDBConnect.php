<?php
$preDir = getcwd();
chdir(PATH_GAMESERVER);
require_once 'common/DB_Info.php';
require_once 'common/DB.php';
chdir($preDir);

class MultiDB {
    public function __construct() {
    }

    public function set_multi_db($country) {

        // $real_db_info = GetAllGameDBs($country);

        // $temp_host = $GLOBALS['DB_HOST'];
        // $temp_name = $GLOBALS['DB_NAME'];
        // $temp_pass = $GLOBALS['DB_PASS'];

        // $db_obj = array();
        // foreach ($real_db_info as $re) {
           
        //     $GLOBALS['DB_HOST'] = $re['DB_HOST'];
        //     $GLOBALS['DB_NAME'] = $re['DB_NAME'];
        //     $GLOBALS['DB_PASS'] = $re['DB_PASS'];
        //     $db = new DB();
        //     $db_obj[] = $db;

        // }

        // $GLOBALS['DB_HOST'] = $temp_host;
        // $GLOBALS['DB_NAME'] = $temp_name;
        // $GLOBALS['DB_PASS'] = $temp_pass;

        return $db_obj;
    }

    public function get_login_db($country) {
        SetGlobalDBInform( $country, 0 );

        $temp_host = $GLOBALS['DB_HOST'];
        $temp_name = $GLOBALS['DB_NAME'];
        $temp_user = $GLOBALS['DB_USER'];
        $temp_pass = $GLOBALS['DB_PASS'];
        $temp_port = $GLOBALS['DB_PORT'];

        $GLOBALS['DB_HOST'] = $GLOBALS['LOGIN_DB_HOST'];
        $GLOBALS['DB_NAME'] = $GLOBALS['LOGIN_DB_NAME'];
        $GLOBALS['DB_USER'] = $GLOBALS['LOGIN_DB_USER'];
        $GLOBALS['DB_PASS'] = $GLOBALS['LOGIN_DB_PASS'];
        $GLOBALS['DB_PORT'] = $GLOBALS['LOGIN_DB_PORT'];

        $db = new DB();
     
        $GLOBALS['DB_HOST'] = $temp_host;
        $GLOBALS['DB_NAME'] = $temp_name;
        $GLOBALS['DB_USER'] = $temp_user;
        $GLOBALS['DB_PASS'] = $temp_pass;
        $GLOBALS['DB_PORT'] = $temp_port;



        return $db;
    }

    public function get_game_db($country, $userId) {
        $temp_host = $GLOBALS['DB_HOST'];
        $temp_name = $GLOBALS['DB_NAME'];
        $temp_user = $GLOBALS['DB_USER'];
        $temp_pass = $GLOBALS['DB_PASS'];
        $temp_port = $GLOBALS['DB_PORT'];

        $accNo = substr($userId, 0,3);

        SetGlobalDBInform( $country, (int)$accNo );

        $db = new DB();
     
        // $GLOBALS['DB_HOST'] = $temp_host;
        // $GLOBALS['DB_NAME'] = $temp_name;
        // $GLOBALS['DB_USER'] = $temp_user;
        // $GLOBALS['DB_PASS'] = $temp_pass;
        // $GLOBALS['DB_PORT'] = $temp_port;

        return $db;
    }

}
?>

