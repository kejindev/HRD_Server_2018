<?php
    $apcNameJson          = $_REQUEST['apcName'];
    $apcContentsJson      = $_REQUEST['apcContents'];
    if ( empty($apcContentsJson) ) {
        echo "Fail Get Contents";
        return;
    }

    $arr_apcName = json_decode($apcNameJson);
    $arr_apcContents = json_decode($apcContentsJson);
    for ( $i=0; $i<count($arr_apcName); $i++ ) {
        $apcName = $arr_apcName[$i];
        $apcContents = $arr_apcContents[$i];

        apc_delete($apcName);
        $result = apc_store($apcName, $apcContents);
        if ( $result == false ) {
            echo "Fail Store";
            return;
        }
    }

    echo "Success";
?>