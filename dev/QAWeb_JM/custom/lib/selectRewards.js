    
    // $(document).ready(function() {
    //     alert("isLoad = "+isLoad);
    //     if ( false == isLoad )
    //         return;
    // });

    var temp_selected_rewardType;
    var temp_selected_nextId=1;

    function LoadSelectRewards() {
        var apcNames = [
            ('EventSelectRewards'+GetCountry()+loadedBannerId)
        ];

        $.ajax({
            type: "POST",
            url: "../lib/apc_fetch.php",
            data: {
                "apcName": JSON.stringify(apcNames)
            },
            cache: false,
            async: false,
            success: function(data) {
                result = data;
            }
        });

        var apcContents = JSON.parse(result);
        var arr_selectRewards = apcContents[0];
        var arr_rewardTypes = arr_selectRewards[0];
        var arr_rewardCounts = arr_selectRewards[1];
        for ( var k=0; k<arr_rewardTypes.length; k++ ) {

            var type = arr_rewardTypes[k];
            var count = arr_rewardCounts[k];
            var isFind = false;
            for ( var i=0; i<arr_resourceTypes.length; i++ ) {
                var arr_minIdxs = arr_resourceTypes[i]['minIdx'];
                var arr_maxIdxs = arr_resourceTypes[i]['maxIdx'];
                for ( var j=0; j<arr_minIdxs.length; j++ ) {
                    if ( arr_minIdxs[j] <= type && type <= arr_maxIdxs[j] ) {
                        isFind = true;
                        $("#sel_rewards"+k+" option:eq("+(i+1)+")").attr("selected", "selected");
                        showSub_sr(k, i);
                        $("#sel_rewards_sub"+k+" option:eq("+(j+1)+")").attr("selected", "selected");
                        showSubSub_sr(k, j);
                        $("#sel_rewards_final"+k).val(type).attr("selected", "selected");
                        $("#sel_rewards_count"+k).attr("value", count);
                        onAppend_SR(this);
                        break;
                    }
                }
                if ( isFind )
                    break;
            }

        }
    }

    function GetSelectRewardsHTML(arr_resourceTypes) {
        var startIdx = 0;

        var returnHTML = "";
        returnHTML += "<div><button type=\"button\" class=\"btn-primary\" onclick=\"onAppend_SR(this)\" >보상 추가</button></div>";
       
        returnHTML += "<div>";
        returnHTML += "<select id=\"sel_rewards"+startIdx+"\" onChange=\"showSub_sr(0, this.options[this.selectedIndex].value)\">";
        returnHTML += "<option value=-1>종류 선택</option>";
        for ( var i=0; i<arr_resourceTypes.length; i++ ) {
            var name = arr_resourceTypes[i]['name'];
            returnHTML += "<option value="+i+">"+name+"</option>";
        }

        returnHTML +="</select>";
        returnHTML += "<select style=\"margin-left:10px;\" id=\"sel_rewards_sub"+startIdx+"\" onChange=\"showSubSub_sr(0, this.options[this.selectedIndex].value)\">";
        returnHTML +="</select>";
        returnHTML += "<select style=\"margin-left:10px;\" id=\"sel_rewards_final"+startIdx+"\">";
        returnHTML +="</select>";
        returnHTML +="<label style=\"margin-left:10px;\">갯수:</label><input type='text' size='6' maxlength='6' value=1 id=\"sel_rewards_count"+startIdx+"\" onkeypress=\"if (event.keyCode<48|| event.keyCode>57) event.returnValue=false;\" style='IME-MODE:disabled;' placeholder=\"갯수 입력\">"; 
        returnHTML += "</div>";
        return returnHTML;
    }

    
    function showSub_sr(selIdx, idx) {
        temp_selected_rewardType = idx;
        var str="";
        var types = arr_resourceTypes[idx]['type'];
        str += "<option value=-1>종류 선택</option>";
        for ( var i=0; i<types.length; i++ ) {
            name = types[i];
            str += "<option value="+i+">"+name+"</option>";
        }
        document.getElementById('sel_rewards_sub'+selIdx).innerHTML = str;
        
    }

    function showSubSub_sr(selIdx, finalIdx) {
        var str="";
        var minIdx = arr_resourceTypes[temp_selected_rewardType]['minIdx'][finalIdx];
        var maxIdx = arr_resourceTypes[temp_selected_rewardType]['maxIdx'][finalIdx];
        str += "<option value=-1>종류 선택</option>";
        for ( var i=minIdx; i<=maxIdx; i++ ) {
            if ( arr_resourceName[i] ) {
                str += "<option value="+i+">"+arr_resourceName[i]+"</option>";
            }
            
        }
        document.getElementById('sel_rewards_final'+selIdx).innerHTML = str;
    }

    function GetSelectHTML(idx) {

        var returnHTML = "<div>";
        returnHTML += "<select id=\"sel_rewards"+idx+"\" onChange=\"showSub_sr("+idx+", this.options[this.selectedIndex].value)\">";
        returnHTML += "<option value=-1>종류 선택</option>";
        for ( var i=0; i< arr_resourceTypes.length; i++ ) {
            var name = arr_resourceTypes[i]['name'];
            returnHTML += "<option value="+i+">"+name+"</option>";
        }

        returnHTML +="</select>";
        returnHTML += "<select style=\"margin-left:10px;\" id=\"sel_rewards_sub"+idx+"\" onChange=\"showSubSub_sr("+idx+", this.options[this.selectedIndex].value)\">";
        returnHTML +="</select>";
        returnHTML += "<select style=\"margin-left:10px;\" id=\"sel_rewards_final"+idx+"\">";
        returnHTML +="</select>";
        returnHTML +="<label style=\"margin-left:10px;\">갯수:</label><input type='text' size='6' maxlength='6' value=1 id=\"sel_rewards_count"+idx+"\" onkeypress=\"if (event.keyCode<48|| event.keyCode>57) event.returnValue=false;\" style='IME-MODE:disabled;' placeholder=\"갯수 입력\">"; 
        returnHTML +="<button style=\"margin-left:10px;\" class=\"btn-danger\" onclick=\"onDelete_SR(this)\">x</button><br>";
        returnHTML +="</div>";
        return returnHTML;
    }

    function onDelete_SR(obj) {
        document.getElementById('deepEventInform').removeChild(obj.parentNode);
    }

    function onAppend_SR(obj) {
        document.getElementById('deepEventInform').insertAdjacentHTML( 'beforeend', GetSelectHTML(temp_selected_nextId++) );
    //    temp_selected_nextId = temp_selected_nextId+1;
    }

    function PoprewardsAndApcstore(bannerId, path_custom_lib) {
        var resultArr = [new Array(), new Array()];

        var pointer =0;
        for ( var i=0; i<temp_selected_nextId; i++ ) {
            var selName = 'sel_rewards_final'+i;
            var sel = document.getElementById(selName);
            if ( sel && sel.value && sel.value != -1 ) {
                resultArr[0][pointer] = sel.value;

                var selCount = document.getElementById('sel_rewards_count'+i); 
                resultArr[1][pointer++] = selCount.value;
            }
        }

        var apcNames = [
            ('EventSelectRewards'+GetCountry()+bannerId)
        ];
        var apcContents = [
            resultArr
        ];

        $.ajax({
            type: "POST",
            url: path_custom_lib+"apc_store.php",
            data: {
                "apcName": JSON.stringify(apcNames),
                "apcContents": JSON.stringify(apcContents)
            },
            cache: false,
            async: false,
            success: function(data) {
                result = data;
            }
        });
        if ( result != "Success" ) {
            alert("!!!이벤트 정보 APC STORE에 실패하였습니다!!!<br>"+result);    
        }

        return resultArr;
    }
