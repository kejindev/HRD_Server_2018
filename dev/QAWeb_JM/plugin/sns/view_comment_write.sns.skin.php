<?php
include_once('./_common.php');

if (!$board['bo_use_sns']) die('');
?>

<div class="btn-group" role="group" aria-label="SNS댓글공유">
<?php
//============================================================================
// 페이스북
//----------------------------------------------------------------------------
if ($config['cf_facebook_appid']) {
    $facebook_user = get_session("ss_facebook_user");
    if (!$facebook_user) {
        include_once(G5_SNS_PATH."/facebook/src/facebook.php");
        $facebook = new Facebook(array(
            'appId'  => $config['cf_facebook_appid'],
            'secret' => $config['cf_facebook_secret']
        ));

        $facebook_user = $facebook->getUser();

        if ($facebook_user) {
            try {
                $facebook_user_profile = $facebook->api('/me');
            } catch (FacebookApiException $e) {
                error_log($e);
                $facebook_user = null;
            }
        }
    }

    if ($facebook_user) {
        echo '<span class="btn btn-default">';
		echo '<i class="fa fa-facebook" id="facebook_icon"></i>';
        echo ' <input type="checkbox" name="facebook_checked" id="facebook_checked" '.(get_cookie('ck_facebook_checked')?'checked':'').' value="1">';
		echo '</span>';
    } else {
        $facebook_url = $facebook->getLoginUrl(array("redirect_uri"=>G5_SNS_URL."/facebook/callback.php", "scope"=>"publish_stream,read_stream,offline_access", "display"=>"popup"));
        echo '<a href="'.$facebook_url.'" id="facebook_url" onclick="return false;" class="btn btn-default"><i class="fa fa-facebook" style="'.($facebook_user?'':'color:#ddd;').'" id="facebook_icon"></i> <input type="checkbox" name="facebook_checked" id="facebook_checked" disabled value="1"></a>';
        echo '<script>$(function(){ $(document).on("click", "#facebook_url", function(){ window.open(this.href, "facebook_url", "width=600,height=250"); }); });</script>';
    }
}
//============================================================================


//============================================================================
// 트위터
//----------------------------------------------------------------------------
if ($config['cf_twitter_key']) {
    $twitter_user = get_session("ss_twitter_user");
    if (!$twitter_user) {
        include_once(G5_SNS_PATH."/twitter/twitteroauth/twitteroauth.php");
        include_once(G5_SNS_PATH."/twitter/twitterconfig.php");

        $twitter_user = false;
        /*
        if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
            $twitter_url = G5_SNS_URL."/twitter/redirect.php";
        } else {
            $access_token = $_SESSION['access_token'];
            $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
            $content = $connection->get('account/verify_credentials');

            switch ($connection->http_code) {
                case 200:
                    $twitter_user = true;
                    $twitter_url = $connection->getAuthorizeURL($token);
                    break;
                default :
                    $twitter_url = G5_SNS_URL."/twitter/redirect.php";
            }
        }
        */
        $access_token = $_SESSION['access_token'];
        $connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
        $content = $connection->get('account/verify_credentials');

        switch ($connection->http_code) {
            case 200:
                $twitter_user = true;
                $twitter_url = $connection->getAuthorizeURL($token);
                break;
            default :
                $twitter_url = G5_SNS_URL."/twitter/redirect.php";
        }
    }

    if ($twitter_user) {
        echo '<span class="btn btn-default">';
		echo '<i class="fa fa-twitter" id="twitter_icon"></i>';
        echo ' <input type="checkbox" name="twitter_checked" id="twitter_checked" '.(get_cookie('ck_twitter_checked')?'checked':'').' value="1">';
		echo '</span>';
    } else {
        echo '<a href="'.$twitter_url.'" id="twitter_url" onclick="return false;" class="btn btn-default"><i class="fa fa-twitter" style="'.($twitter_user?'':'color:#ddd;').'"  id="twitter_icon"></i> <input type="checkbox" name="twitter_checked" id="twitter_checked" disabled value="1"></a>';
        echo '<script>$(function(){ $(document).on("click", "#twitter_url", function(){ window.open(this.href, "twitter_url", "width=600,height=250"); }); });</script>';
    }
}
//============================================================================
?>
		<a href="<?php echo G5_SNS_URL?>/view_comment_modal.php" data-toggle="modal" data-target="#Modal"  class="btn btn-default" style="padding:9px 15px;"><i class="fa fa-question-circle"></i></a>
</div>