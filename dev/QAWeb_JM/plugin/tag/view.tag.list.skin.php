<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if (!$board['bo_use_tag']) return;


if(!sql_query(" DESC ".COMP_TAG, false)) {
	$que = "
		create table ".COMP_TAG."(
		ct_idx  int not null auto_increment,
		bo_table varchar(20) not null default '' comment '게시판코드',
		wr_id int not null default '0' comment '게시판시퀀스', 
		ct_tag varchar(100) not null default '' comment '키워드', 
		ct_ip varchar(25) not null default '' comment 'ip', 
		ct_regdate datetime not null default '0000-00-00 00:00:00', 
		primary key( ct_idx ) , 
		index ".COMP_TAG."_index1(ct_tag) 
		) comment '키워드테이블'";

	sql_query( $que, false );
}
$que	=	"alter table ".$write_table." add column tags varchar(200) default '' comment '키워드'";
sql_query( $que , false );
$arrtag = explode(",", $list[$i]['tags']);
if( $list[$i]['tags'] ){
?>
<div style="line-height:180%;">
	<?php foreach( $arrtag as $key => $val ){ $val = trim($val);?>
	<a href="<?php echo G5_BBS_URL?>/board.php?bo_table=<?php echo $bo_table?>&amp;sfl=tags&amp;stx=<?php echo $val?>" class="cate_label tooltip-top" title="해당 키워드글 모아보기"><i class="fa fa-tag fa-fw"></i><?php echo $val?></a>
	<?php }?>
</div>
<?php }?>