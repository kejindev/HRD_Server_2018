<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if (!$board['bo_use_tag']) return;
?>
<div class="form-group input-group  col-md-12">
		<span class="input-group-addon tooltip-top" title="" for="wr_tags_input" data-original-title="태그"><i class="fa fa-tag"></i><strong class="sr-only">태그</strong></span>
        <input type="text" name="tags" id="wr_tags_input" size="50"value="<?php echo $write['tags']?>" class="form-control" placeholder="입력 ex) 부트스트랩,반응형,탬플릿">
</div>