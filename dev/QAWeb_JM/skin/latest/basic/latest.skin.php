<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>

<div class="panel panel-default">
<div class="panel-heading">
    <i class="fa fa-folder-o"></i> <?php echo $bo_subject; ?>
  	<a class="pull-right tooltip-top" href='<?php echo G5_BBS_URL ?>/board.php?bo_table=<?php echo $bo_table ?>' onfocus='this.blur()'  title=" 전체 보기 "><i class="fa fa-list"></i></a>
</div>
<div class="panel-body">
  	<ul class="list-unstyled">
    <?php for ($i=0; $i<count($list); $i++) {  ?>
        <li class="text-cut">
            <?php
            //echo $list[$i]['icon_reply']." ";
			echo "<span class=\"pull-right\">";
            if (isset($list[$i]['icon_new'])) echo " " . $list[$i]['icon_new'];
            if (isset($list[$i]['icon_hot'])) echo " " . $list[$i]['icon_hot'];
            if (isset($list[$i]['icon_file'])) echo " " . $list[$i]['icon_file'];
            if (isset($list[$i]['icon_link'])) echo " " . $list[$i]['icon_link'];
            if (isset($list[$i]['icon_secret'])) echo " " . $list[$i]['icon_secret'];
			echo "</span>";
            echo "<a href=\"".$list[$i]['href']."\">";
            if ($list[$i]['is_notice'])
                echo "<i class='fa fa-microphone fa-border tooltip-top' title='공지'></i> ".$list[$i]['subject']."";
            else
                echo $list[$i]['subject'];

            if ($list[$i]['comment_cnt'])
                echo $list[$i]['comment_cnt'];
            echo "</a>";
             ?>
        </li>
    <?php }  ?>
    <?php if (count($list) == 0) { //게시물이 없을 때  ?>
    <li>게시물이 없습니다.</li>
    <?php }  ?>
    </ul>
</div>
</div>