<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>

<div class="clearfix"></div>

</div>
</div>
</div>
<a href="#" class="btn btn-default back-to-top"><span class="glyphicon glyphicon-chevron-up"></span></a>

<!--스크롤TOP 버튼-->
<script type="text/javascript">
$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.back-to-top').fadeIn();
        } else {
            $('.back-to-top').fadeOut();
        }
    });

    $('.back-to-top').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});
</script>


<!-- 하단 시작 -->
<div id="layout-line" class="<?php echo $index['wr_1']; ?>"></div>
    <div id="footer" class="<?php echo $index['wr_1']; ?>">
        <div class="footer-box">
		     <ul>
                <a href="http://cafe.naver.com/frd2015" title="히랜디 카페 가기"><i>
                   <!--  <i class="fa fa-facebook"></i> -->
                    <img src="<?php echo G5_URL ?>/img/btn_hrd.png" width="30" height="30" ></i>
                </a>
             <!--    <a href="http://cafe.naver.com/frd2015" class="btn btn-default tooltip-top" title="히랜디 카페 가기"><i class="fa fa-facebook"></i></a> -->
                <li class="pull-right"><i class="fa fa-bookmark"></i> Copyright ©  <a href="#" style="color:red; font-weight:bold;">JM</a></li>
			 </ul>
        </div>
    </div>

<!--
<?php
if(G5_DEVICE_BUTTON_DISPLAY && !G5_IS_MOBILE) { ?>
<a href="<?php echo get_device_change_url(); ?>" id="device_change">모바일 버전으로 보기</a>
<?php
}
if ($config['cf_analytics']) {
    echo $config['cf_analytics'];
}
?>
-->
<!-- } 하단 끝 -->

<script>
$(function() {
    // 폰트 리사이즈 쿠키있으면 실행
    font_resize("container", get_cookie("ck_font_resize_rmv_class"), get_cookie("ck_font_resize_add_class"));
});
</script>

<?php
include_once(G5_THEME_PATH."/tail.sub.php");
?>