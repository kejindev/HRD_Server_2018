<?php
include_once('_common.php');

$g5['title'] = '설정';
include_once('./head.php');

$save_file = G5_DATA_PATH.'/cache/theme/basic/index.php';
if(is_file($save_file))
    include($save_file);
?>

<form name="findex" id="findex" method="post" action="./indexupdate.php">
        
	   <div class="well well-sm">레이아웃 설정</div>
          <div class="col-md-6">
		    <div class="input-group" for="wr_1">
				<span class="input-group-addon">레이아웃 폭선택</span>
				<select name='wr_1' class="form-control"> 
				   <option value='pull' <? if($index[wr_1] == "pull") echo "selected"; ?>> 풀화면형
				   <option value='boxed' <? if($index[wr_1] == "boxed") echo "selected"; ?>> 박스형
				</select> 
			</div>
		  </div>

          <div class="col-md-6">
		    <div class="input-group" for="wr_2">
				<span class="input-group-addon">메뉴 위치</span>
				<select name='wr_2' class="form-control"> 
				   <option value='left' <? if($index[wr_2] == "left") echo "selected"; ?>> 왼쪽
				   <option value='right' <? if($index[wr_2] == "right") echo "selected"; ?>> 오른쪽
				</select>
			</div>
		  </div>
		  <div class="clearfix"></div>
		  <hr />
	      
		  <div class="well well-sm">부트스트랩 설정</div>

           <div class="col-md-6">
		    <div class="input-group" for="wr_3">
				<span class="input-group-addon">부트스트랩 스타일</span>
				<select name='wr_3' class="form-control"> 
				   <option value='normal' <? if($index[wr_3] == "nomal") echo "selected"; ?>> 기본
				   <option value='edge' <? if($index[wr_3] == "edge") echo "selected"; ?>> 둥근모서리 제거
				</select>
			</div>
		  </div>
		  
          <div class="col-md-6">
		    <div class="input-group" for="wr_4">
				<span class="input-group-addon">Textarea 반응형</span>
				<select name='wr_4' class="form-control"> 
				   <option value='on' <? if($index[wr_4] == "on") echo "selected"; ?>> 켜기
				   <option value='off' <? if($index[wr_4] == "off") echo "selected"; ?>> 끄기
				</select>
			</div>
		  </div> 
		  <div class="clearfix"></div>
		  <hr />

          <div class="col-md-6">
		    <div class="input-group" for="wr_5">
				<span class="input-group-addon">툴팁 사용여부</span>
				<select name='wr_5' class="form-control"> 
				   <option value='on' <? if($index[wr_5] == "on") echo "selected"; ?>> 켜기
				   <option value='off' <? if($index[wr_5] == "off") echo "selected"; ?>> 끄기
				</select>
			</div>
		  </div> 
		  
          <div class="col-md-6">
		    <div class="input-group" for="wr_6">
				<span class="input-group-addon">팝오버 사용여부</span>
				<select name='wr_6' class="form-control"> 
				   <option value='on' <? if($index[wr_6] == "on") echo "selected"; ?>> 켜기
				   <option value='off' <? if($index[wr_6] == "off") echo "selected"; ?>> 끄기
				</select>
			</div>
		  </div> 
		  <div class="clearfix"></div>
		  <hr />

    
    <div class="text-center"><input type="submit" class="btn btn-primary" value="저장"></div>
</form>

<?php
include_once('./tail.php');
?>