<?php
include_once('./_common.php');

@mkdir(G5_DATA_PATH."/cache/theme", G5_DIR_PERMISSION);
@chmod(G5_DATA_PATH."/cache/theme", G5_DIR_PERMISSION);
@mkdir(G5_DATA_PATH."/cache/theme/basic", G5_DIR_PERMISSION);
@chmod(G5_DATA_PATH."/cache/theme/basic", G5_DIR_PERMISSION);

$data = array();

function escape_post_data($data)
{
    return str_replace(array("\'", '\"', "'", '"'), '', strip_tags(trim($data)));
}

//$wr_1       = preg_replace('#[^0-9\-\.]#', '', $_POST['wr_1']); //숫자일경우
$wr_1       = escape_post_data($_POST['wr_1']);
$wr_2       = escape_post_data($_POST['wr_2']);
$wr_3       = escape_post_data($_POST['wr_3']);
$wr_4       = escape_post_data($_POST['wr_4']);
$wr_5       = escape_post_data($_POST['wr_5']);
$wr_6       = escape_post_data($_POST['wr_6']);

if(!$wr_1)
    alert('레이아웃 폭을 선택해 주십시오.');

if(!$wr_2)
    alert('메뉴위치를 선택해 주십시오.');
	
if(!$wr_3)
    alert('툴팁 사용여부를 선택해 주십시오.');
	
if(!$wr_4)
    alert('팝오버 사용여부를 선택해 주십시오.');


$data = array('wr_1' => $wr_1, 'wr_2' => $wr_2, 'wr_3' => $wr_3, 'wr_4' => $wr_4, 'wr_5' => $wr_5, 'wr_6' => $wr_6);

$save_file = G5_DATA_PATH.'/cache/theme/basic/index.php';

// 캐시파일로 저장
$cache_fwrite = true;
if($cache_fwrite) {
    $handle = fopen($save_file, 'w');
    $cache_content = "<?php\nif (!defined('_GNUBOARD_')) exit;";
    $cache_content .= "\n\n\$index=".var_export($data, true).";";
    fwrite($handle, $cache_content);
    fclose($handle);
}

goto_url('./index.php');
?>