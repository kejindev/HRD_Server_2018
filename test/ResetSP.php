<?php
	$OLD_DB_ID = 70;	//20~22, 50~52, 70~72, 80~82

	$val = $_SERVER['argv'];
	if ( count($val) !== 2 ) {
        echo "20~22, 50~52, 70~72, 80~82 입력\n";
        return;
    }

    $OLD_DB_ID = (int)$val[1];
    if ( 
    	!(
    		($OLD_DB_ID >= 20 && $OLD_DB_ID <= 22) 
    		|| ($OLD_DB_ID >= 50 && $OLD_DB_ID <= 52)
    		|| ($OLD_DB_ID >= 70 && $OLD_DB_ID <= 72)
    		|| ($OLD_DB_ID >= 80 && $OLD_DB_ID <= 82)
    	)
    ) {
        echo "20~22, 50~52, 70~72, 80~82 입력\n";
        return;
    }

    $LEGACY_DBNAME = "ddookdak".((int)($OLD_DB_ID/10));
    
	echo date("Y-m-d H:i:s")."\n";
	
	function ProcessQuery($newPDO, &$processPointer, $query) {
		$result = $newPDO->query($query);
		$processPointer++;
		if ( $result == false ) {
			echo "Error!!\n".$query."\n".$processPointer;
		    return false;
		}
		echo "Process ".$processPointer." Comp\n";
		return true;
	}
	switch ($OLD_DB_ID) {
		case 20:case 21:case 22:
		case 70:case 71:case 72:
			$host = "mysql:host=172.27.100.3;";
			break;
		
		default:
			$host = "mysql:host=172.27.100.4;";
			break;
	}
	$PLATFORM_ID = $OLD_DB_ID%10;
	$legacyPDO = 	new PDO($host."dbname=".$LEGACY_DBNAME.";charset=utf8", "root", "eoqkrskwk12", array(PDO::ATTR_PERSISTENT => true));
	$newPDO = 		new PDO($host."dbname=hrd_db_game_".$OLD_DB_ID.";charset=utf8", "root", "eoqkrskwk12", array(PDO::ATTR_PERSISTENT => true));
	$processPointer = 0;

	$query = "TRUNCATE TABLE frdEffectForEtc";
	if ( ProcessQuery ($newPDO, $processPointer, $query) == false ) 
		return;

	$query = "TRUNCATE TABLE frdEffectForPreCheckRewards";
	if ( ProcessQuery ($newPDO, $processPointer, $query) == false ) 
		return;

	$query = "TRUNCATE TABLE frdEffectForRewards";
	if ( ProcessQuery ($newPDO, $processPointer, $query) == false ) 
		return;

	$query = "TRUNCATE TABLE frdEffectForRewardsSP";
	if ( ProcessQuery ($newPDO, $processPointer, $query) == false ) 
		return;

	$query = "TRUNCATE TABLE frdSkillLevels";
	if ( ProcessQuery ($newPDO, $processPointer, $query) == false ) 
		return;

	$query = "UPDATE frdUserData SET skillPoint = accuSkillPoint";
	if ( ProcessQuery ($newPDO, $processPointer, $query) == false ) 
		return;

	$query = "UPDATE frdAbillity SET usingBundle0 = 0";
	if ( ProcessQuery ($newPDO, $processPointer, $query) == false ) 
		return;


	echo "\n".date("Y-m-d H:i:s")."\nComplete!\n";

?>
